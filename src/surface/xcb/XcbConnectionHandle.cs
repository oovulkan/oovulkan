﻿using System;
using System.Runtime.InteropServices;

namespace oovulkan.surface.xcb
{
    [StructLayout(LayoutKind.Sequential)]
    public unsafe struct XcbConnectionHandle
    {
        private void* Ptr;

        public static implicit operator XcbConnectionHandle(void* ptr) => new XcbConnectionHandle {Ptr = ptr};
        public static implicit operator void*(XcbConnectionHandle handle) => handle.Ptr;
        public static implicit operator XcbConnectionHandle(IntPtr ptr) => new XcbConnectionHandle {Ptr = (void*)ptr};
        public static implicit operator IntPtr(XcbConnectionHandle handle) => (IntPtr)handle.Ptr;
    }
}