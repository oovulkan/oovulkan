﻿using System.Runtime.InteropServices;

namespace oovulkan.surface.xcb
{
    [StructLayout(LayoutKind.Sequential)]
    public struct XcbWindowHandle
    {
        private uint Value;

        public static implicit operator XcbWindowHandle(uint value) => new XcbWindowHandle {Value = value};
        public static implicit operator uint(XcbWindowHandle handle) => handle.Value;
    }
}