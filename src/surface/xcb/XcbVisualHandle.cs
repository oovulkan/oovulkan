﻿using System.Runtime.InteropServices;

namespace oovulkan.surface.xcb
{
    [StructLayout(LayoutKind.Sequential)]
    public struct XcbVisualHandle
    {
        private uint Value;

        public static implicit operator XcbVisualHandle(uint value) => new XcbVisualHandle {Value = value};
        public static implicit operator uint(XcbVisualHandle handle) => handle.Value;
    }
}