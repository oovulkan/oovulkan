﻿using System.Runtime.InteropServices;
using oovulkan.swapchain;

namespace oovulkan.surface
{
    public static class SurfaceFormat
    {
        [StructLayout(LayoutKind.Sequential)]
        public struct Native
        {
            public readonly Format Format;
            private readonly ColorSpace ColorSpace;

            public static implicit operator Format(Native native) => native.Format;
        }
    }
}