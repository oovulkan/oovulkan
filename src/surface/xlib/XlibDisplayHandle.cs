﻿using System;
using System.Runtime.InteropServices;

namespace oovulkan.surface.xlib
{
    [StructLayout(LayoutKind.Sequential)]
    public unsafe struct XlibDisplayHandle
    {
        private void* Ptr;

        public static implicit operator XlibDisplayHandle(void* ptr) => new XlibDisplayHandle {Ptr = ptr};
        public static implicit operator void*(XlibDisplayHandle handle) => handle.Ptr;
        public static implicit operator XlibDisplayHandle(IntPtr ptr) => new XlibDisplayHandle {Ptr = (void*)ptr};
        public static implicit operator IntPtr(XlibDisplayHandle handle) => (IntPtr)handle.Ptr;
    }
}