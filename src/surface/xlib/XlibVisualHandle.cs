﻿using System.Runtime.InteropServices;

namespace oovulkan.surface.xlib
{
    [StructLayout(LayoutKind.Sequential)]
    public struct XlibVisualHandle
    {
        private uint Ptr;

        public static implicit operator XlibVisualHandle(uint ptr) => new XlibVisualHandle {Ptr = ptr};
        public static implicit operator uint(XlibVisualHandle handle) => handle.Ptr;
    }
}