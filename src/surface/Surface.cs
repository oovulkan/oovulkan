﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using oovulkan.device;
using oovulkan.device.physical;
using oovulkan.image;
using oovulkan.instance;
using oovulkan.queue.family;
using oovulkan.surface.xlib;
using oovulkan.swapchain;
using oovulkan.util;
using oovulkan.util.collection;

// ReSharper disable InconsistentNaming
#pragma warning disable 649

namespace oovulkan.surface
{
    [StructLayout(LayoutKind.Sequential)]
    public unsafe struct SurfaceHandle
    {
        private long Value;

        public static implicit operator SurfaceHandle(long? value) => new SurfaceHandle {Value = value ?? 0};
        public static implicit operator long(SurfaceHandle handle) => handle.Value;
    }

    public abstract unsafe class Surface : VulkanObject<SurfaceHandle>
    {
        static Surface()
        {
            VulkanInterop.Link(typeof(Surface));
        }

        private static readonly ConstructorInfo Sneaky = typeof(Surface).GetAnyConstructor(
            typeof(IEnumerable<QueueFamily>)
        );

        protected internal override bool IsChild => true;
        protected internal override VulkanObject Parent => Device;
        protected internal override bool IsParent => true;
        protected internal override IEnumerable<VulkanObject> Children{ get { yield return swapchain; }}

        public readonly Device Device;
        public readonly ReadonlySet<QueueFamily> QueueFamilies;
        private SurfaceCapabilities capabilities;
        private ReadonlySet<Format> formats;
        private ReadonlySet<PresentMode> modes;
        private Swapchain swapchain;

        public SurfaceCapabilities Capabilities => capabilities ?? (capabilities = GetCapabilities());
        public ReadonlySet<Format> Formats => formats ?? (formats = GetFormats());
        public ReadonlySet<PresentMode> Modes => modes ?? (modes = GetModes());
        public Swapchain Swapchain
        {
            get => swapchain;
            internal set
            {
                swapchain?.Dispose();
                swapchain = value;
            }
        }

        protected Surface(Device device)
        {
            Device = device;
            this.Link(Device);
            QueueFamilies = Device.PhysicalDevice.QueueFamilies.ToSet();
        }

        // ReSharper disable once UnusedMember.Local
        private Surface(IEnumerable<QueueFamily> families)
        {
            QueueFamilies = families.ToSet();
            if(QueueFamilies.Count == 0)
                throw new NotSupportedException();
        }

        protected void FilterQueueFamilies(Func<QueueFamily, bool> filter)
        {
            Sneaky.Invoke(this, QueueFamilies.Where(filter));
        }
        protected override void Destroy()
        {
            using(Device.WeakLock)
                Device.Remove(this);
            using(Device.Allocator?.WeakLock)
                vkDestroySurfaceKHR(Device.PhysicalDevice.Instance, this, Device.Allocator);
        }
        private SurfaceCapabilities GetCapabilities()
        {
            using(Lock)
            {
                vkGetPhysicalDeviceSurfaceCapabilitiesKHR(Device.PhysicalDevice, this, out var capabilities).Throw();
                return capabilities;
            }
        }
        private ReadonlySet<Format> GetFormats()
        {
            using(Lock)
            {
                uint count = 0;
                vkGetPhysicalDeviceSurfaceFormatsKHR(Device.PhysicalDevice, this, ref count, null).Throw();
                var formatPtr = stackalloc SurfaceFormat.Native[(int)count];
                vkGetPhysicalDeviceSurfaceFormatsKHR(Device.PhysicalDevice, this, ref count, formatPtr).Throw();
                var formats = new Format[count];
                for(uint i = 0; i < count; i++)
                    formats[i] = formatPtr[i];
                return new ReadonlySet<Format>(formats);
            }
        }
        private ReadonlySet<PresentMode> GetModes()
        {
            using(Lock)
            {
                uint count = 0;
                vkGetPhysicalDeviceSurfacePresentModesKHR(Device.PhysicalDevice, this, ref count, null).Throw();
                var modes = new PresentMode[count];
                fixed(PresentMode* ptr = modes)
                    vkGetPhysicalDeviceSurfacePresentModesKHR(Device.PhysicalDevice, this, ref count, ptr).Throw();
                return new ReadonlySet<PresentMode>(modes);
            }
        }
        public Swapchain CreateSwapchain(Format format, (uint Width, uint Height) extent, SurfaceTransformFlags preTransform, PresentMode mode, uint minImageCount=2, uint arrayLayers=1, ImageUsageFlags usage=ImageUsageFlags.TransferDst|ImageUsageFlags.ColorAttachment, SharingMode sharingMode=SharingMode.Exclusive, IEnumerable<QueueFamily> families=null, CompositeAlphaFlags compositeAlpha=CompositeAlphaFlags.Opaque, bool clipped=true)
        {
            return new Swapchain(this, minImageCount, format, extent, arrayLayers, usage, sharingMode, families, preTransform, compositeAlpha, mode, clipped);
        }

        private readonly VkDestroySurfaceKHR vkDestroySurfaceKHR;
        [VulkanInterop.InstanceFunctionAttribute]
        private readonly VkGetPhysicalDeviceSurfaceCapabilitiesKHR vkGetPhysicalDeviceSurfaceCapabilitiesKHR;
        [VulkanInterop.InstanceFunctionAttribute]
        private readonly VkGetPhysicalDeviceSurfaceFormatsKHR vkGetPhysicalDeviceSurfaceFormatsKHR;
        [VulkanInterop.InstanceFunctionAttribute]
        private readonly VkGetPhysicalDeviceSurfacePresentModesKHR vkGetPhysicalDeviceSurfacePresentModesKHR;

        private delegate void VkDestroySurfaceKHR(InstanceHandle instance, SurfaceHandle surface, AllocationCallbacks* allocator);
        private delegate Result VkGetPhysicalDeviceSurfaceCapabilitiesKHR(PhysicalDeviceHandle physicalDevice, SurfaceHandle surface, out SurfaceCapabilities.Native capabilities);
        private delegate Result VkGetPhysicalDeviceSurfaceFormatsKHR(PhysicalDeviceHandle physicalDevice, SurfaceHandle surface, ref uint count, SurfaceFormat.Native* formats);
        private delegate Result VkGetPhysicalDeviceSurfacePresentModesKHR(PhysicalDeviceHandle physicalDevice, SurfaceHandle surface, ref uint count, PresentMode* modes);
    }
}