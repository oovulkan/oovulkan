﻿using System.Runtime.InteropServices;
using oovulkan.image;
using oovulkan.swapchain;
using oovulkan.util;

namespace oovulkan.surface
{
    public class SurfaceCapabilities
    {
        public readonly (uint Min, uint Max) ImageCount;
        public readonly (uint Width, uint Height) CurrentExtent;
        public readonly ((uint Width, uint Height) Min, (uint Width, uint Height) Max) Extent;
        public readonly uint MaxArrayLayers;
        public readonly SurfaceTransformFlags SupportedTransforms;
        public readonly SurfaceTransformFlags CurrentTransform;
        public readonly CompositeAlphaFlags SupportedCompositeAlpha;
        public readonly ImageUsageFlags SupportedUsage;

        private SurfaceCapabilities(Native native)
        {
            ImageCount = (native.MinImageCount, native.MaxImageCount);
            CurrentExtent = native.CurrentExtent;
            Extent = (native.MinImageExtent, native.MaxImageExtent);
            MaxArrayLayers = native.MaxImageArrayLayers;
            SupportedTransforms = native.SupportedTransforms;
            CurrentTransform = native.CurrentTransform;
            SupportedCompositeAlpha = native.SupportedCompositeAlpha;
            SupportedUsage = native.SupportedUsage;
        }

        public static implicit operator SurfaceCapabilities(Native native) => new SurfaceCapabilities(native);

        [StructLayout(LayoutKind.Sequential)]
        public struct Native
        {
            public readonly uint MinImageCount;
            public readonly uint MaxImageCount;
            public readonly U2 CurrentExtent;
            public readonly U2 MinImageExtent;
            public readonly U2 MaxImageExtent;
            public readonly uint MaxImageArrayLayers;
            public readonly SurfaceTransformFlags SupportedTransforms;
            public readonly SurfaceTransformFlags CurrentTransform;
            public readonly CompositeAlphaFlags SupportedCompositeAlpha;
            public readonly ImageUsageFlags SupportedUsage;
        }
    }
}