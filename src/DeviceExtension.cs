﻿// ReSharper disable InconsistentNaming
namespace oovulkan
{
    public static class DeviceExtension
    {
        public const string KhrSwapchain = "VK_KHR_swapchain";
        public const string KhrDisplay = "VK_KHR_display";
        public const string KhrDisplaySwapchain = "VK_KHR_display_swapchain";
        public const string NVGlslShader = "VK_NV_glsl_shader";
        public const string KhrSamplerMirrorClampToEdge = "VK_KHR_sampler_mirror_clamp_to_edge";
        public const string ImgFilterCubic = "VK_IMG_filter_cubic";
        public const string AmdRasterizationOrder = "VK_AMD_rasterization_order";
        public const string AmdShaderTrinaryMinMax = "VK_AMD_shader_trinary_minmax";
        public const string AmdShaderExplicitVertexParameter = "VK_AMD_shader_explicit_vertex_parameter";
        public const string ExtDebugMarker = "VK_EXT_debug_marker";
        public const string AmdGcnShader = "VK_AMD_gcn_shader";
        public const string NVDedicatedAllocation = "VK_NV_dedicated_allocation";
        public const string AmdDrawIndirectCount = "VK_AMD_draw_indirect_count";
        public const string AmdNegativeViewportHeight = "VK_AMD_negative_viewport_height";
        public const string AmdGpuShaderHalfFloat = "VK_AMD_gpu_shader_half_float";
        public const string AmdShaderBallot = "VK_AMD_shader_ballot";
        public const string ImgFormatPvrtc = "VK_IMG_format_pvrtc";
        public const string NVExternalMemory = "VK_NV_external_memory";
        public const string NVExternalMemoryWin32 = "VK_NV_external_memory_win32";
        public const string NVWin32KeyedMutex = "VK_NV_win32_keyed_mutex";
        public const string KhrShaderDrawParamters = "VK_KHR_shader_draw_parameters";
        public const string ExtShaderSubgroupBallot = "VK_EXT_shader_subgroup_ballot";
        public const string ExtShaderSubgroupVote = "VK_EXT_shader_subgroup_vote";
        public const string KhrMaintenance1 = "VK_KHR_maintenance1";
        public const string NvxDeviceGeneratedCommands = "VK_NVX_device_generated_commands";
        public const string ExtDisplayControl = "VK_EXT_display_control";
        public const string ExtHdrMetadata = "VK_EXT_hdr_metadata";
        public const string GoogleDisplayTiming = "VK_GOOGLE_display_timing";
        public const string KhxMultiview = "VK_KHX_multiview";
        public const string KhxDeviceGroup = "VK_KHX_device_group";
        public const string KhxExternalMemory = "VK_KHX_external_memory";
        public const string KhxExternalMemoryWin32 = "VK_KHX_external_memory_win32";
        public const string KhxExternalMemoryFd = "VK_KHX_external_memory_fd";
        public const string KhxWin32KeyedMutex = "VK_KHX_win32_keyed_mutex";
        public const string KhxExternalSemaphore = "VK_KHX_external_semaphore";
        public const string KhxExternalSemaphoreWin32 = "VK_KHX_external_semaphore_win32";
        public const string KhxExternalSemaphoreFd = "VK_KHX_external_semaphore_fd";
        public const string KhrPushDescriptor = "VK_KHR_push_descriptor";
        public const string KhrIncrementalPresent = "VK_KHR_incremental_present";
        public const string KhrDescriptorUpdateTemplate = "VK_KHR_descriptor_update_template";
        public const string NVClipSpaceWScaling = "VK_NV_clip_space_w_scaling";
        public const string NVSampleMaskOverrideCoverage = "VK_NV_sample_mask_override_coverage";
        public const string NVGeometryShaderPassthrough = "VK_NV_geometry_shader_passthrough";
        public const string NVViewportArray2 = "VK_NV_viewport_array2";
        public const string NvxMultiviewPerViewAttributes = "VK_NVX_multiview_per_view_attributes";
        public const string NVViewportSwizzle = "VK_NV_viewport_swizzle";
        public const string ExtDiscardRectangles = "VK_EXT_discard_rectangles";
        public const string AmdTextureGatherBiasLod = "VK_AMD_texture_gather_bias_lod";
        public const string ExtSamplerFilterMinmax = "VK_EXT_sampler_filter_minmax";
        public const string AmdGpuShaderInt16 = "VK_AMD_gpu_shader_int16";
        public const string ExtBlendOperationAdvanced = "VK_EXT_blend_operation_advanced";
        public const string NVFragmentCoverageToColor = "VK_NV_fragment_coverage_to_color";
        public const string NVFramebufferMixedSamples = "VK_NV_framebuffer_mixed_samples";
        public const string NVFillRectangle = "VK_NV_fill_rectangle";
    }
}