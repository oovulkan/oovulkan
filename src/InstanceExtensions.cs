﻿// ReSharper disable InconsistentNaming
namespace oovulkan
{
    public static class InstanceExtension
    {
        public const string KhrSamplerMirrorClampToEdge = "VK_KHR_sampler_mirror_clamp_to_edge";
        public const string KhrXlibSurface = "VK_KHR_xlib_surface";
        public const string KhrXcbSurface = "VK_KHR_xcb_surface";
        public const string KhrWaylandSurface = "VK_KHR_wayland_surface";
        public const string KhrMirSurface = "VK_KHR_mir_surface";
        public const string KhrAndroidSurface = "VK_KHR_android_surface";
        public const string KhrWin32Surface = "VK_KHR_win32_surface";
        public const string ExtDebugReport = "VK_EXT_debug_report";
        public const string KhrSurface = "VK_KHR_surface";
        public const string NVExternalMemoryCapabilities = "VK_NV_external_memory_capabilities";
        public const string KhrGetPhysicalDeviceProperties2 = "VK_KHR_get_physical_device_properties2";
        public const string ExtValidationFlags = "VK_EXT_validation_flags";
        public const string NNVISurface = "VK_NN_vi_surface";
        public const string ExtDirectModeDisplay = "VK_EXT_direct_mode_display";
        public const string ExtAcquireXlibDisplay = "VK_EXT_acquire_xlib_display";
        public const string ExtDisplaySurfaceCounter = "VK_EXT_display_surface_counter";
        public const string ExtSwapchainColorspace = "VK_EXT_swapchain_colorspace";
        public const string KhxDeviceGroupCreation = "VK_KHX_device_group_creation";
        public const string KhxExternalMemoryCapabilities = "VK_KHX_external_memory_capabilities";
        public const string KhxExternalSemaphoreCapabilities = "VK_KHX_external_semaphore_capabilities";
        public const string MvkIOSSurface = "VK_MVK_ios_surface";
        public const string MvkMacOSSurface = "VK_MVK_macos_surface";
        public const string MvkMoltenVK = "VK_MVK_moltenvk";
    }
}