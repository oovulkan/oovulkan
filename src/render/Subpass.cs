﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using oovulkan.image;
using oovulkan.pipeline;
using oovulkan.util;
using oovulkan.util.collection;

namespace oovulkan.render
{
    [StructLayout(LayoutKind.Sequential)]
    public struct SubpassHandle
    {
        private uint Value;

        public static implicit operator SubpassHandle(uint value) => new SubpassHandle {Value = value};
        public static implicit operator uint(SubpassHandle handle) => handle.Value;
    }

    public class Subpass : VulkanObject<SubpassHandle>
    {
        public static readonly Subpass External = new Subpass(uint.MaxValue);

        public class Reference
        {
            public readonly Attachment Attachment;
            public readonly ImageLayout Layout;

            private Reference(Attachment attachment, ImageLayout layout)
            {
                Attachment = attachment;
                Layout = layout;
            }

            public bool IsCompatible(Reference reference) => Attachment.IsCompatible(reference.Attachment);

            public static implicit operator Reference((Attachment Attachment, ImageLayout Layout) reference) => new Reference(reference.Attachment, reference.Layout);
            public static implicit operator (Attachment Attachment, ImageLayout Layout)(Reference reference) => (reference.Attachment, reference.Layout);
        }

        protected internal override bool IsChild => true;
        protected internal override VulkanObject Parent => Renderpass;

        public readonly Renderpass Renderpass;
        public readonly ReadonlyList<Reference> Inputs;
        public readonly ReadonlyList<Reference> Colors;
        public readonly ReadonlyList<Reference> Resolves;
        public readonly Reference DepthStencil;
        public readonly ReadonlyList<Attachment> Preserves;

        public IEnumerable<Attachment> Attachments
        {
            get
            {
                foreach(var reference in Inputs)
                    yield return reference.Attachment;
                foreach(var reference in Colors)
                    yield return reference.Attachment;
                foreach(var reference in Resolves)
                    yield return reference.Attachment;
                if(DepthStencil != null)
                    yield return DepthStencil.Attachment;
            }
        }

        public uint Index => Handle;

        private Subpass(SubpassHandle handle)
        {
            Handle = handle;
        }

        internal Subpass(Renderpass renderpass, SubpassHandle handle)
        {
            Renderpass = renderpass;
            Handle = handle;
        }

        //Building "constructor"
        // ReSharper disable once UnusedMember.Local
        private Subpass(IEnumerable<Reference> inputs, IEnumerable<Reference> colors, IEnumerable<Reference> resolves, Reference depthStencil, IEnumerable<Attachment> preserves)
        {
            Inputs = inputs.ToList();
            Colors = colors.ToList();
            Resolves = resolves?.ToList();
            DepthStencil = depthStencil;
            Preserves = preserves.ToList();
            Create();
        }

        private void Create()
        {
            #region Validation

            if(Colors.Count >= Renderpass.Device.PhysicalDevice.Limits.MaxColorAttachments)
                throw new ArgumentOutOfRangeException(nameof(Colors));

            var inputLookup = new Dictionary<Attachment, ImageLayout>();
            var colorLookup = new Dictionary<Attachment, ImageLayout>();
            var resolveLookup = new Dictionary<Attachment, ImageLayout>();
            var depthStencilLookup = new Dictionary<Attachment, ImageLayout>();
            foreach(var input in Inputs)
            {
                if(input.Attachment != Attachment.Unused)
                    inputLookup[input.Attachment] = input.Layout;
            }
            foreach(var color in Colors)
            {
                if(color.Attachment != Attachment.Unused)
                    colorLookup[color.Attachment] = color.Layout;
            }
            if(Resolves != null)
            {
                foreach(var resolve in Resolves)
                {
                    if(resolve.Attachment != Attachment.Unused)
                        resolveLookup[resolve.Attachment] = resolve.Layout;
                }
            }
            if(DepthStencil != null && DepthStencil.Attachment != Attachment.Unused)
                depthStencilLookup[DepthStencil.Attachment] = DepthStencil.Layout;

            if(Inputs
                .Select(reference => reference.Attachment)
                .Any(attachment =>
                    attachment.Uses.FirstOrDefault().Subpass == this &&
                    attachment.LoadOp == AttachmentLoadOp.Clear &&
                    !resolveLookup.ContainsKey(attachment) &&
                    !depthStencilLookup.ContainsKey(attachment)))
            {
                throw new ArgumentException(nameof(Inputs));
            }
            foreach(var reference in Inputs)
            {
                if(colorLookup.TryGetValue(reference.Attachment, out ImageLayout layout) ||
                   depthStencilLookup.TryGetValue(reference.Attachment, out layout))
                {
                    if(reference.Layout != layout)
                        throw new ArgumentException(nameof(Inputs));
                }
            }

            SampleCountFlags? samples = null;
            foreach(var reference in Colors)
            {
                if(reference.Attachment == Attachment.Unused)
                    continue;
                if(samples == null)
                    samples = reference.Attachment.Samples;
                else if(reference.Attachment.Samples != samples)
                    throw new ArgumentException(nameof(Colors));
            }
            if(DepthStencil != null && DepthStencil.Attachment == Attachment.Unused){
                if(samples != null && DepthStencil.Attachment.Samples != samples)
                    throw new ArgumentException(nameof(DepthStencil));
            }

            //TODO: ??? If any input attachments are VK_ATTACHMENT_UNUSED, then any pipelines bound during the subpass must not access those input attachments from the fragment shader

            foreach(var preserve in Preserves)
            {
                if(inputLookup.ContainsKey(preserve) || colorLookup.ContainsKey(preserve) ||
                   resolveLookup.ContainsKey(preserve) || depthStencilLookup.ContainsKey(preserve))
                    throw new ArgumentException();
            }

            #endregion
        }
        public override void Dispose() => throw new InvalidOperationException();
        protected override void Destroy() { }

        public bool IsCompatible(Subpass subpass)
        {
            for(int i = 0; i < Inputs.Count; i++)
            {
                if(!Inputs[i].IsCompatible(subpass.Inputs[i]))
                    return false;
            }
            for(int i = 0; i < Colors.Count; i++)
            {
                if(!Colors[i].IsCompatible(subpass.Colors[i]))
                    return false;
            }
            if((Resolves == null) != (subpass.Resolves == null))
                return false;
            if(Resolves != null)
            {
                for(int i = 0; i < Resolves.Count; i++)
                {
                    if(!Resolves[i].IsCompatible(subpass.Resolves[i]))
                        return false;
                }
            }
            if((DepthStencil == null) != (subpass.DepthStencil == null))
                return false;
            if(DepthStencil != null && !DepthStencil.IsCompatible(subpass.DepthStencil))
                return false;
            return true;
        }

        public class Builder : BuilderBase<Subpass>
        {
            private static readonly ConstructorInfo Sneaky = typeof(Subpass).GetAnyConstructor(
                typeof(ReadonlyList<Reference>),
                typeof(ReadonlyList<Reference>),
                typeof(ReadonlyList<Reference>),
                typeof(Reference),
                typeof(ReadonlyList<Attachment>)
            );
            private readonly Renderpass.Builder builder;
            private readonly List<Reference> inputs = new List<Reference>();
            private readonly List<Reference> colors = new List<Reference>();
            private readonly List<Reference> resolves = new List<Reference>();
            private Reference depthStencil;
            private readonly List<Attachment> preserves = new List<Attachment>();

            private bool? nullResolves;

            internal Builder(Renderpass.Builder builder, Subpass obj) : base(obj)
            {
                this.builder = builder;
            }

            public Builder Inputs(params Reference[] references)
            {
                AssertNotDone();
                foreach(var reference in references)
                {
                    var attachment = reference.Attachment;
                    var layout = reference.Layout;
                    if(attachment == null)
                        throw new ArgumentNullException(nameof(attachment));
                    if(layout == ImageLayout.Undefined || layout == ImageLayout.Preinitialized)
                        throw new ArgumentException(nameof(layout));
                    if(attachment != render.Attachment.Unused)
                    {
                        if(attachment.Renderpass != Object.Renderpass)
                            throw new ArgumentException(nameof(attachment));
                        if(attachment.Uses.Count == 0 && attachment.LoadOp == AttachmentLoadOp.Clear && (
                               layout == ImageLayout.ShaderReadOnlyOptimal ||
                               layout == ImageLayout.DepthStencilReadOnlyOptimal
                           ))
                        {
                            throw new ArgumentException();
                        }
                        attachment.Uses.Add((Object, layout));
                    }
                    attachment.IsInput = true;
                }

                inputs.AddRange(references);
                return this;
            }
            public Builder Colors(params Reference[] references)
            {
                AssertNotDone();
                foreach(var reference in references)
                {
                    var attachment = reference.Attachment;
                    var layout = reference.Layout;
                    if(attachment == null)
                        throw new ArgumentNullException(nameof(attachment));
                    if(nullResolves == null)
                        nullResolves = true;
                    else if(!nullResolves.Value)
                        throw new InvalidOperationException();
                    if(layout == ImageLayout.Undefined || layout == ImageLayout.Preinitialized)
                        throw new ArgumentException(nameof(layout));
                    if(attachment != render.Attachment.Unused)
                    {
                        if(attachment.Renderpass != Object.Renderpass)
                            throw new ArgumentException(nameof(attachment));
                        if(attachment.Uses.Count == 0 && attachment.LoadOp == AttachmentLoadOp.Clear && (
                               layout == ImageLayout.ShaderReadOnlyOptimal ||
                               layout == ImageLayout.DepthStencilReadOnlyOptimal
                           ))
                        {
                            throw new ArgumentException();
                        }
                        attachment.Uses.Add((Object, layout));
                    }
                    attachment.IsColor = true;
                }
                colors.AddRange(references);
                return this;
            }
            public Builder Colors(params (Reference Color, Reference Resolve)[] references)
            {
                AssertNotDone();
                foreach(var pair in references)
                {
                    var attachment = pair.Color.Attachment;
                    var layout = pair.Color.Layout;
                    var resolveAttachment = pair.Resolve.Attachment;
                    var resolveLayout = pair.Resolve.Layout;
                    if(attachment == null)
                        throw new ArgumentNullException(nameof(attachment));
                    if(nullResolves == null)
                        nullResolves = false;
                    else if(nullResolves.Value)
                        throw new InvalidOperationException();
                    if(layout == ImageLayout.Undefined || layout == ImageLayout.Preinitialized)
                        throw new ArgumentException(nameof(layout));
                    if(resolveLayout == ImageLayout.Undefined || resolveLayout == ImageLayout.Preinitialized)
                        throw new ArgumentException(nameof(resolveLayout));
                    if(resolveAttachment != render.Attachment.Unused)
                    {
                        if(attachment == render.Attachment.Unused)
                            throw new ArgumentException(nameof(attachment));
                        if(attachment.Samples == SampleCountFlags.Bit1)
                            throw new ArgumentException(nameof(attachment));
                        if(resolveAttachment.Samples != SampleCountFlags.Bit1)
                            throw new ArgumentException(nameof(resolveAttachment));
                        if(resolveAttachment.Format != attachment.Format)
                            throw new ArgumentException(nameof(resolveAttachment));
                    }
                    if(attachment != render.Attachment.Unused)
                    {
                        if(attachment.Renderpass != Object.Renderpass)
                            throw new ArgumentException(nameof(attachment));
                        if(attachment.Uses.Count == 0 && attachment.LoadOp == AttachmentLoadOp.Clear && (
                               layout == ImageLayout.ShaderReadOnlyOptimal ||
                               layout == ImageLayout.DepthStencilReadOnlyOptimal
                           ))
                        {
                            throw new ArgumentException();
                        }
                        attachment.Uses.Add((Object, layout));
                    }
                    if(resolveAttachment != render.Attachment.Unused)
                    {
                        if(resolveAttachment.Renderpass != Object.Renderpass)
                            throw new ArgumentException(nameof(resolveAttachment));
                        if(resolveAttachment.Uses.Count == 0 && resolveAttachment.LoadOp == AttachmentLoadOp.Clear && (
                               resolveLayout == ImageLayout.ShaderReadOnlyOptimal ||
                               resolveLayout == ImageLayout.DepthStencilReadOnlyOptimal
                           ))
                        {
                            throw new ArgumentException();
                        }
                        resolveAttachment.Uses.Add((Object, resolveLayout));
                    }
                    attachment.IsColor = true;
                }
                colors.AddRange(references.Select(pair => pair.Color));
                resolves.AddRange(references.Select(pair => pair.Resolve));
                return this;
            }
            public Builder DepthStencil(Reference reference)
            {
                AssertNotDone();
                if(depthStencil != null)
                    throw new InvalidOperationException();

                var attachment = reference.Attachment;
                var layout = reference.Layout;
                if(attachment == null)
                    throw new ArgumentNullException(nameof(attachment));
                if(layout == ImageLayout.Undefined || layout == ImageLayout.Preinitialized)
                    throw new ArgumentException(nameof(layout));
                if(attachment != render.Attachment.Unused)
                {
                    if(attachment.Renderpass != Object.Renderpass)
                        throw new ArgumentException(nameof(attachment));
                    if(attachment.Uses.Count == 0 && attachment.LoadOp == AttachmentLoadOp.Clear && (
                           layout == ImageLayout.ShaderReadOnlyOptimal ||
                           layout == ImageLayout.DepthStencilReadOnlyOptimal
                       ))
                    {
                        throw new ArgumentException();
                    }
                    attachment.Uses.Add((Object, layout));
                }
                attachment.IsDepthStencil = true;
                depthStencil = reference;
                return this;
            }
            public Builder Preserve(params Attachment[] attachments)
            {
                AssertNotDone();
                foreach(var attachment in attachments)
                {
                    if(attachment == null || attachment == render.Attachment.Unused)
                        throw new ArgumentNullException(nameof(attachment));
                    if(attachment.Renderpass != Object.Renderpass)
                        throw new ArgumentException(nameof(attachment));
                }
                preserves.AddRange(attachments);
                return this;
            }

            private void Finish()
            {
                AssertNotDone();
                MarkDone();
                Sneaky.Invoke(Object, inputs, colors, nullResolves == false ? resolves : null, depthStencil, preserves);
            }

            public Renderpass.Builder Attachment(bool mayAlias, Format format, SampleCountFlags samples, AttachmentLoadOp loadOp, AttachmentStoreOp storeOp, AttachmentLoadOp stencilLoadOp, AttachmentStoreOp stencilStoreOp, ImageLayout initialLayout, ImageLayout finalLayout)
            {
                Finish();
                return builder.Attachment(mayAlias, format, samples, loadOp, storeOp, stencilLoadOp, stencilStoreOp, initialLayout, finalLayout);
            }
            public Renderpass.Builder Attachment(out Attachment attachment, bool mayAlias, Format format, SampleCountFlags samples, AttachmentLoadOp loadOp, AttachmentStoreOp storeOp, AttachmentLoadOp stencilLoadOp, AttachmentStoreOp stencilStoreOp, ImageLayout initialLayout, ImageLayout finalLayout)
            {
                Finish();
                return builder.Attachment(out attachment, mayAlias, format, samples, loadOp, storeOp, stencilLoadOp, stencilStoreOp, initialLayout, finalLayout);
            }
            public Builder Subpass()
            {
                Finish();
                return builder.Subpass();
            }
            public Builder Subpass(out Subpass subpass)
            {
                Finish();
                return builder.Subpass(out subpass);
            }
            public Renderpass.Builder Dependency((Subpass Subpass, Stage Stage, AccessFlags Access) src, (Subpass Subpass, Stage Stage, AccessFlags Access) dst, bool byRegion)
            {
                Finish();
                return builder.Dependency(src, dst, byRegion);
            }
            public Renderpass Done()
            {
                Finish();
                return builder.Done();
            }
        }
    }
}