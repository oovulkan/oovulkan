﻿using System.Runtime.InteropServices;
using oovulkan.framebuffer;
using oovulkan.util;

// ReSharper disable PrivateFieldCanBeConvertedToLocalVariable

namespace oovulkan.render
{
    [StructLayout(LayoutKind.Sequential)]
    internal unsafe struct RenderpassBeginInfo
    {
        private readonly StructureType Type;
        private readonly void* Next;
        public readonly RenderpassHandle Renderpass;
        public readonly FrameBufferHandle FrameBuffer;
        public readonly Rect2D RenderArea;
        public readonly uint ClearValueCount;
        public readonly ClearValue* ClearValuePtr;

        public RenderpassBeginInfo(RenderpassHandle renderpass, FrameBufferHandle frameBuffer, Rect2D renderArea, uint clearValueCount, ClearValue* clearValuePtr)
        {
            Type = StructureType.RenderPassBeginInfo;
            Next = null;
            Renderpass = renderpass;
            FrameBuffer = frameBuffer;
            RenderArea = renderArea;
            ClearValueCount = clearValueCount;
            ClearValuePtr = clearValuePtr;
        }
    }
}