﻿namespace oovulkan.render
{
    public enum AttachmentLoadOp : int
    {
        Load = 0,
        Clear = 1,
        DontCare = 2
    }
}