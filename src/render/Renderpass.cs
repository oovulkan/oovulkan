﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using oovulkan.device;
using oovulkan.framebuffer;
using oovulkan.image;
using oovulkan.image.view;
using oovulkan.pipeline;
using oovulkan.util;
using oovulkan.util.collection;

#pragma warning disable 649

namespace oovulkan.render
{
    [StructLayout(LayoutKind.Sequential)]
    public struct RenderpassHandle
    {
        private long Value;

        public static implicit operator RenderpassHandle(long? value) => new RenderpassHandle {Value = value ?? 0};
        public static implicit operator long(RenderpassHandle handle) => handle.Value;
    }

    public unsafe class Renderpass : VulkanObject<RenderpassHandle>
    {
        protected internal override bool IsChild => true;
        protected internal override VulkanObject Parent => Device;
        protected internal override bool IsParent => true;
        protected internal override IEnumerable<VulkanObject> Children => CollectionUtil.All<VulkanObject>(Attachments, Subpasses, frameBuffers);

        public readonly Device Device;

        public readonly ReadonlyList<Attachment> Attachments;
        public readonly ReadonlyList<Subpass> Subpasses;
        public readonly ReadonlyList<Dependency> Dependencies;

        private readonly HashSet<FrameBuffer> frameBuffers = new HashSet<FrameBuffer>();
        private (uint, uint)? granularity;

        public (uint Width, uint Height) Granularity => granularity ?? (granularity = GetGranularity()).Value;
        public ReadonlySet<FrameBuffer> FrameBuffers => frameBuffers;

        private Renderpass(Device device)
        {
            Device = device;
            this.Link(Device);
        }

        //Building "constructor"
        // ReSharper disable once UnusedMember.Local
        private Renderpass(IEnumerable<Attachment> attachments, IEnumerable<Subpass> subpasses, IEnumerable<Dependency> dependencies)
        {
            Attachments = attachments.ToList();
            Subpasses = subpasses.ToList();
            Dependencies = dependencies.ToList();
            Create();
        }

        internal void Add(FrameBuffer frameBuffer) { frameBuffers.Add(frameBuffer); }
        internal void Remove(FrameBuffer frameBuffer) { frameBuffers.Remove(frameBuffer); }

        private void Create()
        {
            #region Validation

            //TODO: ??? If any two subpasses operate on attachments with overlapping ranges of the same VkDeviceMemory object, and at least one subpass writes to that area of VkDeviceMemory, a subpass dependency must be included (either directly or via some intermediate subpasses) between them
            //TODO: ??? If the attachment member of any element of pInputAttachments, pColorAttachments, pResolveAttachments or pDepthStencilAttachment, or the attachment indexed by any element of pPreserveAttachments in any given element of pSubpasses is bound to a range of a VkDeviceMemory object that overlaps with any other attachment in any subpass (including the same subpass), the VkAttachmentDescription structures describing them must include VK_ATTACHMENT_DESCRIPTION_MAY_ALIAS_BIT in flags
            if(Subpasses.Count == 0)
                throw new InvalidOperationException();

            #endregion

            uint attachmentCount = (uint)Attachments.Count;
            uint subpassCount = (uint)Subpasses.Count;
            uint dependencyCount = (uint)Dependencies.Count;
            var attachments = stackalloc AttachmentDescription[(int)attachmentCount];
            var subpasses = stackalloc SubpassDescription[(int)subpassCount];
            var dependencies = stackalloc SubpassDependency[(int)dependencyCount];

            for(uint i = 0; i < attachmentCount; i++)
                attachments[i] = Attachments[(int)i];
            for(uint i = 0; i < subpassCount; i++)
            {
                var subpass = Subpasses[(int)i];
                uint inputCount = (uint)subpass.Inputs.Count;
                uint colorCount = (uint)subpass.Colors.Count;
                uint preserveCount = (uint)subpass.Preserves.Count;

                var inputs = stackalloc AttachmentReference[(int)inputCount];
                var colors = stackalloc AttachmentReference[(int)colorCount];
                var resolves = stackalloc AttachmentReference[subpass.Resolves != null ? (int)colorCount : 0];
                var depthStencil = stackalloc AttachmentReference[subpass.DepthStencil != null ? 1 : 0];
                var preserves = stackalloc AttachmentHandle[(int)preserveCount];
                for(uint j = 0; j < inputCount; j++)
                {
                    var reference = subpass.Inputs[(int)i];
                    inputs[i] = new AttachmentReference(reference.Attachment, reference.Layout);
                }
                for(uint j = 0; j < colorCount; j++)
                {
                    var reference = subpass.Colors[(int)i];
                    colors[i] = new AttachmentReference(reference.Attachment, reference.Layout);
                }
                if(subpass.Resolves != null)
                {
                    for(uint j = 0; j < colorCount; j++)
                    {
                        var reference = subpass.Resolves[(int)i];
                        resolves[i] = new AttachmentReference(reference.Attachment, reference.Layout);
                    }
                }
                if(subpass.DepthStencil != null)
                {
                    var reference = subpass.DepthStencil;
                    depthStencil[0] = new AttachmentReference(reference.Attachment, reference.Layout);
                }
                for(uint j = 0; j < preserveCount; j++)
                    preserves[i] = subpass.Preserves[(int)i];
                subpasses[i] = new SubpassDescription(subpass, inputs, colors, resolves, depthStencil, preserves);
            }
            for(uint i = 0; i < dependencyCount; i++)
                dependencies[i] = Dependencies[(int)i];

            var info = new RenderpassCreateInfo(attachmentCount, attachments, subpassCount, subpasses, dependencyCount, dependencies);
            using(Device.WeakLock)
            {
                RenderpassHandle handle;
                using(Device.Allocator?.WeakLock)
                    vkCreateRenderPass(Device, &info, Device.Allocator, out handle).Throw();
                Handle = handle;
                Device.Add(this);
            }
        }
        protected override void Destroy()
        {
            using(Device.WeakLock)
                Device.Remove(this);
            using(Device.Allocator?.WeakLock)
                vkDestroyRenderPass(Device, this, Device.Allocator);
        }

        public FrameBuffer GetFrameBuffer((uint Width, uint Height) size, uint layers, params ImageView[] attachments) => new FrameBuffer(this, size, layers, attachments);

        private (uint, uint) GetGranularity()
        {
            using(Lock)
            {
                vkGetRenderAreaGranularity(Device, this, out U2 granularity);
                return granularity;
            }
        }

        public bool IsCompatible(Renderpass renderpass)
        {
            if(renderpass == null)
                return false;
            if(this == renderpass)
                return true;
            if(Subpasses.Count != renderpass.Subpasses.Count)
                return false;
            for(int i = 0; i < Math.Max(Attachments.Count, renderpass.Attachments.Count); i++)
            {
                var a1 = i < Attachments.Count ? Attachments[i] : Attachment.Unused;
                var a2 = i < renderpass.Attachments.Count ? renderpass.Attachments[i] : Attachment.Unused;
                if(!a1.IsCompatible(a2))
                    return false;
            }
            if(Subpasses.Count == 1)
            {
                var s1 = Subpasses[0];
                var s2 = renderpass.Subpasses[0];
                return s1.IsCompatible(s2);
            }
            else
            {
                if(Subpasses.Count != renderpass.Subpasses.Count)
                    return false;
                for(int i = 0; i < Subpasses.Count; i++)
                {
                    if(!Subpasses[i].IsCompatible(renderpass.Subpasses[i]))
                        return false;
                }
                return true;
            }
        }

        private readonly VkCreateRenderPass vkCreateRenderPass;
        private readonly VkDestroyRenderPass vkDestroyRenderPass;
        private readonly VkGetRenderAreaGranularity vkGetRenderAreaGranularity;

        private delegate Result VkCreateRenderPass(DeviceHandle device, RenderpassCreateInfo* info, AllocationCallbacks* allocator, out RenderpassHandle renderpass);
        private delegate void VkDestroyRenderPass(DeviceHandle device, RenderpassHandle renderpass, AllocationCallbacks* allocator);
        private delegate void VkGetRenderAreaGranularity(DeviceHandle device, RenderpassHandle renderpass, out U2 granularity);

        public class Builder : BuilderBase<Renderpass>
        {
            private static readonly ConstructorInfo Sneaky = typeof(Renderpass).GetAnyConstructor(
                typeof(IEnumerable<Attachment>),
                typeof(IEnumerable<Subpass>),
                typeof(IEnumerable<Dependency>)
            );
            private readonly List<Attachment> attachments = new List<Attachment>();
            private readonly List<Subpass> subpasses = new List<Subpass>();
            private readonly List<Dependency> dependencies = new List<Dependency>();

            internal Builder(Device device) : base(new Renderpass(device)) { }

            public Builder Attachment(bool mayAlias, Format format, SampleCountFlags samples, AttachmentLoadOp loadOp, AttachmentStoreOp storeOp, AttachmentLoadOp stencilLoadOp, AttachmentStoreOp stencilStoreOp, ImageLayout initialLayout, ImageLayout finalLayout)
            {
                return Attachment(out Attachment _, mayAlias, format, samples, loadOp, storeOp, stencilLoadOp, stencilStoreOp, initialLayout, finalLayout);
            }
            public Builder Attachment(out Attachment attachment, bool mayAlias, Format format, SampleCountFlags samples, AttachmentLoadOp loadOp, AttachmentStoreOp storeOp, AttachmentLoadOp stencilLoadOp, AttachmentStoreOp stencilStoreOp, ImageLayout initialLayout, ImageLayout finalLayout)
            {
                AssertNotDone();
                attachment = new Attachment(Object, (uint)attachments.Count, mayAlias, format, samples, loadOp, storeOp, stencilLoadOp, stencilStoreOp, initialLayout, finalLayout);
                attachments.Add(attachment);
                return this;
            }

            public Subpass.Builder Subpass()
            {
                return Subpass(out Subpass _);
            }
            public Subpass.Builder Subpass(out Subpass subpass)
            {
                AssertNotDone();
                subpass = new Subpass(Object, (uint)subpasses.Count);
                subpasses.Add(subpass);
                return new Subpass.Builder(this, subpass);
            }

            public Builder Dependency((Subpass Subpass, Stage Stage, AccessFlags Access) src, (Subpass Subpass, Stage Stage, AccessFlags Access) dst, bool byRegion)
            {
                AssertNotDone();
                var dependency = new Dependency(src, dst, byRegion);
                dependencies.Add(dependency);
                return this;
            }

            public Renderpass Done()
            {
                AssertNotDone();
                MarkDone();
                using(Object.Device.Lock)
                    Sneaky.Invoke(Object, attachments, subpasses, dependencies);
                return Object;
            }
        }
    }
}