﻿using System;
using System.Runtime.InteropServices;
using oovulkan.image;
using oovulkan.pipeline;

namespace oovulkan.render
{
    [Flags]
    internal enum SubpassDescriptionFlags : int
    {
        None = 0
    }

    [StructLayout(LayoutKind.Sequential)]
    internal struct AttachmentReference
    {
        public readonly AttachmentHandle Attachment;
        public readonly ImageLayout Layout;

        public AttachmentReference(Attachment attachment, ImageLayout layout)
        {
            Attachment = attachment;
            Layout = layout;
        }
    }

    [StructLayout(LayoutKind.Sequential)]
    internal unsafe struct SubpassDescription
    {
        public readonly SubpassDescriptionFlags Flags;
        public readonly PipelineBindPoint PipelineBindPoint;
        public readonly uint InputAttachmentCount;
        public readonly AttachmentReference* InputAttachmentPtr;
        public readonly uint ColorAttachmentCount;
        public readonly AttachmentReference* ColorAttachmentPtr;
        public readonly AttachmentReference* ResolveAttachmentPtr;
        public readonly AttachmentReference* DepthStencilAttachmentPtr;
        public readonly uint PreserveAttachmentCount;
        public readonly AttachmentHandle* PreserveAttachmentPtr;

        public SubpassDescription(Subpass subpass,
            AttachmentReference* inputPtr, AttachmentReference* colorPtr, AttachmentReference* resolvePtr,
            AttachmentReference* depthStencilPtr, AttachmentHandle* preservePtr)
        {
            Flags = SubpassDescriptionFlags.None;
            PipelineBindPoint = PipelineBindPoint.Graphics;
            InputAttachmentCount = (uint)subpass.Inputs.Count;
            InputAttachmentPtr = inputPtr;
            ColorAttachmentCount = (uint)subpass.Colors.Count;
            ColorAttachmentPtr = colorPtr;
            ResolveAttachmentPtr = resolvePtr;
            DepthStencilAttachmentPtr = depthStencilPtr;
            PreserveAttachmentCount = (uint)subpass.Preserves.Count;
            PreserveAttachmentPtr = preservePtr;
        }
    }
}