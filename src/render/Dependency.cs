﻿using System;
using oovulkan.pipeline;

namespace oovulkan.render
{
    public class Dependency
    {
        public readonly (Subpass Subpass, Stage Stage, AccessFlags Access) Src;
        public readonly (Subpass Subpass, Stage Stage, AccessFlags Access) Dst;
        public readonly bool ByRegion;

        internal Dependency((Subpass Subpass, Stage Stage, AccessFlags Access) src, (Subpass Subpass, Stage Stage, AccessFlags Access) dst, bool byRegion)
        {
            Src = src;
            Dst = dst;
            ByRegion = byRegion;
            Validate();
        }

        private void Validate()
        {
            const Stage selfDisallowedFlags = ~(
                Stage.TopOfPipe | Stage.DrawIndirect | Stage.VertexInput |
                Stage.VertexShader | Stage.TessellationControlShader | Stage.TessellationEvaluationShader |
                Stage.GeometryShader | Stage.FragmentShader | Stage.EarlyFragmentTests |
                Stage.LateFragmentTests | Stage.ColorAttachmentOutput | Stage.BottomOfPipe |
                Stage.AllGraphics
            );

            Validate(Src);
            Validate(Dst);

            if(Src.Subpass != Subpass.External && Dst.Subpass != Subpass.External &&
               (Src.Subpass.Renderpass != Dst.Subpass.Renderpass || Src.Subpass.Index > Dst.Subpass.Index))
                throw new ArgumentException();
            if(Src.Subpass == Subpass.External && Dst.Subpass == Subpass.External)
                throw new ArgumentException();
            if(Src.Subpass == Dst.Subpass)
            {
                var stages = Src.Stage | Dst.Stage;
                if((stages & selfDisallowedFlags) != Stage.None)
                    throw new ArgumentException();
                if((stages & Stage.FrameBufferSpaceStages) != Stage.None && Src.Stage.Latest(PipelineType.Graphics) > Dst.Stage.Earliest(PipelineType.Graphics))
                    throw new ArgumentException();
            }

            void Validate((Subpass Subpass, Stage Stage, AccessFlags Access) tuple)
            {
                if(tuple.Stage == Stage.None)
                    throw new ArgumentException();
                if(tuple.Subpass != Subpass.External)
                {
                    if(tuple.Stage.HasFlag(Stage.Host))
                        throw new ArgumentException();
                    var features = tuple.Subpass.Renderpass.Device.Features;
                    if(!features.GeometryShader && tuple.Stage.HasFlag(Stage.GeometryShader))
                        throw new NotSupportedException();
                    if(!features.TessellationShader && (
                           tuple.Stage.HasFlag(Stage.TessellationControlShader) ||
                           tuple.Stage.HasFlag(Stage.TessellationEvaluationShader)
                       ))
                        throw new NotSupportedException();
                }
                if(!tuple.Stage.Supports(tuple.Access))
                    throw new ArgumentException();
            }
        }

        public bool IsCompatible(Dependency dependency)
        {
            return Src.Subpass.Index == dependency.Src.Subpass.Index
                   && Src.Stage == dependency.Src.Stage
                   && Src.Access == dependency.Src.Access
                   && Dst.Subpass.Index == dependency.Dst.Subpass.Index
                   && Dst.Stage == dependency.Dst.Stage
                   && Dst.Access == dependency.Dst.Access
                   && ByRegion == dependency.ByRegion;
        }
    }
}