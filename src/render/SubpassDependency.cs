﻿using System;
using System.Runtime.InteropServices;
using oovulkan.pipeline;

namespace oovulkan.render
{
    [Flags]
    internal enum DependencyFlags : int
    {
        None = 0,
        ByRegion = 1
    }

    [StructLayout(LayoutKind.Sequential)]
    internal struct SubpassDependency
    {
        public readonly SubpassHandle SrcSubpass;
        public readonly SubpassHandle DstSubpass;
        public readonly Stage SrcStage;
        public readonly Stage DstStage;
        public readonly AccessFlags SrcAccess;
        public readonly AccessFlags DstAccess;
        public readonly DependencyFlags Flags;

        private SubpassDependency(Dependency dependency)
        {
            SrcSubpass = dependency.Src.Subpass;
            DstSubpass = dependency.Dst.Subpass;
            SrcStage = dependency.Src.Stage;
            DstStage = dependency.Dst.Stage;
            SrcAccess = dependency.Src.Access;
            DstAccess = dependency.Dst.Access;
            Flags = DependencyFlags.None;
            if(dependency.ByRegion)
                Flags |= DependencyFlags.ByRegion;
        }

        public static implicit operator SubpassDependency(Dependency dependency) => new SubpassDependency(dependency);
    }
}