﻿using System;
using System.Runtime.InteropServices;
using oovulkan.image;

namespace oovulkan.render
{
    [Flags]
    internal enum AttachmentDescriptionFlags : int
    {
        None = 0,
        MayAlias = 1
    }

    [StructLayout(LayoutKind.Sequential)]
    internal struct AttachmentDescription
    {
        public readonly AttachmentDescriptionFlags Flags;
        public readonly Format Format;
        public readonly SampleCountFlags Samples;
        public readonly AttachmentLoadOp LoadOp;
        public readonly AttachmentStoreOp StoreOp;
        public readonly AttachmentLoadOp StencilLoadOp;
        public readonly AttachmentStoreOp StencilStoreOp;
        public readonly ImageLayout InitialLayout;
        public readonly ImageLayout FinalLayout;

        private AttachmentDescription(Attachment attachment)
        {
            Flags = AttachmentDescriptionFlags.None;
            if(attachment.MayAlias)
                Flags |= AttachmentDescriptionFlags.MayAlias;
            Format = attachment.Format;
            Samples = attachment.Samples;
            LoadOp = attachment.LoadOp;
            StoreOp = attachment.StoreOp;
            StencilLoadOp = attachment.StencilLoadOp;
            StencilStoreOp = attachment.StencilStoreOp;
            InitialLayout = attachment.InitialLayout;
            FinalLayout = attachment.FinalLayout;
        }

        public static implicit operator AttachmentDescription(Attachment attachment) => new AttachmentDescription(attachment);
    }
}