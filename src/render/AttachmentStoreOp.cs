﻿namespace oovulkan.render
{
    public enum AttachmentStoreOp : int
    {
        Store = 0,
        DontCare = 1
    }
}