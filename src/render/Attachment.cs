﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using oovulkan.image;

namespace oovulkan.render
{
    [StructLayout(LayoutKind.Sequential)]
    public struct AttachmentHandle
    {
        private uint Value;

        public static implicit operator AttachmentHandle(uint value) => new AttachmentHandle {Value = value};
        public static implicit operator uint(AttachmentHandle handle) => handle.Value;
    }

    public class Attachment : VulkanObject<AttachmentHandle>
    {
        public static readonly Attachment Unused = new Attachment(uint.MaxValue);

        protected internal override bool IsChild => true;
        protected internal override VulkanObject Parent => Renderpass;

        public readonly Renderpass Renderpass;
        public readonly bool MayAlias;
        public readonly Format Format;
        public readonly SampleCountFlags Samples;
        public readonly AttachmentLoadOp LoadOp;
        public readonly AttachmentStoreOp StoreOp;
        public readonly AttachmentLoadOp StencilLoadOp;
        public readonly AttachmentStoreOp StencilStoreOp;
        public readonly ImageLayout InitialLayout;
        public readonly ImageLayout FinalLayout;

        internal readonly List<(Subpass Subpass, ImageLayout Layout)> Uses = new List<(Subpass Subpass, ImageLayout Layout)>();

        public uint Index => Handle;

        public bool IsInput{ get; internal set; }
        public bool IsColor{ get; internal set; }
        public bool IsDepthStencil{ get; internal set; }

        private Attachment(AttachmentHandle handle)
        {
            Handle = handle;
        }

        internal Attachment(Renderpass renderpass, AttachmentHandle handle, bool mayAlias, Format format, SampleCountFlags samples, AttachmentLoadOp loadOp, AttachmentStoreOp storeOp, AttachmentLoadOp stencilLoadOp, AttachmentStoreOp stencilStoreOp, ImageLayout initialLayout, ImageLayout finalLayout)
        {
            Renderpass = renderpass;
            Handle = handle;
            MayAlias = mayAlias;
            Format = format;
            Samples = samples;
            LoadOp = loadOp;
            StoreOp = storeOp;
            StencilLoadOp = stencilLoadOp;
            StencilStoreOp = stencilStoreOp;
            InitialLayout = initialLayout;
            FinalLayout = finalLayout;
            Create();
        }

        private void Create()
        {
            #region Validation

            if(FinalLayout == ImageLayout.Undefined || FinalLayout == ImageLayout.Preinitialized)
                throw new ArgumentException(nameof(FinalLayout));

            #endregion
        }
        public override void Dispose() => throw new InvalidOperationException();
        protected override void Destroy() { }

        public bool IsCompatible(Attachment attachment)
        {
            if(this == Unused)
                return attachment == Unused;
            return Format == attachment.Format && Samples == attachment.Samples;
        }
    }
}