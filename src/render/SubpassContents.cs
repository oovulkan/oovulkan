﻿namespace oovulkan.render
{
    public enum SubpassContents : int
    {
        Inline = 0,
        SecondaryCommandBuffers = 1
    }
}