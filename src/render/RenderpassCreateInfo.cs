﻿using System;
using System.Runtime.InteropServices;

// ReSharper disable PrivateFieldCanBeConvertedToLocalVariable

namespace oovulkan.render
{
    [Flags]
    internal enum RenderpassCreateFlags : int
    {
        None = 0
    }

    [StructLayout(LayoutKind.Sequential)]
    internal unsafe struct RenderpassCreateInfo
    {
        private readonly StructureType Type;
        private readonly void* Next;
        private readonly RenderpassCreateFlags Flags;
        public readonly uint AttachmentCount;
        public readonly AttachmentDescription* AttachmentPtr;
        public readonly uint SubPassCount;
        public readonly SubpassDescription* SubPassPtr;
        public readonly uint DependencyCount;
        public readonly SubpassDependency* DependencyPtr;

        public RenderpassCreateInfo(
            uint attachmentCount, AttachmentDescription* attachmentPtr,
            uint subPassCount, SubpassDescription* subPassPtr,
            uint dependencyCount, SubpassDependency* dependencyPtr)
        {
            Type = StructureType.RenderPassCreateInfo;
            Next = null;
            Flags = RenderpassCreateFlags.None;
            AttachmentCount = attachmentCount;
            AttachmentPtr = attachmentPtr;
            SubPassCount = subPassCount;
            SubPassPtr = subPassPtr;
            DependencyCount = dependencyCount;
            DependencyPtr = dependencyPtr;
        }
    }
}