﻿// ReSharper disable InconsistentNaming
namespace oovulkan
{
    public static class InstanceLayer
    {
        public const string LunarGStandardValidation = "VK_LAYER_LUNARG_standard_validation";
        public const string GoogleUniqueObjects = "VK_LAYER_GOOGLE_unique_objects";
        public const string LunarGApiDump = "VK_LAYER_LUNARG_api_dump";
        public const string LunarGDeviceLimits = "VK_LAYER_LUNARG_device_limits";
        public const string LunarGCoreValidation = "VK_LAYER_LUNARG_core_validation";
        public const string LunarGImage = "VK_LAYER_LUNARG_image";
        public const string LunarGObjectTracker = "VK_LAYER_LUNARG_object_tracker";
        public const string LunarGParameterValidation = "VK_LAYER_LUNARG_parameter_validation";
        public const string LunarGSwapchain = "VK_LAYER_LUNARG_swapchain";
        public const string GoogleThreading = "VK_LAYER_GOOGLE_threading";
    }
}