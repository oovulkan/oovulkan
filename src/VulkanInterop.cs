﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.InteropServices;
using oovulkan.device;
using oovulkan.instance;
using oovulkan.util;

// ReSharper disable InconsistentNaming

namespace oovulkan
{
    public static class VulkanInterop
    {
        [AttributeUsage(AttributeTargets.Field)]
        public class IgnoreAttribute : Attribute { }
        [AttributeUsage(AttributeTargets.Field)]
        public class InstanceFunctionAttribute : Attribute { }

        static VulkanInterop()
        {
            vkGetInstanceProcAddr = Vulkan.GetProc("vkGetInstanceProcAddr").ToFunction<VkGetInstanceProcAddr>();
        }

        private static T ToFunction<T>(this IntPtr ptr) where T : class
        {
            return ptr == IntPtr.Zero ? null : Marshal.GetDelegateForFunctionPointer<T>(ptr);
        }
        private static Delegate ToFunction(this IntPtr ptr, Type type)
        {
            return ptr == IntPtr.Zero ? null : Marshal.GetDelegateForFunctionPointer(ptr, type);
        }

        public static T GetFunction<T>(string name) where T : class
        {
            using(AnsiString namePtr = (AnsiString)name)
                return vkGetInstanceProcAddr(null, namePtr).ToFunction<T>();
        }
        public static Delegate GetFunction(string name, Type type)
        {
            using(AnsiString namePtr = (AnsiString)name)
                return vkGetInstanceProcAddr(null, namePtr).ToFunction(type);
        }
        public static T GetFunction<T>(this Instance instance, string name) where T : class
        {
            using(AnsiString namePtr = (AnsiString)name)
                return vkGetInstanceProcAddr(instance, namePtr).ToFunction<T>();
        }
        public static Delegate GetFunction(this Instance instance, string name, Type type)
        {
            using(AnsiString namePtr = (AnsiString)name)
                return vkGetInstanceProcAddr(instance, namePtr).ToFunction(type);
        }
        public static T GetFunction<T>(this Device device, string name) where T : class
        {
            using(AnsiString namePtr = (AnsiString)name)
                return device.vkGetDeviceProcAddr(device, namePtr).ToFunction<T>();
        }
        public static Delegate GetFunction(this Device device, string name, Type type)
        {
            using(AnsiString namePtr = (AnsiString)name)
                return device.vkGetDeviceProcAddr(device, namePtr).ToFunction(type);
        }

        public static void Link(Type type)
        {
            foreach(var field in type.GetFields(BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic))
            {
                if(!typeof(Delegate).IsAssignableFrom(field.FieldType) || !field.Name.StartsWith("vk") || field.GetCustomAttribute<IgnoreAttribute>() != null)
                    continue;
                field.SetStatic(GetFunction(field.Name, field.FieldType));
            }
        }
        public static void Link(this VulkanObject obj, Instance instance)
        {
            //TODO: compile at runtime by type
            foreach(var field in WalkFields(obj.GetType()))
            {
                if(!typeof(Delegate).IsAssignableFrom(field.FieldType) || !field.Name.StartsWith("vk") || field.GetCustomAttribute<IgnoreAttribute>() != null)
                    continue;
                field.Set(obj, instance.GetFunction(field.Name, field.FieldType));
            }
        }
        public static void Link(this VulkanObject obj, Device device)
        {
            foreach(var field in WalkFields(obj.GetType()))
            {
                if(!typeof(Delegate).IsAssignableFrom(field.FieldType) || !field.Name.StartsWith("vk") || field.GetCustomAttribute<IgnoreAttribute>() != null)
                    continue;
                if(field.GetCustomAttribute<InstanceFunctionAttribute>() != null)
                {
                    field.Set(obj, device.PhysicalDevice.Instance.GetFunction(field.Name, field.FieldType));
                }
                else
                {
                    field.Set(obj, device.GetFunction(field.Name, field.FieldType));
                }
            }
        }

        private static IEnumerable<FieldInfo> WalkFields(Type type)
        {
            do
            {
                foreach(var field in type.GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.DeclaredOnly))
                    yield return field;
                type = type.BaseType;
            } while(type != typeof(VulkanObject) && type != typeof(VulkanObject<>));
        }

        private static readonly VkGetInstanceProcAddr vkGetInstanceProcAddr;
        private delegate IntPtr VkGetInstanceProcAddr(InstanceHandle instance, AnsiString name);
    }
}