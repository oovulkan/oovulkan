﻿using System;
using System.Runtime.InteropServices;

namespace oovulkan
{
    [StructLayout(LayoutKind.Sequential)]
    public struct Version
    {
        private const int MAJOR_BITS = 10;
        private const int MINOR_BITS = 10;
        private const int PATCH_BITS = 12;
        private const uint MAJOR_MASK = 1 << MAJOR_BITS - 1;
        private const uint MINOR_MASK = 1 << MINOR_BITS - 1;
        private const uint PATCH_MASK = 1 << PATCH_BITS - 1;

        public static Version Zero => new Version(0, 0, 0);

        private readonly uint value;

        public uint Major => value >> (MINOR_BITS + PATCH_BITS);
        public uint Minor => (value >> PATCH_BITS) & MINOR_MASK;
        public uint Patch => value & PATCH_MASK;

        private Version(uint major, uint minor, uint patch)
        {
            if(major > MAJOR_MASK)
                throw new ArgumentOutOfRangeException(nameof(major));
            if(minor > MINOR_MASK)
                throw new ArgumentOutOfRangeException(nameof(minor));
            if(patch > PATCH_MASK)
                throw new ArgumentOutOfRangeException(nameof(patch));
            value = ((major << MINOR_BITS | minor) << PATCH_BITS) | patch;
        }

        public override string ToString() => $"{Major}.{Minor}.{Patch}";

        public static implicit operator Version((uint Major, uint Minor, uint Patch) version) => new Version(version.Major, version.Minor, version.Patch);
        public static implicit operator (uint Major, uint Minor, uint Patch)(Version version) => (version.Major, version.Minor, version.Patch);

        public static bool operator ==(Version left, Version right) => left.value == right.value;
        public static bool operator !=(Version left, Version right) => !(left == right);
        public static bool operator >(Version left, Version right) => left.value > right.value;
        public static bool operator <(Version left, Version right) => left.value < right.value;
        public static bool operator >=(Version left, Version right) => left.value >= right.value;
        public static bool operator <=(Version left, Version right) => left.value <= right.value;
        public bool Equals(Version other) => this == other;
        public override int GetHashCode() => (int)value;
        public override bool Equals(object obj) => obj is Version && Equals((Version)obj);
    }
}