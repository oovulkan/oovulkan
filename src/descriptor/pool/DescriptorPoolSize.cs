﻿using System.Runtime.InteropServices;

namespace oovulkan.descriptor.pool
{
    [StructLayout(LayoutKind.Sequential)]
    public struct DescriptorPoolSize
    {
        public readonly DescriptorType Type;
        public readonly uint Count;

        private DescriptorPoolSize(DescriptorType type, uint count)
        {
            Type = type;
            Count = count;
        }

        public static implicit operator DescriptorPoolSize((DescriptorType Type, uint Count) size) => new DescriptorPoolSize(size.Type, size.Count);
        public static implicit operator (DescriptorType Type, uint Count)(DescriptorPoolSize size) => (size.Type, size.Count);
    }
}