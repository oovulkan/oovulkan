﻿using System;
using System.Runtime.InteropServices;

// ReSharper disable PrivateFieldCanBeConvertedToLocalVariable

namespace oovulkan.descriptor.pool
{
    [Flags]
    internal enum DescriptorPoolCreateFlags : int
    {
        None = 0,
        FreeDescriptorSet = 1
    }

    [StructLayout(LayoutKind.Sequential)]
    internal unsafe struct DescriptorPoolCreateInfo
    {
        private readonly StructureType Type;
        private readonly void* Next;
        public readonly DescriptorPoolCreateFlags Flags;
        public readonly uint MaxSets;
        public readonly uint PoolSizeCount;
        public readonly DescriptorPoolSize* PoolSizePtr;

        public DescriptorPoolCreateInfo(DescriptorPoolCreateFlags flags, uint maxSets, uint poolSizeCount, DescriptorPoolSize* poolSizePtr)
        {
            Type = StructureType.DescriptorPoolCreateInfo;
            Next = null;
            Flags = flags;
            MaxSets = maxSets;
            PoolSizeCount = poolSizeCount;
            PoolSizePtr = poolSizePtr;
        }
    }
}