﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using oovulkan.descriptor.layout;
using oovulkan.device;
using oovulkan.util;
using oovulkan.util.collection;

#pragma warning disable 649

namespace oovulkan.descriptor.pool
{
    [StructLayout(LayoutKind.Sequential)]
    public struct DescriptorPoolHandle
    {
        private long Value;

        public static implicit operator DescriptorPoolHandle(long? value) => new DescriptorPoolHandle {Value = value ?? 0};
        public static implicit operator long(DescriptorPoolHandle handle) => handle.Value;
    }

    public unsafe class DescriptorPool : VulkanObject<DescriptorPoolHandle>
    {
        protected internal override bool IsChild => true;
        protected internal override VulkanObject Parent => Device;
        protected internal override bool IsParent => true;
        protected internal override IEnumerable<VulkanObject> Children => sets;

        public readonly Device Device;
        public readonly bool FreeableSets;
        public readonly ReadonlyList<DescriptorPoolSize> Sizes;
        public readonly uint MaxSets;
        public readonly ReadonlyDictionary<DescriptorType, uint> MaxDescriptors;

        private readonly HashSet<DescriptorSet> sets = new HashSet<DescriptorSet>();
        private readonly Dictionary<DescriptorType, uint> descriptors = new Dictionary<DescriptorType, uint>();

        public ReadonlySet<DescriptorSet> Sets => sets;

        public uint RemainingSets => MaxSets - (uint)sets.Count;
        public uint RemainingDescriptors(DescriptorType type) => MaxDescriptors.GetOrDefault(type) - descriptors.GetOrDefault(type);

        internal DescriptorPool(Device device, bool freeableSets, uint maxSets, IEnumerable<DescriptorPoolSize> sizes)
        {
            Device = device;
            this.Link(Device);
            FreeableSets = freeableSets;
            MaxSets = maxSets;
            Sizes = sizes.ToList();
            var dict = new Dictionary<DescriptorType, uint>();
            foreach(var size in Sizes)
                dict[size.Type] = dict.GetOrDefault(size.Type) + size.Count;
            MaxDescriptors = dict;
            Create();
        }

        private void Create()
        {
            #region Validation

            if(MaxSets == 0)
                throw new ArgumentOutOfRangeException(nameof(MaxSets));
            if(Sizes.Count == 0 || Sizes.Any(size => size.Count == 0))
                throw new ArgumentException(nameof(Sizes));
            var flags = FreeableSets ? DescriptorPoolCreateFlags.FreeDescriptorSet : DescriptorPoolCreateFlags.None;

            #endregion

            var sizePtr = stackalloc DescriptorPoolSize[Sizes.Count];
            for(uint i = 0; i < Sizes.Count; i++)
                sizePtr[i] = Sizes[(int)i];
            var info = new DescriptorPoolCreateInfo(flags, MaxSets, (uint)Sizes.Count, sizePtr);
            using(Device.WeakLock)
            {
                DescriptorPoolHandle handle;
                using(Device.Allocator?.WeakLock)
                    vkCreateDescriptorPool(Device, &info, Device.Allocator, out handle);
                Handle = handle;
                Device.Add(this);
            }
        }
        protected override void Destroy()
        {
            using(Device.WeakLock)
                Device.Remove(this);
            using(Device.Allocator?.WeakLock)
                vkDestroyDescriptorPool(Device, this, Device.Allocator);
        }

        public DescriptorSet Allocate(DescriptorSetLayout layout)
        {
            // ReSharper disable once PossiblyMistakenUseOfParamsMethod
            return Allocate(layouts: layout)[0];
        }
        public DescriptorSet[] Allocate(params DescriptorSetLayout[] layouts)
        {
            #region Validation

            if(layouts.LongLength == 0)
                return new DescriptorSet[0];
            if(layouts.Any(layout => layout.Device != Device))
                throw new ArgumentException(nameof(layouts));

            #endregion

            var typedUsage = new Dictionary<DescriptorType, uint>();
            foreach(var layout in layouts)
            foreach(var binding in layout.Bindings)
                typedUsage[binding.Type] = typedUsage.GetOrDefault(binding.Type) + binding.Count;

            var layoutPtr = stackalloc DescriptorSetLayoutHandle[(int)layouts.LongLength];
            var handles = stackalloc DescriptorSetHandle[(int)layouts.LongLength];
            for(uint i = 0; i < layouts.LongLength; i++)
                layoutPtr[i] = layouts[i];

            var info = new DescriptorSetAllocateInfo(this, (uint)layouts.LongLength, layoutPtr);
            using(Lock)
            {
                #region Validation

                if(RemainingSets < layouts.LongLength || typedUsage.Any(entry => RemainingDescriptors(entry.Key) < entry.Value))
                    throw new ArgumentException(nameof(layouts));

                #endregion

                var result = vkAllocateDescriptorSets(Device, &info, handles);
                if(result.IsError())
                    Result.ErrorFragmentedPool.Throw();

                foreach(var entry in typedUsage)
                    descriptors[entry.Key] -= entry.Value;

                var sets = new DescriptorSet[layouts.LongLength];
                for(uint i = 0; i < sets.LongLength; i++)
                {
                    var set = new DescriptorSet(this, layouts[i], handles[i]);
                    this.sets.Add(set);
                    sets[i] = set;
                }
                return sets;
            }
        }
        public void Free(params DescriptorSet[] sets)
        {
            #region Validation

            //prevent recursive freeing
            if(sets.LongLength == 1 && !this.sets.Contains(sets[0]))
                return;
            if(sets.LongLength == 0)
                return;
            if(sets.Any(set => set.Pool != this))
                throw new ArgumentException(nameof(sets));
            if(sets.Distinct().LongCount() != sets.LongLength)
                throw new ArgumentException(nameof(sets));

            #endregion

            //sort to prevent deadlock by ensuring constant enter order
            Array.Sort(sets, (set1, set2) => set1.GetHashCode() - set2.GetHashCode());

            var handles = stackalloc DescriptorSetHandle[(int)sets.LongLength];
            for(uint i = 0; i < sets.LongLength; i++)
                handles[i] = sets[i];
            using(Lock)
            using(sets.LockAll(set => set.StrongLock))
            {
                vkFreeDescriptorSets(Device, this, (uint)sets.LongLength, handles);
                foreach(var set in sets)
                {
                    foreach(var binding in set.Layout.Bindings)
                        descriptors[binding.Type] -= binding.Count;
                    this.sets.Remove(set);
                    set.Dispose();
                }
            }
        }

        private readonly VkCreateDescriptorPool vkCreateDescriptorPool;
        private readonly VkDestroyDescriptorPool vkDestroyDescriptorPool;
        private readonly VkAllocateDescriptorSets vkAllocateDescriptorSets;
        private readonly VkFreeDescriptorSets vkFreeDescriptorSets;

        private delegate Result VkCreateDescriptorPool(DeviceHandle device, DescriptorPoolCreateInfo* info, AllocationCallbacks* allocator, out DescriptorPoolHandle pool);
        private delegate void VkDestroyDescriptorPool(DeviceHandle device, DescriptorPoolHandle pool, AllocationCallbacks* allocator);
        private delegate Result VkAllocateDescriptorSets(DeviceHandle device, DescriptorSetAllocateInfo* info, DescriptorSetHandle* sets);
        private delegate void VkFreeDescriptorSets(DeviceHandle device, DescriptorPoolHandle pool, uint count, DescriptorSetHandle* sets);
    }
}