﻿using System.Runtime.InteropServices;
using oovulkan.sampler;
using oovulkan.util;

namespace oovulkan.descriptor.layout
{
    public struct DescriptorSetLayoutBinding
    {
        public readonly DescriptorType Type;
        public readonly uint Count;
        public readonly ShaderStageFlags StageFlags;
        public readonly Sampler[] ImmutableSamplers;

        private DescriptorSetLayoutBinding(DescriptorType type, uint count, ShaderStageFlags stageFlags, Sampler[] immutableSamplers)
        {
            Type = type;
            Count = count;
            StageFlags = stageFlags;
            ImmutableSamplers = immutableSamplers;
        }

        public static implicit operator DescriptorSetLayoutBinding((DescriptorType Type, uint Count, ShaderStageFlags StageFlags) binding)
        {
            return new DescriptorSetLayoutBinding(binding.Type, binding.Count, binding.StageFlags, null);
        }
        public static implicit operator DescriptorSetLayoutBinding((DescriptorType Type, uint Count, ShaderStageFlags StageFlags, Sampler[] ImmutableSamplers) binding)
        {
            return new DescriptorSetLayoutBinding(binding.Type, binding.Count, binding.StageFlags, binding.ImmutableSamplers);
        }

        [StructLayout(LayoutKind.Sequential)]
        internal unsafe struct Native
        {
            public readonly uint Binding;
            public readonly DescriptorType Type;
            public readonly uint Count;
            public readonly ShaderStageFlags StageFlags;
            public readonly SamplerHandle* ImmutableSamplers;

            public Native(uint binding, DescriptorType type, uint count, ShaderStageFlags stageFlags, SamplerHandle* immutableSamplers)
            {
                Binding = binding;
                Type = type;
                Count = count;
                StageFlags = stageFlags;
                ImmutableSamplers = immutableSamplers;
            }
        }

        public bool Equals(DescriptorSetLayoutBinding other) => this == other;
        public override bool Equals(object obj) => obj is DescriptorSetLayoutBinding && Equals((DescriptorSetLayoutBinding)obj);
        public static bool operator ==(DescriptorSetLayoutBinding left, DescriptorSetLayoutBinding right) => left.Type == right.Type && left.Count == right.Count && left.StageFlags == right.StageFlags && Util.Equals(left.ImmutableSamplers, right.ImmutableSamplers);
        public static bool operator !=(DescriptorSetLayoutBinding left, DescriptorSetLayoutBinding right) => !(left == right);
        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = (int)Type;
                hashCode = (hashCode * 397) ^ (int)Count;
                hashCode = (hashCode * 397) ^ (int)StageFlags;
                hashCode = (hashCode * 397) ^ CollectionUtil.GetHashCode(ImmutableSamplers);
                return hashCode;
            }
        }
    }
}