﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using oovulkan.device;
using oovulkan.sampler;
using oovulkan.util;
using oovulkan.util.collection;

#pragma warning disable 649

namespace oovulkan.descriptor.layout
{
    [StructLayout(LayoutKind.Sequential)]
    public struct DescriptorSetLayoutHandle
    {
        private long Value;

        public static implicit operator DescriptorSetLayoutHandle(long? value) => new DescriptorSetLayoutHandle {Value = value ?? 0};
        public static implicit operator long(DescriptorSetLayoutHandle handle) => handle.Value;
    }

    public unsafe class DescriptorSetLayout : VulkanObject<DescriptorSetLayoutHandle>
    {
        protected internal override bool IsChild => true;
        protected internal override VulkanObject Parent => Device;

        public readonly Device Device;
        public readonly ReadonlyList<DescriptorSetLayoutBinding> Bindings;

        internal DescriptorSetLayout(Device device, IEnumerable<DescriptorSetLayoutBinding> bindings)
        {
            Device = device;
            this.Link(Device);
            Bindings = bindings.ToList();
            Create();
        }

        private void Create()
        {
            #region Validation

            if(Bindings.Any(binding => binding.ImmutableSamplers != null && binding.ImmutableSamplers.Any(sampler => sampler.Device != Device)))
                throw new ArgumentException(nameof(Bindings));
            var bindingPtr = stackalloc DescriptorSetLayoutBinding.Native[Bindings.Count];
            for(uint i = 0; i < Bindings.Count; i++)
            {
                var binding = Bindings[(int)i];
                var samplerPtr = stackalloc SamplerHandle[(int)(binding.ImmutableSamplers?.LongLength ?? 0)];
                if(binding.ImmutableSamplers != null)
                {
                    if(binding.Type != DescriptorType.Sampler && binding.Type != DescriptorType.CombinedImageSampler)
                        throw new ArgumentException(nameof(Bindings));
                    if(binding.ImmutableSamplers.LongLength != binding.Count)
                        throw new ArgumentException(nameof(Bindings));
                    for(uint j = 0; j < binding.Count; j++)
                        samplerPtr[j] = binding.ImmutableSamplers[j];
                }
                bindingPtr[i] = new DescriptorSetLayoutBinding.Native(i, binding.Type, binding.Count, binding.StageFlags, samplerPtr);
            }

            #endregion

            var info = new DescriptorSetLayoutCreateInfo((uint)Bindings.Count, bindingPtr);
            using(Device.WeakLock)
            {
                DescriptorSetLayoutHandle handle;
                using(Device.Allocator?.WeakLock)
                    vkCreateDescriptorSetLayout(Device, &info, Device.Allocator, out handle).Throw();
                Handle = handle;
                Device.Add(this);
            }
        }
        protected override void Destroy()
        {
            using(Device.WeakLock)
                Device.Remove(this);
            using(Device.Allocator?.WeakLock)
                vkDestroyDescriptorSetLayout(Device, this, Device.Allocator);
        }

        private readonly VkCreateDescriptorSetLayout vkCreateDescriptorSetLayout;
        private readonly VkDestroyDescriptorSetLayout vkDestroyDescriptorSetLayout;

        private delegate Result VkCreateDescriptorSetLayout(DeviceHandle device, DescriptorSetLayoutCreateInfo* info, AllocationCallbacks* allocator, out DescriptorSetLayoutHandle layout);
        private delegate void VkDestroyDescriptorSetLayout(DeviceHandle device, DescriptorSetLayoutHandle layout, AllocationCallbacks* allocator);

        public bool Equals(DescriptorSetLayout other) => this == other;
        public override bool Equals(object obj) => obj is DescriptorSetLayout && Equals((DescriptorSetLayout)obj);
        public static bool operator ==(DescriptorSetLayout left, DescriptorSetLayout right) => Util.Equals(left?.Bindings, right?.Bindings);
        public static bool operator !=(DescriptorSetLayout left, DescriptorSetLayout right) => !(left == right);
        public override int GetHashCode() => CollectionUtil.GetHashCode(Bindings);
    }
}