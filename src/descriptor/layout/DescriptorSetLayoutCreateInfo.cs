﻿using System;
using System.Runtime.InteropServices;

namespace oovulkan.descriptor.layout
{
    [Flags]
    internal enum DescriptorSetLayoutCreateFlags : int
    {
        None = 0
    }

    [StructLayout(LayoutKind.Sequential)]
    internal unsafe struct DescriptorSetLayoutCreateInfo
    {
        private readonly StructureType Type;
        private readonly void* Next;
        private readonly DescriptorSetLayoutCreateFlags Flags;
        public readonly uint BindingCount;
        public readonly DescriptorSetLayoutBinding.Native* BindingPtr;

        public DescriptorSetLayoutCreateInfo(uint bindingCount, DescriptorSetLayoutBinding.Native* bindingPtr)
        {
            Type = StructureType.DescriptorSetLayoutCreateInfo;
            Next = null;
            Flags = DescriptorSetLayoutCreateFlags.None;
            BindingCount = bindingCount;
            BindingPtr = bindingPtr;
        }
    }
}