﻿using System.Runtime.InteropServices;
using oovulkan.descriptor.layout;
using oovulkan.descriptor.pool;

// ReSharper disable PrivateFieldCanBeConvertedToLocalVariable

namespace oovulkan.descriptor
{
    [StructLayout(LayoutKind.Sequential)]
    internal unsafe struct DescriptorSetAllocateInfo
    {
        private readonly StructureType Type;
        private readonly void* Next;
        public readonly DescriptorPoolHandle DescriptorPool;
        public readonly uint Count;
        public readonly DescriptorSetLayoutHandle* LayoutPtr;

        public DescriptorSetAllocateInfo(DescriptorPool descriptorPool, uint count, DescriptorSetLayoutHandle* layoutPtr)
        {
            Type = StructureType.DescriptorSetAllocateInfo;
            Next = null;
            DescriptorPool = descriptorPool;
            Count = count;
            LayoutPtr = layoutPtr;
        }
    }
}