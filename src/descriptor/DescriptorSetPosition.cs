﻿using System;
using System.Linq;

namespace oovulkan.descriptor
{
    public struct DescriptorSetPosition
    {
        public readonly DescriptorSet Set;
        public readonly ulong[] DynamicOffsets;

        private DescriptorSetPosition(DescriptorSet set, ulong[] dynamicOffsets)
        {
            Set = set;
            DynamicOffsets = dynamicOffsets;
            var count = Set.Layout.Bindings.Count(binding => binding.Type == DescriptorType.StorageBufferDynamic || binding.Type == DescriptorType.UniformBufferDynamic);
            if(count != (DynamicOffsets?.Length ?? 0))
                throw new ArgumentOutOfRangeException(nameof(dynamicOffsets));
        }

        public static implicit operator DescriptorSetPosition(DescriptorSet set) => new DescriptorSetPosition(set, null);
        public static implicit operator DescriptorSetPosition((DescriptorSet Set, ulong[] DynamicOffsets) pos) => new DescriptorSetPosition(pos.Set, pos.DynamicOffsets);
    }
}