﻿using System.Runtime.InteropServices;
using oovulkan.descriptor.layout;
using oovulkan.descriptor.pool;

namespace oovulkan.descriptor
{
    [StructLayout(LayoutKind.Sequential)]
    public struct DescriptorSetHandle
    {
        private long Value;

        public static implicit operator DescriptorSetHandle(long? value) => new DescriptorSetHandle {Value = value ?? 0};
        public static implicit operator long(DescriptorSetHandle handle) => handle.Value;
    }

    public class DescriptorSet : VulkanObject<DescriptorSetHandle>
    {
        protected internal override bool IsChild => true;
        protected internal override VulkanObject Parent => Pool;

        public readonly DescriptorPool Pool;
        public readonly DescriptorSetLayout Layout;

        internal DescriptorSet(DescriptorPool pool, DescriptorSetLayout layout, DescriptorSetHandle handle)
        {
            Pool = pool;
            this.Link(Pool.Device);
            Layout = layout;
            Handle = handle;
        }

        protected override void Destroy()
        {
            Pool.Free(this);
        }
    }
}