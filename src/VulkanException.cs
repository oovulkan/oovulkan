﻿using System;
using System.Reflection;

namespace oovulkan
{
    public class VulkanException : Exception
    {
        private static readonly FieldInfo MessageInfo = typeof(Exception).GetField("_message", BindingFlags.NonPublic | BindingFlags.Instance);

        public Result Result;

        public new string Message
        {
            get => base.Message;
            set => MessageInfo.SetValue(this, value);
        }

        public VulkanException(Result result) : base($"Vulkan Exception: {result}")
        {
            Result = result;
        }
    }
}