﻿using System;
using System.Collections.Generic;
using System.Linq;
using oovulkan.device;
using oovulkan.device.physical;
using oovulkan.image.view;
using oovulkan.queue.family;
using oovulkan.util;
using oovulkan.util.collection;

#pragma warning disable 649

namespace oovulkan.image
{
    public abstract unsafe class Image : ImageBase
    {
        public override ImageCreateFlags Flags{ get; }
        public override Format Format{ get; }
        public override ImageType Type{ get; }
        protected internal override (uint Width, uint Height, uint Depth) Extent{ get; }
        public override uint MipLevels{ get; }
        public override uint ArrayLayers{ get; }
        public override ImageTiling Tiling{ get; }
        public override ImageUsageFlags Usage{ get; }
        protected internal override SharingMode SharingMode{ get; }
        protected internal override ReadonlyList<QueueFamily> Families{ get; }
        public override SampleCountFlags Samples{ get; }
        public override ImageLayout InitialLayout{ get; }

        internal Image(Device device, ImageCreateFlags flags, Format format, ImageType type,
            (uint, uint, uint) extent, uint mipLevels, uint arrayLayers, SampleCountFlags samples,
            ImageTiling tiling, ImageUsageFlags usage, ImageLayout initialLayout, SharingMode sharingMode,
            IEnumerable<QueueFamily> families) : base(device)
        {
            this.Link(Device);
            Flags = flags;
            Format = format;
            Type = type;
            Extent = extent;
            MipLevels = mipLevels;
            ArrayLayers = arrayLayers;
            Samples = samples;
            Tiling = tiling;
            Usage = usage;
            InitialLayout = initialLayout;
            SharingMode = sharingMode;
            Families = families?.ToList();
            Create();
        }

        private void Create()
        {
            #region Validation

            if(Usage == ImageUsageFlags.None)
                throw new ArgumentNullException(nameof(Usage));
            if(SharingMode == SharingMode.Concurrent)
            {
                if(Families.Count == 0)
                    throw new ArgumentNullException(nameof(Families));
                if(Families.Any(family => family.PhysicalDevice != Device.PhysicalDevice))
                    throw new ArgumentException(nameof(Families));
                if(Families.Distinct().Count() != Families.Count)
                    throw new ArgumentException(nameof(Families));
            }
            if(Format == Format.Undefined)
                throw new ArgumentException(nameof(Format));
            if(Extent.Width == 0 || Extent.Height == 0 || Extent.Depth == 0)
                throw new ArgumentOutOfRangeException(nameof(Extent));
            if(MipLevels == 0)
                throw new ArgumentOutOfRangeException(nameof(MipLevels));
            if(ArrayLayers == 0)
                throw new ArgumentOutOfRangeException(nameof(ArrayLayers));
            if(Flags.HasFlag(ImageCreateFlags.CubeCompatible) && Type != ImageType.X2D)
                throw new ArgumentException(nameof(Flags));
            var limits = Device.PhysicalDevice.Limits;
            var features = Device.Features;
            var formatProperties = Device.PhysicalDevice.FormatProperties(Format);
            var imageFormatProperties = Device.PhysicalDevice.ImageFormatProperties(Format, Type, Tiling, Usage, Flags);
            switch(Type)
            {
                case ImageType.X1D:
                {
                    uint maxWidth = Math.Max(limits.MaxImageDimension1D, imageFormatProperties.MaxExtent.Width);
                    if(Extent.Width > maxWidth)
                        throw new ArgumentOutOfRangeException(nameof(Extent));
                    if(Extent.Height != 1 || Extent.Depth != 1)
                        throw new ArgumentException(nameof(Extent));
                    if(Flags.HasFlag(ImageCreateFlags.SparseResidency))
                        throw new ArgumentException(nameof(Flags));
                    break;
                }
                case ImageType.X2D:
                {
                    uint maxWidth = imageFormatProperties.MaxExtent.Width;
                    uint maxHeight = imageFormatProperties.MaxExtent.Height;
                    if(Flags.HasFlag(ImageCreateFlags.CubeCompatible))
                    {
                        maxWidth = Math.Max(limits.MaxImageDimensionCube, maxWidth);
                        maxHeight = Math.Max(limits.MaxImageDimensionCube, maxHeight);
                        if(Extent.Width > maxWidth || Extent.Height > maxHeight)
                            throw new ArgumentOutOfRangeException(nameof(Extent));
                        if(Extent.Width != Extent.Height)
                            throw new ArgumentException(nameof(Extent));
                        if(ArrayLayers < 6)
                            throw new ArgumentException(nameof(ArrayLayers));
                        if(Extent.Depth != 1)
                            throw new ArgumentException(nameof(Extent));
                    }
                    else
                    {
                        maxWidth = Math.Max(limits.MaxImageDimension2D, maxWidth);
                        maxHeight = Math.Max(limits.MaxImageDimension2D, maxHeight);
                        if(Extent.Width > maxWidth || Extent.Height > maxHeight)
                            throw new ArgumentOutOfRangeException(nameof(Extent));
                    }
                    if(Flags.HasFlag(ImageCreateFlags.SparseResidency))
                    {
                        if(!features.SparseResidencyImage2D)
                            throw new NotSupportedException(nameof(Flags));
                        if(!features.SparseResidency2Samples && Samples == SampleCountFlags.Bit2)
                            throw new NotSupportedException(nameof(Flags));
                        if(!features.SparseResidency4Samples && Samples == SampleCountFlags.Bit4)
                            throw new NotSupportedException(nameof(Flags));
                        if(!features.SparseResidency8Samples && Samples == SampleCountFlags.Bit8)
                            throw new NotSupportedException(nameof(Flags));
                        if(!features.SparseResidency16Samples && Samples == SampleCountFlags.Bit16)
                            throw new NotSupportedException(nameof(Flags));
                    }
                    break;
                }
                case ImageType.X3D:
                {
                    uint maxWidth = Math.Max(limits.MaxImageDimension3D, imageFormatProperties.MaxExtent.Width);
                    uint maxHeight = Math.Max(limits.MaxImageDimension3D, imageFormatProperties.MaxExtent.Height);
                    uint maxDepth = Math.Max(limits.MaxImageDimension3D, imageFormatProperties.MaxExtent.Depth);
                    if(Extent.Width > maxWidth || Extent.Height > maxHeight || Extent.Depth > maxDepth)
                        throw new ArgumentOutOfRangeException(nameof(Extent));
                    if(ArrayLayers != 1)
                        throw new ArgumentOutOfRangeException(nameof(ArrayLayers));
                    if(!features.SparseResidencyImage3D && Flags.HasFlag(ImageCreateFlags.SparseResidency))
                        throw new NotSupportedException(nameof(Flags));
                    break;
                }
            }
            if(MipLevels > Util.Log2(Math.Max(Math.Max(Extent.Width, Extent.Height), Extent.Depth)) + 1)
                throw new ArgumentOutOfRangeException(nameof(MipLevels));
            if((Extent.Width > limits.MaxImageDimension3D || Extent.Height > limits.MaxImageDimension3D || Extent.Depth > limits.MaxImageDimension3D) &&
               MipLevels > imageFormatProperties.MaxMipLevels)
                throw new ArgumentOutOfRangeException(nameof(MipLevels));
            if(ArrayLayers > imageFormatProperties.MaxArrayLayers)
                throw new ArgumentOutOfRangeException(nameof(ArrayLayers));
            if(Samples != SampleCountFlags.Bit1)
            {
                if(Type != ImageType.X2D)
                    throw new ArgumentException(nameof(Type));
                if(Flags.HasFlag(ImageCreateFlags.CubeCompatible))
                    throw new ArgumentException(nameof(Flags));
                if(Tiling != ImageTiling.Optimal)
                    throw new ArgumentException(nameof(Tiling));
                if(MipLevels != 1)
                    throw new ArgumentException(nameof(MipLevels));
            }
            const ImageUsageFlags primaryAttachments = ImageUsageFlags.ColorAttachment | ImageUsageFlags.DepthStencilAttachment | ImageUsageFlags.InputAttachment;
            const ImageUsageFlags attachments = ImageUsageFlags.TransientAttachment | primaryAttachments;
            if(Usage.HasFlag(ImageUsageFlags.TransientAttachment))
            {
                if((Usage & ~attachments) != 0)
                    throw new ArgumentException(nameof(Usage));
                if((Usage & primaryAttachments) == 0)
                    throw new ArgumentException(nameof(Usage));
            }
            if((Usage & attachments) != 0)
            {
                if(Extent.Width > limits.MaxFramebufferWidth || Extent.Height > limits.MaxFramebufferHeight)
                    throw new ArgumentOutOfRangeException(nameof(Extent));
            }
            if(!imageFormatProperties.SampleCounts.HasFlag(Samples))
                throw new ArgumentException(nameof(Samples));
            if(!features.ShaderStorageImageMultisample && Usage.HasFlag(ImageUsageFlags.Storage) && Samples != SampleCountFlags.Bit1)
                throw new NotSupportedException(nameof(Samples));
            if(!features.SparseBinding && Flags.HasFlag(ImageCreateFlags.SparseBinding))
                throw new NotSupportedException(nameof(Flags));
            if((Flags.HasFlag(ImageCreateFlags.SparseResidency) || Flags.HasFlag(ImageCreateFlags.SparseAliased)) && !Flags.HasFlag(ImageCreateFlags.SparseBinding))
                throw new ArgumentException(nameof(Flags));

            FormatFeatureFlags featureFlags = formatProperties.GetFeatures(Tiling);
            if(featureFlags == FormatFeatureFlags.None)
                throw new NotSupportedException(nameof(Tiling));
            if(!featureFlags.HasFlag(FormatFeatureFlags.SampledImage) && Usage.HasFlag(ImageUsageFlags.Sampled))
                throw new NotSupportedException(nameof(Usage));
            if(!featureFlags.HasFlag(FormatFeatureFlags.StorageImage) && Usage.HasFlag(ImageUsageFlags.Storage))
                throw new NotSupportedException(nameof(Usage));
            if(!featureFlags.HasFlag(FormatFeatureFlags.ColorAttachment) && Usage.HasFlag(ImageUsageFlags.ColorAttachment))
                throw new NotSupportedException(nameof(Usage));
            if(!featureFlags.HasFlag(FormatFeatureFlags.DepthStencilAttachment) && Usage.HasFlag(ImageUsageFlags.DepthStencilAttachment))
                throw new NotSupportedException(nameof(Usage));

            if(InitialLayout != ImageLayout.Undefined && InitialLayout != ImageLayout.Preinitialized)
                throw new ArgumentException(nameof(InitialLayout));

            #endregion

            uint familyCount = (uint)(Families?.Count ?? 0);
            var familyPtr = stackalloc QueueFamilyHandle[(int)familyCount];
            for(uint i = 0; i < familyCount; i++)
                // ReSharper disable once PossibleNullReferenceException
                familyPtr[i] = Families[(int)i];
            ImageCreateInfo info = new ImageCreateInfo(Flags, Type, Format, Extent, MipLevels, ArrayLayers, Samples, Tiling, Usage, SharingMode, familyCount, familyPtr, InitialLayout);
            using(Device.WeakLock)
            {
                ImageHandle handle;
                using(Device.Allocator?.WeakLock)
                    vkCreateImage(Device, &info, Device.Allocator, out handle).Throw();
                Handle = handle;
                Device.Add(this);
            }
        }
        protected override void Destroy()
        {
            using(Device.WeakLock)
                Device.Remove(this);
            using(Device.Allocator?.WeakLock)
                vkDestroyImage(Device, this, Device.Allocator);
        }

        public ImageView GetView(ImageViewType type, Format format, Swizzle swizzle, ImageAspectFlags aspect, uint baseMipLevel, uint levelCount, uint baseArrayLayer, uint layerCount)
        {
            return new ImageView(this, type, format, swizzle, aspect, baseMipLevel, levelCount, baseArrayLayer, layerCount);
        }

        private readonly VkCreateImage vkCreateImage;
        private readonly VkDestroyImage vkDestroyImage;

        private delegate Result VkCreateImage(DeviceHandle device, ImageCreateInfo* info, AllocationCallbacks* allocator, out ImageHandle image);
        private delegate void VkDestroyImage(DeviceHandle device, ImageHandle image, AllocationCallbacks* allocator);
    }
}