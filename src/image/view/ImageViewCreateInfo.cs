﻿using System;
using System.Runtime.InteropServices;

// ReSharper disable PrivateFieldCanBeConvertedToLocalVariable

namespace oovulkan.image.view
{
    [Flags]
    public enum ImageViewCreateFlags : int
    {
        None = 0
    }

    [StructLayout(LayoutKind.Sequential)]
    internal unsafe struct ImageViewCreateInfo
    {
        private readonly StructureType Type;
        private readonly void* Next;
        private readonly ImageViewCreateFlags Flags;
        public readonly ImageHandle Image;
        public readonly ImageViewType ViewType;
        public readonly Format Format;
        public readonly Swizzle Components;
        public readonly ImageSubresourceRange SubresourceRange;

        public ImageViewCreateInfo(ImageBase image, ImageViewType viewType, Format format,
            (Comp R, Comp G, Comp B, Comp A) components,
            ImageSubresourceRange subresourceRange)
        {
            Type = StructureType.ImageViewCreateInfo;
            Next = null;
            Flags = ImageViewCreateFlags.None;
            Image = image;
            ViewType = viewType;
            Format = format;
            Components = components;
            SubresourceRange = subresourceRange;
        }
    }
}