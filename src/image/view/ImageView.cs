﻿using System;
using System.Runtime.InteropServices;
using oovulkan.device;
using oovulkan.device.physical;

#pragma warning disable 649

namespace oovulkan.image.view
{
    [StructLayout(LayoutKind.Sequential)]
    public struct ImageViewHandle
    {
        private long Value;

        public static implicit operator ImageViewHandle(long? value) => new ImageViewHandle {Value = value ?? 0};
        public static implicit operator long(ImageViewHandle handle) => handle.Value;
    }

    public unsafe class ImageView : VulkanObject<ImageViewHandle>
    {
        protected internal override bool IsChild => true;
        protected internal override VulkanObject Parent => Image;

        public readonly ImageBase Image;
        public readonly ImageViewType Type;
        public readonly Format Format;
        public readonly Swizzle Swizzle;
        public readonly ImageAspectFlags Aspect;
        public readonly uint BaseMipLevel;
        public readonly uint LevelCount;
        public readonly uint BaseArrayLayer;
        public readonly uint LayerCount;

        internal ImageView(ImageBase image, ImageViewType type, Format format, Swizzle swizzle, ImageAspectFlags aspect,
            uint baseMipLevel, uint levelCount, uint baseArrayLayer, uint layerCount)
        {
            Image = image;
            this.Link(Image.Device);
            Type = type;
            Format = format;
            Swizzle = swizzle;
            Aspect = aspect;
            BaseMipLevel = baseMipLevel;
            LevelCount = levelCount;
            BaseArrayLayer = baseArrayLayer;
            LayerCount = layerCount;
            Create();
        }

        private void Create()
        {
            #region Validation

            if(Aspect == ImageAspectFlags.None)
                throw new ArgumentException(nameof(Aspect));
            if(Image.Flags.HasFlag(ImageCreateFlags.MutableFormat))
            {
                if(!Format.IsCompatible(Image.Format))
                    throw new ArgumentException(nameof(Format));
            }
            else
            {
                if(Format != Image.Format)
                    throw new ArgumentException(nameof(Format));
            }
            if(Type.GetBaseType() != Image.Type)
                throw new ArgumentException(nameof(Format));
            if(LayerCount == 0)
                throw new ArgumentOutOfRangeException(nameof(LayerCount));
            if(LevelCount == 0)
                throw new ArgumentOutOfRangeException(nameof(LevelCount));
            if(Type.IsArrayType())
            {
                if(Type.IsCubeType() && LayerCount % 6 != 0)
                    throw new ArgumentException(nameof(LayerCount));
            }
            else
            {
                if(LayerCount != (Type.IsCubeType() ? 6 : 1))
                    throw new ArgumentException(nameof(LayerCount));
            }
            if(Type == ImageViewType.X3D && BaseArrayLayer != 0)
                throw new ArgumentException(nameof(BaseArrayLayer));
            if(!Image.Flags.HasFlag(ImageCreateFlags.CubeCompatible) && Type.IsCubeType())
                throw new ArgumentException(nameof(Type));
            if(!Image.Device.Features.ImageCubeArray && Type == ImageViewType.ArrayCube)
                throw new NotSupportedException(nameof(Type));
            const ImageUsageFlags requiredUsage = ImageUsageFlags.Sampled
                                                  | ImageUsageFlags.Storage | ImageUsageFlags.ColorAttachment
                                                  | ImageUsageFlags.DepthStencilAttachment | ImageUsageFlags.InputAttachment;
            if((Image.Usage & requiredUsage) == 0)
                throw new InvalidOperationException();
            var formatProperties = Image.Device.PhysicalDevice.FormatProperties(Format);
            var featureFlags = formatProperties.GetFeatures(Image.Tiling);
            if(featureFlags == FormatFeatureFlags.None)
                throw new NotSupportedException(nameof(Format));
            if(Image.Usage.HasFlag(ImageUsageFlags.Sampled) && !featureFlags.HasFlag(FormatFeatureFlags.SampledImage))
                throw new NotSupportedException(nameof(Format));
            if(Image.Usage.HasFlag(ImageUsageFlags.Storage) && !featureFlags.HasFlag(FormatFeatureFlags.StorageImage))
                throw new NotSupportedException(nameof(Format));
            if(Image.Usage.HasFlag(ImageUsageFlags.ColorAttachment) && !featureFlags.HasFlag(FormatFeatureFlags.ColorAttachment))
                throw new NotSupportedException(nameof(Format));
            if(Image.Usage.HasFlag(ImageUsageFlags.DepthStencilAttachment) && !featureFlags.HasFlag(FormatFeatureFlags.DepthStencilAttachment))
                throw new NotSupportedException(nameof(Format));
            if(BaseMipLevel >= Image.MipLevels)
                throw new ArgumentOutOfRangeException(nameof(BaseMipLevel));
            if(LevelCount != ImageBase.REMAINING && (BaseMipLevel + LevelCount) > Image.MipLevels)
                throw new ArgumentOutOfRangeException(nameof(LevelCount));
            if(LayerCount != ImageBase.REMAINING && (BaseArrayLayer + LayerCount) > Image.ArrayLayers)
                throw new ArgumentOutOfRangeException(nameof(LayerCount));
            //If image is non-sparse then it must be bound completely and contiguously to a single VkDeviceMemory object

            #endregion

            var range = new ImageSubresourceRange(Aspect, BaseMipLevel, LevelCount, BaseArrayLayer, LayerCount);
            var info = new ImageViewCreateInfo(Image, Type, Format, Swizzle, range);
            using(Image.WeakLock)
            {
                ImageViewHandle handle;
                using(Image.Device.Allocator?.WeakLock)
                    vkCreateImageView(Image.Device, &info, Image.Device.Allocator, out handle).Throw();
                Handle = handle;
                Image.Add(this);
            }
        }
        protected override void Destroy()
        {
            using(Image.WeakLock)
                Image.Remove(this);
            using(Image.Device.Allocator?.WeakLock)
                vkDestroyImageView(Image.Device, this, Image.Device.Allocator);
        }

        private readonly VkCreateImageView vkCreateImageView;
        private readonly VkDestroyImageView vkDestroyImageView;

        private delegate Result VkCreateImageView(DeviceHandle device, ImageViewCreateInfo* info, AllocationCallbacks* allocator, out ImageViewHandle view);
        private delegate void VkDestroyImageView(DeviceHandle device, ImageViewHandle image, AllocationCallbacks* allocator);
    }
}