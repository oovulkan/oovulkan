﻿using System;

namespace oovulkan.image.view
{
    public enum ImageViewType : int
    {
        X1D = 0,
        X2D = 1,
        X3D = 2,
        Cube = 3,
        Array1D = 4,
        Array2D = 5,
        ArrayCube = 6
    }

    public static class ImageViewTypeHelper
    {
        public static ImageType GetBaseType(this ImageViewType type)
        {
            switch(type)
            {
                case ImageViewType.X1D:
                case ImageViewType.Array1D:
                    return ImageType.X1D;
                case ImageViewType.X2D:
                case ImageViewType.Array2D:
                case ImageViewType.Cube:
                case ImageViewType.ArrayCube:
                    return ImageType.X2D;
                case ImageViewType.X3D:
                    return ImageType.X3D;
                default:
                    throw new ArgumentException(nameof(type));
            }
        }
        public static bool IsArrayType(this ImageViewType type)
        {
            switch(type)
            {
                case ImageViewType.X1D:
                case ImageViewType.X2D:
                case ImageViewType.Cube:
                case ImageViewType.X3D:
                    return false;
                case ImageViewType.Array1D:
                case ImageViewType.Array2D:
                case ImageViewType.ArrayCube:
                    return true;
                default:
                    throw new ArgumentException(nameof(type));
            }
        }
        public static bool IsCubeType(this ImageViewType type)
        {
            switch(type)
            {
                case ImageViewType.X1D:
                case ImageViewType.Array1D:
                case ImageViewType.X2D:
                case ImageViewType.Array2D:
                case ImageViewType.X3D:
                    return false;
                case ImageViewType.Cube:
                case ImageViewType.ArrayCube:
                    return true;
                default:
                    throw new ArgumentException(nameof(type));
            }
        }
    }
}