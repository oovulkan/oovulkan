﻿using System;

namespace oovulkan.image.view
{
    [Flags]
    public enum ImageAspectFlags : int
    {
        None = 0,
        Color = 1,
        Depth = 2,
        Stencil = 4,
        Metadata = 8
    }
}