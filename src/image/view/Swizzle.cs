﻿using System.Runtime.InteropServices;

namespace oovulkan.image.view
{
    public enum Comp : int
    {
        Identity = 0,
        Zero = 1,
        One = 2,
        R = 3,
        G = 4,
        B = 5,
        A = 6
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct Swizzle
    {
        public static readonly Swizzle Identity = default(Swizzle);

        public readonly Comp R;
        public readonly Comp G;
        public readonly Comp B;
        public readonly Comp A;

        private Swizzle(Comp r, Comp g, Comp b, Comp a)
        {
            R = r;
            G = g;
            B = b;
            A = a;
        }

        public override string ToString() => $"({R}, {G}, {B}, {A})";

        public static implicit operator Swizzle((Comp R, Comp G, Comp B, Comp A) swizzle) => new Swizzle(swizzle.R, swizzle.G, swizzle.B, swizzle.A);
        public static implicit operator (Comp R, Comp G, Comp B, Comp A)(Swizzle swizzle) => (swizzle.R, swizzle.G, swizzle.B, swizzle.A);

        public static bool operator ==(Swizzle s1, Swizzle s2) => s1.R == s2.R && s1.G == s2.G && s1.B == s2.B && s1.A == s2.A;
        public static bool operator !=(Swizzle s1, Swizzle s2) => !(s1 == s2);
        public bool Equals(Swizzle other) => this == other;
        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = (int)R;
                hashCode = (hashCode * 397) ^ (int)G;
                hashCode = (hashCode * 397) ^ (int)B;
                hashCode = (hashCode * 397) ^ (int)A;
                return hashCode;
            }
        }
        public override bool Equals(object obj) => obj is Swizzle && Equals((Swizzle)obj);

    }
}