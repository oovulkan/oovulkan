﻿using System.Runtime.InteropServices;
using oovulkan.image.view;

namespace oovulkan.image
{
    [StructLayout(LayoutKind.Sequential)]
    public struct ImageSubresourceRange
    {
        public readonly ImageAspectFlags Aspect;
        public readonly uint MipBase;
        public readonly uint MipCount;
        public readonly uint LayerBase;
        public readonly uint LayerCount;

        public ImageSubresourceRange(ImageAspectFlags aspect, uint mipBase=0, uint mipCount=ImageBase.REMAINING, uint layerBase=0, uint layerCount=ImageBase.REMAINING)
        {
            Aspect = aspect;
            MipBase = mipBase;
            MipCount = mipCount;
            LayerBase = layerBase;
            LayerCount = layerCount;
        }

        public static implicit operator ImageSubresourceRange(ImageAspectFlags aspect) => new ImageSubresourceRange(aspect);
        public static implicit operator ImageSubresourceRange((ImageAspectFlags Aspect, uint MipBase, uint MipCount, uint LayerBase, uint LayerCount) range)
            => new ImageSubresourceRange(range.Aspect, range.MipBase, range.MipCount, range.LayerBase, range.LayerCount);
    }
}