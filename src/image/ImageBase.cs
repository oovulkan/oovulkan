﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using oovulkan.device;
using oovulkan.image.view;
using oovulkan.memory;
using oovulkan.queue.family;
using oovulkan.util;
using oovulkan.util.collection;

#pragma warning disable 649

namespace oovulkan.image
{
    [StructLayout(LayoutKind.Sequential)]
    public struct ImageHandle
    {
        private long Value;

        public static implicit operator ImageHandle(long? value) => new ImageHandle {Value = value ?? 0};
        public static implicit operator long(ImageHandle handle) => handle.Value;
    }

    public abstract class ImageBase : VulkanObject<ImageHandle>
    {
        public const uint REMAINING = uint.MaxValue;

        protected internal override bool IsChild => true;
        protected internal override VulkanObject Parent => Device;
        protected internal override bool IsParent => true;
        protected internal override IEnumerable<VulkanObject> Children => views;

        public readonly Device Device;

        public abstract ImageCreateFlags Flags{ get; }
        public abstract Format Format{ get; }
        public abstract ImageType Type{ get; }
        protected internal abstract (uint Width, uint Height, uint Depth) Extent{ get; }
        public abstract uint MipLevels{ get; }
        public abstract uint ArrayLayers{ get; }
        public abstract ImageTiling Tiling{ get; }
        public abstract ImageUsageFlags Usage{ get; }
        protected internal abstract SharingMode SharingMode{ get; }
        protected internal abstract ReadonlyList<QueueFamily> Families{ get; }
        public abstract SampleCountFlags Samples{ get; }
        public abstract ImageLayout InitialLayout{ get; }
        public Memory BoundMemory{ get; private set; }
        public Word BoundOffset{ get; private set; }

        private MemoryRequirements memoryRequirements;
        private readonly HashSet<ImageView> views = new HashSet<ImageView>();

        public MemoryRequirements MemoryRequirements => memoryRequirements ?? (memoryRequirements = GetMemoryRequirements());
        public ReadonlySet<ImageView> Views => views;

        protected ImageBase(Device device)
        {
            Device = device;
        }

        internal void Add(ImageView view) { views.Add(view); }
        internal void Remove(ImageView view) { views.Remove(view); }

        public ImageView GetView(ImageViewType type, Format format, ImageAspectFlags aspect, Swizzle swizzle=default(Swizzle), uint baseMipLevel=0, uint levelCount=1, uint baseArrayLayer=0, uint layerCount=1) => new ImageView(this, type, format, swizzle, aspect, baseMipLevel, layerCount, baseArrayLayer, layerCount);

        private MemoryRequirements GetMemoryRequirements()
        {
            using(Lock)
            {
                vkGetImageMemoryRequirements(Device, this, out var requirements);
                return new MemoryRequirements(requirements, Device.PhysicalDevice.MemoryProperties.Types);
            }
        }

        public void Bind(Memory memory, Word offset=default(Word))
        {
            if(memory.Device != Device)
                throw new ArgumentException(nameof(memory));
            if(Flags.HasFlag(ImageCreateFlags.SparseAliased) || Flags.HasFlag(ImageCreateFlags.SparseBinding) || Flags.HasFlag(ImageCreateFlags.SparseResidency))
                throw new InvalidOperationException();
            if(offset >= memory.Size)
                throw new ArgumentOutOfRangeException();
            if(!MemoryRequirements.Types.Contains(memory.Type))
                throw new ArgumentException(nameof(memory));
            if(offset % MemoryRequirements.Alignment != 0)
                throw new ArgumentException(nameof(offset));
            if(offset + MemoryRequirements.Size > memory.Size)
                throw new ArgumentException(nameof(memory));

            using(Lock)
            using(memory.Lock)
            {
                if(BoundMemory != null)
                    throw new InvalidOperationException();
                vkBindImageMemory(Device, this, memory, offset).Throw();
                BoundMemory = memory;
                BoundOffset = offset;
            }
        }

        private readonly VkGetImageMemoryRequirements vkGetImageMemoryRequirements;
        private readonly VkBindImageMemory vkBindImageMemory;

        private delegate void VkGetImageMemoryRequirements(DeviceHandle device, ImageHandle image, out MemoryRequirements.Native requirements);
        private delegate Result VkBindImageMemory(DeviceHandle device, ImageHandle image, MemoryHandle memory, Word offset);
    }
}