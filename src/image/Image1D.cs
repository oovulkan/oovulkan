﻿using System.Collections.Generic;
using oovulkan.device;
using oovulkan.image.view;
using oovulkan.queue.family;
using oovulkan.util.collection;

namespace oovulkan.image
{
    public class Image1D : Image
    {
        public uint Size => Extent.Width;

        internal Image1D(Device device, ImageCreateFlags flags, Format format, ImageType type,
            (uint Width, uint Height, uint Depth) extent, uint mipLevels, uint arrayLayers, SampleCountFlags samples,
            ImageTiling tiling, ImageUsageFlags usage, ImageLayout initialLayout, SharingMode sharingMode,
            IEnumerable<QueueFamily> families)
            : base(device, flags, format, type, extent, mipLevels, arrayLayers, samples, tiling, usage, initialLayout, sharingMode, families) { }


        public ImageView GetView(Format? format=null, Swizzle swizzle=/*Identity*/default(Swizzle), ImageAspectFlags aspect=ImageAspectFlags.None, uint baseMipLevel=0, uint levelCount=1, uint baseArrayLayer=0)
        {
            return new ImageView(this, ImageViewType.X1D, format ?? Format, swizzle, aspect, baseMipLevel, levelCount, baseArrayLayer, 1);
        }
        public ImageView GetArrayView(Format? format=null, Swizzle swizzle=/*Identity*/default(Swizzle), ImageAspectFlags aspect=ImageAspectFlags.None, uint baseMipLevel=0, uint levelCount=1, uint baseArrayLayer=0, uint layerCount=1)
        {
            return new ImageView(this, ImageViewType.Array1D, format ?? Format, swizzle, aspect, baseMipLevel, levelCount, baseArrayLayer, layerCount);
        }
    }

    public class ConcurrentImage1D : Image1D
    {
        public new ReadonlyList<QueueFamily> Families => base.Families;

        internal ConcurrentImage1D(Device device, ImageCreateFlags flags, Format format, ImageType type,
            (uint Width, uint Height, uint Depth) extent, uint mipLevels, uint arrayLayers, SampleCountFlags samples,
            ImageTiling tiling, ImageUsageFlags usage, ImageLayout initialLayout, SharingMode sharingMode,
            IEnumerable<QueueFamily> families)
            : base(device, flags, format, type, extent, mipLevels, arrayLayers, samples, tiling, usage, initialLayout, sharingMode, families) { }
    }
}