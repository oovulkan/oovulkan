﻿using System;
using System.Runtime.InteropServices;
using oovulkan.queue.family;
using oovulkan.util;

namespace oovulkan.image
{
    [Flags]
    public enum ImageCreateFlags : int
    {
        None = 0,
        SparseBinding = 1,
        SparseResidency = 2,
        SparseAliased = 4,
        MutableFormat = 8,
        CubeCompatible = 16
    }

    [StructLayout(LayoutKind.Sequential)]
    internal unsafe struct ImageCreateInfo
    {
        private readonly StructureType Type;
        private readonly void* Next;
        public readonly ImageCreateFlags Flags;
        public readonly ImageType ImageType;
        public readonly Format Format;
        public readonly U3 Extent;
        public readonly uint MipLevels;
        public readonly uint ArrayLayers;
        public readonly SampleCountFlags Samples;
        public readonly ImageTiling Tiling;
        public readonly ImageUsageFlags Usage;
        public readonly SharingMode SharingMode;
        public readonly uint QueueFamilyCount;
        public readonly QueueFamilyHandle* QueueFamilyPtr;
        public readonly ImageLayout InitialLayout;

        public ImageCreateInfo(ImageCreateFlags flags, ImageType imageType, Format format, (uint Width, uint Height, uint Depth) extent, uint mipLevels, uint arrayLayers, SampleCountFlags samples, ImageTiling tiling, ImageUsageFlags usage, SharingMode sharingMode, uint queueFamilyCount, QueueFamilyHandle* queueFamilyPtr, ImageLayout initialLayout)
        {
            Type = StructureType.ImageCreateInfo;
            Next = null;
            Flags = flags;
            ImageType = imageType;
            Format = format;
            Extent = extent;
            MipLevels = mipLevels;
            ArrayLayers = arrayLayers;
            Samples = samples;
            Tiling = tiling;
            Usage = usage;
            SharingMode = sharingMode;
            QueueFamilyCount = queueFamilyCount;
            QueueFamilyPtr = queueFamilyPtr;
            InitialLayout = initialLayout;
        }
    }
}