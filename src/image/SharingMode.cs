﻿namespace oovulkan.image
{
    public enum SharingMode : int
    {
        Exclusive = 0,
        Concurrent = 1
    }
}