﻿namespace oovulkan.image
{
    public enum ImageType : int
    {
        X1D = 0,
        X2D = 1,
        X3D = 2
    }
}