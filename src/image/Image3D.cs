﻿using System.Collections.Generic;
using oovulkan.device;
using oovulkan.image.view;
using oovulkan.queue.family;
using oovulkan.util.collection;

namespace oovulkan.image
{
    public class Image3D : Image
    {
        public (uint With, uint Height, uint Depth) Size => Extent;

        internal Image3D(Device device, ImageCreateFlags flags, Format format, ImageType type,
            (uint Width, uint Height, uint Depth) extent, uint mipLevels, uint arrayLayers, SampleCountFlags samples,
            ImageTiling tiling, ImageUsageFlags usage, ImageLayout initialLayout, SharingMode sharingMode,
            IEnumerable<QueueFamily> families)
            : base(device, flags, format, type, extent, mipLevels, arrayLayers, samples, tiling, usage, initialLayout, sharingMode, families) { }

        public ImageView GetView(Format? format=null, Swizzle swizzle=/*Identity*/default(Swizzle), ImageAspectFlags aspect=ImageAspectFlags.None, uint baseMipLevel=0, uint levelCount=1)
        {
            return new ImageView(this, ImageViewType.X3D, format ?? Format, swizzle, aspect, baseMipLevel, levelCount, 0, 1);
        }
    }

    public class ConcurrentImage3D : Image3D
    {
        public new ReadonlyList<QueueFamily> Families => base.Families;

        internal ConcurrentImage3D(Device device, ImageCreateFlags flags, Format format, ImageType type,
            (uint Width, uint Height, uint Depth) extent, uint mipLevels, uint arrayLayers, SampleCountFlags samples,
            ImageTiling tiling, ImageUsageFlags usage, ImageLayout initialLayout, SharingMode sharingMode,
            IEnumerable<QueueFamily> families)
            : base(device, flags, format, type, extent, mipLevels, arrayLayers, samples, tiling, usage, initialLayout, sharingMode, families) { }
    }
}