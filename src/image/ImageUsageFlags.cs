﻿using System;

namespace oovulkan.image
{
    [Flags]
    public enum ImageUsageFlags : int
    {
        None = 0,
        TransferSrc = 1,
        TransferDst = 2,
        Sampled = 4,
        Storage = 8,
        ColorAttachment = 16,
        DepthStencilAttachment = 32,
        TransientAttachment = 64,
        InputAttachment = 128
    }
}