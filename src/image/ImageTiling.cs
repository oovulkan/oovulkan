﻿namespace oovulkan.image
{
    public enum ImageTiling : int
    {
        Optimal = 0,
        Linear = 1
    }
}