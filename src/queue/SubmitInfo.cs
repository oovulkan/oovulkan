﻿using System.Runtime.InteropServices;
using oovulkan.command;
using oovulkan.pipeline;
using oovulkan.synchronization.semaphore;

// ReSharper disable PrivateFieldCanBeConvertedToLocalVariable

namespace oovulkan.queue
{
    [StructLayout(LayoutKind.Sequential)]
    internal unsafe struct SubmitInfo
    {
        private readonly StructureType Type;
        private readonly void* Next;
        public readonly uint WaitCount;
        public readonly SemaphoreHandle* WaitSemaphorePtr;
        public readonly Stage* WaitStagePtr;
        public readonly uint BufferCount;
        public readonly CommandBufferHandle* BufferPtr;
        public readonly uint SignalCount;
        public readonly SemaphoreHandle* SignalPtr;

        public SubmitInfo(uint waitCount, SemaphoreHandle* waitSemaphorePtr, Stage* waitStagePtr, uint bufferCount, CommandBufferHandle* bufferPtr, uint signalCount, SemaphoreHandle* signalPtr)
        {
            Type = StructureType.SubmitInfo;
            Next = null;
            WaitCount = waitCount;
            WaitSemaphorePtr = waitSemaphorePtr;
            WaitStagePtr = waitStagePtr;
            BufferCount = bufferCount;
            BufferPtr = bufferPtr;
            SignalCount = signalCount;
            SignalPtr = signalPtr;
        }
    }
}