﻿using System.Runtime.InteropServices;
using oovulkan.util;

namespace oovulkan.queue.family
{
    public class QueueFamilyProperties
    {
        public readonly QueueFlags QueueFlags;
        public readonly uint QueueCount;
        public readonly uint TimestampValidBits;
        public readonly (uint, uint, uint) MinImageTransferGranularity;

        private QueueFamilyProperties(Native native)
        {
            QueueFlags = native.QueueFlags;
            //graphics or compute implies transfer
            if(QueueFlags.HasFlag(QueueFlags.Graphics) || QueueFlags.HasFlag(QueueFlags.Compute))
                QueueFlags |= QueueFlags.Transfer;
            QueueCount = native.QueueCount;
            TimestampValidBits = native.TimestampValidBits;
            MinImageTransferGranularity = native.MinImageTransferGranularity;
        }

        public static implicit operator QueueFamilyProperties(Native native) => new QueueFamilyProperties(native);

        [StructLayout(LayoutKind.Sequential)]
        public struct Native
        {
            public readonly QueueFlags QueueFlags;
            public readonly uint QueueCount;
            public readonly uint TimestampValidBits;
            public readonly U3 MinImageTransferGranularity;
        }
    }
}