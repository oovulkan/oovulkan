﻿using System;
using System.Runtime.InteropServices;
using oovulkan.device.physical;
using oovulkan.surface;
using oovulkan.surface.xcb;
using oovulkan.surface.xlib;
using util;

// ReSharper disable InconsistentNaming
#pragma warning disable 649

namespace oovulkan.queue.family
{
    [StructLayout(LayoutKind.Sequential)]
    public struct QueueFamilyHandle
    {
        public static readonly QueueFamilyHandle Ignored = uint.MaxValue;

        private uint Index;

        public static implicit operator QueueFamilyHandle(uint index) => new QueueFamilyHandle {Index = index};
        public static implicit operator uint(QueueFamilyHandle handle) => handle.Index;
    }

    public class QueueFamily : VulkanObject<QueueFamilyHandle>
    {
        public static readonly QueueFamily Ignored = new QueueFamily(QueueFamilyHandle.Ignored);

        protected internal override bool IsChild => true;
        protected internal override VulkanObject Parent => PhysicalDevice;

        public readonly PhysicalDevice PhysicalDevice;

        public readonly QueueFlags Flags;
        public readonly uint Length;
        public readonly uint TimestampValidBits;
        public readonly (uint Width, uint Height, uint Depth) MinImageTransferGranularity;

        private QueueFamily(QueueFamilyHandle handle)
        {
            Handle = handle;
        }

        internal QueueFamily(PhysicalDevice physicalDevice, QueueFamilyHandle handle, QueueFamilyProperties props)
        {
            PhysicalDevice = physicalDevice;
            this.Link(PhysicalDevice.Instance);
            Handle = handle;
            Flags = props.QueueFlags;
            Length = props.QueueCount;
            TimestampValidBits = props.TimestampValidBits;
            MinImageTransferGranularity = props.MinImageTransferGranularity;
        }

        public bool SupportsSurface(Surface surface)
        {
            vkGetPhysicalDeviceSurfaceSupportKHR(PhysicalDevice, this, surface, out var supported).Throw();
            return supported;
        }
        public bool SupportsXlib(XlibDisplayHandle display, XlibVisualHandle visual)
        {
            return vkGetPhysicalDeviceXlibPresentationSupportKHR(PhysicalDevice, this, display, visual);
        }
        public bool SupportsXcb(XcbConnectionHandle connection, XcbVisualHandle visual)
        {
            return vkGetPhysicalDeviceXcbPresentationSupportKHR(PhysicalDevice, this, connection, visual);
        }

        public override void Dispose() => throw new InvalidOperationException();
        protected override void Destroy() { }

        private readonly VkGetPhysicalDeviceSurfaceSupportKHR vkGetPhysicalDeviceSurfaceSupportKHR;
        private readonly VkGetPhysicalDeviceXlibPresentationSupportKHR vkGetPhysicalDeviceXlibPresentationSupportKHR;
        private readonly VkGetPhysicalDeviceXcbPresentationSupportKHR vkGetPhysicalDeviceXcbPresentationSupportKHR;
        private delegate Result VkGetPhysicalDeviceSurfaceSupportKHR(PhysicalDeviceHandle physicalDeviceHandle, QueueFamilyHandle family, SurfaceHandle surface, out Bool supported);
        private delegate Bool VkGetPhysicalDeviceXlibPresentationSupportKHR(PhysicalDeviceHandle physicalDeviceHandle, QueueFamilyHandle family, XlibDisplayHandle display, XlibVisualHandle visual);
        private delegate Bool VkGetPhysicalDeviceXcbPresentationSupportKHR(PhysicalDeviceHandle physicalDeviceHandle, QueueFamilyHandle family, XcbConnectionHandle connection, XcbVisualHandle visual);
    }
}