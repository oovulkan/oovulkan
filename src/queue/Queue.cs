﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using oovulkan.command;
using oovulkan.device;
using oovulkan.pipeline;
using oovulkan.queue.family;
using oovulkan.swapchain;
using oovulkan.synchronization.fence;
using oovulkan.synchronization.semaphore;
using oovulkan.util;
using Semaphore = oovulkan.synchronization.semaphore.Semaphore;

// ReSharper disable InconsistentNaming

#pragma warning disable 649

namespace oovulkan.queue
{
    [StructLayout(LayoutKind.Sequential)]
    public unsafe struct QueueHandle
    {
        private void* Ptr;

        public static implicit operator QueueHandle(void* ptr) => new QueueHandle {Ptr = ptr};
        public static implicit operator void*(QueueHandle handle) => handle.Ptr;
    }

    public unsafe class Queue : VulkanObject<QueueHandle>
    {
        protected internal override bool IsChild => true;
        protected internal override VulkanObject Parent => Device;

        public readonly Device Device;
        public readonly QueueFamily Family;
        public readonly float Weight;

        internal Queue(Device device, QueueHandle handle, QueueFamily family, float weight)
        {
            Device = device;
            this.Link(Device);
            Handle = handle;
            Family = family;
            Weight = weight;
        }
        public override void Dispose() => throw new InvalidOperationException();
        protected override void Destroy(){ }
        public void Wait()
        {
            using(Lock)
                vkQueueWaitIdle(this).Throw();
        }
        public Task WaitAsync() => Task.Run(() => Wait());

        public SubmissionBuilder Submit()
        {
            return new SubmissionBuilder(this, submits =>
            {
                var objects = submits.SelectMany(submit =>
                    CollectionUtil.All(
                        submit.Waits.Select(wait => wait.Item1),
                        submit.Buffers.SelectMany(buffer => buffer.Parts),
                        submit.Signals
                    )
                );

                var infos = stackalloc SubmitInfo[submits.Count];
                uint infoCount = 0;
                foreach(var submit in submits)
                {
                    var waitSemaphores = stackalloc SemaphoreHandle[submit.Waits.Count];
                    var waitStages = stackalloc Stage[submit.Waits.Count];
                    uint waitCount = 0;

                    var buffers = stackalloc CommandBufferHandle[submit.Buffers.Count];
                    uint bufferCount = 0;

                    var signals = stackalloc SemaphoreHandle[submit.Signals.Count];
                    uint signalCount = 0;

                    foreach(var (semaphore, stage) in submit.Waits)
                    {
                        waitSemaphores[waitCount] = semaphore;
                        waitStages[waitCount] = stage;
                        waitCount++;
                    }
                    foreach(var buffer in submit.Buffers)
                        buffers[bufferCount++] = buffer;
                    foreach(var semaphore in submit.Signals)
                        signals[signalCount++] = semaphore;

                    infos[infoCount++] = new SubmitInfo(waitCount, waitSemaphores, waitStages, bufferCount, buffers, signalCount, signals);
                }

                var fence = Device.FencePool.Get();

                IDisposable locks = objects.LockAll(obj => obj.WeakLock);
                try
                {
                    using(fence.Lock)
                    using(Lock)
                    {
                        if(IsDestroyed)
                            throw new ObjectDisposedException(GetType().FullName);
                        //TODO: Any calls to vkCmdSetEvent, vkCmdResetEvent or vkCmdWaitEvents that have been recorded into any of the command buffer elements of the pCommandBuffers member of any element of pSubmits, must not reference any VkEvent that is referenced by any of those commands in a command buffer that has been submitted to another queue and is still in the pending state.
                        foreach(var buffer in submits.SelectMany(submit => submit.Buffers))
                        {
                            if(buffer.State != CommandBufferState.Executable && buffer.State != CommandBufferState.Pending)
                                throw new InvalidOperationException();
                            if(buffer.State == CommandBufferState.Pending && !buffer.Usage.HasFlag(CommandBufferUsageFlags.SimultaneousUse))
                                throw new NotSupportedException();
                        }
                        vkQueueSubmit(this, (uint)submits.Count, infos, fence).Throw();
                        return Task.Run(() =>
                        {
                            fence.Wait();
                            Done();
                        });
                    }
                }
                catch(Exception)
                {
                    Done();
                    throw;
                }

                void Done()
                {
                    // ReSharper disable once AccessToModifiedClosure
                    Interlocked.Exchange(ref locks, null)?.Dispose();
                    Device.FencePool.Release(fence);
                }
            });
        }
        public PresentBuilder Present()
        {
            return new PresentBuilder(this, (semaphores, images) =>
            {
                var objects = CollectionUtil.All<VulkanObject>(semaphores, images);
                var waits = stackalloc SemaphoreHandle[semaphores.Count];
                uint waitCount = 0;
                var swapchains = stackalloc SwapchainHandle[images.Count];
                var indexes = stackalloc uint[images.Count];
                uint swapchainCount = 0;

                foreach(var semaphore in semaphores)
                    waits[waitCount++] = semaphore;
                foreach(var image in images)
                {
                    swapchains[swapchainCount] = image.Swapchain;
                    indexes[swapchainCount] = image.Index;
                    swapchainCount++;
                }

                var info = new PresentInfo(waitCount, waits, swapchainCount, swapchains, indexes, null);
                using(objects.LockAll(obj => obj.WeakLock))
                using(Lock)
                {
                    vkQueuePresentKHR(this, &info).Throw();
                    Wait();
                }
            });
        }

        private readonly VkQueueWaitIdle vkQueueWaitIdle;
        private readonly VkQueueSubmit vkQueueSubmit;
        private readonly VkQueuePresentKHR vkQueuePresentKHR;

        private delegate Result VkQueueWaitIdle(QueueHandle queue);
        private delegate Result VkQueueSubmit(QueueHandle queue, uint count, SubmitInfo* infos, FenceHandle fence);
        private delegate Result VkQueuePresentKHR(QueueHandle queue, PresentInfo* info);

        public class SubmissionBuilder : BuilderBase<Queue>
        {
            internal delegate Task DoneDelegate(List<(List<(Semaphore, Stage)> Waits, List<CommandBuffer> Buffers, List<Semaphore> Signals)> submits);

            private readonly DoneDelegate done;
            private readonly List<(List<(Semaphore, Stage)> Waits, List<CommandBuffer> Buffers, List<Semaphore> Signals)> submits = new List<(List<(Semaphore, Stage)> Waits, List<CommandBuffer> Buffers, List<Semaphore> Signals)>();

            internal SubmissionBuilder(Queue queue, DoneDelegate done) : base(queue)
            {
                this.done = done;
                Next();
            }

            public SubmissionBuilder Wait(Semaphore semaphore, Stage stage)
            {
                AssertNotDone();

                #region Validation
                if(semaphore.Device != Object.Device)
                    throw new ArgumentException(nameof(semaphore));
                if(stage == Stage.None)
                    throw new ArgumentException(nameof(stage));
                var features = Object.Device.Features;
                if(!features.GeometryShader && stage.HasFlag(Stage.GeometryShader))
                    throw new NotSupportedException(nameof(stage));
                if(!features.GeometryShader && (stage.HasFlag(Stage.TessellationControlShader) || stage.HasFlag(Stage.TessellationEvaluationShader)))
                    throw new NotSupportedException(nameof(stage));
                if(stage.HasFlag(Stage.Host))
                    throw new ArgumentException(nameof(stage));
                if(!Object.Family.Flags.Supports(stage))
                    throw new NotSupportedException(nameof(stage));
                #endregion

                submits[submits.Count - 1].Waits.Add((semaphore, stage));
                return this;
            }
            public SubmissionBuilder Buffer(params CommandBuffer[] buffers)
            {
                AssertNotDone();

                #region Validation
                if(buffers.Any(buffer => buffer.Pool.Device != Object.Device))
                    throw new ArgumentException(nameof(buffers));
                if(buffers.Any(buffer => !buffer.IsPrimary))
                    throw new ArgumentException(nameof(buffers));
                #endregion

                submits[submits.Count - 1].Buffers.AddRange(buffers);
                return this;
            }
            public SubmissionBuilder Signal(Semaphore semaphore)
            {
                AssertNotDone();

                #region Validation
                if(semaphore.Device != Object.Device)
                    throw new ArgumentException(nameof(semaphore));
                #endregion

                submits[submits.Count - 1].Signals.Add(semaphore);
                return this;
            }
            public SubmissionBuilder Next()
            {
                AssertNotDone();
                submits.Add((new List<(Semaphore, Stage)>(), new List<CommandBuffer>(), new List<Semaphore>()));
                return this;
            }
            public Task Done()
            {
                AssertNotDone();
                MarkDone();
                return done(submits);
            }
        }
        public class PresentBuilder : BuilderBase<Queue>
        {
            internal delegate void DoneDelegate(List<Semaphore> waits, List<SwapchainImage> images);

            private readonly DoneDelegate done;
            private readonly List<Semaphore> waits = new List<Semaphore>();
            private readonly List<SwapchainImage> images = new List<SwapchainImage>();

            internal PresentBuilder(Queue queue, DoneDelegate done) : base(queue)
            {
                this.done = done;
            }

            public PresentBuilder Wait(params Semaphore[] semaphores)
            {
                AssertNotDone();

                #region Validation
                if(semaphores.Any(semaphore => semaphore.Device != Object.Device))
                    throw new ArgumentException(nameof(semaphores));
                #endregion

                waits.AddRange(semaphores);
                return this;
            }
            public PresentBuilder Image(params SwapchainImage[] images)
            {
                AssertNotDone();

                #region Validation
                if(images.Any(image => image.Swapchain.Surface.Device != Object.Device))
                    throw new ArgumentException(nameof(images));
                #endregion

                this.images.AddRange(images);
                return this;
            }
            public void Done()
            {
                AssertNotDone();
                MarkDone();
                done(waits, images);
            }
        }
    }
}