﻿using System;
using System.Collections.Generic;
using oovulkan.pipeline;
using oovulkan.util;

namespace oovulkan.queue
{
    [Flags]
    public enum QueueFlags : int
    {
        None = 0,
        Graphics = 1,
        Compute = 2,
        Transfer = 4,
        SparseBinding = 8
    }

    public static class QueueFlagsHelper
    {
        private static readonly Dictionary<QueueFlags, Stage> SupportedStages = new Dictionary<QueueFlags, Stage>
        {
            {QueueFlags.None,
                Stage.TopOfPipe | Stage.BottomOfPipe | Stage.Host |
                Stage.AllCommands
            },
            {QueueFlags.Graphics,
                Stage.DrawIndirect | Stage.VertexInput | Stage.VertexShader |
                Stage.TessellationControlShader | Stage.TessellationEvaluationShader |
                Stage.GeometryShader | Stage.FragmentShader | Stage.EarlyFragmentTests |
                Stage.LateFragmentTests | Stage.ColorAttachmentOutput | Stage.Transfer |
                Stage.AllGraphics
            },
            {QueueFlags.Compute, Stage.DrawIndirect | Stage.ComputeShader | Stage.Transfer},
            {QueueFlags.Transfer, Stage.Transfer}
        };

        public static bool Supports(this QueueFlags flags, Stage desiredStages)
        {
            Stage stages = SupportedStages[QueueFlags.None];
            foreach(var flag in Util.GetFlags(flags))
                stages |= SupportedStages.GetOrDefault(flag);
            return (desiredStages & ~stages) == Stage.None;
        }
        public static bool Supports(this QueueFlags flags, PipelineBindPoint bind)
        {
            var flag = QueueFlags.None;
            switch(bind)
            {
                case PipelineBindPoint.Graphics:
                    flag = QueueFlags.Graphics;
                    break;
                case PipelineBindPoint.Compute:
                    flag = QueueFlags.Compute;
                    break;
            }
            return flags.HasFlag(flag);
        }
    }
}