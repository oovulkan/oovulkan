﻿using System;

namespace oovulkan
{
    [Flags]
    public enum SampleCountFlags : int
    {
        None = 0,
        Bit1 = 1,
        Bit2 = 2,
        Bit4 = 4,
        Bit8 = 8,
        Bit16 = 16,
        Bit32 = 32,
        Bit64 = 64
    }
}