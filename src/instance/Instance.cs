﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using oovulkan.application;
using oovulkan.device.physical;
using oovulkan.util;
using oovulkan.util.collection;

// ReSharper disable InconsistentNaming
#pragma warning disable 649

namespace oovulkan.instance
{
    [StructLayout(LayoutKind.Sequential)]
    public unsafe struct InstanceHandle
    {
        private void* Ptr;

        public static implicit operator InstanceHandle(void* ptr) => new InstanceHandle {Ptr = ptr};
        public static implicit operator void*(InstanceHandle handle) => handle.Ptr;
    }

    public unsafe class Instance : VulkanObject<InstanceHandle>
    {
        static Instance()
        {
            VulkanInterop.Link(typeof(Instance));
        }

        private static ReadonlyList<Layer> instanceLayers;
        public static ReadonlyList<Layer> InstanceLayers => instanceLayers ?? (instanceLayers = GetLayers());

        protected internal override bool IsChild => true;
        protected internal override VulkanObject Parent => Application;
        protected internal override bool IsParent => true;
        protected internal override IEnumerable<VulkanObject> Children => PhysicalDevices;

        public readonly Application Application;
        public readonly Allocator Allocator;
        public readonly ReadonlySet<string> Layers;
        public readonly ReadonlySet<string> Extensions;
        public readonly ReadonlyList<PhysicalDevice> PhysicalDevices;

        private Instance(Application application, Allocator allocator)
        {
            Application = application;
            Allocator = allocator;

        }

        //Building "constructor"
        // ReSharper disable once UnusedMember.Local
        private Instance(IEnumerable<string> layers, IEnumerable<string> extensions)
        {
            Layers = new ReadonlySet<string>(layers);
            Extensions = new ReadonlySet<string>(extensions);

            Create();
            this.Link(this);
            PhysicalDevices = GetPhysicalDevices();
        }

        private void Create()
        {
            var layers = stackalloc AnsiString[Layers.Count];
            var extensions = stackalloc AnsiString[Extensions.Count];
            uint layerCount = 0;
            foreach(var layer in Layers)
                layers[layerCount++] = (AnsiString)layer;
            uint extensionCount = 0;
            foreach(var extension in Extensions)
                extensions[extensionCount++] = (AnsiString)extension;

            try
            {
                using(Application.WeakLock)
                {
                    var info = new InstanceCreateInfo(Application, layerCount, layers, extensionCount, extensions);
                    InstanceHandle handle;
                    using(Allocator?.WeakLock)
                        vkCreateInstance(&info, Allocator, out handle).Throw();
                    Handle = handle;
                    Application.Add(this);
                }
            }
            catch(VulkanException ex)
            {
                if(ex.Result == Result.ErrorLayerNotPresent)
                    RefineLayerErrorMessage(ex, layerCount, layers);
                throw;
            }
            finally
            {
                for(uint i = 0; i < layerCount; i++)
                    layers[i].Dispose();
                for(uint i = 0; i < extensionCount; i++)
                    extensions[i].Dispose();
            }
        }
        private void RefineLayerErrorMessage(VulkanException ex, uint layerCount, AnsiString* layers)
        {
            var unsupported = new List<string>();
            for(uint i = 0; i < layerCount; i++)
            {
                AnsiString layer = layers[i];
                InstanceCreateInfo info = new InstanceCreateInfo(null, 1, &layer, 0, null);
                Result result = vkCreateInstance(&info, null, out InstanceHandle handle);
                if(result == Result.ErrorLayerNotPresent)
                    unsupported.Add(layer);
                else if(result.IsError())
                    result.Throw();
                else
                    vkDestroyInstance(handle, null);
            }
            ex.Message += "\n" + string.Join(", ", unsupported);
        }
        protected override void Destroy()
        {
            using(Application.WeakLock)
                Application.Remove(this);
            using(Allocator?.WeakLock)
                vkDestroyInstance(this, Allocator);
        }

        public static ReadonlyList<Layer> GetLayers()
        {
            uint count = 0;
            vkEnumerateInstanceLayerProperties(ref count, null).Throw();
            var ptr = stackalloc LayerProperties[(int)count];
            vkEnumerateInstanceLayerProperties(ref count, ptr).Throw();
            var layers = new Layer[count];
            for(uint i = 0; i < count; i++)
                layers[i] = new Layer(ptr[i]);
            return layers.ToList();
        }

        private ReadonlyList<PhysicalDevice> GetPhysicalDevices()
        {
            using(Lock)
            {
                uint count = 0;
                vkEnumeratePhysicalDevices(this, ref count, null).Throw();
                var handles = stackalloc PhysicalDeviceHandle[(int)count];
                vkEnumeratePhysicalDevices(this, ref count, handles);
                var devices = new List<PhysicalDevice>();
                for(int i = 0; i < count; i++)
                    devices.Add(new PhysicalDevice(this, handles[i]));
                return new ReadonlyList<PhysicalDevice>(devices);
            }
        }

        private static readonly VkCreateInstance vkCreateInstance;
        private readonly VkDestroyInstance vkDestroyInstance;
        private static readonly VkEnumerateInstanceLayerProperties vkEnumerateInstanceLayerProperties;
        private readonly VkEnumeratePhysicalDevices vkEnumeratePhysicalDevices;

        private delegate Result VkCreateInstance(InstanceCreateInfo* info, AllocationCallbacks* allocator, out InstanceHandle instance);
        private delegate void VkDestroyInstance(InstanceHandle instance, AllocationCallbacks* allocator);
        private delegate Result VkEnumerateInstanceLayerProperties(ref uint count, LayerProperties* props);
        private delegate Result VkEnumeratePhysicalDevices(InstanceHandle instance, ref uint count, PhysicalDeviceHandle* handles);

        public class Builder : BuilderBase<Instance>
        {
            private static readonly ConstructorInfo Sneaky = typeof(Instance).GetAnyConstructor(
                typeof(IEnumerable<string>),
                typeof(IEnumerable<string>)
            );
            private readonly HashSet<string> layers = new HashSet<string>();
            private readonly HashSet<string> extensions = new HashSet<string>();

            internal Builder(Application application, Allocator allocator) : base(new Instance(application, allocator)) { }

            public Builder Layers(params string[] layers)
            {
                return Layers((IEnumerable<string>)layers);
            }
            public Builder Layers(IEnumerable<string> layers)
            {
                AssertNotDone();
                this.layers.UnionWith(layers);
                return this;
            }

            public Builder Extensions(params string[] extensions)
            {
                return Extensions((IEnumerable<string>)extensions);
            }
            public Builder Extensions(IEnumerable<string> extensions)
            {
                AssertNotDone();
                this.extensions.UnionWith(extensions);
                return this;
            }

            public Instance Done()
            {
                AssertNotDone();
                MarkDone();
                Sneaky.Invoke(Object, layers, extensions);
                return Object;
            }
        }
    }
}