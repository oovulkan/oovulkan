﻿using System;
using System.Runtime.InteropServices;
using oovulkan.application;
using oovulkan.util;

// ReSharper disable PrivateFieldCanBeConvertedToLocalVariable

namespace oovulkan.instance
{
    [Flags]
    internal enum InstanceCreateFlags : int
    {
        None = 0
    }

    [StructLayout(LayoutKind.Sequential)]
    internal unsafe struct InstanceCreateInfo
    {
        private readonly StructureType Type;
        private readonly void* Next;
        private readonly InstanceCreateFlags Flags;
        public readonly ApplicationInfo* ApplicationInfo;
        public readonly uint LayerCount;
        public readonly AnsiString* LayerPtr;
        public readonly uint ExtensionCount;
        public readonly AnsiString* ExtensionPtr;

        public InstanceCreateInfo(Application application, uint layerCount, AnsiString* layerPtr, uint extensionCount, AnsiString* extensionPtr)
        {
            Type = StructureType.InstanceCreateInfo;
            Next = null;
            Flags = InstanceCreateFlags.None;
            ApplicationInfo = application;
            LayerCount = layerCount;
            LayerPtr = layerPtr;
            ExtensionCount = extensionCount;
            ExtensionPtr = extensionPtr;
        }
    }
}