﻿using System;

namespace oovulkan.synchronization
{
    [Flags]
    public enum DependencyFlags
    {
        None = 0,
        ByRegion = 1
    }
}