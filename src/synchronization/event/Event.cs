﻿using System;
using System.Runtime.InteropServices;
using oovulkan.device;

#pragma warning disable 649

namespace oovulkan.synchronization.@event
{
    [StructLayout(LayoutKind.Sequential)]
    public struct EventHandle
    {
        private long Value;

        public static implicit operator EventHandle(long? value) => new EventHandle {Value = value ?? 0};
        public static implicit operator long(EventHandle handle) => handle.Value;
    }

    public unsafe class Event : VulkanObject<EventHandle>
    {
        protected internal override bool IsChild => true;
        protected internal override VulkanObject Parent => Device;

        public readonly Device Device;

        public bool Status
        {
            get => GetStatus();
            set => SetStatus(value);
        }

        internal Event(Device device)
        {
            Device = device;
            this.Link(Device);
            Create();
        }

        private void Create()
        {
            EventCreateInfo info = EventCreateInfo.DEFAULT;
            using(Device.WeakLock)
            {
                EventHandle handle;
                using(Device.Allocator?.WeakLock)
                    vkCreateEvent(Device, &info, Device.Allocator, out handle).Throw();
                Handle = handle;
                Device.Add(this);
            }
        }
        protected override void Destroy()
        {
            using(Device.WeakLock)
                Device.Remove(this);
            using(Device.Allocator?.WeakLock)
                vkDestroyEvent(Device, this, Device.Allocator);
        }
        private bool GetStatus()
        {
            Result result;
            using(Lock)
                result = vkGetEventStatus(Device, this);
            switch(result)
            {
                case Result.EventSet:
                    return true;
                case Result.EventReset:
                    return false;
                default:
                    result.Throw();
                    throw new NotSupportedException();
            }
        }
        private void SetStatus(bool status)
        {
            using(Lock)
            {
                if(status)
                    vkSetEvent(Device, this).Throw();
                else
                    vkResetEvent(Device, this).Throw();
            }
        }

        private readonly VkCreateEvent vkCreateEvent;
        private readonly VkDestroyEvent vkDestroyEvent;
        private readonly VkGetEventStatus vkGetEventStatus;
        private readonly VkSetEvent vkSetEvent;
        private readonly VkResetEvent vkResetEvent;

        private delegate Result VkCreateEvent(DeviceHandle device, EventCreateInfo* info, AllocationCallbacks* allocator, out EventHandle @event);
        private delegate void VkDestroyEvent(DeviceHandle device, EventHandle @event, AllocationCallbacks* allocator);
        private delegate Result VkGetEventStatus(DeviceHandle device, EventHandle @event);
        private delegate Result VkSetEvent(DeviceHandle device, EventHandle @event);
        private delegate Result VkResetEvent(DeviceHandle device, EventHandle @event);
    }
}