﻿using System;
using System.Runtime.InteropServices;

// ReSharper disable PrivateFieldCanBeConvertedToLocalVariable

namespace oovulkan.synchronization.@event
{
    [Flags]
    internal enum EventCreateFlags : int
    {
        None = 0
    }

    [StructLayout(LayoutKind.Sequential)]
    internal unsafe struct EventCreateInfo
    {
        public static readonly EventCreateInfo DEFAULT = new EventCreateInfo(false);

        private readonly StructureType Type;
        private readonly void* Next;
        private readonly EventCreateFlags Flags;

        private EventCreateInfo(bool _)
        {
            Type = StructureType.EventCreateInfo;
            Next = null;
            Flags = EventCreateFlags.None;
        }
    }
}