﻿using System;
using System.Runtime.InteropServices;

// ReSharper disable PrivateFieldCanBeConvertedToLocalVariable

namespace oovulkan.synchronization.fence
{
    [Flags]
    internal enum FenceCreateFlags : int
    {
        None = 0,
        Signaled = 1
    }

    [StructLayout(LayoutKind.Sequential)]
    internal unsafe struct FenceCreateInfo
    {
        private readonly StructureType Type;
        private readonly void* Next;
        public readonly FenceCreateFlags Flags;

        public FenceCreateInfo(bool status)
        {
            Type = StructureType.FenceCreateInfo;
            Next = null;
            Flags = status ? FenceCreateFlags.Signaled : FenceCreateFlags.None;
        }
    }
}