﻿using System;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using oovulkan.device;
using util;

#pragma warning disable 649

namespace oovulkan.synchronization.fence
{
    [StructLayout(LayoutKind.Sequential)]
    public struct FenceHandle
    {
        private long Value;

        public static implicit operator FenceHandle(long? value) => new FenceHandle {Value = value ?? 0};
        public static implicit operator long(FenceHandle handle) => handle.Value;
    }

    public unsafe class Fence : VulkanObject<FenceHandle>
    {
        protected internal override bool IsChild => true;
        protected internal override VulkanObject Parent => Device;

        public readonly Device Device;

        public bool Status => GetStatus();

        internal Fence(Device device, bool status)
        {
            Device = device;
            this.Link(Device);
            Create(status);
        }

        private void Create(bool status)
        {
            FenceCreateInfo info = new FenceCreateInfo(status);
            using(Device.WeakLock)
            {
                FenceHandle handle;
                using(Device.Allocator?.WeakLock)
                    vkCreateFence(Device, &info, Device.Allocator, out handle).Throw();
                Handle = handle;
                Device.Add(this);
            }
        }
        protected override void Destroy()
        {
            using(Device.WeakLock)
                Device.Remove(this);
            using(Device.Allocator?.WeakLock)
                vkDestroyFence(Device, this, Device.Allocator);
        }
        private bool GetStatus()
        {
            Result result;
            using(Lock)
                result = vkGetFenceStatus(Device, this);
            switch(result)
            {
                case Result.Success:
                    return true;
                case Result.NotReady:
                    return false;
                default:
                    result.Throw();
                    throw new NotSupportedException();
            }
        }
        public void Wait(ulong timeout = ulong.MaxValue)
        {
            FenceHandle handle = this;
            using(Lock)
                vkWaitForFences(Device, 1, &handle, true, timeout).Throw();
        }
        public Task WaitAsync(ulong timeout = ulong.MaxValue) => Task.Run(() => Wait(timeout));
        public void Reset()
        {
            FenceHandle handle = this;
            using(StrongLock)
                vkResetFences(Device, 1, &handle).Throw();
        }

        private readonly VkCreateFence vkCreateFence;
        private readonly VkDestroyFence vkDestroyFence;
        private readonly VkWaitForFences vkWaitForFences;
        private readonly VkGetFenceStatus vkGetFenceStatus;
        private readonly VkResetFences vkResetFences;

        private delegate Result VkCreateFence(DeviceHandle device, FenceCreateInfo* info, AllocationCallbacks* allocator, out FenceHandle fence);
        private delegate void VkDestroyFence(DeviceHandle device, FenceHandle fence, AllocationCallbacks* allocator);
        private delegate Result VkWaitForFences(DeviceHandle device, uint count, FenceHandle* fences, Bool waitAll, ulong timeout);
        private delegate Result VkGetFenceStatus(DeviceHandle device, FenceHandle fence);
        private delegate Result VkResetFences(DeviceHandle device, uint count, FenceHandle* fences);
    }
}