﻿using System.Runtime.InteropServices;
using oovulkan.device;

#pragma warning disable 649

namespace oovulkan.synchronization.semaphore
{
    [StructLayout(LayoutKind.Sequential)]
    public struct SemaphoreHandle
    {
        private long Value;

        public static implicit operator SemaphoreHandle(long? value) => new SemaphoreHandle {Value = value ?? 0};
        public static implicit operator long(SemaphoreHandle handle) => handle.Value;
    }

    public unsafe class Semaphore : VulkanObject<SemaphoreHandle>
    {
        protected internal override bool IsChild => true;
        protected internal override VulkanObject Parent => Device;

        public readonly Device Device;

        internal Semaphore(Device device)
        {
            Device = device;
            this.Link(Device);
            Create();
        }

        private void Create()
        {
            SemaphoreCreateInfo info = SemaphoreCreateInfo.DEFAULT;
            using(Device.WeakLock)
            {
                SemaphoreHandle handle;
                using(Device.Allocator?.WeakLock)
                    vkCreateSemaphore(Device, &info, Device.Allocator, out handle).Throw();
                Handle = handle;
                Device.Add(this);
            }
        }
        protected override void Destroy()
        {
            using(Device.WeakLock)
                Device.Remove(this);
            using(Device.Allocator?.WeakLock)
                vkDestroySemaphore(Device, this, Device.Allocator);
        }

        private readonly VkCreateSemaphore vkCreateSemaphore;
        private readonly VkDestroySemaphore vkDestroySemaphore;

        private delegate Result VkCreateSemaphore(DeviceHandle device, SemaphoreCreateInfo* info, AllocationCallbacks* allocator, out SemaphoreHandle semaphore);
        private delegate void VkDestroySemaphore(DeviceHandle device, SemaphoreHandle semaphore, AllocationCallbacks* allocator);
    }
}