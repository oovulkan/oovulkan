﻿using System;
using System.Runtime.InteropServices;

// ReSharper disable PrivateFieldCanBeConvertedToLocalVariable

namespace oovulkan.synchronization.semaphore
{
    [Flags]
    internal enum SemaphoreCreateFlags : int
    {
        None = 0
    }

    [StructLayout(LayoutKind.Sequential)]
    internal unsafe struct SemaphoreCreateInfo
    {
        public static readonly SemaphoreCreateInfo DEFAULT = new SemaphoreCreateInfo(false);

        private readonly StructureType Type;
        private readonly void* Next;
        private readonly SemaphoreCreateFlags Flags;

        private SemaphoreCreateInfo(bool _)
        {
            Type = StructureType.SemaphoreCreateInfo;
            Next = null;
            Flags = SemaphoreCreateFlags.None;
        }
    }
}