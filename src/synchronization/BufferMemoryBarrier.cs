﻿using System.Runtime.InteropServices;
using oovulkan.buffer;
using oovulkan.queue.family;
using oovulkan.util;

// ReSharper disable PrivateFieldCanBeConvertedToLocalVariable

namespace oovulkan.synchronization
{
    [StructLayout(LayoutKind.Sequential)]
    public unsafe struct BufferMemoryBarrier
    {
        private readonly StructureType Type;
        private readonly void* Next;
        public readonly AccessFlags SrcAccess;
        public readonly AccessFlags DstAccess;
        public readonly QueueFamilyHandle SrcFamily;
        public readonly QueueFamilyHandle DstFamily;
        public readonly BufferHandle Buffer;
        public readonly Word Offset;
        public readonly Word Size;

        internal BufferMemoryBarrier(AccessFlags srcAccess, AccessFlags dstAccess, QueueFamilyHandle srcFamily, QueueFamilyHandle dstFamily, BufferHandle buffer, Word offset, Word size)
        {
            Type = StructureType.BufferMemoryBarrier;
            Next = null;
            SrcAccess = srcAccess;
            DstAccess = dstAccess;
            SrcFamily = srcFamily;
            DstFamily = dstFamily;
            Buffer = buffer;
            Offset = offset;
            Size = size;
        }
    }
}