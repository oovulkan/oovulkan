﻿using System.Runtime.InteropServices;

// ReSharper disable PrivateFieldCanBeConvertedToLocalVariable

namespace oovulkan.synchronization
{
    [StructLayout(LayoutKind.Sequential)]
    public unsafe struct MemoryBarrier
    {
        private readonly StructureType Type;
        private readonly void* Next;
        public readonly AccessFlags SrcAccess;
        public readonly AccessFlags DstAccess;

        internal MemoryBarrier(AccessFlags srcA, AccessFlags dstA)
        {
            Type = StructureType.MemoryBarrier;
            Next = null;
            SrcAccess = srcA;
            DstAccess = dstA;
        }
    }
}