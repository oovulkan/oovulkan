﻿using System.Runtime.InteropServices;
using oovulkan.image;
using oovulkan.queue.family;

// ReSharper disable PrivateFieldCanBeConvertedToLocalVariable

namespace oovulkan.synchronization
{
    [StructLayout(LayoutKind.Sequential)]
    public unsafe struct ImageMemoryBarrier
    {
        private readonly StructureType Type;
        private readonly void* Next;
        public readonly AccessFlags SrcAccess;
        public readonly AccessFlags DstAccess;
        public readonly ImageLayout SrcLayout;
        public readonly ImageLayout DstLayout;
        public readonly QueueFamilyHandle SrcFamily;
        public readonly QueueFamilyHandle DstFamily;
        public readonly ImageHandle Image;
        public readonly ImageSubresourceRange Range;

        internal ImageMemoryBarrier(AccessFlags srcAccess, AccessFlags dstAccess, ImageLayout srcLayout, ImageLayout dstLayout, QueueFamilyHandle srcFamily, QueueFamilyHandle dstFamily, ImageHandle image, ImageSubresourceRange range)
        {
            Type = StructureType.ImageMemoryBarrier;
            Next = null;
            SrcAccess = srcAccess;
            DstAccess = dstAccess;
            SrcLayout = srcLayout;
            DstLayout = dstLayout;
            SrcFamily = srcFamily;
            DstFamily = dstFamily;
            Image = image;
            Range = range;
        }
    }
}