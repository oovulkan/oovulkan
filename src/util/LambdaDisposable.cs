﻿using System;

namespace oovulkan.util
{
    public class LambdaDisposable : IDisposable
    {
        private readonly Action dispose;

        public LambdaDisposable(Action dispose)
        {
            this.dispose = dispose;
        }

        public void Dispose()
        {
            dispose();
        }
    }
}