﻿using System;
using System.Threading;

namespace oovulkan.util
{
    public class ThreeLock : IDisposable
    {
        private bool isDisposed;
        private readonly FastLock loc = new FastLock();

        private readonly object weakWait = new object();
        private int weakWaiting;

        private readonly object halfWait = new object();
        private int halfWaiting;

        private readonly object strongWait = new object();
        private int strongWaiting;

        private int ownerId = -1;
        private uint weakCount;
        private uint halfCount;
        private uint strongCount;

        public IDisposable WeakLock
        {
            get
            {
                WeakEnter();
                return new WeakLockDisposer {ThreeLock = this};
            }
        }
        public IDisposable Lock
        {
            get
            {
                HalfEnter();
                return new LockDisposer {ThreeLock = this};
            }
        }
        public IDisposable StrongLock
        {
            get
            {
                StrongEnter();
                return new StrongLockDisposer {ThreeLock = this};
            }
        }

        private void WeakEnter()
        {
            if(isDisposed)
                throw new ObjectDisposedException(GetType().FullName);
            loc.Enter();
            int id = Thread.CurrentThread.ManagedThreadId;
            //if there is a strong lock on another thread
            if(strongCount > 0 && ownerId != id)
            {
                weakWaiting++;
                do
                {
                    //wait on the weak wait object
                    lock(weakWait)
                    {
                        loc.Exit();
                        Monitor.Wait(weakWait);
                        loc.Enter();
                    }
                    if(isDisposed)
                        throw new ObjectDisposedException(GetType().FullName);
                    //while there is a strong lock on another thread
                } while(strongCount > 0 && ownerId != id);
                weakWaiting--;
            }
            //mark our use
            weakCount++;
            loc.Exit();
        }
        private void WeakExit()
        {
            if(isDisposed)
                return;
            loc.Enter();
            //remove the lock
            weakCount--;
            //if there is a strong waiting and there is nothing left blocking it, signal the strong
            if(strongWaiting > 0 && weakCount == 0 && halfCount == 0 && strongCount == 0)
            {
                lock(strongWait)
                    Monitor.Pulse(strongWait);
            }
            loc.Exit();
        }

        private void HalfEnter()
        {
            if(isDisposed)
                throw new ObjectDisposedException(GetType().FullName);
            loc.Enter();
            int id = Thread.CurrentThread.ManagedThreadId;
            //if there is a half/strong lock on another thread
            if(ownerId != -1 && ownerId != id)
            {
                halfWaiting++;
                do
                {
                    //wait on the half wait object
                    lock(halfWait)
                    {
                        loc.Exit();
                        Monitor.Wait(halfWait);
                        loc.Enter();
                    }
                    if(isDisposed)
                        throw new ObjectDisposedException(GetType().FullName);
                    //while there is a half/strong lock on another thread
                } while(ownerId != -1 && ownerId != id);
                halfWaiting--;
            }
            //mark our use
            ownerId = id;
            halfCount++;
            loc.Exit();
        }
        private void HalfExit()
        {
            if(isDisposed)
                return;
            loc.Enter();
            //remove the lock
            halfCount--;
            //if ownership is removed
            if(halfCount == 0 && strongCount == 0)
            {
                //clear ownership
                ownerId = -1;
                //if there is a strong waiting and there is nothing left blocking it
                if(strongWaiting > 0 && weakCount == 0)
                {
                    lock(strongWait)
                        Monitor.Pulse(strongWait);
                }
                //if there are halves waiting
                else if(halfWaiting > 0)
                {
                    lock(halfWait)
                        Monitor.Pulse(halfWait);
                }
            }
            loc.Exit();
        }

        private void StrongEnter()
        {
            if(isDisposed)
                throw new ObjectDisposedException(GetType().FullName);
            loc.Enter();
            int id = Thread.CurrentThread.ManagedThreadId;
            //if there is a half/strong lock on another thread
            if(ownerId != -1 && ownerId != id)
            {
                strongWaiting++;
                do
                {
                    //wait on the strong wait object
                    lock(strongWait)
                    {
                        loc.Exit();
                        Monitor.Wait(strongWait);
                        loc.Enter();
                    }
                    if(isDisposed)
                        throw new ObjectDisposedException(GetType().FullName);
                    //while there is a half/strong lock on another thread
                } while(ownerId != -1 && ownerId != id);
                strongWaiting--;
            }
            //mark our use
            ownerId = id;
            strongCount++;
            loc.Exit();
        }
        private void StrongExit()
        {
            if(isDisposed)
                return;
            loc.Enter();
            //remove the lock
            strongCount--;
            //if we're done with the strong lock, and there are weaks waiting, signal them
            if(strongCount == 0 && weakWaiting > 0)
            {
                lock(weakWait)
                    Monitor.PulseAll(weakWait);
            }
            //if ownership is removed
            if(halfCount == 0 && strongCount == 0)
            {
                //clear ownership
                ownerId = -1;
                //if there is a strong waiting and there is nothing left blocking it
                if(strongWaiting > 0 && weakCount == 0 && weakWaiting == 0)
                {
                    lock(strongWait)
                        Monitor.Pulse(strongWait);
                }
                //if there are halves waiting
                else if(halfWaiting > 0)
                {
                    lock(halfWait)
                        Monitor.Pulse(halfWait);
                }
            }
            loc.Exit();
        }

        public void Dispose()
        {
            loc.Enter();
            isDisposed = true;
            weakCount = 0;
            halfCount = 0;
            strongCount = 0;
            ownerId = -1;
            lock(weakWait)
                Monitor.PulseAll(weakWait);
            lock(halfWait)
                Monitor.PulseAll(halfWait);
            lock(strongWait)
                Monitor.PulseAll(strongWait);
            loc.Exit();
        }

        private struct WeakLockDisposer : IDisposable
        {
            public ThreeLock ThreeLock;
            public void Dispose() => ThreeLock.WeakExit();
        }
        private struct LockDisposer : IDisposable
        {
            public ThreeLock ThreeLock;
            public void Dispose() => ThreeLock.HalfExit();
        }
        private struct StrongLockDisposer : IDisposable
        {
            public ThreeLock ThreeLock;
            public void Dispose() => ThreeLock.StrongExit();
        }
    }
}