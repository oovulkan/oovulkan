﻿using System.Runtime.InteropServices;

namespace util
{
    [StructLayout(LayoutKind.Sequential)]
    public struct Bool
    {
        public static readonly Bool True = new Bool(1);
        public static readonly Bool False = new Bool(0);

        private readonly uint value;

        private Bool(uint value)
        {
            this.value = value;
        }

        public override string ToString()
        {
            return ((bool)this).ToString();
        }

        public static implicit operator Bool(bool value) => value ? True : False;
        public static implicit operator bool(Bool value) => value.value != 0;
    }
}