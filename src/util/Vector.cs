﻿using System.Runtime.InteropServices;

namespace oovulkan.util
{
    [StructLayout(LayoutKind.Sequential)]
    public struct I2
    {
        public readonly int a;
        public readonly int b;

        private I2(int a, int b)
        {
            this.a = a;
            this.b = b;
        }

        public static implicit operator I2((int, int) tuple) => new I2(tuple.Item1, tuple.Item2);
        public static implicit operator (int, int)(I2 tuple) => (tuple.a, tuple.b);
        public void Deconstruct(out int x, out int y)
        {
            x = a;
            y = b;
        }
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct I3
    {
        public readonly int a;
        public readonly int b;
        public readonly int c;

        private I3(int a, int b, int c)
        {
            this.a = a;
            this.b = b;
            this.c = c;
        }

        public static implicit operator I3((int, int, int) tuple) => new I3(tuple.Item1, tuple.Item2, tuple.Item3);
        public static implicit operator (int, int, int)(I3 tuple) => (tuple.a, tuple.b, tuple.c);
        public void Deconstruct(out int x, out int y, out int z)
        {
            x = a;
            y = b;
            z = c;
        }
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct I4
    {
        public readonly int a;
        public readonly int b;
        public readonly int c;
        public readonly int d;

        private I4(int a, int b, int c, int d)
        {
            this.a = a;
            this.b = b;
            this.c = c;
            this.d = d;
        }

        public static implicit operator I4((int, int, int, int) tuple) => new I4(tuple.Item1, tuple.Item2, tuple.Item3, tuple.Item4);
        public static implicit operator (int, int, int, int)(I4 tuple) => (tuple.a, tuple.b, tuple.c, tuple.d);
        public void Deconstruct(out int x, out int y, out int z, out int w)
        {
            x = a;
            y = b;
            z = c;
            w = d;
        }
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct U2
    {
        public readonly uint a;
        public readonly uint b;

        private U2(uint a, uint b)
        {
            this.a = a;
            this.b = b;
        }

        public static implicit operator U2((uint, uint) tuple) => new U2(tuple.Item1, tuple.Item2);
        public static implicit operator (uint, uint)(U2 tuple) => (tuple.a, tuple.b);
        public void Deconstruct(out uint x, out uint y)
        {
            x = a;
            y = b;
        }
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct U3
    {
        public readonly uint a;
        public readonly uint b;
        public readonly uint c;

        private U3(uint a, uint b, uint c)
        {
            this.a = a;
            this.b = b;
            this.c = c;
        }

        public static implicit operator U3((uint, uint, uint) tuple) => new U3(tuple.Item1, tuple.Item2, tuple.Item3);
        public static implicit operator (uint, uint, uint)(U3 tuple) => (tuple.a, tuple.b, tuple.c);
        public void Deconstruct(out uint x, out uint y, out uint z)
        {
            x = a;
            y = b;
            z = c;
        }
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct U4
    {
        public readonly uint a;
        public readonly uint b;
        public readonly uint c;
        public readonly uint d;

        private U4(uint a, uint b, uint c, uint d)
        {
            this.a = a;
            this.b = b;
            this.c = c;
            this.d = d;
        }

        public static implicit operator U4((uint, uint, uint, uint) tuple) => new U4(tuple.Item1, tuple.Item2, tuple.Item3, tuple.Item4);
        public static implicit operator (uint, uint, uint, uint)(U4 tuple) => (tuple.a, tuple.b, tuple.c, tuple.d);
        public void Deconstruct(out uint x, out uint y, out uint z, out uint w)
        {
            x = a;
            y = b;
            z = c;
            w = d;
        }
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct F2
    {
        public readonly float a;
        public readonly float b;

        private F2(float a, float b)
        {
            this.a = a;
            this.b = b;
        }

        public static implicit operator F2((float, float) tuple) => new F2(tuple.Item1, tuple.Item2);
        public static implicit operator (float, float)(F2 tuple) => (tuple.a, tuple.b);
        public void Deconstruct(out float x, out float y)
        {
            x = a;
            y = b;
        }
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct F3
    {
        public readonly float a;
        public readonly float b;
        public readonly float c;

        private F3(float a, float b, float c)
        {
            this.a = a;
            this.b = b;
            this.c = c;
        }

        public static implicit operator F3((float, float, float) tuple) => new F3(tuple.Item1, tuple.Item2, tuple.Item3);
        public static implicit operator (float, float, float)(F3 tuple) => (tuple.a, tuple.b, tuple.c);
        public void Deconstruct(out float x, out float y, out float z)
        {
            x = a;
            y = b;
            z = c;
        }
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct F4
    {
        public readonly float a;
        public readonly float b;
        public readonly float c;
        public readonly float d;

        private F4(float a, float b, float c, float d)
        {
            this.a = a;
            this.b = b;
            this.c = c;
            this.d = d;
        }

        public static implicit operator F4((float, float, float, float) tuple) => new F4(tuple.Item1, tuple.Item2, tuple.Item3, tuple.Item4);
        public static implicit operator (float, float, float, float)(F4 tuple) => (tuple.a, tuple.b, tuple.c, tuple.d);
        public void Deconstruct(out float x, out float y, out float z, out float w)
        {
            x = a;
            y = b;
            z = c;
            w = d;
        }
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct Rect2D
    {
        public readonly int X;
        public readonly int Y;
        public readonly uint Width;
        public readonly uint Height;

        private Rect2D(int x, int y, uint width, uint height)
        {
            X = x;
            Y = y;
            Width = width;
            Height = height;
        }

        public static implicit operator Rect2D((int X, int Y, uint W, uint H) rect) => new Rect2D(rect.X, rect.Y, rect.W, rect.H);
        public static implicit operator Rect2D(((int X, int Y), (uint W, uint H)) rect) => new Rect2D(rect.Item1.X, rect.Item1.Y, rect.Item2.W, rect.Item2.H);
        public static implicit operator Rect2D((uint W, uint H) size) => new Rect2D(0, 0, size.W, size.H);

        public static implicit operator (int X, int Y, uint W, uint H)(Rect2D rect) => (rect.X, rect.Y, rect.Width, rect.Height);
        public static implicit operator ((int X, int Y), (uint W, uint H))(Rect2D rect) => ((rect.X, rect.Y), (rect.Width, rect.Height));

        public void Deconstruct(out int x, out int y, out uint w, out uint h)
        {
            x = X;
            y = Y;
            w = Width;
            h = Height;
        }
        public void Deconstruct(out (int X, int Y) pos, out (uint W, uint H) size)
        {
            pos = (X, Y);
            size = (Width, Height);
        }
    }
}