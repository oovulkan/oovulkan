﻿using System;
using System.Runtime.CompilerServices;
using System.Threading;

namespace oovulkan.util
{
    public struct FastLock
    {
        private int loc;

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Enter()
        {
            if(Interlocked.CompareExchange(ref loc, 1, 0) == 0)
                return;
            EnterSpin();
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Exit()
        {
            Volatile.Write(ref loc, 0);
        }
        private void EnterSpin()
        {
            int count = 1;
            if(Environment.ProcessorCount > 1)
            {
                for(; count <= 10; count++)
                {
                    Thread.SpinWait(20 * count);
                    if(Interlocked.CompareExchange(ref loc, 1, 0) == 0)
                        return;
                }
            }
            for(; count <= 15; count++)
            {
                Thread.Sleep(1);
                if(Interlocked.CompareExchange(ref loc, 1, 0) == 0)
                    return;
            }
            while(true)
            {
                Thread.Sleep(0);
                if(Interlocked.CompareExchange(ref loc, 1, 0) == 0)
                    return;
            }
        }
    }
}