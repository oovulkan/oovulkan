﻿using System.Collections.Generic;
using System.Linq;

namespace oovulkan.util
{
    public static class StringHelper
    {
        public static IEnumerable<AnsiString> CastAString(this IEnumerable<string> strings) => strings.Select(str => (AnsiString)str);
        public static IEnumerable<string> CastString(this IEnumerable<AnsiString> strings) => strings.Select(str => (string)str);
        public static IEnumerable<UniString> CastUString(this IEnumerable<string> strings) => strings.Select(str => (UniString)str);
        public static IEnumerable<string> CastString(this IEnumerable<UniString> strings) => strings.Select(str => (string)str);
    }
}