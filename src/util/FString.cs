﻿using System;
using System.Runtime.InteropServices;
using System.Threading;

namespace oovulkan.util {
    [StructLayout(LayoutKind.Sequential)]
    public unsafe struct FString : IDisposable
    {
        private IntPtr ptr;
        public readonly int length;

        public IntPtr Ptr => ptr;
        public int Length => length;

        public bool IsNull => ptr == IntPtr.Zero;

        public FString(IntPtr ptr, int length)
        {
            this.ptr = ptr;
            this.length = length;
        }

        public override string ToString()
        {
            return this;
        }

        public void Dispose()
        {
            IntPtr ptr = Interlocked.Exchange(ref this.ptr, IntPtr.Zero);
            if(ptr == IntPtr.Zero)
                return;
            Marshal.FreeHGlobal(ptr);
        }

        public static implicit operator string(FString str)
        {
            return str.ptr == IntPtr.Zero ? null : Marshal.PtrToStringAnsi(str.ptr);
        }
        public static explicit operator FString(string str)
        {
            if(str == null)
                return new FString(IntPtr.Zero, 0);
            IntPtr ptr = Marshal.StringToHGlobalAnsi(str);
            int len = 0;
            var cptr = (byte*)ptr;
            while(*cptr++ != 0)
                len++;
            return new FString(ptr, len);
        }

        public static implicit operator (IntPtr Ptr, int Length)(FString str)
        {
            return (str.ptr, str.length);
        }
        public static implicit operator FString((IntPtr Ptr, int Length) str)
        {
            return new FString(str.Ptr, str.Length);
        }
    }
}