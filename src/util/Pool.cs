﻿using System;
using System.Collections.Concurrent;

namespace oovulkan.util
{
    public class Pool<T>
    {
        private readonly ConcurrentBag<T> objects;
        private readonly Func<T> factory;
        private readonly Func<T, bool> filter;
        private readonly Action<T> normalize;

        public Pool(Func<T> factory, Func<T, bool> filter=null, Action<T> normalize=null)
        {
            objects = new ConcurrentBag<T>();
            this.factory = factory ?? throw new ArgumentNullException(nameof(factory));
            this.filter = filter;
            this.normalize = normalize;
        }

        public T Get()
        {
            T value;
            do
            {
                value = objects.TryTake(out T item) ? item : factory();
            } while(filter != null && !filter(value));
            return value;
        }

        public void Release(T item)
        {
            normalize?.Invoke(item);
            objects.Add(item);
        }
    }
}