﻿using System;
using System.Collections.Generic;

namespace oovulkan.util.collection
{
    public class ReadonlyList<T> : ReadonlyCollection<T>, IList<T>, IReadOnlyList<T>
    {
        protected IList<T> List => (IList<T>)Collection;

        public ReadonlyList(IEnumerable<T> enumerable)
            : base(enumerable as IList<T> ?? new List<T>(enumerable)) { }

        public ReadonlyList(out IList<T> list)
            : base(list = new List<T>()) { }

        public T this[int index] => List[index];

        T IList<T>.this[int index]
        {
            get => List[index];
            set => throw new NotSupportedException();
        }

        public int IndexOf(T item) => List.IndexOf(item);

        void IList<T>.Insert(int index, T item) => throw new NotSupportedException();
        void IList<T>.RemoveAt(int index) => throw new NotSupportedException();

        public static implicit operator ReadonlyList<T>(List<T> list) => list == null ? null : new ReadonlyList<T>(list);
    }
}