﻿using System;
using System.Collections.Generic;

namespace oovulkan.util.collection
{
    public class ReadonlySet<T> : ReadonlyCollection<T>, ISet<T>
    {
        protected ISet<T> Set => (ISet<T>)Collection;

        public ReadonlySet(IEnumerable<T> enumerable)
            : base(enumerable as ISet<T> ?? new HashSet<T>(enumerable)) { }

        public ReadonlySet(out ISet<T> set)
            : base(set = new HashSet<T>()) { }

        public bool IsProperSubsetOf(IEnumerable<T> other) => Set.IsProperSubsetOf(other);
        public bool IsProperSupersetOf(IEnumerable<T> other) => Set.IsProperSupersetOf(other);
        public bool IsSubsetOf(IEnumerable<T> other) => Set.IsSubsetOf(other);
        public bool IsSupersetOf(IEnumerable<T> other) => Set.IsSupersetOf(other);
        public bool Overlaps(IEnumerable<T> other) => Set.Overlaps(other);
        public bool SetEquals(IEnumerable<T> other) => Set.SetEquals(other);

        void ISet<T>.ExceptWith(IEnumerable<T> other) => throw new NotSupportedException();
        void ISet<T>.IntersectWith(IEnumerable<T> other) => throw new NotSupportedException();
        void ISet<T>.SymmetricExceptWith(IEnumerable<T> other) => throw new NotSupportedException();
        void ISet<T>.UnionWith(IEnumerable<T> other) => throw new NotSupportedException();
        bool ISet<T>.Add(T item) => throw new NotSupportedException();

        public static implicit operator ReadonlySet<T>(HashSet<T> set) => set == null ? null : new ReadonlySet<T>(set);
    }
}