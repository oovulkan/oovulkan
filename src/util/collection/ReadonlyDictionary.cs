﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace oovulkan.util.collection
{
    public class ReadonlyDictionary<K, V> : ReadonlyCollection<KeyValuePair<K, V>>, IDictionary<K, V>, IReadOnlyDictionary<K, V>
    {
        protected IDictionary<K, V> Dictionary => (IDictionary<K, V>)Collection;

        public ICollection<K> Keys => Dictionary.Keys;
        IEnumerable<K> IReadOnlyDictionary<K, V>.Keys => Keys;
        public ICollection<V> Values => Dictionary.Values;
        IEnumerable<V> IReadOnlyDictionary<K, V>.Values => Values;

        public ReadonlyDictionary(IEnumerable<KeyValuePair<K, V>> enumerable)
            : base(enumerable as IDictionary<K, V> ?? enumerable.ToDictionary(entry => entry.Key, entry => entry.Value)) { }

        public ReadonlyDictionary(out IDictionary<K, V> dictionary)
            : base(dictionary = new Dictionary<K, V>()) { }

        public V this[K key] => Dictionary[key];
        V IDictionary<K, V>.this[K key]
        {
            get => Dictionary[key];
            set => throw new NotSupportedException();
        }

        public bool ContainsKey(K key) => Dictionary.ContainsKey(key);
        public bool TryGetValue(K key, out V value) => Dictionary.TryGetValue(key, out value);

        public void Add(K key, V value) => throw new NotSupportedException();
        public bool Remove(K key) => throw new NotSupportedException();

        public static implicit operator ReadonlyDictionary<K, V>(Dictionary<K, V> dictionary) => dictionary == null ? null : new ReadonlyDictionary<K, V>(dictionary);
    }
}