﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace oovulkan.util.collection
{
    public class ReadonlyCollection<T> : ICollection<T>
    {
        protected readonly ICollection<T> Collection;

        public int Count => Collection.Count;
        public bool IsReadOnly => true;

        public ReadonlyCollection(IEnumerable<T> enumerable)
        {
            Collection = enumerable as ICollection<T> ?? new List<T>(enumerable);
        }
        public ReadonlyCollection(out ICollection<T> collection)
        {
            Collection = collection = new List<T>();
        }

        public bool Contains(T item) => Collection.Contains(item);
        public void CopyTo(T[] array, int arrayIndex) => Collection.CopyTo(array, arrayIndex);

        public IEnumerator<T> GetEnumerator() => Collection.GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        void ICollection<T>.Add(T item) => throw new NotSupportedException();
        void ICollection<T>.Clear() => throw new NotSupportedException();
        bool ICollection<T>.Remove(T item) => throw new NotSupportedException();
    }
}