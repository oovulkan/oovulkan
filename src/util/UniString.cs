﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;

namespace oovulkan.util {
    [StructLayout(LayoutKind.Sequential)]
    public unsafe struct UniString : IDisposable
    {
        private IntPtr ptr;

        public bool IsNull => ptr == IntPtr.Zero;

        public UniString(IntPtr ptr)
        {
            this.ptr = ptr;
        }

        public override string ToString()
        {
            return this;
        }

        public void Dispose()
        {
            IntPtr ptr = Interlocked.Exchange(ref this.ptr, IntPtr.Zero);
            if(ptr == IntPtr.Zero)
                return;
            Marshal.FreeHGlobal(ptr);
        }

        public static implicit operator string(UniString str)
        {
            return str.ptr == IntPtr.Zero ? null : Marshal.PtrToStringUni(str.ptr);
        }
        public static explicit operator UniString(string str)
        {
            return new UniString(str == null ? IntPtr.Zero : Marshal.StringToHGlobalUni(str));
        }

        public static implicit operator IntPtr(UniString str)
        {
            return str.ptr;
        }
        public static implicit operator UniString(IntPtr str)
        {
            return new UniString(str);
        }
        public static implicit operator void*(UniString str)
        {
            return (void*)str.ptr;
        }
        public static implicit operator UniString(void* str)
        {
            return new UniString((IntPtr)str);
        }
    }

    public static class UniStringHelper
    {
        public static IEnumerable<UniString> CastAString(this IEnumerable<string> strings) => strings.Select(str => (UniString)str);
        public static IEnumerable<string> CastString(this IEnumerable<UniString> strings) => strings.Select(str => (string)str);
    }
}