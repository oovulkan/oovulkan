﻿using System;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;

namespace oovulkan.util
{
    public static class ReflectionUtil
    {
        public static FieldInfo GetAnyField(this Type type, string name)
        {
            return type.GetField(name, BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
        }

        public static PropertyInfo GetAnyProperty(this Type type, string name)
        {
            return type.GetProperty(name, BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
        }

        public static MethodInfo GetAnyMethod(this Type type, string name)
        {
            return type.GetMethod(name, BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
        }

        public static MethodInfo GetAnyMethod(this Type type, string name, params Type[] args)
        {
            return type.GetMethod(name, BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic, null, args, null);
        }

        public static ConstructorInfo GetAnyConstructor(this Type type, params Type[] args)
        {
            return type.GetConstructor(BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic, null, args, null);
        }

        public static MethodInfo GetCast(this Type inType, Type outType, bool? isExplicit=null)
        {
            const BindingFlags flags = BindingFlags.Static | BindingFlags.Public;
            var infos = inType.GetMethods(flags).Concat(outType.GetMethods(flags));
            infos = infos.Where(info => info.IsSpecialName);
            if(isExplicit != null)
            {
                if(isExplicit.Value)
                    infos = infos.Where(info => info.Name == "op_Explicit");
                else
                    infos = infos.Where(info => info.Name == "op_Implicit");
            }
            foreach(var info in infos)
            {
                if(info.ReturnType == outType && info.GetParameters()[0].ParameterType == inType)
                    return info;
            }
            throw new InvalidCastException();
        }

        public static object Invoke(this MethodInfo info, object obj, params object[] args)
        {
            return info.Invoke(obj, args);
        }
        public static object InvokeStatic(this MethodInfo info, params object[] args)
        {
            return info.Invoke(null, args);
        }
        public static object Invoke(this ConstructorInfo info, object obj, params object[] args)
        {
            return info.Invoke(obj, args);
        }
        public static object InvokeStatic(this ConstructorInfo info, params object[] args)
        {
            return info.Invoke(args);
        }

        public static object Get(this FieldInfo info, object obj) => info.GetValue(obj);
        public static object GetStatic(this FieldInfo info) => info.GetValue(null);
        public static void Set(this FieldInfo info, object obj, object value) => info.SetValue(obj, value);
        public static void SetStatic(this FieldInfo info, object value) => info.SetValue(null, value);

        public static object Get(this PropertyInfo info, object obj) => info.GetValue(obj);
        public static object GetStatic(this PropertyInfo info) => info.GetValue(null);
        public static void Set(this PropertyInfo info, object obj, object value) => info.SetValue(obj, value);
        public static void SetStatic(this PropertyInfo info, object value) => info.SetValue(null, value);

        public static void EmitInt(this ILGenerator gen, int value)
        {
            switch(value) {
                case 0:
                    gen.Emit(OpCodes.Ldc_I4_0);
                    break;
                case 1:
                    gen.Emit(OpCodes.Ldc_I4_1);
                    break;
                case 2:
                    gen.Emit(OpCodes.Ldc_I4_2);
                    break;
                case 3:
                    gen.Emit(OpCodes.Ldc_I4_3);
                    break;
                case 4:
                    gen.Emit(OpCodes.Ldc_I4_4);
                    break;
                case 5:
                    gen.Emit(OpCodes.Ldc_I4_5);
                    break;
                case 6:
                    gen.Emit(OpCodes.Ldc_I4_6);
                    break;
                case 7:
                    gen.Emit(OpCodes.Ldc_I4_7);
                    break;
                case 8:
                    gen.Emit(OpCodes.Ldc_I4_8);
                    break;
                default:
                    if(value < 256)
                        gen.Emit(OpCodes.Ldc_I4_S, (byte)value);
                    else
                        gen.Emit(OpCodes.Ldc_I4, value);
                    break;
            }
        }
    }
}