﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Runtime.ConstrainedExecution;
using System.Runtime.InteropServices;

namespace oovulkan.util
{
    public static unsafe class Util
    {
        public struct ManagedPtr : IDisposable
        {
            public static readonly ManagedPtr Null = new ManagedPtr(null);
            private readonly GCHandle handle;
            public readonly IntPtr Ptr;

            public ManagedPtr(object obj)
            {
                if(obj != null)
                {
                    handle = GCHandle.Alloc(obj, GCHandleType.Pinned);
                    Ptr = handle.AddrOfPinnedObject();
                }
                else
                {
                    handle = default;
                    Ptr = IntPtr.Zero;
                }
            }

            public void Dispose()
            {
                if(handle.IsAllocated)
                    handle.Free();
            }

            public static implicit operator IntPtr(ManagedPtr ptr) => ptr.Ptr;
            public static implicit operator void*(ManagedPtr ptr) => (void*)ptr.Ptr;
        }

        public static ManagedPtr Fix(this object obj)
        {
            return obj == null ? ManagedPtr.Null : new ManagedPtr(obj);
        }

        public static ManagedPtr FixList<T>(this List<T> list)
        {
            if(list == null || list.Count == 0)
                return ManagedPtr.Null;
            // ReSharper disable once PossibleNullReferenceException
            var items = typeof(List<T>).GetField("_items", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(list);
            return items.Fix();
        }

        private static readonly uint[] MultiplyDeBruijnBitPosition = {
            0, 9, 1, 10, 13, 21, 2, 29, 11, 14, 16, 18, 22, 25, 3, 30,
            8, 12, 20, 28, 15, 17, 24, 7, 19, 27, 23, 6, 26, 5, 4, 31
        };

        public static uint Log2(uint x)
        {
            x |= x >> 1;
            x |= x >> 2;
            x |= x >> 4;
            x |= x >> 8;
            x |= x >> 16;
            return MultiplyDeBruijnBitPosition[(x * 0x07C4ACDDu) >> 27];
        }

        public static IEnumerable<T> GetValues<T>()
        {
            return (T[])Enum.GetValues(typeof(T));
        }

        public static IEnumerable<T> GetFlags<T>(T flags)
        {
            return GetValues<T>().Where(flag =>
                !Equals(flag, default(T))
                // ReSharper disable once PossibleNullReferenceException
                // ReSharper disable once AssignNullToNotNullAttribute
                && (flags as Enum).HasFlag(flag as Enum)
            );
        }

        public static bool IsPow2(sbyte x) => (x & (x - 1)) != 0;
        public static bool IsPow2(byte x) => (x & (x - 1)) != 0;
        public static bool IsPow2(short x) => (x & (x - 1)) != 0;
        public static bool IsPow2(ushort x) => (x & (x - 1)) != 0;
        public static bool IsPow2(int x) => (x & (x - 1)) != 0;
        public static bool IsPow2(uint x) => (x & (x - 1)) != 0;
        public static bool IsPow2(long x) => (x & (x - 1)) != 0;
        public static bool IsPow2(ulong x) => (x & (x - 1)) != 0;

        public static void ReadTo(this Stream stream, byte[] bytes, int index=0, int length=-1)
        {
            if(stream == null)
                throw new NullReferenceException();
            if(length < 0)
                length = bytes.Length - index;
            do
            {
                int count = stream.Read(bytes, index, length);
                if(count == 0)
                    return;
                index += count;
                length -= count;
            } while(length > 0);
        }
    }
}