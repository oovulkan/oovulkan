﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;

namespace oovulkan.util {
    [StructLayout(LayoutKind.Sequential)]
    public unsafe struct AnsiString : IDisposable
    {
        private IntPtr ptr;

        public bool IsNull => ptr == IntPtr.Zero;

        public AnsiString(IntPtr ptr)
        {
            this.ptr = ptr;
        }

        public override string ToString()
        {
            return this;
        }

        public void Dispose()
        {
            IntPtr ptr = Interlocked.Exchange(ref this.ptr, IntPtr.Zero);
            if(ptr == IntPtr.Zero)
                return;
            Marshal.FreeHGlobal(ptr);
        }

        public static implicit operator string(AnsiString str)
        {
            return str.ptr == IntPtr.Zero ? null : Marshal.PtrToStringAnsi(str.ptr);
        }
        public static explicit operator AnsiString(string str)
        {
            return new AnsiString(str == null ? IntPtr.Zero : Marshal.StringToHGlobalAnsi(str));
        }

        public static implicit operator IntPtr(AnsiString str)
        {
            return str.ptr;
        }
        public static implicit operator AnsiString(IntPtr str)
        {
            return new AnsiString(str);
        }
        public static implicit operator void*(AnsiString str)
        {
            return (void*)str.ptr;
        }
        public static implicit operator AnsiString(void* str)
        {
            return new AnsiString((IntPtr)str);
        }
    }

    public static class AnsiStringHelper
    {
        public static IEnumerable<AnsiString> CastAString(this IEnumerable<string> strings) => strings.Select(str => (AnsiString)str);
        public static IEnumerable<string> CastString(this IEnumerable<AnsiString> strings) => strings.Select(str => (string)str);
    }
}