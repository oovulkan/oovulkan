﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.ConstrainedExecution;

namespace oovulkan.util
{
    public static class CollectionUtil
    {
        public static HashSet<T> ToSet<T>(this IEnumerable<T> enumerable)
        {
            return new HashSet<T>(enumerable);
        }

        public static IEnumerable<(K, V)> ToTuples<K, V>(this IDictionary<K, V> dictionary)
        {
            return dictionary.Select(pair => (pair.Key, pair.Value));
        }

        public static Dictionary<K, V> ToDictionary<K, V>(this IEnumerable<(K, V)> entries)
        {
            return entries.ToDictionary(x => x.Item1, x => x.Item2);
        }

        public static V GetOrDefault<K, V>(this IDictionary<K, V> dict, K key, V @default = default)
        {
            return dict.TryGetValue(key, out V value) ? value : @default;
        }
        public static V GetOrDefault<K, V>(this IDictionary<K, V> dict, K key, Func<K, V> factory)
        {
            return dict.TryGetValue(key, out V value) ? value : factory(key);
        }

        private static readonly Func<Array, int, Array, int, int, bool> ArrayFastCopy =
            (Func<Array, int, Array, int, int, bool>)Delegate.CreateDelegate(
                typeof(Func<Array, int, Array, int, int, bool>),
                // ReSharper disable once AssignNullToNotNullAttribute
                typeof(Array).GetMethod("FastCopy", BindingFlags.NonPublic | BindingFlags.Static)
            );

        [ReliabilityContract(Consistency.WillNotCorruptState, Cer.MayFail)]
        public static void Resize<T>(ref T[] array, long newSize)
        {
            if (newSize < 0)
                throw new ArgumentOutOfRangeException(nameof(newSize));
            if (array == null)
                array = new T[newSize];
            else
            {
                if (array.LongLength == newSize)
                    return;
                var newArray = new T[newSize];
                long length = Math.Min(newSize, array.Length);

                if(length < 9 || length > int.MaxValue)
                {
                    for(long index = 0; index < length; index++)
                        newArray[index] = array[index];
                }
                else
                {
                    ArrayFastCopy(array, 0, newArray, 0, (int)length);
                }
                array = newArray;
            }
        }

        public static bool IsEmpty<T>(this IEnumerable<T> enumerable)
            => !enumerable.Any();

        public static IEnumerable<T> All<T>(params IEnumerable<T>[] enumerables)
            => enumerables.SelectMany(enumerable => enumerable);

        public static int GetHashCode<T>(IEnumerable<T> e)
        {
            if(e == null)
                return 0;
            unchecked
            {
                int hashCode = 397;
                foreach(var value in e)
                    hashCode = (hashCode * 397) ^ (value?.GetHashCode() ?? 0);
                return hashCode;
            }
        }

        public static bool Equals<T>(IEnumerable<T> e1, IEnumerable<T> e2)
        {
            if(e1 == null && e2 == null)
                return true;
            if(e1 == null || e2 == null)
                return false;
            using(var iter1 = e1.GetEnumerator())
            using(var iter2 = e2.GetEnumerator())
            {
                while(true)
                {
                    var more1 = iter1.MoveNext();
                    var more2 = iter2.MoveNext();
                    if(!more1 && !more2)
                        return true;
                    if(!more1 || !more2)
                        return false;
                    if(!Equals(iter1.Current, iter2.Current))
                        return false;
                }
            }
        }

        public static void Deconstruct<K, V>(this KeyValuePair<K, V> pair, out K key, out V value)
        {
            key = pair.Key;
            value = pair.Value;
        }
    }
}