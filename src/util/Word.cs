﻿using System;
using System.Runtime.InteropServices;

namespace oovulkan.util
{
    [StructLayout(LayoutKind.Sequential)]
    public struct Word
    {
        private IntPtr ptr;

        public override string ToString()
        {
            return ((long)ptr).ToString();
        }

        public static implicit operator long(Word w) => (long)w.ptr;
        public static implicit operator ulong(Word w) => (ulong)w.ptr;
        public static implicit operator int(Word w) => (int)w.ptr;
        public static implicit operator uint(Word w) => (uint)w.ptr;
        public static implicit operator short(Word w) => (short)w.ptr;
        public static implicit operator ushort(Word w) => (ushort)w.ptr;
        public static implicit operator sbyte(Word w) => (sbyte)w.ptr;
        public static implicit operator byte(Word w) => (byte)w.ptr;

        public static implicit operator Word(long l) => new Word {ptr = (IntPtr)l};
        public static implicit operator Word(ulong l) => new Word {ptr = (IntPtr)l};
        public static implicit operator Word(int l) => new Word {ptr = (IntPtr)l};
        public static implicit operator Word(uint l) => new Word {ptr = (IntPtr)l};
        public static implicit operator Word(short l) => new Word {ptr = (IntPtr)l};
        public static implicit operator Word(ushort l) => new Word {ptr = (IntPtr)l};
        public static implicit operator Word(sbyte l) => new Word {ptr = (IntPtr)l};
        public static implicit operator Word(byte l) => new Word {ptr = (IntPtr)l};
    }
}