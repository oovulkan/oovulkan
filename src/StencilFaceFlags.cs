﻿using System;

namespace oovulkan
{
    [Flags]
    public enum StencilFaceFlags : int
    {
        None = 0,
        Front = 1,
        Back = 2,
        FrontBack = 3
    }
}