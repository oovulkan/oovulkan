﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Threading;
using oovulkan.util;

namespace oovulkan
{
    [AttributeUsage(AttributeTargets.Interface)]
    public class DataInterfaceAttribute : Attribute
    {
        public readonly bool IsExplicitLayout;

        public DataInterfaceAttribute(bool isExplicitLayout=false)
        {
            IsExplicitLayout = isExplicitLayout;
        }
    }

    [AttributeUsage(AttributeTargets.Property)]
    public class DataAttribute : Attribute
    {
        public readonly uint Offset;
        public readonly int Line;

        public DataAttribute(uint offset=uint.MaxValue, [CallerLineNumber]int line=0)
        {
            Offset = offset;
            Line = line;
        }

        protected DataAttribute(uint offset, int line, bool _)
        {
            Offset = offset;
            Line = line;
        }
    }

    public abstract class DataInterface : IDisposable
    {
        private static readonly ModuleBuilder Module;
        private static readonly FieldInfo PtrInfo = typeof(DataInterface).GetAnyField("Ptr");
        private static readonly MethodInfo PtrCastInfo = typeof(IntPtr).GetCast(typeof(void*));
        private static readonly MethodInfo DirtyInfo = typeof(DataInterface).GetAnyMethod("Dirty");
        static DataInterface()
        {
            AssemblyName name = new AssemblyName {Name = "OovulkanDataInterface"};
            AssemblyBuilder assembly = AppDomain.CurrentDomain.DefineDynamicAssembly(name, AssemblyBuilderAccess.Run);
            Module = assembly.DefineDynamicModule(name.Name);
        }

        internal static void Validate(Type type, Type baseType, Type classAttr, Type propAttr)
        {
            if(!typeof(DataInterface).IsAssignableFrom(baseType))
                throw new ArgumentException();
            if(!typeof(DataInterfaceAttribute).IsAssignableFrom(classAttr))
                throw new ArgumentException();
            if(!typeof(DataAttribute).IsAssignableFrom(propAttr))
                throw new ArgumentException();
            if(!type.IsInterface)
                throw new ArgumentException();
            if(type.GetMethods(BindingFlags.Public | BindingFlags.Instance).Count(method => !method.IsSpecialName) != 0)
                throw new ArgumentException();
            var typeAttr = (DataInterfaceAttribute)type.GetCustomAttribute(classAttr);
            if(typeAttr == null)
                throw new ArgumentException($"{type.Name} must have {classAttr.FullName}.");
            foreach(var info in type.GetProperties(BindingFlags.Public | BindingFlags.Instance))
            {
                var attr = (DataAttribute)info.GetCustomAttribute(propAttr);
                if(attr == null)
                    throw new ArgumentException($"{type.Name}.{info.Name} must have {propAttr.FullName}.");
                if(attr.Offset == -1)
                    throw new ArgumentException($"{type.Name}.{info.Name} must have an explicit offset.");
            }
        }
        internal static IEnumerable<(PropertyInfo Info, uint Offset, uint Size)> GetProperties(Type type, Type classAttr, Type propAttr)
        {
            var typeAttr = (DataInterfaceAttribute)type.GetCustomAttribute(classAttr);
            uint offset = 0;
            foreach(var info in type.GetProperties(BindingFlags.Public | BindingFlags.Instance).OrderBy(info => ((DataAttribute)info.GetCustomAttribute(propAttr)).Line))
            {
                var attr = (DataAttribute)info.GetCustomAttribute(propAttr);
                var size = (uint)Marshal.SizeOf(info.PropertyType);
                if(typeAttr.IsExplicitLayout)
                {
                    yield return (info, attr.Offset, size);
                }
                else
                {
                    yield return (info, offset, size);
                    offset += size;
                }
            }
        }

        internal static T Implement<T>(Type baseType, Type classAttr, Type propAttr)
        {
            Validate(typeof(T), baseType, classAttr, propAttr);
            var properties = GetProperties(typeof(T), classAttr, propAttr);
            var (outer, inner) = Create<T>(baseType);
            var ptr = CreateNativePtr(outer, inner);
            CreateProperties(outer, inner, ptr, properties);
            CreateConstructor(outer, inner, baseType);
            return Finish<T>(outer, inner);
        }
        private static (TypeBuilder Outer, TypeBuilder Inner) Create<T>(Type baseType)
        {
            var outer = Module.DefineType($"oovulkan.data.interface.{typeof(T).Name}Interface", TypeAttributes.Public, baseType, new []{ typeof(T) });
            var inner = outer.DefineNestedType("Native", TypeAttributes.NestedPublic | TypeAttributes.ExplicitLayout, typeof(ValueType));
            return (outer, inner);
        }
        private static MethodBuilder CreateNativePtr(TypeBuilder outer, TypeBuilder inner)
        {
            var ptrType = inner.MakePointerType();
            var ptr = outer.DefineProperty("NativePtr", PropertyAttributes.None, ptrType, null);
            var ptrGetter = outer.DefineMethod("get_NativePtr", MethodAttributes.Private | MethodAttributes.SpecialName | MethodAttributes.HideBySig, ptrType, Type.EmptyTypes);
            var ptrGetterGen = ptrGetter.GetILGenerator();
            ptrGetterGen.Emit(OpCodes.Ldarg_0);
            ptrGetterGen.Emit(OpCodes.Ldfld, PtrInfo);
            ptrGetterGen.Emit(OpCodes.Call, PtrCastInfo);
            ptrGetterGen.Emit(OpCodes.Ret);
            ptr.SetGetMethod(ptrGetter);
            return ptrGetter;
        }
        private static void CreateProperties(TypeBuilder outer, TypeBuilder inner, MethodBuilder ptr, IEnumerable<(PropertyInfo Info, uint Offset, uint Size)> properties)
        {
            foreach(var (property, offset, size) in properties)
            {
                var field = inner.DefineField(property.Name.ToLower(), property.PropertyType, FieldAttributes.Public | FieldAttributes.HasDefault);
                field.SetOffset((int)offset);

                var prop = outer.DefineProperty(property.Name, PropertyAttributes.HasDefault, property.PropertyType, Type.EmptyTypes);

                const MethodAttributes attrs = MethodAttributes.Public | MethodAttributes.SpecialName | MethodAttributes.HideBySig | MethodAttributes.Virtual;
                var getter = outer.DefineMethod($"get_{prop.Name}", attrs, prop.PropertyType, Type.EmptyTypes);
                var getterGen = getter.GetILGenerator();
                getterGen.Emit(OpCodes.Ldarg_0);
                getterGen.Emit(OpCodes.Call, ptr);
                getterGen.Emit(OpCodes.Ldfld, field);
                getterGen.Emit(OpCodes.Ret);

                var setter = outer.DefineMethod($"set_{prop.Name}", attrs, null, new[] {prop.PropertyType});
                var setterGen = setter.GetILGenerator();
                setterGen.Emit(OpCodes.Ldarg_0);
                setterGen.Emit(OpCodes.Call, ptr);
                setterGen.Emit(OpCodes.Ldarg_1);
                setterGen.Emit(OpCodes.Stfld, field);
                setterGen.Emit(OpCodes.Ldarg_0);
                setterGen.EmitInt((int)offset);
                setterGen.EmitInt((int)size);
                setterGen.Emit(OpCodes.Call, DirtyInfo);
                setterGen.Emit(OpCodes.Ret);

                prop.SetGetMethod(getter);
                prop.SetSetMethod(setter);

                outer.DefineMethodOverride(getter, property.GetMethod);
                outer.DefineMethodOverride(setter, property.SetMethod);
            }
        }
        private static void CreateConstructor(TypeBuilder outer, TypeBuilder inner, Type baseType)
        {
            var con = outer.DefineConstructor(MethodAttributes.Public, CallingConventions.HasThis, Type.EmptyTypes);
            var conGen = con.GetILGenerator();
            conGen.Emit(OpCodes.Ldarg_0);
            conGen.Emit(OpCodes.Sizeof, inner);
            conGen.Emit(OpCodes.Call, baseType.GetAnyConstructor(typeof(int)));
            conGen.Emit(OpCodes.Ret);
        }
        private static T Finish<T>(TypeBuilder outer, TypeBuilder inner)
        {
            inner.CreateType();
            var type = outer.CreateType();
            var constructor = type.GetConstructor(Type.EmptyTypes);

            return (T)constructor.InvokeStatic();
        }

        protected internal IntPtr Ptr;
        protected internal readonly int Size;
        private DirtyNode dirtyRoot;

        protected DataInterface(int size)
        {
            Size = size;
            Ptr = Marshal.AllocHGlobal(size);
        }

        ~DataInterface() => Dispose();

        public void Dispose()
        {
            IntPtr ptr = Interlocked.Exchange(ref Ptr, IntPtr.Zero);
            if(ptr != IntPtr.Zero)
                Marshal.FreeHGlobal(ptr);
        }

        internal IEnumerable<(int Offset, int Size)> DirtyRegions
        {
            get
            {
                if(dirtyRoot != null)
                {
                    var node = dirtyRoot;
                    while(node != null)
                    {
                        var next = node.Next;
                        yield return (node.Value, next.Value - node.Value);
                        node = next.Next;
                    }
                    dirtyRoot = null;
                }
            }
        }

        protected void Dirty(int offset, int size)
        {
            //find our position and dirty flag
            bool dirty = false;
            DirtyNode prev = null;
            DirtyNode next = dirtyRoot;
            while(next != null && next.Value <= offset)
            {
                dirty = !dirty;
                prev = next;
                next = next.Next;
            }

            bool dirty2 = dirty;
            while(next != null && next.Value < offset + size)
            {
                dirty2 = !dirty2;
                next = next.Next;
            }

            if(prev != null)
            {
                if(!dirty)
                    prev = prev.Next = new DirtyNode {Value = offset, Next = prev.Next};
            }
            else
            {
                prev = dirtyRoot = new DirtyNode {Value = offset, Next = dirtyRoot};
            }

            if(next != null)
            {
                if(!dirty2)
                    next = new DirtyNode {Value = offset + size, Next = next};
            }
            else
            {
                next = new DirtyNode {Value = offset + size, Next = null};
            }

            prev.Next = next;
        }

        private class DirtyNode
        {
            public int Value;
            public DirtyNode Next;
        }
    }
}