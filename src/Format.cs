﻿// ReSharper disable InconsistentNaming

using System;

namespace oovulkan
{
    public enum Format : int
    {
        Undefined = 0,
        R4G4UNormPack8 = 1,
        R4G4B4A4UNormPack16 = 2,
        B4G4R4A4UNormPack16 = 3,
        R5G6B5UNormPack16 = 4,
        B5G6R5UNormPack16 = 5,
        R5G5B5A1UNormPack16 = 6,
        B5G5R5A1UNormPack16 = 7,
        A1R5G5B5UNormPack16 = 8,
        R8UNorm = 9,
        R8SNorm = 10,
        R8UScaled = 11,
        R8SScaled = 12,
        R8UInt = 13,
        R8SInt = 14,
        R8SRGB = 15,
        R8G8UNorm = 16,
        R8G8SNorm = 17,
        R8G8UScaled = 18,
        R8G8SScaled = 19,
        R8G8UInt = 20,
        R8G8SInt = 21,
        R8G8SRGB = 22,
        R8G8B8UNorm = 23,
        R8G8B8SNorm = 24,
        R8G8B8UScaled = 25,
        R8G8B8SScaled = 26,
        R8G8B8UInt = 27,
        R8G8B8SInt = 28,
        R8G8B8SRGB = 29,
        B8G8R8UNorm = 30,
        B8G8R8SNorm = 31,
        B8G8R8UScaled = 32,
        B8G8R8SScaled = 33,
        B8G8R8UInt = 34,
        B8G8R8SInt = 35,
        B8G8R8SRGB = 36,
        R8G8B8A8UNorm = 37,
        R8G8B8A8SNorm = 38,
        R8G8B8A8UScaled = 39,
        R8G8B8A8SScaled = 40,
        R8G8B8A8UInt = 41,
        R8G8B8A8SInt = 42,
        R8G8B8A8SRGB = 43,
        B8G8R8A8UNorm = 44,
        B8G8R8A8SNorm = 45,
        B8G8R8A8UScaled = 46,
        B8G8R8A8SScaled = 47,
        B8G8R8A8UInt = 48,
        B8G8R8A8SInt = 49,
        B8G8R8A8SRGB = 50,
        A8B8G8R8UNormPack32 = 51,
        A8B8G8R8SNormPack32 = 52,
        A8B8G8R8UScaledPack32 = 53,
        A8B8G8R8SScaledPack32 = 54,
        A8B8G8R8UIntPack32 = 55,
        A8B8G8R8SIntPack32 = 56,
        A8B8G8R8SRGBPack32 = 57,
        A2R10G10B10UNormPack32 = 58,
        A2R10G10B10SNormPack32 = 59,
        A2R10G10B10UScaledPack32 = 60,
        A2R10G10B10SScaledPack32 = 61,
        A2R10G10B10UIntPack32 = 62,
        A2R10G10B10SIntPack32 = 63,
        A2B10G10R10UNormPack32 = 64,
        A2B10G10R10SNormPack32 = 65,
        A2B10G10R10UScaledPack32 = 66,
        A2B10G10R10SScaledPack32 = 67,
        A2B10G10R10UIntPack32 = 68,
        A2B10G10R10SIntPack32 = 69,
        R16UNorm = 70,
        R16SNorm = 71,
        R16UScaled = 72,
        R16SScaled = 73,
        R16UInt = 74,
        R16SInt = 75,
        R16SFloat = 76,
        R16G16UNorm = 77,
        R16G16SNorm = 78,
        R16G16UScaled = 79,
        R16G16SScaled = 80,
        R16G16UInt = 81,
        R16G16SInt = 82,
        R16G16SFloat = 83,
        R16G16B16UNorm = 84,
        R16G16B16SNorm = 85,
        R16G16B16UScaled = 86,
        R16G16B16SScaled = 87,
        R16G16B16UInt = 88,
        R16G16B16SInt = 89,
        R16G16B16SFloat = 90,
        R16G16B16A16UNorm = 91,
        R16G16B16A16SNorm = 92,
        R16G16B16A16UScaled = 93,
        R16G16B16A16SScaled = 94,
        R16G16B16A16UInt = 95,
        R16G16B16A16SInt = 96,
        R16G16B16A16SFloat = 97,
        R32UInt = 98,
        R32SInt = 99,
        R32SFloat = 100,
        R32G32UInt = 101,
        R32G32SInt = 102,
        R32G32SFloat = 103,
        R32G32B32UInt = 104,
        R32G32B32SInt = 105,
        R32G32B32SFloat = 106,
        R32G32B32A32UInt = 107,
        R32G32B32A32SInt = 108,
        R32G32B32A32SFloat = 109,
        R64UInt = 110,
        R64SInt = 111,
        R64SFloat = 112,
        R64G64UInt = 113,
        R64G64SInt = 114,
        R64G64SFloat = 115,
        R64G64B64UInt = 116,
        R64G64B64SInt = 117,
        R64G64B64SFloat = 118,
        R64G64B64A64UInt = 119,
        R64G64B64A64SInt = 120,
        R64G64B64A64SFloat = 121,
        B10G11R11UFloatPack32 = 122,
        E5B9G9R9UFloatPack32 = 123,
        D16UNorm = 124,
        X8D24UNormPack32 = 125,
        D32SFloat = 126,
        S8UInt = 127,
        D16UNormS8UInt = 128,
        D24UNormS8UInt = 129,
        D32SFloatS8UInt = 130,
        BC1RGBUNormBlock = 131,
        BC1RGBSRGBBlock = 132,
        BC1RGBAUNormBlock = 133,
        BC1RGBASRGBBlock = 134,
        BC2UNormBlock = 135,
        BC2SRGBBlock = 136,
        BC3UNormBlock = 137,
        BC3SRGBBlock = 138,
        BC4UNormBlock = 139,
        BC4SNormBlock = 140,
        BC5UNormBlock = 141,
        BC5SNormBlock = 142,
        BC6HUFloatBlock = 143,
        BC6HSFloatBlock = 144,
        BC7UNormBlock = 145,
        BC7SRGBBlock = 146,
        ETC2R8G8B8UNormBlock = 147,
        ETC2R8G8B8SRGBBlock = 148,
        ETC2R8G8B8A1UNormBlock = 149,
        ETC2R8G8B8A1SRGBBlock = 150,
        ETC2R8G8B8A8UNormBlock = 151,
        ETC2R8G8B8A8SRGBBlock = 152,
        EACR11UNormBlock = 153,
        EACR11SNormBlock = 154,
        EACR11G11UNormBlock = 155,
        EACR11G11SNormBlock = 156,
        ASTC4x4UNormBlock = 157,
        ASTC4x4SRGBBlock = 158,
        ASTC5x4UNormBlock = 159,
        ASTC5x4SRGBBlock = 160,
        ASTC5x5UNormBlock = 161,
        ASTC5x5SRGBBlock = 162,
        ASTC6x5UNormBlock = 163,
        ASTC6x5SRGBBlock = 164,
        ASTC6x6UNormBlock = 165,
        ASTC6x6SRGBBlock = 166,
        ASTC8x5UNormBlock = 167,
        ASTC8x5SRGBBlock = 168,
        ASTC8x6UNormBlock = 169,
        ASTC8x6SRGBBlock = 170,
        ASTC8x8UNormBlock = 171,
        ASTC8x8SRGBBlock = 172,
        ASTC10x5UNormBlock = 173,
        ASTC10x5SRGBBlock = 174,
        ASTC10x6UNormBlock = 175,
        ASTC10x6SRGBBlock = 176,
        ASTC10x8UNormBlock = 177,
        ASTC10x8SRGBBlock = 178,
        ASTC10x10UNormBlock = 179,
        ASTC10x10SRGBBlock = 180,
        ASTC12x10UNormBlock = 181,
        ASTC12x10SRGBBlock = 182,
        ASTC12x12UNormBlock = 183,
        ASTC12x12SRGBBlock = 184,
        PVRTC12BPPUNormBlockImg = 1000054000,
        PVRTC14BPPUNormBlockImg = 1000054001,
        PVRTC22BPPUNormBlockImg = 1000054002,
        PVRTC24BPPUNormBlockImg = 1000054003,
        PVRTC12BPPSRGBBlockImg = 1000054004,
        PVRTC14BPPSRGBBlockImg = 1000054005,
        PVRTC22BPPSRGBBlockImg = 1000054006,
        PVRTC24BPPSRGBBlockImg = 1000054007
    }
    public enum FormatClass
    {
        Other,
        X8,
        X16,
        X24,
        X32,
        X48,
        X64,
        X96,
        X128,
        X192,
        X256,
        BC1RGB,
        BC1RGBA,
        BC2,
        BC3,
        BC4,
        BC5,
        BC6H,
        BC7,
        ETC2RGB,
        ETC2RGBA,
        ETC2EACRGBA,
        EACR,
        EACRG,
        ASTC4x4,
        ASTC5x4,
        ASTC5x5,
        ASTC6x5,
        ASTC6x6,
        ASTC8x5,
        ASTC8x6,
        ASTC8x8,
        ASTC10x5,
        ASTC10x6,
        ASTC10x8,
        ASTC10x10,
        ASTC12x10,
        ASTC12x12,
        D16,
        D24,
        D32,
        S8,
        D16S8,
        D24S8,
        D32S8
    }

    public static class FormatHelper
    {
        public static int GetElementSize(this FormatClass @class)
        {
            switch(@class)
            {
                case FormatClass.X8:
                case FormatClass.S8:
                    return 8;
                case FormatClass.X16:
                case FormatClass.D16:
                    return 16;
                case FormatClass.X24:
                case FormatClass.D16S8:
                    return 24;
                case FormatClass.X32:
                case FormatClass.D24:
                case FormatClass.D32:
                case FormatClass.D24S8:
                    return 32;
                case FormatClass.X48:
                    return 48;
                case FormatClass.X64:
                case FormatClass.BC1RGB:
                case FormatClass.BC1RGBA:
                case FormatClass.BC4:
                case FormatClass.ETC2RGB:
                case FormatClass.ETC2RGBA:
                case FormatClass.EACR:
                case FormatClass.D32S8:
                    return 64;
                case FormatClass.X96:
                    return 96;
                case FormatClass.X128:
                case FormatClass.BC2:
                case FormatClass.BC3:
                case FormatClass.BC5:
                case FormatClass.BC6H:
                case FormatClass.BC7:
                case FormatClass.ETC2EACRGBA:
                case FormatClass.EACRG:
                case FormatClass.ASTC4x4:
                case FormatClass.ASTC5x4:
                case FormatClass.ASTC5x5:
                case FormatClass.ASTC6x5:
                case FormatClass.ASTC6x6:
                case FormatClass.ASTC8x5:
                case FormatClass.ASTC8x6:
                case FormatClass.ASTC8x8:
                case FormatClass.ASTC10x5:
                case FormatClass.ASTC10x6:
                case FormatClass.ASTC10x8:
                case FormatClass.ASTC10x10:
                case FormatClass.ASTC12x10:
                case FormatClass.ASTC12x12:
                    return 128;
                case FormatClass.X192:
                    return 192;
                case FormatClass.X256:
                    return 256;
                default:
                    throw new ArgumentException();
            }
        }

        public static int GetElementSize(this Format format)
        {
            return format.GetClass().GetElementSize();
        }

        public static FormatClass GetClass(this Format format)
        {
            switch(format)
            {
                case Format.R4G4UNormPack8:
                case Format.R8UNorm:
                case Format.R8SNorm:
                case Format.R8UScaled:
                case Format.R8SScaled:
                case Format.R8UInt:
                case Format.R8SInt:
                case Format.R8SRGB:
                    return FormatClass.X8;
                case Format.R4G4B4A4UNormPack16:
                case Format.B4G4R4A4UNormPack16:
                case Format.R5G6B5UNormPack16:
                case Format.B5G6R5UNormPack16:
                case Format.R5G5B5A1UNormPack16:
                case Format.B5G5R5A1UNormPack16:
                case Format.A1R5G5B5UNormPack16:
                case Format.R8G8UNorm:
                case Format.R8G8SNorm:
                case Format.R8G8UScaled:
                case Format.R8G8SScaled:
                case Format.R8G8UInt:
                case Format.R8G8SInt:
                case Format.R8G8SRGB:
                case Format.R16UNorm:
                case Format.R16SNorm:
                case Format.R16UScaled:
                case Format.R16SScaled:
                case Format.R16UInt:
                case Format.R16SInt:
                case Format.R16SFloat:
                    return FormatClass.X16;
                case Format.R8G8B8UNorm:
                case Format.R8G8B8SNorm:
                case Format.R8G8B8UScaled:
                case Format.R8G8B8SScaled:
                case Format.R8G8B8UInt:
                case Format.R8G8B8SInt:
                case Format.R8G8B8SRGB:
                case Format.B8G8R8UNorm:
                case Format.B8G8R8SNorm:
                case Format.B8G8R8UScaled:
                case Format.B8G8R8SScaled:
                case Format.B8G8R8UInt:
                case Format.B8G8R8SInt:
                case Format.B8G8R8SRGB:
                    return FormatClass.X24;
                case Format.R8G8B8A8UNorm:
                case Format.R8G8B8A8SNorm:
                case Format.R8G8B8A8UScaled:
                case Format.R8G8B8A8SScaled:
                case Format.R8G8B8A8UInt:
                case Format.R8G8B8A8SInt:
                case Format.R8G8B8A8SRGB:
                case Format.B8G8R8A8UNorm:
                case Format.B8G8R8A8SNorm:
                case Format.B8G8R8A8UScaled:
                case Format.B8G8R8A8SScaled:
                case Format.B8G8R8A8UInt:
                case Format.B8G8R8A8SInt:
                case Format.B8G8R8A8SRGB:
                case Format.A8B8G8R8UNormPack32:
                case Format.A8B8G8R8SNormPack32:
                case Format.A8B8G8R8UScaledPack32:
                case Format.A8B8G8R8SScaledPack32:
                case Format.A8B8G8R8UIntPack32:
                case Format.A8B8G8R8SIntPack32:
                case Format.A8B8G8R8SRGBPack32:
                case Format.A2R10G10B10UNormPack32:
                case Format.A2R10G10B10SNormPack32:
                case Format.A2R10G10B10UScaledPack32:
                case Format.A2R10G10B10SScaledPack32:
                case Format.A2R10G10B10UIntPack32:
                case Format.A2R10G10B10SIntPack32:
                case Format.A2B10G10R10UNormPack32:
                case Format.A2B10G10R10SNormPack32:
                case Format.A2B10G10R10UScaledPack32:
                case Format.A2B10G10R10SScaledPack32:
                case Format.A2B10G10R10UIntPack32:
                case Format.A2B10G10R10SIntPack32:
                case Format.R16G16UNorm:
                case Format.R16G16SNorm:
                case Format.R16G16UScaled:
                case Format.R16G16SScaled:
                case Format.R16G16UInt:
                case Format.R16G16SInt:
                case Format.R16G16SFloat:
                case Format.R32UInt:
                case Format.R32SInt:
                case Format.R32SFloat:
                case Format.B10G11R11UFloatPack32:
                case Format.E5B9G9R9UFloatPack32:
                    return FormatClass.X32;
                case Format.R16G16B16UNorm:
                case Format.R16G16B16SNorm:
                case Format.R16G16B16UScaled:
                case Format.R16G16B16SScaled:
                case Format.R16G16B16UInt:
                case Format.R16G16B16SInt:
                case Format.R16G16B16SFloat:
                    return FormatClass.X48;
                case Format.R16G16B16A16UNorm:
                case Format.R16G16B16A16SNorm:
                case Format.R16G16B16A16UScaled:
                case Format.R16G16B16A16SScaled:
                case Format.R16G16B16A16UInt:
                case Format.R16G16B16A16SInt:
                case Format.R16G16B16A16SFloat:
                case Format.R32G32UInt:
                case Format.R32G32SInt:
                case Format.R32G32SFloat:
                case Format.R64UInt:
                case Format.R64SInt:
                case Format.R64SFloat:
                    return FormatClass.X64;
                case Format.R32G32B32UInt:
                case Format.R32G32B32SInt:
                case Format.R32G32B32SFloat:
                    return FormatClass.X96;
                case Format.R32G32B32A32UInt:
                case Format.R32G32B32A32SInt:
                case Format.R32G32B32A32SFloat:
                case Format.R64G64UInt:
                case Format.R64G64SInt:
                case Format.R64G64SFloat:
                    return FormatClass.X128;
                case Format.R64G64B64UInt:
                case Format.R64G64B64SInt:
                case Format.R64G64B64SFloat:
                    return FormatClass.X192;
                case Format.R64G64B64A64UInt:
                case Format.R64G64B64A64SInt:
                case Format.R64G64B64A64SFloat:
                    return FormatClass.X256;
                case Format.BC1RGBUNormBlock:
                case Format.BC1RGBSRGBBlock:
                    return FormatClass.BC1RGB;
                case Format.BC1RGBAUNormBlock:
                case Format.BC1RGBASRGBBlock:
                    return FormatClass.BC1RGBA;
                case Format.BC2UNormBlock:
                case Format.BC2SRGBBlock:
                    return FormatClass.BC2;
                case Format.BC3UNormBlock:
                case Format.BC3SRGBBlock:
                    return FormatClass.BC3;
                case Format.BC4UNormBlock:
                case Format.BC4SNormBlock:
                    return FormatClass.BC4;
                case Format.BC5UNormBlock:
                case Format.BC5SNormBlock:
                    return FormatClass.BC5;
                case Format.BC6HUFloatBlock:
                case Format.BC6HSFloatBlock:
                    return FormatClass.BC6H;
                case Format.BC7UNormBlock:
                case Format.BC7SRGBBlock:
                    return FormatClass.BC7;
                case Format.ETC2R8G8B8UNormBlock:
                case Format.ETC2R8G8B8SRGBBlock:
                    return FormatClass.ETC2RGB;
                case Format.ETC2R8G8B8A1UNormBlock:
                case Format.ETC2R8G8B8A1SRGBBlock:
                    return FormatClass.ETC2RGBA;
                case Format.ETC2R8G8B8A8UNormBlock:
                case Format.ETC2R8G8B8A8SRGBBlock:
                    return FormatClass.ETC2EACRGBA;
                case Format.EACR11UNormBlock:
                case Format.EACR11SNormBlock:
                    return FormatClass.EACR;
                case Format.EACR11G11UNormBlock:
                case Format.EACR11G11SNormBlock:
                    return FormatClass.EACRG;
                case Format.ASTC4x4UNormBlock:
                case Format.ASTC4x4SRGBBlock:
                    return FormatClass.ASTC4x4;
                case Format.ASTC5x4UNormBlock:
                case Format.ASTC5x4SRGBBlock:
                    return FormatClass.ASTC5x4;
                case Format.ASTC5x5UNormBlock:
                case Format.ASTC5x5SRGBBlock:
                    return FormatClass.ASTC5x5;
                case Format.ASTC6x5UNormBlock:
                case Format.ASTC6x5SRGBBlock:
                    return FormatClass.ASTC6x5;
                case Format.ASTC6x6UNormBlock:
                case Format.ASTC6x6SRGBBlock:
                    return FormatClass.ASTC6x6;
                case Format.ASTC8x5UNormBlock:
                case Format.ASTC8x5SRGBBlock:
                    return FormatClass.ASTC8x5;
                case Format.ASTC8x6UNormBlock:
                case Format.ASTC8x6SRGBBlock:
                    return FormatClass.ASTC8x6;
                case Format.ASTC8x8UNormBlock:
                case Format.ASTC8x8SRGBBlock:
                    return FormatClass.ASTC8x8;
                case Format.ASTC10x5UNormBlock:
                case Format.ASTC10x5SRGBBlock:
                    return FormatClass.ASTC10x5;
                case Format.ASTC10x6UNormBlock:
                case Format.ASTC10x6SRGBBlock:
                    return FormatClass.ASTC10x6;
                case Format.ASTC10x8UNormBlock:
                case Format.ASTC10x8SRGBBlock:
                    return FormatClass.ASTC10x8;
                case Format.ASTC10x10UNormBlock:
                case Format.ASTC10x10SRGBBlock:
                    return FormatClass.ASTC10x10;
                case Format.ASTC12x10UNormBlock:
                case Format.ASTC12x10SRGBBlock:
                    return FormatClass.ASTC12x10;
                case Format.ASTC12x12UNormBlock:
                case Format.ASTC12x12SRGBBlock:
                    return FormatClass.ASTC12x12;
                case Format.D16UNorm:
                    return FormatClass.D16;
                case Format.X8D24UNormPack32:
                    return FormatClass.D24;
                case Format.D32SFloat:
                    return FormatClass.D32;
                case Format.S8UInt:
                    return FormatClass.S8;
                case Format.D16UNormS8UInt:
                    return FormatClass.D16S8;
                case Format.D24UNormS8UInt:
                    return FormatClass.D24S8;
                case Format.D32SFloatS8UInt:
                    return FormatClass.D32S8;
                default:
                    return FormatClass.Other;
            }
        }

        public static bool IsCompressed(this Format format)
        {
            switch(format)
            {
                case Format.R4G4UNormPack8:
                case Format.R8UNorm:
                case Format.R8SNorm:
                case Format.R8UScaled:
                case Format.R8SScaled:
                case Format.R8UInt:
                case Format.R8SInt:
                case Format.R8SRGB:
                case Format.R4G4B4A4UNormPack16:
                case Format.B4G4R4A4UNormPack16:
                case Format.R5G6B5UNormPack16:
                case Format.B5G6R5UNormPack16:
                case Format.R5G5B5A1UNormPack16:
                case Format.B5G5R5A1UNormPack16:
                case Format.A1R5G5B5UNormPack16:
                case Format.R8G8UNorm:
                case Format.R8G8SNorm:
                case Format.R8G8UScaled:
                case Format.R8G8SScaled:
                case Format.R8G8UInt:
                case Format.R8G8SInt:
                case Format.R8G8SRGB:
                case Format.R16UNorm:
                case Format.R16SNorm:
                case Format.R16UScaled:
                case Format.R16SScaled:
                case Format.R16UInt:
                case Format.R16SInt:
                case Format.R16SFloat:
                case Format.R8G8B8UNorm:
                case Format.R8G8B8SNorm:
                case Format.R8G8B8UScaled:
                case Format.R8G8B8SScaled:
                case Format.R8G8B8UInt:
                case Format.R8G8B8SInt:
                case Format.R8G8B8SRGB:
                case Format.B8G8R8UNorm:
                case Format.B8G8R8SNorm:
                case Format.B8G8R8UScaled:
                case Format.B8G8R8SScaled:
                case Format.B8G8R8UInt:
                case Format.B8G8R8SInt:
                case Format.B8G8R8SRGB:
                case Format.R8G8B8A8UNorm:
                case Format.R8G8B8A8SNorm:
                case Format.R8G8B8A8UScaled:
                case Format.R8G8B8A8SScaled:
                case Format.R8G8B8A8UInt:
                case Format.R8G8B8A8SInt:
                case Format.R8G8B8A8SRGB:
                case Format.B8G8R8A8UNorm:
                case Format.B8G8R8A8SNorm:
                case Format.B8G8R8A8UScaled:
                case Format.B8G8R8A8SScaled:
                case Format.B8G8R8A8UInt:
                case Format.B8G8R8A8SInt:
                case Format.B8G8R8A8SRGB:
                case Format.A8B8G8R8UNormPack32:
                case Format.A8B8G8R8SNormPack32:
                case Format.A8B8G8R8UScaledPack32:
                case Format.A8B8G8R8SScaledPack32:
                case Format.A8B8G8R8UIntPack32:
                case Format.A8B8G8R8SIntPack32:
                case Format.A8B8G8R8SRGBPack32:
                case Format.A2R10G10B10UNormPack32:
                case Format.A2R10G10B10SNormPack32:
                case Format.A2R10G10B10UScaledPack32:
                case Format.A2R10G10B10SScaledPack32:
                case Format.A2R10G10B10UIntPack32:
                case Format.A2R10G10B10SIntPack32:
                case Format.A2B10G10R10UNormPack32:
                case Format.A2B10G10R10SNormPack32:
                case Format.A2B10G10R10UScaledPack32:
                case Format.A2B10G10R10SScaledPack32:
                case Format.A2B10G10R10UIntPack32:
                case Format.A2B10G10R10SIntPack32:
                case Format.R16G16UNorm:
                case Format.R16G16SNorm:
                case Format.R16G16UScaled:
                case Format.R16G16SScaled:
                case Format.R16G16UInt:
                case Format.R16G16SInt:
                case Format.R16G16SFloat:
                case Format.R32UInt:
                case Format.R32SInt:
                case Format.R32SFloat:
                case Format.B10G11R11UFloatPack32:
                case Format.E5B9G9R9UFloatPack32:
                case Format.R16G16B16UNorm:
                case Format.R16G16B16SNorm:
                case Format.R16G16B16UScaled:
                case Format.R16G16B16SScaled:
                case Format.R16G16B16UInt:
                case Format.R16G16B16SInt:
                case Format.R16G16B16SFloat:
                case Format.R16G16B16A16UNorm:
                case Format.R16G16B16A16SNorm:
                case Format.R16G16B16A16UScaled:
                case Format.R16G16B16A16SScaled:
                case Format.R16G16B16A16UInt:
                case Format.R16G16B16A16SInt:
                case Format.R16G16B16A16SFloat:
                case Format.R32G32UInt:
                case Format.R32G32SInt:
                case Format.R32G32SFloat:
                case Format.R64UInt:
                case Format.R64SInt:
                case Format.R64SFloat:
                case Format.R32G32B32UInt:
                case Format.R32G32B32SInt:
                case Format.R32G32B32SFloat:
                case Format.R32G32B32A32UInt:
                case Format.R32G32B32A32SInt:
                case Format.R32G32B32A32SFloat:
                case Format.R64G64UInt:
                case Format.R64G64SInt:
                case Format.R64G64SFloat:
                case Format.R64G64B64UInt:
                case Format.R64G64B64SInt:
                case Format.R64G64B64SFloat:
                case Format.R64G64B64A64UInt:
                case Format.R64G64B64A64SInt:
                case Format.R64G64B64A64SFloat:
                    return false;
                case Format.BC1RGBUNormBlock:
                case Format.BC1RGBSRGBBlock:
                case Format.BC1RGBAUNormBlock:
                case Format.BC1RGBASRGBBlock:
                case Format.BC2UNormBlock:
                case Format.BC2SRGBBlock:
                case Format.BC3UNormBlock:
                case Format.BC3SRGBBlock:
                case Format.BC4UNormBlock:
                case Format.BC4SNormBlock:
                case Format.BC5UNormBlock:
                case Format.BC5SNormBlock:
                case Format.BC6HUFloatBlock:
                case Format.BC6HSFloatBlock:
                case Format.BC7UNormBlock:
                case Format.BC7SRGBBlock:
                case Format.ETC2R8G8B8UNormBlock:
                case Format.ETC2R8G8B8SRGBBlock:
                case Format.ETC2R8G8B8A1UNormBlock:
                case Format.ETC2R8G8B8A1SRGBBlock:
                case Format.ETC2R8G8B8A8UNormBlock:
                case Format.ETC2R8G8B8A8SRGBBlock:
                case Format.EACR11UNormBlock:
                case Format.EACR11SNormBlock:
                case Format.EACR11G11UNormBlock:
                case Format.EACR11G11SNormBlock:
                case Format.ASTC4x4UNormBlock:
                case Format.ASTC4x4SRGBBlock:
                case Format.ASTC5x4UNormBlock:
                case Format.ASTC5x4SRGBBlock:
                case Format.ASTC5x5UNormBlock:
                case Format.ASTC5x5SRGBBlock:
                case Format.ASTC6x5UNormBlock:
                case Format.ASTC6x5SRGBBlock:
                case Format.ASTC6x6UNormBlock:
                case Format.ASTC6x6SRGBBlock:
                case Format.ASTC8x5UNormBlock:
                case Format.ASTC8x5SRGBBlock:
                case Format.ASTC8x6UNormBlock:
                case Format.ASTC8x6SRGBBlock:
                case Format.ASTC8x8UNormBlock:
                case Format.ASTC8x8SRGBBlock:
                case Format.ASTC10x5UNormBlock:
                case Format.ASTC10x5SRGBBlock:
                case Format.ASTC10x6UNormBlock:
                case Format.ASTC10x6SRGBBlock:
                case Format.ASTC10x8UNormBlock:
                case Format.ASTC10x8SRGBBlock:
                case Format.ASTC10x10UNormBlock:
                case Format.ASTC10x10SRGBBlock:
                case Format.ASTC12x10UNormBlock:
                case Format.ASTC12x10SRGBBlock:
                case Format.ASTC12x12UNormBlock:
                case Format.ASTC12x12SRGBBlock:
                case Format.D16UNorm:
                case Format.X8D24UNormPack32:
                case Format.D32SFloat:
                case Format.S8UInt:
                case Format.D16UNormS8UInt:
                case Format.D24UNormS8UInt:
                case Format.D32SFloatS8UInt:
                    return true;
                default:
                    throw new NotImplementedException();
            }
        }

        public static bool IsCompatible(this Format format, Format other)
        {
            var clazz = format.GetClass();
            return clazz != FormatClass.Other && clazz == other.GetClass();
        }
    }
}