﻿using System;
using System.Runtime.InteropServices;
using System.Threading;
using oovulkan.util;

// ReSharper disable PrivateFieldCanBeConvertedToLocalVariable

namespace oovulkan
{
    public enum SystemAllocationScope : int
    {
        Command = 0,
        Object = 1,
        Cache = 2,
        Device = 3,
        Instance = 4
    }

    public enum InternalAllocationType : int
    {
        Executable = 0
    }

    [StructLayout(LayoutKind.Sequential)]
    public unsafe struct AllocationCallbacks
    {
        public void* UserData;
        public void* Allocation;
        public void* Reallocation;
        public void* Free;
        public void* InternalAllocation;
        public void* InternalFree;
    }

    public abstract unsafe class Allocator : VulkanObject
    {
        private delegate void* AllocationFunction(void* userData, Word size, Word alignment, SystemAllocationScope scope);
        private delegate void* ReallocationFunction(void* userData, void* original, Word size, Word alignment, SystemAllocationScope scope);
        private delegate void FreeFunction(void* userData, void* memory);

        protected IntPtr Native;
        private readonly AllocationFunction allocate;
        private readonly ReallocationFunction reallocate;
        private readonly FreeFunction free;

        protected Allocator()
        {
            allocate = NativeAllocate;
            reallocate = NativeReallocate;
            free = NativeFree;

            Native = Marshal.AllocHGlobal(sizeof(AllocationCallbacks));
            var ptr = (AllocationCallbacks*)Native;
            ptr->UserData = null;
            ptr->Allocation = (void*)Marshal.GetFunctionPointerForDelegate(allocate);
            ptr->Reallocation = (void*)Marshal.GetFunctionPointerForDelegate(reallocate);
            ptr->Free = (void*)Marshal.GetFunctionPointerForDelegate(free);
            ptr->InternalAllocation = null;
            ptr->InternalFree = null;
        }

        private void* NativeAllocate(void* _, Word size, Word alignment, SystemAllocationScope scope) => Allocate(size, alignment, scope);
        private void* NativeReallocate(void* _, void* original, Word size, Word alignment, SystemAllocationScope scope) => Reallocate(original, size, alignment, scope);
        private void NativeFree(void* _, void* memory) => Free(memory);

        protected abstract void* Allocate(long size, long alignment, SystemAllocationScope scope);
        protected abstract void* Reallocate(void* original, long size, long alignment, SystemAllocationScope scope);
        protected abstract void Free(void* memory);

        protected override void Destroy()
        {
            IntPtr ptr = Interlocked.Exchange(ref Native, IntPtr.Zero);
            if(ptr != IntPtr.Zero)
                Marshal.FreeHGlobal(ptr);
        }

        public static implicit operator AllocationCallbacks*(Allocator allocator)
        {
            return allocator == null ? null : (AllocationCallbacks*)allocator.Native;
        }
    }

    public abstract unsafe class ExtendedAllocator : Allocator
    {
        private delegate void InternalAllocationCallback(void* userData, Word size, InternalAllocationType type, SystemAllocationScope scope);
        private delegate void InternalFreeCallback(void* userData, Word size, InternalAllocationType type, SystemAllocationScope scope);

        private readonly InternalAllocationCallback internalAllocate;
        private readonly InternalFreeCallback internalFree;

        protected ExtendedAllocator()
        {
            internalAllocate = NativeInternalAllocate;
            internalFree = NativeInternalFree;

            var ptr = (AllocationCallbacks*)Native;
            ptr->InternalAllocation = (void*)Marshal.GetFunctionPointerForDelegate(internalAllocate);
            ptr->InternalFree = (void*)Marshal.GetFunctionPointerForDelegate(internalFree);
        }

        private void NativeInternalAllocate(void* userData, Word size, InternalAllocationType type, SystemAllocationScope scope) => InternalAllocate(size, type, scope);
        private void NativeInternalFree(void* userData, Word size, InternalAllocationType type, SystemAllocationScope scope) => InternalFree(size, type, scope);

        protected abstract void InternalAllocate(long size, InternalAllocationType type, SystemAllocationScope scope);
        protected abstract void InternalFree(long size, InternalAllocationType type, SystemAllocationScope scope);
    }
}