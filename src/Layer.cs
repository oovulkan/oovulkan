﻿using System;
using System.Runtime.InteropServices;

#pragma warning disable 649

namespace oovulkan
{
    [StructLayout(LayoutKind.Sequential)]
    internal unsafe struct LayerProperties
    {
        public const int MAX_EXTENSION_NAME_SIZE = 256;
        public const int MAX_DESCRIPTION_SIZE = 256;

        private fixed byte layerName[MAX_EXTENSION_NAME_SIZE];
        public readonly Version SpecVersion;
        public readonly Version ImplementationVersion;
        private fixed byte description[MAX_DESCRIPTION_SIZE];

        public string LayerName
        {
            get
            {
                fixed(byte* ptr = layerName)
                    return Marshal.PtrToStringAnsi((IntPtr)ptr);
            }
        }
        public string Description
        {
            get
            {
                fixed(byte* ptr = description)
                    return Marshal.PtrToStringAnsi((IntPtr)ptr);
            }
        }
    }

    public class Layer
    {
        public readonly string Name;
        public readonly Version SpecVersion;
        public readonly Version ImplementationVersion;
        public readonly string Description;

        internal Layer(LayerProperties props)
        {
            Name = props.LayerName;
            SpecVersion = props.SpecVersion;
            ImplementationVersion = props.ImplementationVersion;
            Description = props.Description;
        }
    }
}