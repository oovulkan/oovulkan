﻿using System.Runtime.InteropServices;
using oovulkan.image.view;
using oovulkan.render;
using oovulkan.util;

namespace oovulkan
{
    [StructLayout(LayoutKind.Explicit)]
    public struct ClearValue
    {
        [FieldOffset(0)]
        public readonly Color Color;
        [FieldOffset(0)]
        public readonly DepthStencilValue DepthStencil;

        private ClearValue(Color color) : this()
        {
            Color = color;
        }
        private ClearValue(DepthStencilValue depthStencil) : this()
        {
            DepthStencil = depthStencil;
        }

        public static implicit operator ClearValue((float R, float G, float B, float A) color) => (Color)(color.R, color.G, color.B, color.A);
        public static implicit operator ClearValue((int R, int G, int B, int A) color) => (Color)(color.R, color.G, color.B, color.A);
        public static implicit operator ClearValue((uint R, uint G, uint B, uint A) color) => (Color)(color.R, color.G, color.B, color.A);
        public static implicit operator ClearValue(Color color) => new ClearValue(color);

        public static implicit operator ClearValue((float Depth, uint Stencil) value) => (DepthStencilValue)(value.Depth, value.Stencil);
        public static implicit operator ClearValue(DepthStencilValue value) => new ClearValue(value);
    }

    [StructLayout(LayoutKind.Sequential)]
    internal struct ClearAttachment
    {
        public readonly ImageAspectFlags Aspect;
        public readonly AttachmentHandle Attachment;
        public readonly ClearValue Value;

        public ClearAttachment(ImageAspectFlags aspect, AttachmentHandle attachment, ClearValue value)
        {
            Aspect = aspect;
            Attachment = attachment;
            Value = value;
        }
    }

    [StructLayout(LayoutKind.Sequential)]
    internal struct ClearRect
    {
        public readonly Rect2D Rect;
        public readonly uint BaseArrayLayer;
        public readonly uint LayerCount;

        public ClearRect(Rect2D rect, uint baseArrayLayer, uint layerCount)
        {
            Rect = rect;
            BaseArrayLayer = baseArrayLayer;
            LayerCount = layerCount;
        }
    }
}