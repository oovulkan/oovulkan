﻿using System.Runtime.InteropServices;

namespace oovulkan
{
    [StructLayout(LayoutKind.Sequential)]
    public struct DrawIndirectCommand
    {
        public readonly uint VertexCount;
        public readonly uint InstanceCount;
        public readonly uint FirstVertex;
        public readonly uint FirstInstance;

        private DrawIndirectCommand(uint firstVertex, uint vertexCount, uint firstInstance, uint instanceCount)
        {
            FirstVertex = firstVertex;
            VertexCount = vertexCount;
            FirstInstance = firstInstance;
            InstanceCount = instanceCount;
        }

        public static implicit operator DrawIndirectCommand((uint FirstVertex, uint VertexCount, uint FirstInstance, uint InstanceCount) cmd) => new DrawIndirectCommand(cmd.FirstVertex, cmd.VertexCount, cmd.FirstInstance, cmd.InstanceCount);
        public static implicit operator (uint FirstVertex, uint VertexCount, uint FirstInstance, uint InstanceCount)(DrawIndirectCommand cmd) => (cmd.FirstVertex, cmd.VertexCount, cmd.FirstInstance, cmd.InstanceCount);
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct DrawIndexedIndirectCommand
    {
        public readonly uint IndexCount;
        public readonly uint InstanceCount;
        public readonly uint FirstIndex;
        public readonly int VertexOffset;
        public readonly uint FirstInstance;

        private DrawIndexedIndirectCommand(uint firstIndex, uint indexCount, uint firstInstance, uint instanceCount, int vertexoffset)
        {
            FirstIndex = firstIndex;
            IndexCount = indexCount;
            FirstInstance = firstInstance;
            InstanceCount = instanceCount;
            VertexOffset = vertexoffset;
        }

        public static implicit operator DrawIndexedIndirectCommand((uint FirstIndex, uint IndexCount, uint FirstInstance, uint InstanceCount, int VertexOffset) cmd) => new DrawIndexedIndirectCommand(cmd.FirstIndex, cmd.IndexCount, cmd.FirstInstance, cmd.InstanceCount, cmd.VertexOffset);
        public static implicit operator (uint FirstIndex, uint IndexCount, uint FirstInstance, uint InstanceCount, int VertexOffset)(DrawIndexedIndirectCommand cmd) => (cmd.FirstIndex, cmd.IndexCount, cmd.FirstInstance, cmd.InstanceCount, cmd.VertexOffset);
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct DispatchIndirectCommand
    {
        public readonly uint X;
        public readonly uint Y;
        public readonly uint Z;

        private DispatchIndirectCommand(uint x, uint y, uint z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        public static implicit operator DispatchIndirectCommand((uint X, uint Y, uint Z) cmd) => new DispatchIndirectCommand(cmd.X, cmd.Y, cmd.Z);
        public static implicit operator (uint X, uint Y, uint Z)(DispatchIndirectCommand cmd) => (cmd.X, cmd.Y, cmd.Z);
    }
}