﻿namespace oovulkan.sampler
{
    public enum SamplerMipmapMode : int
    {
        Nearest = 0,
        Linear = 1
    }
}