﻿namespace oovulkan.sampler
{
    public enum SamplerAddressMode : int
    {
        Repeat = 0,
        MirroredRepeat = 1,
        ClampToEdge = 2,
        ClampToBorder = 3,
        MirrorClampToEdge = 4
    }
}