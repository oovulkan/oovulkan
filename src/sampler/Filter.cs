﻿namespace oovulkan.sampler
{
    public enum Filter : int
    {
        Nearest = 0,
        Linear = 1
    }
}