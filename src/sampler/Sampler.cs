﻿using System;
using System.Runtime.InteropServices;
using oovulkan.device;

// ReSharper disable CompareOfFloatsByEqualityOperator
#pragma warning disable 649

namespace oovulkan.sampler
{
    [StructLayout(LayoutKind.Sequential)]
    public struct SamplerHandle
    {
        private long Value;

        public static implicit operator SamplerHandle(long? value) => new SamplerHandle {Value = value ?? 0};
        public static implicit operator long(SamplerHandle handle) => handle.Value;
    }

    public unsafe class Sampler : VulkanObject<SamplerHandle>
    {
        protected internal override bool IsChild => true;
        protected internal override VulkanObject Parent => Device;

        public readonly Device Device;
        public readonly Filter MagFilter;
        public readonly Filter MinFilter;
        public readonly SamplerMipmapMode MipmapMode;
        public readonly (SamplerAddressMode U, SamplerAddressMode V, SamplerAddressMode W) AddressMode;
        public readonly float MipLoadBias;
        public readonly bool AnisotropyEnable;
        public readonly float MaxAnisotropy;
        public readonly bool CompareEnable;
        public readonly CompareOp CompareOp;
        public readonly float MinLod;
        public readonly float MaxLod;
        public readonly BorderColor BorderColor;
        public readonly bool UnnormalizedCoordinates;

        internal Sampler(Device device, Filter magFilter, Filter minFilter, SamplerMipmapMode mipmapMode, (SamplerAddressMode U, SamplerAddressMode V, SamplerAddressMode W) addressMode, float mipLoadBias, bool anisotropyEnable, float maxAnisotropy, bool compareEnable, CompareOp compareOp, float minLod, float maxLod, BorderColor borderColor, bool unnormalizedCoordinates)
        {
            Device = device;
            this.Link(Device);
            MagFilter = magFilter;
            MinFilter = minFilter;
            MipmapMode = mipmapMode;
            AddressMode = addressMode;
            MipLoadBias = mipLoadBias;
            AnisotropyEnable = anisotropyEnable;
            MaxAnisotropy = maxAnisotropy;
            CompareEnable = compareEnable;
            CompareOp = compareOp;
            MinLod = minLod;
            MaxLod = maxLod;
            BorderColor = borderColor;
            UnnormalizedCoordinates = unnormalizedCoordinates;
            Create();
        }

        private void Create()
        {
            #region Validation

            var limits = Device.PhysicalDevice.Limits;
            if(MipLoadBias >= limits.MaxSamplerLodBias)
                throw new ArgumentOutOfRangeException(nameof(MipLoadBias));
            if(!Device.Features.SamplerAnisotropy && AnisotropyEnable)
                throw new NotSupportedException(nameof(AnisotropyEnable));
            if(AnisotropyEnable)
            {
                if(MaxAnisotropy < 1 || MaxAnisotropy > limits.MaxSamplerAnisotropy)
                    throw new ArgumentOutOfRangeException(nameof(MaxAnisotropy));
            }
            if(UnnormalizedCoordinates)
            {
                if(MinFilter != MagFilter || MipmapMode != SamplerMipmapMode.Nearest || MinLod != 0 || MaxLod != 0
                   || (AddressMode.U != SamplerAddressMode.ClampToEdge && AddressMode.U != SamplerAddressMode.ClampToBorder)
                   || (AddressMode.V != SamplerAddressMode.ClampToEdge && AddressMode.V != SamplerAddressMode.ClampToBorder)
                   || AnisotropyEnable || CompareEnable)
                    throw new NotSupportedException(nameof(UnnormalizedCoordinates));
            }
            if((AddressMode.U == SamplerAddressMode.MirrorClampToEdge || AddressMode.V == SamplerAddressMode.MirrorClampToEdge
                || AddressMode.W == SamplerAddressMode.MirrorClampToEdge)
               && !Device.Extensions.Contains(InstanceExtension.KhrSamplerMirrorClampToEdge))
                throw new ArgumentException(nameof(AddressMode));

            #endregion

            SamplerCreateInfo info = new SamplerCreateInfo(MagFilter, MinFilter, MipmapMode, AddressMode, MipLoadBias,
                AnisotropyEnable, MaxAnisotropy, CompareEnable, CompareOp, MinLod, MaxLod, BorderColor, UnnormalizedCoordinates);
            using(Device.WeakLock)
            {
                SamplerHandle handle;
                using(Device.Allocator?.WeakLock)
                    vkCreateSampler(Device, &info, Device.Allocator, out handle).Throw();
                Handle = handle;
                Device.Add(this);
            }
        }

        protected override void Destroy()
        {
            using(Device.WeakLock)
                Device.Remove(this);
            using(Device.Allocator?.WeakLock)
                vkDestroySampler(Device, this, Device.Allocator);
        }

        private readonly VkCreateSampler vkCreateSampler;
        private readonly VkDestroySampler vkDestroySampler;

        private delegate Result VkCreateSampler(DeviceHandle device, SamplerCreateInfo* info, AllocationCallbacks* allocator, out SamplerHandle sampler);
        private delegate void VkDestroySampler(DeviceHandle device, SamplerHandle sampler, AllocationCallbacks* allocator);
    }
}