﻿using System;
using System.Runtime.InteropServices;
using util;

// ReSharper disable PrivateFieldCanBeConvertedToLocalVariable

namespace oovulkan.sampler
{
    [Flags]
    internal enum SamplerCreateFlags : int
    {
        None = 0
    }

    [StructLayout(LayoutKind.Sequential)]
    internal unsafe struct SamplerCreateInfo
    {
        private readonly StructureType Type;
        private readonly void* Next;
        private readonly SamplerCreateFlags Flags;
        public readonly Filter MagFilter;
        public readonly Filter MinFilter;
        public readonly SamplerMipmapMode MipmapMode;
        public readonly SamplerAddressMode AddressModeU;
        public readonly SamplerAddressMode AddressModeV;
        public readonly SamplerAddressMode AddressModeW;
        public readonly float MipLodBias;
        public readonly Bool AnisotropyEnable;
        public readonly float MaxAnisotropy;
        public readonly Bool CompareEnable;
        public readonly CompareOp CompareOp;
        public readonly float MinLod;
        public readonly float MaxLod;
        public readonly BorderColor BorderColor;
        public readonly Bool UnnormalizedCoordinates;

        public SamplerCreateInfo(Filter magFilter, Filter minFilter, SamplerMipmapMode mipmapMode,
            (SamplerAddressMode U, SamplerAddressMode V, SamplerAddressMode W) addressMode,
            float mipLodBias, bool anisotropyEnable, float maxAnisotropy, bool compareEnable, CompareOp compareOp,
            float minLod, float maxLod, BorderColor borderColor, bool unnormalizedCoordinates)
        {
            Type = StructureType.SamplerCreateInfo;
            Next = null;
            Flags = SamplerCreateFlags.None;
            MagFilter = magFilter;
            MinFilter = minFilter;
            MipmapMode = mipmapMode;
            AddressModeU = addressMode.U;
            AddressModeV = addressMode.V;
            AddressModeW = addressMode.W;
            MipLodBias = mipLodBias;
            AnisotropyEnable = anisotropyEnable;
            MaxAnisotropy = maxAnisotropy;
            CompareEnable = compareEnable;
            CompareOp = compareOp;
            MinLod = minLod;
            MaxLod = maxLod;
            BorderColor = borderColor;
            UnnormalizedCoordinates = unnormalizedCoordinates;
        }
    }
}