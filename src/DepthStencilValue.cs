﻿using System.Runtime.InteropServices;

namespace oovulkan
{
    [StructLayout(LayoutKind.Sequential)]
    public struct DepthStencilValue
    {
        public readonly float Depth;
        public readonly uint Stencil;

        private DepthStencilValue(float depth, uint stencil)
        {
            Depth = depth;
            Stencil = stencil;
        }

        public static implicit operator DepthStencilValue((float Depth, uint Stencil) value) => new DepthStencilValue(value.Depth, value.Stencil);
        public static implicit operator (float Depth, uint Stencil)(DepthStencilValue value) => (value.Depth, value.Stencil);
    }
}