﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using oovulkan.device.physical;
using oovulkan.util;
using oovulkan.util.collection;

namespace oovulkan
{
    public class MemoryRequirements
    {
        public readonly Word Size;
        public readonly Word Alignment;
        public readonly ReadonlySet<MemoryType> Types;

        internal MemoryRequirements(Native native, IEnumerable<MemoryType> types)
        {
            Size = native.Size;
            Alignment = native.Alignment;
            Types = types.Where((type, i) => (native.Types & (1 << i)) != 0).ToSet();
        }

        [StructLayout(LayoutKind.Sequential)]
        internal struct Native
        {
            public readonly Word Size;
            public readonly Word Alignment;
            public readonly uint Types;
        }
    }
}