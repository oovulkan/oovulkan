﻿using System;
using System.Runtime.InteropServices;
using oovulkan.device;
using oovulkan.device.physical;
using oovulkan.util;

#pragma warning disable 649

namespace oovulkan.memory
{
    [Flags]
    internal enum MemoryMapFlags
    {
        None = 0
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct MemoryHandle
    {
        private long Value;

        public static implicit operator MemoryHandle(long? value) => new MemoryHandle {Value = value ?? 0};
        public static implicit operator long(MemoryHandle handle) => handle.Value;
    }

    public unsafe class Memory : VulkanObject<MemoryHandle>
    {
        public const ulong WHOLE_SIZE = 0xFFFFFFFFFFFFFFFF;
        protected internal override bool IsChild => true;
        protected internal override VulkanObject Parent => Device;

        public readonly Device Device;
        public readonly Word Size;
        public readonly MemoryType Type;
        public bool Mapped{ get; private set; }

        internal Memory(Device device, Word size, MemoryType type)
        {
            Device = device;
            Size = size;
            Type = type;
            this.Link(Device);
            Create();
        }

        private void Create()
        {
            if(Type.PhysicalDevice != Device.PhysicalDevice)
                throw new ArgumentException(nameof(Type));
            if(Size == 0)
                throw new ArgumentOutOfRangeException(nameof(Size));
            MemoryAllocateInfo info = new MemoryAllocateInfo(Size, Type);
            using(Device.WeakLock)
            {
                MemoryHandle handle;
                using(Device.Allocator?.WeakLock)
                    vkAllocateMemory(Device, &info, Device.Allocator, out handle).Throw();
                Handle = handle;
                Device.Add(this);
            }
        }
        protected override void Destroy()
        {
            using(Device.WeakLock)
                Device.Remove(this);
            using(Device.Allocator?.WeakLock)
                vkFreeMemory(Device, this, Device.Allocator);
        }

        public void* Map(ulong offset=0, ulong size=WHOLE_SIZE)
        {
            if(offset >= Size)
                throw new ArgumentOutOfRangeException(nameof(offset));
            if(size != WHOLE_SIZE)
            {
                if(size <= 0)
                    throw new ArgumentOutOfRangeException(nameof(size));
                if(offset + size > Size)
                    throw new ArgumentOutOfRangeException(nameof(size));
            }
            if(!Type.Properties.HasFlag(MemoryPropertyFlags.HostVisible))
                throw new NotSupportedException();
            using(Lock)
            {
                if(Mapped)
                    throw new InvalidOperationException();
                vkMapMemory(Device, this, offset, size, MemoryMapFlags.None, out var ptr).Throw();
                Mapped = true;
                return ptr;
            }
        }

        public void Unmap()
        {
            using(Lock)
            {
                if(!Mapped)
                    throw new InvalidOperationException();
                vkUnmapMemory(Device, this);
                Mapped = false;
            }
        }

        private readonly VkAllocateMemory vkAllocateMemory;
        private readonly VkFreeMemory vkFreeMemory;
        private readonly VkMapMemory vkMapMemory;
        private readonly VkUnmapMemory vkUnmapMemory;

        private delegate Result VkAllocateMemory(DeviceHandle device, MemoryAllocateInfo* info, AllocationCallbacks* allocator, out MemoryHandle memory);
        private delegate void VkFreeMemory(DeviceHandle device, MemoryHandle memory, AllocationCallbacks* allocator);
        private delegate Result VkMapMemory(DeviceHandle device, MemoryHandle memory, Word offset, Word size, MemoryMapFlags flags, out void* data);
        private delegate void VkUnmapMemory(DeviceHandle device, MemoryHandle memory);

        //TODO: more binding functions
    }
}