﻿using System;
using System.Runtime.InteropServices;
using oovulkan.device.physical;
using oovulkan.util;

// ReSharper disable PrivateFieldCanBeConvertedToLocalVariable

namespace oovulkan.memory
{
    [Flags]
    internal enum SemaphoreCreateFlags : int
    {
        None = 0
    }

    [StructLayout(LayoutKind.Sequential)]
    internal unsafe struct MemoryAllocateInfo
    {
        private readonly StructureType Type;
        private readonly void* Next;
        public readonly Word Size;
        public readonly MemoryTypeHandle MemoryType;

        public MemoryAllocateInfo(Word size, MemoryType memoryType)
        {
            Type = StructureType.MemoryAllocateInfo;
            Next = null;
            Size = size;
            MemoryType = memoryType;
        }
    }
}