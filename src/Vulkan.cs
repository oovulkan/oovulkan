﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

// ReSharper disable InconsistentNaming

namespace oovulkan
{
    public enum OS
    {
        Windows,
        Linux,
        OSX
    }

    public static unsafe class Vulkan
    {
        public static readonly OS Platform;
        private static readonly IntPtr VulkanHandle;

        static Vulkan()
        {
            Platform = GetOS();
            foreach(var name in GetVulkanLibraryNames())
            {
                var ptr = Load(name);
                if(ptr != null)
                {
                    VulkanHandle = ptr;
                    break;
                }
            }
            if(VulkanHandle == null)
                throw new NotSupportedException();
        }

        private static OS GetOS()
        {
            switch(Environment.OSVersion.Platform)
            {
                case PlatformID.Win32NT:
                case PlatformID.Win32S:
                case PlatformID.Win32Windows:
                case PlatformID.WinCE:
                    return OS.Windows;
                case PlatformID.MacOSX:
                    return OS.OSX;
                case PlatformID.Unix:
                    //might still be OSX
                    var buf = stackalloc byte[8192];
                    if(uname(buf) == 0)
                    {
                        string os = Marshal.PtrToStringAnsi((IntPtr)buf);
                        if(os == "Darwin")
                            return OS.OSX;
                    }
                    return OS.Linux;
                default:
                    throw new NotSupportedException();
            }
        }
        private static IEnumerable<string> GetVulkanLibraryNames()
        {
            switch(Platform)
            {
                case OS.Windows:
                    yield return "vulkan-1.dll";
                    break;
                case OS.Linux:
                    yield return "libvulkan.so.1";
                    yield return "libvulkan.so";
                    break;
                default:
                    throw new NotSupportedException();
            }
        }
        private static IntPtr Load(string name)
        {
            switch(Platform)
            {
                case OS.Windows:
                    return Kernel32LoadLibrary(name);
                case OS.Linux:
                    return LibDLLoadLibrary(name);
                default:
                    throw new NotSupportedException();
            }
        }
        internal static IntPtr GetProc(string name)
        {
            switch(Platform)
            {
                case OS.Windows:
                    return Kernel32GetProcAddress(VulkanHandle, name);
                case OS.Linux:
                    return LibDLGetProcAddress(VulkanHandle, name);
                default:
                    throw new NotSupportedException();
            }
        }

        [DllImport("kernel32.dll", EntryPoint = "LoadLibrary")]
        private static extern IntPtr Kernel32LoadLibrary(string name);
        [DllImport("kernel32.dll", EntryPoint = "GetProcAddress")]
        private static extern IntPtr Kernel32GetProcAddress(IntPtr module, string name);
        [DllImport("libdl.so", EntryPoint = "dlopen")]
        private static extern IntPtr LibDLLoadLibrary(string name, int flags=/*RTLD_NOW*/2);
        [DllImport("libdl.so", EntryPoint = "dlsym")]
        private static extern IntPtr LibDLGetProcAddress(IntPtr handle, string name);
        [DllImport ("libc")]
        private static extern int uname(void* buf);

    }
}