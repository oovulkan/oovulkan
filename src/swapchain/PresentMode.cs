﻿// ReSharper disable InconsistentNaming
namespace oovulkan.swapchain
{
    public enum PresentMode : int
    {
        Immediate = 0,
        Mailbox = 1,
        FIFO = 2,
        FIFORelaxed = 3
    }
}