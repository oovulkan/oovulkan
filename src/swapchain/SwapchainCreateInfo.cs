﻿using System;
using System.Runtime.InteropServices;
using oovulkan.image;
using oovulkan.queue.family;
using oovulkan.surface;
using oovulkan.util;
using util;

// ReSharper disable PrivateFieldCanBeConvertedToLocalVariable
// ReSharper disable InconsistentNaming

namespace oovulkan.swapchain
{
    [Flags]
    internal enum SwapchainCreateFlags : int
    {
        None = 0
    }

    internal enum ColorSpace : int
    {
        SRGBNonlinear = 0
    }

    [StructLayout(LayoutKind.Sequential)]
    internal unsafe struct SwapchainCreateInfo
    {
        private readonly StructureType Type;
        private readonly void* Next;
        private readonly SwapchainCreateFlags Flags;
        public readonly SurfaceHandle Surface;
        public readonly uint MinImageCount;
        public readonly Format Format;
        public readonly ColorSpace ColorSpace;
        public readonly U2 Extent;
        public readonly uint ArrayLayers;
        public readonly ImageUsageFlags Usage;
        public readonly SharingMode SharingMode;
        public readonly uint FamilyCount;
        public readonly QueueFamilyHandle* FamilyPtr;
        public readonly SurfaceTransformFlags PreTransform;
        public readonly CompositeAlphaFlags CompositeAlpha;
        public readonly PresentMode Mode;
        public readonly Bool Clipped;
        public readonly SwapchainHandle OldSwapchain;

        public SwapchainCreateInfo(SurfaceHandle surface, uint minImageCount, Format format, (uint, uint) extent,
            uint arrayLayers, ImageUsageFlags usage, SharingMode sharingMode, uint familyCount, QueueFamilyHandle* familyPtr,
            SurfaceTransformFlags preTransform, CompositeAlphaFlags compositeAlpha, PresentMode mode, bool clipped,
            Swapchain oldSwapchain)
        {
            Type = StructureType.SwapchainCreateInfoKhr;
            Next = null;
            Flags = SwapchainCreateFlags.None;
            Surface = surface;
            MinImageCount = minImageCount;
            Format = format;
            ColorSpace = ColorSpace.SRGBNonlinear;
            Extent = extent;
            ArrayLayers = arrayLayers;
            Usage = usage;
            SharingMode = sharingMode;
            FamilyCount = familyCount;
            FamilyPtr = familyPtr;
            PreTransform = preTransform;
            CompositeAlpha = compositeAlpha;
            Mode = mode;
            Clipped = clipped;
            OldSwapchain = oldSwapchain;
        }
    }
}