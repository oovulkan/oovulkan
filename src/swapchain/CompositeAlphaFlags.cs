﻿using System;

namespace oovulkan.swapchain
{
    [Flags]
    public enum CompositeAlphaFlags : int
    {
        None = 0,
        Opaque = 1,
        PreMultiplied = 2,
        PostMultiplied = 4,
        Inherit = 8
    }
}