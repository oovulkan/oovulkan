﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using oovulkan.device;
using oovulkan.image;
using oovulkan.queue;
using oovulkan.queue.family;
using oovulkan.surface;
using oovulkan.synchronization.fence;
using oovulkan.synchronization.semaphore;
using oovulkan.util.collection;

// ReSharper disable InconsistentNaming
#pragma warning disable 649

namespace oovulkan.swapchain
{
    [StructLayout(LayoutKind.Sequential)]
    public struct SwapchainHandle
    {
        private long Value;

        public static implicit operator SwapchainHandle(long? value) => new SwapchainHandle {Value = value ?? 0};
        public static implicit operator long(SwapchainHandle handle) => handle.Value;
    }

    public unsafe class Swapchain : VulkanObject<SwapchainHandle>
    {
        protected internal override bool IsChild => true;
        protected internal override VulkanObject Parent => Surface;
        protected internal override bool IsParent => true;
        protected internal override IEnumerable<VulkanObject> Children => images ?? Enumerable.Empty<VulkanObject>();

        public readonly Surface Surface;
        public readonly uint MinImageCount;
        public readonly Format Format;
        public readonly (uint Width, uint Height) Extent;
        public readonly uint ArrayLayers;
        public readonly ImageUsageFlags Usage;
        public readonly SharingMode SharingMode;
        public readonly ReadonlyList<QueueFamily> Families;
        public readonly SurfaceTransformFlags PreTransform;
        public readonly CompositeAlphaFlags CompositeAlpha;
        public readonly PresentMode Mode;
        public readonly bool Clipped;

        public uint Width => Extent.Width;
        public uint Height => Extent.Height;

        private ReadonlyList<SwapchainImage> images;
        private Queue<SwapchainImage> imageQueue = new Queue<SwapchainImage>();

        public ReadonlyList<SwapchainImage> Images => images ?? (images = GetImages());
        public Result Status => GetStatus();

        internal Swapchain(Surface surface, uint minImageCount, Format format, (uint Width, uint Height) extent,
            uint arrayLayers, ImageUsageFlags usage, SharingMode sharingMode, IEnumerable<QueueFamily> families,
            SurfaceTransformFlags preTransform, CompositeAlphaFlags compositeAlpha, PresentMode mode, bool clipped)
        {
            Surface = surface;
            this.Link(Surface.Device);
            MinImageCount = minImageCount;
            Format = format;
            Extent = extent;
            ArrayLayers = arrayLayers;
            Usage = usage;
            SharingMode = sharingMode;
            Families = families?.ToList();
            PreTransform = preTransform;
            CompositeAlpha = compositeAlpha;
            Mode = mode;
            Clipped = clipped;
            Create();
        }

        private void Create()
        {
            #region Validation
            var capabilities = Surface.Capabilities;
            if(MinImageCount < capabilities.ImageCount.Min)
                throw new ArgumentOutOfRangeException(nameof(MinImageCount));
            if(capabilities.ImageCount.Max != 0 && MinImageCount > capabilities.ImageCount.Max)
                throw new ArgumentOutOfRangeException(nameof(MinImageCount));
            if(!Surface.Formats.Contains(Format))
                throw new NotSupportedException(nameof(Format));
            if(Extent.Width < capabilities.Extent.Min.Width || Extent.Width > capabilities.Extent.Max.Width
               || Extent.Height < capabilities.Extent.Min.Height || Extent.Height > capabilities.Extent.Max.Height)
                throw new ArgumentOutOfRangeException(nameof(Extent));
            if(ArrayLayers == 0 || ArrayLayers > capabilities.MaxArrayLayers)
                throw new ArgumentOutOfRangeException(nameof(ArrayLayers));
            if((Usage & ~capabilities.SupportedUsage) != 0)
                throw new NotSupportedException(nameof(Usage));
            if(SharingMode == SharingMode.Concurrent && Families.Count == 0)
                throw new ArgumentException(nameof(Families));
            if(PreTransform == SurfaceTransformFlags.None)
                throw new ArgumentException(nameof(PreTransform));
            if((PreTransform & ~capabilities.SupportedTransforms) != 0)
                throw new NotSupportedException(nameof(PreTransform));
            if(CompositeAlpha == CompositeAlphaFlags.None)
                throw new ArgumentException(nameof(CompositeAlpha));
            if((CompositeAlpha & ~capabilities.SupportedCompositeAlpha) != 0)
                throw new NotSupportedException(nameof(CompositeAlpha));
            if(!Surface.Modes.Contains(Mode))
                throw new NotSupportedException(nameof(Mode));
            if(Families == null)
            {
                if(Surface.Device.PhysicalDevice.QueueFamilies.All(family => !family.SupportsSurface(Surface)))
                    throw new NotSupportedException();
            }
            else
            {
                if(!Families.Any(family => family.SupportsSurface(Surface)))
                    throw new NotSupportedException(nameof(Families));
            }
            #endregion

            var families = stackalloc QueueFamilyHandle[Families?.Count ?? 0];
            for(int i = 0; i < Families?.Count; i++)
                families[i] = Families[i];
            var info = new SwapchainCreateInfo(Surface, MinImageCount, Format, Extent, ArrayLayers, Usage, SharingMode,
                (uint)(Families?.Count ?? 0), families, PreTransform, CompositeAlpha, Mode, Clipped, Surface.Swapchain);
            using(Surface.Swapchain?.Lock)
            using(Surface.WeakLock)
            {
                SwapchainHandle handle;
                using(Surface.Device.Allocator?.WeakLock)
                    vkCreateSwapchainKHR(Surface.Device, &info, Surface.Device.Allocator, out handle).Throw();
                Handle = handle;
                Surface.Swapchain = this;
            }
        }
        protected override void Destroy()
        {
            using(Surface.WeakLock)
                Surface.Swapchain = this;
            using(Surface.Device.Allocator?.WeakLock)
                vkDestroySwapchainKHR(Surface.Device, this, Surface.Device.Allocator);
        }

        private Result GetStatus()
        {
            using(Lock)
                return vkGetSwapchainStatusKHR(Surface.Device, this);
        }
        private ReadonlyList<SwapchainImage> GetImages()
        {
            using(Lock)
            {
                uint count = 0;
                vkGetSwapchainImagesKHR(Surface.Device, this, ref count, null).Throw();
                var handles = stackalloc ImageHandle[(int)count];
                vkGetSwapchainImagesKHR(Surface.Device, this, ref count, handles).Throw();
                var images = new List<SwapchainImage>();
                for(uint i = 0; i < count; i++)
                    images.Add(new SwapchainImage(this, handles[i], i));
                return new ReadonlyList<SwapchainImage>(images);
            }
        }
        private SwapchainImage AcquireNextImage(Semaphore semaphore=null, Fence fence=null, ulong timeout=ulong.MaxValue)
        {
            vkAcquireNextImageKHR(Surface.Device, this, timeout, semaphore, fence, out uint index);
            return Images[(int)index];
        }

        public IEnumerable<(SwapchainImage Image, Semaphore Start, Semaphore End)> FrameLoop(Queue queue)
        {
            var start = Surface.Device.SemaphorePool.Get();
            var end = Surface.Device.SemaphorePool.Get();
            try
            {
                while(true)
                {
                    SwapchainImage image = AcquireNextImage(start);
                    yield return (image, start, end);
                    queue.Present()
                        .Wait(end)
                        .Image(image)
                        .Done();
                }
            }
            finally
            {
                Surface.Device.SemaphorePool.Release(start);
                Surface.Device.SemaphorePool.Release(end);
            }
        }

        private readonly VkCreateSwapchainKHR vkCreateSwapchainKHR;
        private readonly VkDestroySwapchainKHR vkDestroySwapchainKHR;
        private readonly VkGetSwapchainImagesKHR vkGetSwapchainImagesKHR;
        private readonly VkAcquireNextImageKHR vkAcquireNextImageKHR;
        private readonly VkGetSwapchainStatusKHR vkGetSwapchainStatusKHR;

        private delegate Result VkCreateSwapchainKHR(DeviceHandle device, SwapchainCreateInfo* info, AllocationCallbacks* allocator, out SwapchainHandle swapchain);
        private delegate void VkDestroySwapchainKHR(DeviceHandle device, SwapchainHandle swapchain, AllocationCallbacks* allocator);
        private delegate Result VkGetSwapchainImagesKHR(DeviceHandle device, SwapchainHandle swapchain, ref uint count, ImageHandle* images);
        private delegate Result VkAcquireNextImageKHR(DeviceHandle device, SwapchainHandle swapchain, ulong timeout, SemaphoreHandle semaphore, FenceHandle fence, out uint imageIndex);
        private delegate Result VkGetSwapchainStatusKHR(DeviceHandle device, SwapchainHandle swapchain);
    }
}