﻿using System;
using oovulkan.image;
using oovulkan.queue.family;
using oovulkan.util.collection;

namespace oovulkan.swapchain
{
    public class SwapchainImage : ImageBase
    {
        public readonly Swapchain Swapchain;
        public readonly uint Index;

        public override ImageCreateFlags Flags => ImageCreateFlags.None;
        public override Format Format => Swapchain.Format;
        public override ImageType Type => ImageType.X2D;
        protected internal override (uint Width, uint Height, uint Depth) Extent => (Swapchain.Extent.Width, Swapchain.Extent.Height, 1);
        public override uint MipLevels => 1;
        public override uint ArrayLayers => Swapchain.ArrayLayers;
        public override ImageTiling Tiling => ImageTiling.Optimal;
        public override ImageUsageFlags Usage => Swapchain.Usage;
        protected internal override SharingMode SharingMode => Swapchain.SharingMode;
        protected internal override ReadonlyList<QueueFamily> Families => Swapchain.Families;
        public override SampleCountFlags Samples => SampleCountFlags.Bit1;
        public override ImageLayout InitialLayout => ImageLayout.Undefined;

        internal SwapchainImage(Swapchain swapchain, ImageHandle handle, uint index) : base(swapchain.Surface.Device)
        {
            Swapchain = swapchain;
            Handle = handle;
            Index = index;
        }

        public override void Dispose() => throw new NotSupportedException();
        protected override void Destroy() { }
    }
}