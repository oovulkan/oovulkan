﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Threading;
using oovulkan.instance;
using oovulkan.util;
using oovulkan.util.collection;

namespace oovulkan.application
{
    public unsafe class Application : VulkanObject
    {
        public static readonly string EngineName = "Oovulkan";
        public static readonly Version EngineVersion = Version.Zero;

        protected internal override bool IsParent => true;
        protected internal override IEnumerable<VulkanObject> Children => instances;

        private readonly HashSet<Instance> instances = new HashSet<Instance>();
        public ReadonlySet<Instance> Instances => instances;

        private IntPtr native;
        public readonly string Name;
        public Version Version => ((ApplicationInfo*)native)->ApplicationVersion;

        public Application(string name, Version version)
        {
            native = Marshal.AllocHGlobal(sizeof(ApplicationInfo));
            *((ApplicationInfo*)native) = new ApplicationInfo((AnsiString)name, version, (AnsiString)EngineName, EngineVersion, Version.Zero);
            Name = name;
        }

        internal void Add(Instance instance) { instances.Add(instance); }
        internal void Remove(Instance instance) { instances.Remove(instance); }

        public Instance.Builder CreateInstance(Allocator allocator=null)
        {
            return new Instance.Builder(this, allocator);
        }

        protected override void Destroy()
        {
            IntPtr ptr = Interlocked.Exchange(ref native, IntPtr.Zero);
            if(ptr != IntPtr.Zero)
                Marshal.FreeHGlobal(ptr);
        }

        public static implicit operator ApplicationInfo*(Application application)
        {
            return application == null ? null : (ApplicationInfo*)application.native;
        }
    }
}