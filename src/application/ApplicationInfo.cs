﻿using System;
using System.Runtime.InteropServices;
using oovulkan.util;

// ReSharper disable PrivateFieldCanBeConvertedToLocalVariable

namespace oovulkan.application
{
    [StructLayout(LayoutKind.Sequential)]
    public unsafe struct ApplicationInfo : IDisposable
    {
        private readonly StructureType Type;
        private readonly void* Next;
        public readonly AnsiString ApplicationName;
        public readonly Version ApplicationVersion;
        public readonly AnsiString EngineName;
        public readonly Version EngineVersion;
        public readonly Version ApiVersion;

        public ApplicationInfo(AnsiString appName, Version appVersion, AnsiString engineName, Version engineVersion, Version apiVersion)
        {
            Type = StructureType.ApplicationInfo;
            Next = null;
            ApplicationName = appName;
            ApplicationVersion = appVersion;
            EngineName = engineName;
            EngineVersion = engineVersion;
            ApiVersion = apiVersion;
        }

        public void Dispose()
        {
            ApplicationName.Dispose();
            EngineName.Dispose();
        }
    }
}