﻿using System;
using System.Runtime.InteropServices;
using oovulkan.image.view;
using oovulkan.render;

// ReSharper disable PrivateFieldCanBeConvertedToLocalVariable

namespace oovulkan.framebuffer
{
    [Flags]
    internal enum FrameBufferCreateFlags : int
    {
        None = 0
    }

    [StructLayout(LayoutKind.Sequential)]
    internal unsafe struct FrameBufferCreateInfo
    {
        private readonly StructureType Type;
        private readonly void* Next;
        private readonly FrameBufferCreateFlags Flags;
        public readonly RenderpassHandle Renderpass;
        public readonly uint AttachmentCount;
        public readonly ImageViewHandle* Attachments;
        public readonly uint Width;
        public readonly uint Height;
        public readonly uint Layers;

        public FrameBufferCreateInfo(Renderpass renderpass, ImageViewHandle* attachments, (uint Width, uint Height) size, uint layers)
        {
            Type = StructureType.FrameBufferCreateInfo;
            Next = null;
            Flags = FrameBufferCreateFlags.None;
            Renderpass = renderpass;
            AttachmentCount = (uint)renderpass.Attachments.Count;
            Attachments = attachments;
            Width = size.Width;
            Height = size.Height;
            Layers = layers;
        }
    }
}