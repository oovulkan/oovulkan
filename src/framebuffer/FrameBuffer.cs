﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using oovulkan.device;
using oovulkan.image;
using oovulkan.image.view;
using oovulkan.render;
using oovulkan.util.collection;

#pragma warning disable 649

namespace oovulkan.framebuffer
{
    [StructLayout(LayoutKind.Sequential)]
    public struct FrameBufferHandle
    {
        private long Value;

        public static implicit operator FrameBufferHandle(long? value) => new FrameBufferHandle {Value = value ?? 0};
        public static implicit operator long(FrameBufferHandle handle) => handle.Value;
    }

    public unsafe class FrameBuffer : VulkanObject<FrameBufferHandle>
    {
        protected internal override bool IsChild => true;
        protected internal override VulkanObject Parent => Renderpass;

        public readonly Renderpass Renderpass;
        public readonly (uint Width, uint Height) Size;
        public readonly uint Layers;
        public readonly ReadonlyList<ImageView> Attachments;

        internal FrameBuffer(Renderpass renderpass, (uint, uint) size, uint layers, IEnumerable<ImageView> attachments)
        {
            Renderpass = renderpass;
            this.Link(Renderpass.Device);
            Size = size;
            Layers = layers;
            Attachments = attachments.ToList();
            Create();
        }

        private void Create()
        {
            #region Validation

            var limits = Renderpass.Device.PhysicalDevice.Limits;
            if(Size.Width == 0 || Size.Width > limits.MaxFramebufferWidth ||
               Size.Height == 0 || Size.Height > limits.MaxFramebufferHeight)
                throw new ArgumentOutOfRangeException(nameof(Size));
            if(Layers == 0 || Layers > limits.MaxFramebufferLayers)
                throw new ArgumentOutOfRangeException(nameof(Layers));
            if(Attachments.Count != Renderpass.Attachments.Count)
                throw new ArgumentException(nameof(Attachments));
            for(uint i = 0; i < Attachments.Count; i++)
            {
                var attachment = Renderpass.Attachments[(int)i];
                var view = Attachments[(int)i];
                if(view.Image.Device != Renderpass.Device)
                    throw new ArgumentException(nameof(Attachments));
                if(attachment.IsInput && !view.Image.Usage.HasFlag(ImageUsageFlags.InputAttachment))
                    throw new ArgumentException(nameof(Attachments));
                if(attachment.IsColor && !view.Image.Usage.HasFlag(ImageUsageFlags.ColorAttachment))
                    throw new ArgumentException(nameof(Attachments));
                if(attachment.IsDepthStencil && !view.Image.Usage.HasFlag(ImageUsageFlags.DepthStencilAttachment))
                    throw new ArgumentException(nameof(Attachments));
                if(attachment.Format != view.Format)
                    throw new ArgumentException(nameof(Attachments));
                if(attachment.Samples != view.Image.Samples)
                    throw new ArgumentException(nameof(Attachments));
                if(view.Image.Extent.Width < Size.Width || view.Image.Extent.Height < Size.Height)
                    throw new ArgumentException(nameof(Attachments));
                if(view.LevelCount != 1)
                    throw new ArgumentException(nameof(Attachments));
                if(view.Swizzle != Swizzle.Identity)
                    throw new ArgumentException(nameof(Attachments));
            }

            #endregion

            var attachmentPtr = stackalloc ImageViewHandle[Attachments.Count];
            for(uint i = 0; i < Attachments.Count; i++)
                attachmentPtr[i] = Attachments[(int)i];
            FrameBufferCreateInfo info = new FrameBufferCreateInfo(Renderpass, attachmentPtr, Size, Layers);
            using(Renderpass.WeakLock)
            {
                FrameBufferHandle handle;
                using(Renderpass.Device.Allocator?.WeakLock)
                    vkCreateFramebuffer(Renderpass.Device, &info, Renderpass.Device.Allocator, out handle).Throw();
                Handle = handle;
                Renderpass.Add(this);
            }
        }
        protected override void Destroy()
        {
            using(Renderpass.WeakLock)
                Renderpass.Remove(this);
            using(Renderpass.Device.Allocator?.WeakLock)
                vkDestroyFramebuffer(Renderpass.Device, this, Renderpass.Device.Allocator);
        }

        private readonly VkCreateFramebuffer vkCreateFramebuffer;
        private readonly VkDestroyFramebuffer vkDestroyFramebuffer;

        private delegate Result VkCreateFramebuffer(DeviceHandle device, FrameBufferCreateInfo* info, AllocationCallbacks* allocator, out FrameBufferHandle buffer);
        private delegate void VkDestroyFramebuffer(DeviceHandle device, FrameBufferHandle semaphore, AllocationCallbacks* allocator);
    }
}