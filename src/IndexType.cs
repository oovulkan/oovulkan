﻿// ReSharper disable InconsistentNaming

using System;

namespace oovulkan
{
    public enum IndexType : int
    {
        UINT16 = 0,
        UINT32 = 1
    }

    public static class IndexTypeHelper
    {
        public static uint Size(this IndexType indexType)
        {
            switch(indexType)
            {
                case IndexType.UINT16:
                    return 2;
                case IndexType.UINT32:
                    return 4;
                default:
                    throw new NotSupportedException();
            }
        }
    }
}