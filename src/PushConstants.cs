﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using oovulkan.util;

namespace oovulkan
{
    [AttributeUsage(AttributeTargets.Interface)]
    public class PushConstantsAttribute : DataInterfaceAttribute
    {
        public PushConstantsAttribute(bool isExplicitLayout=false) : base(isExplicitLayout) { }
    }
    [AttributeUsage(AttributeTargets.Property)]
    public class PushConstantAttribute : DataAttribute
    {
        public readonly ShaderStageFlags Stages;

        public PushConstantAttribute(ShaderStageFlags stages, uint offset=uint.MaxValue, [CallerLineNumber]int line=-1) : base(offset, line, false)
        {
            Stages = stages;
        }
    }

    public abstract class PushConstants : DataInterface
    {
        protected PushConstants(int size) : base(size) { }

        internal static IEnumerable<(ShaderStageFlags Stage, uint Offset, uint Size)> GetRanges(Type type)
        {
            if(type == null)
                return Enumerable.Empty<(ShaderStageFlags Stage, uint Offset, uint Size)>();
            Validate(type, typeof(PushConstants), typeof(PushConstantsAttribute), typeof(PushConstantAttribute));

            var ranges = new Dictionary<ShaderStageFlags, (uint Min, uint Max)>();
            foreach(var (info, offset, size) in GetProperties(type, typeof(PushConstantsAttribute), typeof(PushConstantAttribute)))
            {
                var attr = info.GetCustomAttribute<PushConstantAttribute>();
                foreach(var stage in Util.GetFlags(attr.Stages))
                {
                    var range = ranges.GetOrDefault(stage, (Min: uint.MaxValue, Max: uint.MinValue));
                    ranges[stage] = (Math.Min(offset, range.Min), Math.Max(offset + size, range.Max));
                }
            }

            var stageConsolidation = new Dictionary<(uint Min, uint Max), ShaderStageFlags>();
            foreach(var entry in ranges)
                stageConsolidation[entry.Value] = stageConsolidation.GetOrDefault(entry.Value) | entry.Key;

            return stageConsolidation.Select(entry => (entry.Value, entry.Key.Min, entry.Key.Max - entry.Key.Min));
        }

        public static T Implement<T>()
        {
            return Implement<T>(typeof(PushConstants), typeof(PushConstantsAttribute), typeof(PushConstantAttribute));
        }
    }
}