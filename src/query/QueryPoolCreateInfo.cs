﻿using System;
using System.Runtime.InteropServices;

// ReSharper disable PrivateFieldCanBeConvertedToLocalVariable

namespace oovulkan.query
{
    [Flags]
    internal enum QueryPoolCreateFlags : int
    {
        None = 0
    }

    [StructLayout(LayoutKind.Sequential)]
    internal unsafe struct QueryPoolCreateInfo
    {
        private readonly StructureType Type;
        private readonly void* Next;
        private readonly QueryPoolCreateFlags Flags;
        public readonly QueryType QueryType;
        public readonly uint Count;
        public readonly PipelineStatisticFlags Statistics;

        public QueryPoolCreateInfo(QueryType queryType, uint count, PipelineStatisticFlags statistics)
        {
            Type = StructureType.QueryPoolCreateInfo;
            Next = null;
            Flags = QueryPoolCreateFlags.None;
            QueryType = queryType;
            Count = count;
            Statistics = statistics;
        }
    }
}