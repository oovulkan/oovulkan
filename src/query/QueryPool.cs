﻿using System;
using System.Runtime.InteropServices;
using oovulkan.device;

#pragma warning disable 649

namespace oovulkan.query
{
    [StructLayout(LayoutKind.Sequential)]
    public struct QueryPoolHandle
    {
        private long Value;

        public static implicit operator QueryPoolHandle(long? value) => new QueryPoolHandle {Value = value ?? 0};
        public static implicit operator long(QueryPoolHandle handle) => handle.Value;
    }

    public struct Query
    {
        public readonly QueryPool Pool;
        public readonly uint Index;

        internal Query(QueryPool pool, uint index)
        {
            Pool = pool;
            Index = index;
        }

        public bool Equals(Query other) => this == other;
        public override bool Equals(object obj) => obj is Query && Equals((Query)obj);
        public static bool operator ==(Query left, Query right) => left.Pool == right.Pool && left.Index == right.Index;
        public static bool operator !=(Query left, Query right) => !(left == right);
        public override int GetHashCode()
        {
            unchecked
            {
                return ((Pool?.GetHashCode() ?? 0) * 397) ^ (int)Index;
            }
        }
    }

    public class PipelineStatisticQueryPool : QueryPool
    {
        public readonly PipelineStatisticFlags Statistics;
        protected override PipelineStatisticFlags statistics => Statistics;

        internal PipelineStatisticQueryPool(Device device, QueryType type, uint count, PipelineStatisticFlags statistics)
            : base(device, type, count)
        {
            Statistics = statistics;
        }
    }

    public unsafe class QueryPool : VulkanObject<QueryPoolHandle>
    {
        protected internal override bool IsChild => true;
        protected internal override VulkanObject Parent => Device;

        public readonly Device Device;
        public readonly QueryType Type;
        public readonly uint Count;

        public Query this[uint index]
        {
            get
            {
                if(index >= Count)
                    throw new ArgumentOutOfRangeException(nameof(index));
                return new Query(this, index);
            }
        }

        // ReSharper disable once InconsistentNaming
        protected virtual PipelineStatisticFlags statistics => default(PipelineStatisticFlags);

        internal QueryPool(Device device, QueryType type, uint count)
        {
            Device = device;
            this.Link(Device);
            Type = type;
            Count = count;
            Create();
        }

        private void Create()
        {
            var info = new QueryPoolCreateInfo(Type, Count, statistics);
            using(Device.WeakLock)
            {
                QueryPoolHandle handle;
                using(Device.Allocator?.WeakLock)
                    vkCreateQueryPool(Device, &info, Device.Allocator, out handle).Throw();
                Handle = handle;
                Device.Add(this);
            }
        }
        protected override void Destroy()
        {
            using(Device.WeakLock)
                Device.Remove(this);
            using(Device.Allocator?.WeakLock)
                vkDestroyQueryPool(Device, this, Device.Allocator);
        }

        private readonly VkCreateQueryPool vkCreateQueryPool;
        private readonly VkDestroyQueryPool vkDestroyQueryPool;

        private delegate Result VkCreateQueryPool(DeviceHandle device, QueryPoolCreateInfo* info, AllocationCallbacks* allocator, out QueryPoolHandle pool);
        private delegate void VkDestroyQueryPool(DeviceHandle device, QueryPoolHandle pool, AllocationCallbacks* allocator);
    }
}