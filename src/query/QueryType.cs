﻿namespace oovulkan.query
{
    public enum QueryType : int
    {
        Occlusion = 0,
        PipelineStatistics = 1,
        Timestamp = 2
    }
}