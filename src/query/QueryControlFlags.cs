﻿using System;

namespace oovulkan.query
{
    [Flags]
    public enum QueryControlFlags
    {
        None = 0,
        Precise = 1
    }
}