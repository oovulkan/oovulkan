﻿using System;

namespace oovulkan.query
{
    [Flags]
    public enum QueryResultFlags : int
    {
        None = 0,
        Is64 = 1,
        Wait = 2,
        WithAvailability = 4,
        Partial = 8
    }
}