﻿using System;
using oovulkan.queue;
using oovulkan.util;

namespace oovulkan.query
{
    [Flags]
    public enum PipelineStatisticFlags : int
    {
        None = 0,
        InputAssemblyVertices = 1,
        InputAssemblyPrimitives = 2,
        VertexShaderInvocations = 4,
        GeometryShaderInvocations = 8,
        GeometryShaderPrimitives = 16,
        ClippingInvocations = 32,
        ClippingPrimitives = 64,
        FragmentShaderInvocations = 128,
        TessellationControlShaderPatches = 256,
        TessellationEvaluationShaderInvocations = 512,
        ComputeShaderInvocations = 1024
    }

    public static class PipelineStatisticsHelper
    {
        public static QueueFlags GetQueueTypes(this PipelineStatisticFlags flags)
        {
            var types = QueueFlags.None;
            foreach(var flag in Util.GetFlags(flags))
            {
                switch(flag)
                {
                    case PipelineStatisticFlags.InputAssemblyVertices:
                    case PipelineStatisticFlags.InputAssemblyPrimitives:
                    case PipelineStatisticFlags.VertexShaderInvocations:
                    case PipelineStatisticFlags.GeometryShaderInvocations:
                    case PipelineStatisticFlags.GeometryShaderPrimitives:
                    case PipelineStatisticFlags.ClippingInvocations:
                    case PipelineStatisticFlags.ClippingPrimitives:
                    case PipelineStatisticFlags.FragmentShaderInvocations:
                    case PipelineStatisticFlags.TessellationControlShaderPatches:
                    case PipelineStatisticFlags.TessellationEvaluationShaderInvocations:
                        types |= QueueFlags.Graphics;
                        break;
                    case PipelineStatisticFlags.ComputeShaderInvocations:
                        types |= QueueFlags.Compute;
                        break;
                }
            }
            return types;
        }
    }
}