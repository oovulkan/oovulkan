﻿using System.Runtime.InteropServices;
using oovulkan.pipeline.graphics.assembly;
using oovulkan.pipeline.graphics.color;
using oovulkan.pipeline.graphics.depthstencil;
using oovulkan.pipeline.graphics.dynamic;
using oovulkan.pipeline.graphics.multisample;
using oovulkan.pipeline.graphics.rasterization;
using oovulkan.pipeline.graphics.tesselation;
using oovulkan.pipeline.graphics.vertex;
using oovulkan.pipeline.graphics.viewport;
using oovulkan.pipeline.layout;
using oovulkan.render;

// ReSharper disable PrivateFieldCanBeConvertedToLocalVariable

namespace oovulkan.pipeline.graphics
{
    [StructLayout(LayoutKind.Sequential)]
    internal unsafe struct GraphicsPipelineCreateInfo
    {
        private readonly StructureType Type;
        private readonly void* Next;
        public readonly PipelineCreateFlags Flags;
        public readonly uint StageCount;
        public readonly PipelineShaderStageCreateInfo* StagePtr;
        public readonly PipelineVertexInputStateCreateInfo* VertexInputState;
        public readonly PipelineInputAssemblyStateCreateInfo* InputAssemblyState;
        public readonly PipelineTessellationStateCreateInfo* TessellationState;
        public readonly PipelineViewportStateCreateInfo* ViewportState;
        public readonly PipelineRasterizationStateCreateInfo* RasterizationState;
        public readonly PipelineMultisampleStateCreateInfo* MultisampleState;
        public readonly PipelineDepthStencilStateCreateInfo* DepthStencilState;
        public readonly PipelineColorBlendStateCreateInfo* ColorBlendState;
        public readonly PipelineDynamicStateCreateInfo* DynamicState;
        public readonly PipelineLayoutHandle Layout;
        public readonly RenderpassHandle Renderpass;
        public readonly SubpassHandle Subpass;
        public readonly PipelineHandle BasePipelineHandle;
        public readonly int BasePipelineIndex;

        public GraphicsPipelineCreateInfo(PipelineCreateFlags flags, uint stageCount,
            PipelineShaderStageCreateInfo* stagePtr,
            PipelineVertexInputStateCreateInfo* vertexInputState,
            PipelineInputAssemblyStateCreateInfo* inputAssemblyState,
            PipelineTessellationStateCreateInfo* tessellationState,
            PipelineViewportStateCreateInfo* viewportState,
            PipelineRasterizationStateCreateInfo* rasterizationState,
            PipelineMultisampleStateCreateInfo* multisampleState,
            PipelineDepthStencilStateCreateInfo* depthStencilState,
            PipelineColorBlendStateCreateInfo* colorBlendState,
            PipelineDynamicStateCreateInfo* dynamicState,
            PipelineLayout layout, Renderpass renderpass,
            Subpass subpass, Pipeline basePipelineHandle, int basePipelineIndex)
        {
            Type = StructureType.GraphicsPipelineCreateInfo;
            Next = null;
            Flags = flags;
            StageCount = stageCount;
            StagePtr = stagePtr;
            VertexInputState = vertexInputState;
            InputAssemblyState = inputAssemblyState;
            TessellationState = tessellationState;
            ViewportState = viewportState;
            RasterizationState = rasterizationState;
            MultisampleState = multisampleState;
            DepthStencilState = depthStencilState;
            ColorBlendState = colorBlendState;
            DynamicState = dynamicState;
            Layout = layout;
            Renderpass = renderpass;
            Subpass = subpass;
            BasePipelineHandle = basePipelineHandle;
            BasePipelineIndex = basePipelineIndex;
        }
    }
}