﻿using System;
using System.Runtime.InteropServices;

// ReSharper disable PrivateFieldCanBeConvertedToLocalVariable

namespace oovulkan.pipeline.graphics.dynamic
{
    [Flags]
    internal enum PipelineDynamicStateCreateFlags : int
    {
        None = 0
    }

    [StructLayout(LayoutKind.Sequential)]
    internal unsafe struct PipelineDynamicStateCreateInfo
    {
        private readonly StructureType Type;
        private readonly void* Next;
        private readonly PipelineDynamicStateCreateFlags Flags;
        public readonly uint DynamicStateCount;
        public readonly DynamicState* DynamicStatePtr;

        public PipelineDynamicStateCreateInfo(uint dynamicStateCount, DynamicState* dynamicStatePtr)
        {
            Type = StructureType.PipelineDynamicStateCreateInfo;
            Next = null;
            Flags = PipelineDynamicStateCreateFlags.None;
            DynamicStateCount = dynamicStateCount;
            DynamicStatePtr = dynamicStatePtr;
        }
    }
}