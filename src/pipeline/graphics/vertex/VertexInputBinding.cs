﻿using System.Runtime.InteropServices;

namespace oovulkan.pipeline.graphics.vertex
{
    [StructLayout(LayoutKind.Sequential)]
    public struct VertexInputBindingHandle
    {
        private uint Value;

        public static implicit operator VertexInputBindingHandle(uint value) => new VertexInputBindingHandle {Value = value};
        public static implicit operator uint(VertexInputBindingHandle handle) => handle.Value;
    }

    public class VertexInputBinding
    {
        public readonly VertexInputBindingHandle Handle;
        public readonly uint Stride;
        public readonly VertexInputRate InputRate;

        internal VertexInputBinding(VertexInputBindingHandle handle, uint stride, VertexInputRate inputRate)
        {
            Handle = handle;
            Stride = stride;
            InputRate = inputRate;
        }

        public static implicit operator VertexInputBindingHandle(VertexInputBinding obj) => obj.Handle;
    }
}