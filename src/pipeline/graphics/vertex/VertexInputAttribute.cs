﻿using System.Runtime.InteropServices;

namespace oovulkan.pipeline.graphics.vertex
{
    [StructLayout(LayoutKind.Sequential)]
    public struct VertexInputAttributeHandle
    {
        private uint Value;

        public static implicit operator VertexInputAttributeHandle(uint value) => new VertexInputAttributeHandle {Value = value};
        public static implicit operator uint(VertexInputAttributeHandle handle) => handle.Value;
    }

    public class VertexInputAttribute
    {
        public readonly VertexInputAttributeHandle Handle;
        public readonly VertexInputBinding Binding;
        public readonly Format Format;
        public readonly uint Offset;

        public VertexInputAttribute(VertexInputAttributeHandle handle, VertexInputBinding binding, Format format, uint offset)
        {
            Handle = handle;
            Binding = binding;
            Format = format;
            Offset = offset;
        }

        public static implicit operator VertexInputAttributeHandle(VertexInputAttribute obj) => obj.Handle;
    }
}