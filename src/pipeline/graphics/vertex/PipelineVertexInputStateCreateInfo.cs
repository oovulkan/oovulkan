﻿using System;
using System.Runtime.InteropServices;

// ReSharper disable PrivateFieldCanBeConvertedToLocalVariable

namespace oovulkan.pipeline.graphics.vertex
{
    [Flags]
    internal enum PipelineVertexInputStateCreateFlags : int
    {
        None = 0
    }

    [StructLayout(LayoutKind.Sequential)]
    internal struct VertexInputBindingDescription
    {
        public readonly VertexInputBindingHandle Binding;
        public readonly uint Stride;
        public readonly VertexInputRate InputRate;

        private VertexInputBindingDescription(VertexInputBindingHandle binding, uint stride, VertexInputRate inputRate)
        {
            Binding = binding;
            Stride = stride;
            InputRate = inputRate;
        }

        public static implicit operator VertexInputBindingDescription(VertexInputBinding binding)
            => new VertexInputBindingDescription(binding.Handle, binding.Stride, binding.InputRate);
    }

    [StructLayout(LayoutKind.Sequential)]
    internal struct VertexInputAttributeDescription
    {
        public readonly VertexInputAttributeHandle Location;
        public readonly VertexInputBindingHandle Binding;
        public readonly Format Format;
        public readonly uint Offset;

        private VertexInputAttributeDescription(VertexInputAttributeHandle location, VertexInputBinding binding, Format format, uint offset)
        {
            Location = location;
            Binding = binding.Handle;
            Format = format;
            Offset = offset;
        }

        public static implicit operator VertexInputAttributeDescription(VertexInputAttribute attr)
            => new VertexInputAttributeDescription(attr.Handle, attr.Binding, attr.Format, attr.Offset);
    }

    [StructLayout(LayoutKind.Sequential)]
    internal unsafe struct PipelineVertexInputStateCreateInfo
    {
        private readonly StructureType Type;
        private readonly void* Next;
        private readonly PipelineVertexInputStateCreateFlags Flags;
        public readonly uint BindingCount;
        public readonly VertexInputBindingDescription* BindingPtr;
        public readonly uint AttributeCountl;
        public readonly VertexInputAttributeDescription* AttributePtr;

        public PipelineVertexInputStateCreateInfo(uint bindingCount, VertexInputBindingDescription* bindingPtr, uint attributeCountl, VertexInputAttributeDescription* attributePtr)
        {
            Type = StructureType.PipelineVertexInputStateCreateInfo;
            Next = null;
            Flags = PipelineVertexInputStateCreateFlags.None;
            BindingCount = bindingCount;
            BindingPtr = bindingPtr;
            AttributeCountl = attributeCountl;
            AttributePtr = attributePtr;
        }
    }
}