﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using oovulkan.device.physical;
using oovulkan.pipeline.graphics.assembly;
using oovulkan.pipeline.graphics.color;
using oovulkan.pipeline.graphics.depthstencil;
using oovulkan.pipeline.graphics.dynamic;
using oovulkan.pipeline.graphics.rasterization;
using oovulkan.pipeline.graphics.viewport;
using oovulkan.shader;
using oovulkan.util;
using oovulkan.util.collection;

namespace oovulkan.pipeline.graphics.vertex
{
    public enum VertexInputRate : int
    {
        Vertex = 0,
        Instance = 1
    }

    public class VertexInputState
    {
        public readonly ReadonlyList<VertexInputBinding> Bindings;
        public readonly ReadonlyList<VertexInputAttribute> Attributes;

        private VertexInputState(){ }

        //Building "constructor"
        // ReSharper disable once UnusedMember.Local
        private VertexInputState(IEnumerable<VertexInputBinding> bindings, IEnumerable<VertexInputAttribute> attributes)
        {
            Bindings = bindings.ToList();
            Attributes = attributes.ToList();
        }

        public class Builder : BuilderBase<VertexInputState>
        {
            private static readonly ConstructorInfo Sneaky = typeof(VertexInputState).GetAnyConstructor(
                typeof(IEnumerable<VertexInputBinding>),
                typeof(IEnumerable<VertexInputAttribute>)
            );
            private readonly GraphicsPipeline.Builder builder;
            private readonly HashSet<VertexInputBinding> bindings = new HashSet<VertexInputBinding>();
            private readonly List<VertexInputAttribute> attributes = new List<VertexInputAttribute>();

            internal Builder(GraphicsPipeline.Builder builder, out VertexInputState vertexInputState) : base(new VertexInputState())
            {
                this.builder = builder;
                vertexInputState = Object;
            }

            public Builder Binding(out VertexInputBinding binding, uint stride, VertexInputRate rate)
            {
                AssertNotDone();
                var limits = builder.Object.Device.PhysicalDevice.Limits;
                if(bindings.Count >= limits.MaxVertexInputBindings)
                    throw new IndexOutOfRangeException();
                if(stride > limits.MaxVertexInputBindingStride)
                    throw new ArgumentException(nameof(stride));
                binding = new VertexInputBinding((uint)bindings.Count, stride, rate);
                bindings.Add(binding);
                return this;
            }

            public Builder Attribute(VertexInputBinding binding, Format format, uint offset)
            {
                AssertNotDone();
                if(!bindings.Contains(binding))
                    throw new ArgumentException(nameof(binding));
                var limits = builder.Object.Device.PhysicalDevice.Limits;
                if(attributes.Count >= limits.MaxVertexInputAttributes)
                    throw new IndexOutOfRangeException();
                if(offset > limits.MaxVertexInputAttributeOffset)
                    throw new ArgumentException(nameof(offset));
                if(!builder.Object.Device.PhysicalDevice.FormatProperties(format).BufferFeatures.HasFlag(FormatFeatureFlags.VertexBuffer))
                    throw new NotSupportedException(nameof(format));
                var attr = new VertexInputAttribute((uint)attributes.Count, binding, format, offset);
                attributes.Add(attr);
                return this;
            }

            private void Done()
            {
                AssertNotDone();
                MarkDone();
                Sneaky.Invoke(Object, bindings.OrderBy(binding => (uint)binding.Handle), attributes);
            }

            public GraphicsPipeline.Builder Stage(ShaderStageFlags stage, ShaderModule shader, string entryPoint, object specialization=null)
            {
                Done();
                return builder.Stage(stage, shader, entryPoint, specialization);
            }
            public Builder VertexInput()
            {
                Done();
                return builder.VertexInput();
            }
            public GraphicsPipeline.Builder InputAssembly(PrimitiveTopology topology, bool primitiveRestartEnable)
            {
                Done();
                return builder.InputAssembly(topology, primitiveRestartEnable);
            }
            public GraphicsPipeline.Builder Tesselation(uint patchControlPoints)
            {
                Done();
                return builder.Tesselation(patchControlPoints);
            }
            public ViewportState.ViewportBuilder Viewport(Viewport viewport)
            {
                Done();
                return builder.Viewport(viewport);
            }
            public ViewportState.ScissorBuilder Viewport(Rect2D scissor)
            {
                Done();
                return builder.Viewport(scissor);
            }
            public ViewportState.ViewportScissorBuilder Viewport(Viewport viewport, Rect2D scissor)
            {
                Done();
                return builder.Viewport(viewport, scissor);
            }
            public GraphicsPipeline.Builder Rasterization(bool depthClampEnable, bool rasterizerDiscardEnable, PolygonMode polygonMode, CullModeFlags cullMode, FrontFace frontFace, bool depthBiasEnable, float depthBiasConstantFactor, float depthBiasClamp, float depthBiasSlopeFactor, float lineWidth)
            {
                Done();
                return builder.Rasterization(depthClampEnable, rasterizerDiscardEnable, polygonMode, cullMode, frontFace, depthBiasEnable, depthBiasConstantFactor, depthBiasClamp, depthBiasSlopeFactor, lineWidth);
            }
            public GraphicsPipeline.Builder Multisample(SampleCountFlags rasterizationSamples, bool sampleShadingEnable, float minSampleShading, bool alphaToCoverageEnable, bool alphaToOneEnable, params uint[] masks)
            {
                Done();
                return builder.Multisample(rasterizationSamples, sampleShadingEnable, minSampleShading, alphaToCoverageEnable, alphaToOneEnable, masks);
            }
            public GraphicsPipeline.Builder DepthStencil(bool depthTestEnable, bool depthWriteEnable, CompareOp depthCompareOp, bool depthBoundsTestEnable, bool stencilTestEnable, StencilOpState front, StencilOpState back, (float Min, float Max)? depthBounds=null)
            {
                Done();
                return builder.DepthStencil(depthTestEnable, depthWriteEnable, depthCompareOp, depthBoundsTestEnable, stencilTestEnable, front, back, depthBounds);
            }
            public ColorBlendState.Builder ColorBlend((float R, float G, float B, float A) blendConstants, LogicOp? logicOp = null)
            {
                Done();
                return builder.ColorBlend(blendConstants, logicOp);
            }
            public GraphicsPipeline.Builder Dynamic(params DynamicState[] states)
            {
                Done();
                return builder.Dynamic(states);
            }
        }
    }
}