﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using oovulkan.device;
using oovulkan.device.physical;
using oovulkan.image;
using oovulkan.pipeline.cache;
using oovulkan.pipeline.graphics.assembly;
using oovulkan.pipeline.graphics.color;
using oovulkan.pipeline.graphics.depthstencil;
using oovulkan.pipeline.graphics.dynamic;
using oovulkan.pipeline.graphics.multisample;
using oovulkan.pipeline.graphics.rasterization;
using oovulkan.pipeline.graphics.tesselation;
using oovulkan.pipeline.graphics.vertex;
using oovulkan.pipeline.graphics.viewport;
using oovulkan.pipeline.layout;
using oovulkan.render;
using oovulkan.shader;
using oovulkan.util;
using oovulkan.util.collection;

#pragma warning disable 649

namespace oovulkan.pipeline.graphics
{
    public unsafe class GraphicsPipeline : Pipeline
    {
        public readonly Renderpass Renderpass;
        public readonly Subpass Subpass;
        public readonly VertexInputState VertexInputState;
        public readonly InputAssemblyState InputAssemblyState;
        public readonly TesselationState TesselationState;
        public readonly ViewportState ViewportState;
        public readonly RasterizationState RasterizationState;
        public readonly MultisampleState MultisampleState;
        public readonly DepthStencilState DepthStencilState;
        public readonly ColorBlendState ColorBlendState;
        public readonly ReadonlySet<DynamicState> DynamicStates;

        private GraphicsPipeline(Device device, PipelineCreateFlags flags, PipelineLayout layout, Subpass subpass, Pipeline basePipeline)
            : base(device, PipelineBindPoint.Graphics, flags, layout, basePipeline)
        {
            Renderpass = subpass.Renderpass;
            Subpass = subpass;
        }

        //Building "constructor"
        // ReSharper disable once UnusedMember.Local
        private GraphicsPipeline(PipelineCache cache, IEnumerable<PipelineStage> stages, VertexInputState vertexInputState,
            InputAssemblyState inputAssemblyState, TesselationState tesselationState, ViewportState viewportState,
            RasterizationState rasterizationState, MultisampleState multisampleState, DepthStencilState depthStencilState,
            ColorBlendState colorBlendState, IEnumerable<DynamicState> dynamicStates)
            : base(stages)
        {
            VertexInputState = vertexInputState;
            InputAssemblyState = inputAssemblyState;
            TesselationState = tesselationState;
            ViewportState = viewportState;
            RasterizationState = rasterizationState;
            MultisampleState = multisampleState;
            DepthStencilState = depthStencilState;
            ColorBlendState = colorBlendState;
            DynamicStates = dynamicStates?.ToSet() ?? new HashSet<DynamicState>();
            Create(cache);
        }

        private void Create(PipelineCache cache)
        {
            #region Validation

            Validate(cache);

            if(Stages.Count == 0)
                throw new ArgumentException();

            var stages = ShaderStageFlags.None;
            foreach(var stage in Stages)
                stages |= stage.Stage;
            if(!stages.HasFlag(ShaderStageFlags.Vertex))
                throw new ArgumentException();
            if(stages.HasFlag(ShaderStageFlags.TessallationControl))
            {
                if(!stages.HasFlag(ShaderStageFlags.TessallationEvaluation))
                    throw new ArgumentException();
                if(TesselationState == null)
                    throw new ArgumentException();
                if(InputAssemblyState.Topology != PrimitiveTopology.PatchList)
                    throw new ArgumentException();
            }
            else
            {
                if(InputAssemblyState.Topology == PrimitiveTopology.PatchList)
                    throw new ArgumentException();
            }

            //TODO: rasterization enabled?
            if(DepthStencilState != null && Subpass.DepthStencil?.Layout == ImageLayout.DepthStencilReadOnlyOptimal)
            {
                if(DepthStencilState.DepthWriteEnable)
                    throw new ArgumentException();
                if(DepthStencilState.Front.FailOp != StencilOp.Keep || DepthStencilState.Front.PassOp != StencilOp.Keep || DepthStencilState.Front.DepthFailOp != StencilOp.Keep
                   || DepthStencilState.Back.FailOp != StencilOp.Keep || DepthStencilState.Back.PassOp != StencilOp.Keep || DepthStencilState.Back.DepthFailOp != StencilOp.Keep)
                    throw new ArgumentException();
            }
            if(ColorBlendState != null)
            {
                if(Subpass.Colors.Count != ColorBlendState.Attachments.Count)
                    throw new ArgumentException();
                for(uint i = 0; i < Subpass.Colors.Count; i++)
                {
                    var attachment = ColorBlendState.Attachments[(int)i];
                    if(!attachment.BlendEnable)
                        continue;
                    var reference = Subpass.Colors[(int)i];
                    var props = Device.PhysicalDevice.FormatProperties(reference.Attachment.Format);
                    if(!props.LinearTilingFeatures.HasFlag(FormatFeatureFlags.ColorAttachmentBlend) &&
                       !props.OptimalTilingFeatures.HasFlag(FormatFeatureFlags.ColorAttachmentBlend))
                        throw new NotSupportedException();
                }
            }

            if(ViewportState != null)
            {
                if(DynamicStates.Contains(DynamicState.Viewport) != (ViewportState.Viewports.Count == 0))
                    throw new ArgumentException();
                if(DynamicStates.Contains(DynamicState.Scissor) != (ViewportState.Scissors.Count == 0))
                    throw new ArgumentException();
            }
            else
            {
                if(DynamicStates.Contains(DynamicState.Viewport) || DynamicStates.Contains(DynamicState.Scissor))
                    throw new ArgumentException();
            }

            // ReSharper disable once CompareOfFloatsByEqualityOperator
            if(!Device.Features.WideLines && !DynamicStates.Contains(DynamicState.LineWidth) && RasterizationState.LineWidth != 1)
                throw new ArgumentException();
            if(!RasterizationState.RasterizerDiscardEnable)
            {
                if(ViewportState == null || MultisampleState == null)
                    throw new ArgumentException();
                if(Subpass.DepthStencil != null && DepthStencilState == null)
                    throw new ArgumentException();
                if(Subpass.Colors.Count > 0 && ColorBlendState == null)
                    throw new ArgumentException();
            }
            // ReSharper disable once CompareOfFloatsByEqualityOperator
            if(!Device.Features.DepthBiasClamp && !DynamicStates.Contains(DynamicState.DepthBias)
               && RasterizationState.DepthBiasEnable && RasterizationState.DepthBiasClamp != 0)
                throw new NotSupportedException();
            if(DepthStencilState != null)
            {
                if(DepthStencilState.DepthBoundsTestEnable)
                {
                    if(DynamicStates.Contains(DynamicState.DepthBounds))
                    {
                        if(!float.IsNaN(DepthStencilState.DepthBounds.Min) || !float.IsNaN(DepthStencilState.DepthBounds.Max))
                            throw new ArgumentException();
                    }
                    else
                    {
                        if(float.IsNaN(DepthStencilState.DepthBounds.Min) || float.IsNaN(DepthStencilState.DepthBounds.Max))
                            throw new ArgumentException();
                    }
                }
            }
            if(MultisampleState != null)
            {
                if(Subpass.Colors.Any(reference => reference.Attachment.Samples != MultisampleState.RasterizationSamples)
                   || (Subpass.DepthStencil != null && Subpass.DepthStencil?.Attachment.Samples != MultisampleState.RasterizationSamples))
                    throw new ArgumentException();
            }
            //TODO: If subpass does not use any color and/or depth/stencil attachments, then the rasterizationSamples member of pMultisampleState must follow the rules for a zero-attachment subpass

            #endregion

            UsingStageInfos((stageCount, stageInfos) =>
            {
                #region VertexInput
                var bindings = stackalloc VertexInputBindingDescription[VertexInputState.Bindings.Count];
                for(int i = 0; i < VertexInputState.Bindings.Count; i++)
                    bindings[i] = VertexInputState.Bindings[i];
                var attributes = stackalloc VertexInputAttributeDescription[VertexInputState.Attributes.Count];
                for(int i = 0; i < VertexInputState.Attributes.Count; i++)
                    attributes[i] = VertexInputState.Attributes[i];
                var vertexInputInfo = new PipelineVertexInputStateCreateInfo(
                    (uint)VertexInputState.Bindings.Count, bindings,
                    (uint)VertexInputState.Attributes.Count, attributes
                );
                #endregion
                #region InputAssembly
                var inputAssemblyInfo = (PipelineInputAssemblyStateCreateInfo)InputAssemblyState;
                #endregion
                #region Tessellation
                PipelineTessellationStateCreateInfo* tessellationInfoPtr = null;
                if(TesselationState != null)
                {
                    var ptr = stackalloc PipelineTessellationStateCreateInfo[1];
                    tessellationInfoPtr = ptr;
                    tessellationInfoPtr[0] = TesselationState;
                }
                #endregion
                #region Viewport
                PipelineViewportStateCreateInfo* viewportInfoPtr = null;
                if(ViewportState != null)
                {
                    var viewports = stackalloc Viewport[ViewportState.Viewports.Count];
                    for(int i = 0; i < ViewportState.Viewports.Count; i++)
                        viewports[i] = ViewportState.Viewports[i];
                    var scissors = stackalloc Rect2D[ViewportState.Scissors.Count];
                    for(int i = 0; i < ViewportState.Scissors.Count; i++)
                        scissors[i] = ViewportState.Scissors[i];

                    var ptr = stackalloc PipelineViewportStateCreateInfo[1];
                    viewportInfoPtr = ptr;
                    viewportInfoPtr[0] = new PipelineViewportStateCreateInfo(
                        (uint)Math.Max(ViewportState.Viewports.Count, ViewportState.Scissors.Count),
                        viewports, scissors
                    );
                }
                #endregion
                #region Rasterization
                var rasterizationInfo = (PipelineRasterizationStateCreateInfo)RasterizationState;
                #endregion
                #region Multisample
                PipelineMultisampleStateCreateInfo* multisampleInfoPtr = null;
                if(MultisampleState != null)
                {
                    var masks = stackalloc uint[MultisampleState.SampleMasks.Count];
                    for(int i = 0; i < MultisampleState.SampleMasks.Count; i++)
                        masks[i] = MultisampleState.SampleMasks[i];

                    var ptr = stackalloc PipelineMultisampleStateCreateInfo[1];
                    multisampleInfoPtr = ptr;
                    multisampleInfoPtr[0] = new PipelineMultisampleStateCreateInfo(MultisampleState.RasterizationSamples,
                        MultisampleState.SampleShadingEnable, MultisampleState.MinSampleShading, masks,
                        MultisampleState.AlphaToCoverageEnable, MultisampleState.AlphaToOneEnable
                    );
                }
                #endregion
                #region DepthStencil
                PipelineDepthStencilStateCreateInfo* depthStencilInfoPtr = null;
                if(DepthStencilState != null)
                {
                    var ptr = stackalloc PipelineDepthStencilStateCreateInfo[1];
                    depthStencilInfoPtr = ptr;
                    depthStencilInfoPtr[0] = DepthStencilState;
                }
                #endregion
                #region ColorBlend
                PipelineColorBlendStateCreateInfo* colorBlendInfoPtr = null;
                if(ColorBlendState != null)
                {
                    var attachments = stackalloc PipelineColorBlendAttachmentState[ColorBlendState.Attachments.Count];
                    for(int i = 0; i < ColorBlendState.Attachments.Count; i++)
                        attachments[i] = ColorBlendState.Attachments[i];

                    var ptr = stackalloc PipelineColorBlendStateCreateInfo[1];
                    colorBlendInfoPtr = ptr;
                    colorBlendInfoPtr[0] = new PipelineColorBlendStateCreateInfo(ColorBlendState.LogicOpEnable,
                        ColorBlendState.LogicOp, (uint)ColorBlendState.Attachments.Count, attachments,
                        ColorBlendState.BlendConstants);
                }
                #endregion
                #region DynamicInfoPtr
                PipelineDynamicStateCreateInfo* dynamicInfoPtr = null;
                if(DynamicStates.Count != 0)
                {
                    var states = stackalloc DynamicState[DynamicStates.Count];
                    {
                        int i = 0;
                        foreach(var state in DynamicStates)
                            states[i++] = state;
                    }

                    var ptr = stackalloc PipelineDynamicStateCreateInfo[1];
                    dynamicInfoPtr = ptr;
                    dynamicInfoPtr[0] = new PipelineDynamicStateCreateInfo((uint)DynamicStates.Count, states);
                }
                #endregion

                var info = new GraphicsPipelineCreateInfo(Flags, stageCount, stageInfos,
                    &vertexInputInfo, &inputAssemblyInfo, tessellationInfoPtr, viewportInfoPtr,
                    &rasterizationInfo, multisampleInfoPtr, depthStencilInfoPtr, colorBlendInfoPtr, dynamicInfoPtr,
                    Layout, Renderpass, Subpass, BasePipeline, -1);
                using(Device.WeakLock)
                {
                    PipelineHandle handle;
                    using(Device.Allocator?.WeakLock)
                        vkCreateGraphicsPipelines(Device, cache, 1, &info, Device.Allocator, &handle).Throw();
                    Handle = handle;
                    Device.Add(this);
                }
            });
        }

        private readonly VkCreateGraphicsPipelines vkCreateGraphicsPipelines;

        private delegate Result VkCreateGraphicsPipelines(DeviceHandle device, PipelineCacheHandle cache, uint count, GraphicsPipelineCreateInfo* infos, AllocationCallbacks* allocator, PipelineHandle* pipelines);

        public class Builder : BuilderBase<GraphicsPipeline>
        {
            private static readonly ConstructorInfo Sneaky = typeof(GraphicsPipeline).GetAnyConstructor(
                typeof(PipelineCache), typeof(IEnumerable<PipelineStage>), typeof(VertexInputState),
                typeof(InputAssemblyState), typeof(TesselationState), typeof(ViewportState),
                typeof(RasterizationState), typeof(MultisampleState), typeof(DepthStencilState),
                typeof(ColorBlendState), typeof(IEnumerable<DynamicState>)
            );

            private readonly PipelineCache cache;
            private readonly List<PipelineStage> stages = new List<PipelineStage>();
            private ShaderStageFlags currentStages = ShaderStageFlags.None;
            private VertexInputState vertexInputState;
            private InputAssemblyState inputAssemblyState;
            private TesselationState tesselationState;
            private ViewportState viewportState;
            private RasterizationState rasterizationState;
            private MultisampleState multisampleState;
            private DepthStencilState depthStencilState;
            private ColorBlendState colorBlendState;
            private HashSet<DynamicState> dynamicStates;

            internal Builder(Device device, PipelineCreateFlags flags, PipelineLayout layout, Subpass subpass, Pipeline basePipeline, PipelineCache cache)
                : base(new GraphicsPipeline(device, flags, layout, subpass, basePipeline))
            {
                this.cache = cache;
            }

            public Builder Stage(ShaderStageFlags stage, ShaderModule shader, string entryPoint, object specialization=null)
            {
                AssertNotDone();
                if(shader.Device != Object.Device)
                    throw new ArgumentException(nameof(shader));
                if(specialization != null && !(specialization is SpecConstants))
                    throw new ArgumentException(nameof(specialization));
                if(!Object.Device.Features.GeometryShader && stage == ShaderStageFlags.Geometery)
                    throw new ArgumentException(nameof(stage));
                if(!Object.Device.Features.TessellationShader && (stage == ShaderStageFlags.TessallationControl || stage == ShaderStageFlags.TessallationEvaluation))
                    throw new ArgumentException(nameof(stage));
                if(stage == ShaderStageFlags.AllGraphics || stage == ShaderStageFlags.All || stage == ShaderStageFlags.Compute)
                    throw new ArgumentException(nameof(stage));
                if((currentStages & stage) != 0)
                    throw new ArgumentException(nameof(stage));
                currentStages |= stage;
                stages.Add((stage, shader, entryPoint, (SpecConstants)specialization));
                return this;
            }
            public VertexInputState.Builder VertexInput()
            {
                AssertNotDone();
                return new VertexInputState.Builder(this, out vertexInputState);
            }
            public Builder InputAssembly(PrimitiveTopology topology, bool primitiveRestartEnable)
            {
                AssertNotDone();
                inputAssemblyState = new InputAssemblyState(topology, primitiveRestartEnable);
                return this;
            }
            public Builder Tesselation(uint patchControlPoints)
            {
                AssertNotDone();
                if(patchControlPoints <= 0 || patchControlPoints > Object.Device.PhysicalDevice.Limits.MaxTessellationPatchSize)
                    throw new ArgumentOutOfRangeException(nameof(patchControlPoints));
                tesselationState = new TesselationState(patchControlPoints);
                return this;
            }
            public ViewportState.ViewportBuilder Viewport(Viewport viewport)
            {
                return new ViewportState.ViewportBuilder(this, out viewportState).Viewport(viewport);
            }
            public ViewportState.ScissorBuilder Viewport(Rect2D scissor)
            {
                return new ViewportState.ScissorBuilder(this, out viewportState).Viewport(scissor);
            }
            public ViewportState.ViewportScissorBuilder Viewport(Viewport viewport, Rect2D scissor)
            {
                return new ViewportState.ViewportScissorBuilder(this, out viewportState).Viewport(viewport, scissor);
            }
            public Builder Rasterization(bool depthClampEnable, bool rasterizerDiscardEnable, PolygonMode polygonMode, CullModeFlags cullMode, FrontFace frontFace, bool depthBiasEnable, float depthBiasConstantFactor, float depthBiasClamp, float depthBiasSlopeFactor, float lineWidth)
            {
                AssertNotDone();
                if(depthClampEnable && !Object.Device.Features.DepthClamp)
                    throw new NotSupportedException(nameof(depthBiasEnable));
                if(polygonMode != PolygonMode.Fill && !Object.Device.Features.FillModeNonSolid)
                    throw new NotSupportedException(nameof(polygonMode));
                rasterizationState = new RasterizationState(depthClampEnable, rasterizerDiscardEnable, polygonMode, cullMode,
                    frontFace, depthBiasEnable, depthBiasConstantFactor, depthBiasClamp, depthBiasSlopeFactor, lineWidth);
                return this;
            }
            public Builder Multisample(SampleCountFlags rasterizationSamples, bool sampleShadingEnable, float minSampleShading=1, bool alphaToCoverageEnable=false, bool alphaToOneEnable=false, params uint[] masks)
            {
                AssertNotDone();
                if(sampleShadingEnable && !Object.Device.Features.SampleRateShading)
                    throw new NotSupportedException(nameof(sampleShadingEnable));
                if(alphaToOneEnable && !Object.Device.Features.AlphaToOne)
                    throw new NotSupportedException(nameof(alphaToOneEnable));
                if(minSampleShading < 0 || minSampleShading > 1)
                    throw new ArgumentOutOfRangeException(nameof(minSampleShading));
                if(masks != null && masks.Length != 0 && masks.LongLength != (uint)Math.Ceiling((uint)rasterizationSamples / 32D))
                    throw new ArgumentException(nameof(masks));
                multisampleState = new MultisampleState(rasterizationSamples, sampleShadingEnable, minSampleShading, masks, alphaToCoverageEnable, alphaToOneEnable);
                return this;
            }
            public Builder DepthStencil(bool depthTestEnable, bool depthWriteEnable, CompareOp depthCompareOp, bool depthBoundsTestEnable, bool stencilTestEnable, StencilOpState front, StencilOpState back, (float Min, float Max)? depthBounds=null)
            {
                AssertNotDone();
                if(depthWriteEnable && !depthTestEnable)
                    throw new ArgumentException(nameof(depthWriteEnable));
                if(depthBoundsTestEnable)
                {
                    if(!Object.Device.Features.DepthBounds)
                        throw new NotSupportedException(nameof(depthBoundsTestEnable));
                    if(depthBounds != null)
                    {
                        var bounds = depthBounds.Value;
                        if(bounds.Min < 0 || bounds.Min > 1 || bounds.Max < 0 || bounds.Max > 1)
                            throw new ArgumentOutOfRangeException(nameof(depthBounds));
                    }
                }
                else
                {
                    if(depthBounds != null)
                        throw new ArgumentException(nameof(depthBounds));
                }
                depthStencilState = new DepthStencilState(depthTestEnable, depthWriteEnable, depthCompareOp,
                    depthBoundsTestEnable, stencilTestEnable, front, back,
                    depthBounds ?? (float.NaN, float.NaN));
                return this;
            }
            public ColorBlendState.Builder ColorBlend((float R, float G, float B, float A) blendConstants, LogicOp? logicOp = null)
            {
                AssertNotDone();
                if(logicOp != null && !Object.Device.Features.LogicOp)
                    throw new NotSupportedException(nameof(logicOp));
                return new ColorBlendState.Builder(this, logicOp != null, logicOp ?? default(LogicOp), blendConstants, out colorBlendState);
            }
            public Builder Dynamic(params DynamicState[] states)
            {
                AssertNotDone();
                if(states.LongLength == 0)
                    throw new ArgumentException(nameof(states));
                var set = new HashSet<DynamicState>(states);
                if(set.Count != states.LongLength)
                    throw new ArgumentException(nameof(states));
                dynamicStates = set;
                return this;
            }

            public GraphicsPipeline Done()
            {
                AssertNotDone();
                MarkDone();
                Sneaky.Invoke(Object, cache, stages, vertexInputState, inputAssemblyState, tesselationState, viewportState,
                    rasterizationState, multisampleState, depthStencilState, colorBlendState, dynamicStates);
                return Object;
            }
        }
    }
}