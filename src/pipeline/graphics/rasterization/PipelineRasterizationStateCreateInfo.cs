﻿using System;
using System.Runtime.InteropServices;
using util;

// ReSharper disable PrivateFieldCanBeConvertedToLocalVariable

namespace oovulkan.pipeline.graphics.rasterization
{
    [Flags]
    internal enum PipelineRasterizationStateCreateFlags : int
    {
        None = 0
    }

    [StructLayout(LayoutKind.Sequential)]
    internal unsafe struct PipelineRasterizationStateCreateInfo
    {
        private readonly StructureType Type;
        private readonly void* Next;
        private readonly PipelineRasterizationStateCreateFlags Flags;
        public readonly Bool DepthClampEnable;
        public readonly Bool RasterizerDiscardEnable;
        public readonly PolygonMode PolygonMode;
        public readonly CullModeFlags CullMode;
        public readonly FrontFace FrontFace;
        public readonly Bool DepthBiasEnable;
        public readonly float DepthBiasConstantFactor;
        public readonly float DepthBiasClamp;
        public readonly float DepthBiasSlopeFactor;
        public readonly float LineWidth;

        private PipelineRasterizationStateCreateInfo(bool depthClampEnable, bool rasterizerDiscardEnable, PolygonMode polygonMode, CullModeFlags cullMode, FrontFace frontFace, bool depthBiasEnable, float depthBiasConstantFactor, float depthBiasClamp, float depthBiasSlopeFactor, float lineWidth)
        {
            Type = StructureType.PipelineRasterizationStateCreateInfo;
            Next = null;
            Flags = PipelineRasterizationStateCreateFlags.None;
            DepthClampEnable = depthClampEnable;
            RasterizerDiscardEnable = rasterizerDiscardEnable;
            PolygonMode = polygonMode;
            CullMode = cullMode;
            FrontFace = frontFace;
            DepthBiasEnable = depthBiasEnable;
            DepthBiasConstantFactor = depthBiasConstantFactor;
            DepthBiasClamp = depthBiasClamp;
            DepthBiasSlopeFactor = depthBiasSlopeFactor;
            LineWidth = lineWidth;
        }

        public static implicit operator PipelineRasterizationStateCreateInfo(RasterizationState state)
            => new PipelineRasterizationStateCreateInfo(state.DepthClampEnable, state.RasterizerDiscardEnable,
                state.PolygonMode, state.CullMode, state.FrontFace, state.DepthBiasEnable, state.DepthBiasConstantFactor,
                state.DepthBiasClamp, state.DepthBiasSlopeFactor, state.LineWidth);
    }
}