﻿using System;

namespace oovulkan.pipeline.graphics.rasterization
{
    public enum PolygonMode : int
    {
        Fill = 0,
        Line = 1,
        Point = 2
    }

    [Flags]
    public enum CullModeFlags : int
    {
        None = 0,
        Front = 1,
        Back = 2,
        FrontBack = 3
    }

    public enum FrontFace : int
    {
        CounterClockwise = 0,
        Clockwise = 1
    }

    public class RasterizationState
    {
        public readonly bool DepthClampEnable;
        public readonly bool RasterizerDiscardEnable;
        public readonly PolygonMode PolygonMode;
        public readonly CullModeFlags CullMode;
        public readonly FrontFace FrontFace;
        public readonly bool DepthBiasEnable;
        public readonly float DepthBiasConstantFactor;
        public readonly float DepthBiasClamp;
        public readonly float DepthBiasSlopeFactor;
        public readonly float LineWidth;

        public RasterizationState(bool depthClampEnable, bool rasterizerDiscardEnable, PolygonMode polygonMode,
            CullModeFlags cullMode, FrontFace frontFace, bool depthBiasEnable, float depthBiasConstantFactor,
            float depthBiasClamp, float depthBiasSlopeFactor, float lineWidth)
        {
            DepthClampEnable = depthClampEnable;
            RasterizerDiscardEnable = rasterizerDiscardEnable;
            PolygonMode = polygonMode;
            CullMode = cullMode;
            FrontFace = frontFace;
            DepthBiasEnable = depthBiasEnable;
            DepthBiasConstantFactor = depthBiasConstantFactor;
            DepthBiasClamp = depthBiasClamp;
            DepthBiasSlopeFactor = depthBiasSlopeFactor;
            LineWidth = lineWidth;
        }
    }
}