﻿using System;
using System.Runtime.InteropServices;
using util;

// ReSharper disable PrivateFieldCanBeConvertedToLocalVariable

namespace oovulkan.pipeline.graphics.multisample
{
    [Flags]
    internal enum PipelineMultisampleStateCreateFlags : int
    {
        None = 0
    }

    [StructLayout(LayoutKind.Sequential)]
    internal unsafe struct PipelineMultisampleStateCreateInfo
    {
        private readonly StructureType Type;
        private readonly void* Next;
        private readonly PipelineMultisampleStateCreateFlags Flags;
        public readonly SampleCountFlags RasterizationSamples;
        public readonly Bool SampleShadingEnable;
        public readonly float MinSampleShading;
        public readonly uint* SampleMaskPtr;
        public readonly Bool AlphaToCoverageEnable;
        public readonly Bool AlphaToOneEnable;

        public PipelineMultisampleStateCreateInfo(SampleCountFlags rasterizationSamples, Bool sampleShadingEnable, float minSampleShading, uint* sampleMaskPtr, Bool alphaToCoverageEnable, Bool alphaToOneEnable)
        {
            Type = StructureType.PipelineMultisampleStateCreateInfo;
            Next = null;
            Flags = PipelineMultisampleStateCreateFlags.None;
            RasterizationSamples = rasterizationSamples;
            SampleShadingEnable = sampleShadingEnable;
            MinSampleShading = minSampleShading;
            SampleMaskPtr = sampleMaskPtr;
            AlphaToCoverageEnable = alphaToCoverageEnable;
            AlphaToOneEnable = alphaToOneEnable;
        }
    }
}