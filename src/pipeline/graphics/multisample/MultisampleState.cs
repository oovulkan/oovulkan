﻿using System.Collections.Generic;
using System.Linq;
using oovulkan.util.collection;

namespace oovulkan.pipeline.graphics.multisample
{
    public class MultisampleState
    {
        public readonly SampleCountFlags RasterizationSamples;
        public readonly bool SampleShadingEnable;
        public readonly float MinSampleShading;
        public readonly ReadonlyList<uint> SampleMasks;
        public readonly bool AlphaToCoverageEnable;
        public readonly bool AlphaToOneEnable;

        public MultisampleState(SampleCountFlags rasterizationSamples, bool sampleShadingEnable, float minSampleShading, IEnumerable<uint> sampleMasks, bool alphaToCoverageEnable, bool alphaToOneEnable)
        {
            RasterizationSamples = rasterizationSamples;
            SampleShadingEnable = sampleShadingEnable;
            MinSampleShading = minSampleShading;
            SampleMasks = sampleMasks.ToList();
            AlphaToCoverageEnable = alphaToCoverageEnable;
            AlphaToOneEnable = alphaToOneEnable;
        }
    }
}