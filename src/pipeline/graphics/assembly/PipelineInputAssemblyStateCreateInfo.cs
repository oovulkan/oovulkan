﻿using System;
using System.Runtime.InteropServices;
using util;

// ReSharper disable PrivateFieldCanBeConvertedToLocalVariable

namespace oovulkan.pipeline.graphics.assembly
{
    [Flags]
    internal enum PipelineInputAssemblyStateCreateFlags : int
    {
        None = 0
    }

    [StructLayout(LayoutKind.Sequential)]
    internal unsafe struct PipelineInputAssemblyStateCreateInfo
    {
        private readonly StructureType Type;
        private readonly void* Next;
        private readonly PipelineInputAssemblyStateCreateFlags Flags;
        public readonly PrimitiveTopology Topology;
        public readonly Bool PrimitiveRestartEnable;

        private PipelineInputAssemblyStateCreateInfo(PrimitiveTopology topology, bool primitiveRestartEnable)
        {
            Type = StructureType.PipelineInputAssemblyStateCreateInfo;
            Next = null;
            Flags = PipelineInputAssemblyStateCreateFlags.None;
            Topology = topology;
            PrimitiveRestartEnable = primitiveRestartEnable;
        }

        public static implicit operator PipelineInputAssemblyStateCreateInfo(InputAssemblyState state)
            => new PipelineInputAssemblyStateCreateInfo(state.Topology, state.PrimitiveRestartEnable);
    }
}