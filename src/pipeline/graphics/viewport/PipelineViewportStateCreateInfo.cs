﻿using System;
using System.Runtime.InteropServices;
using oovulkan.util;

// ReSharper disable PrivateFieldCanBeConvertedToLocalVariable

namespace oovulkan.pipeline.graphics.viewport
{
    [Flags]
    internal enum PipelineViewportStateCreateFlags : int
    {
        None = 0
    }

    [StructLayout(LayoutKind.Sequential)]
    internal unsafe struct PipelineViewportStateCreateInfo
    {
        private readonly StructureType Type;
        private readonly void* Next;
        private readonly PipelineViewportStateCreateFlags Flags;
        public readonly uint ViewportCount;
        public readonly Viewport* ViewportPtr;
        public readonly uint ScissorCount;
        public readonly Rect2D* ScissorPtr;

        public PipelineViewportStateCreateInfo(uint count, Viewport* viewportPtr, Rect2D* scissorPtr)
        {
            Type = StructureType.PipelineViewportStateCreateInfo;
            Next = null;
            Flags = PipelineViewportStateCreateFlags.None;
            ViewportCount = count;
            ViewportPtr = viewportPtr;
            ScissorCount = count;
            ScissorPtr = scissorPtr;
        }
    }
}