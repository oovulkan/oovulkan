﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using oovulkan.pipeline.graphics.assembly;
using oovulkan.pipeline.graphics.color;
using oovulkan.pipeline.graphics.depthstencil;
using oovulkan.pipeline.graphics.dynamic;
using oovulkan.pipeline.graphics.rasterization;
using oovulkan.pipeline.graphics.vertex;
using oovulkan.shader;
using oovulkan.util;
using oovulkan.util.collection;

namespace oovulkan.pipeline.graphics.viewport
{
    public class ViewportState
    {
        public readonly ReadonlyList<Viewport> Viewports;
        public readonly ReadonlyList<Rect2D> Scissors;

        private ViewportState(){ }

        //Building "constructor"
        // ReSharper disable once UnusedMember.Local
        private ViewportState(IEnumerable<Viewport> viewports, IEnumerable<Rect2D> scissors)
        {
            Viewports = viewports.ToList();
            Scissors = scissors.ToList();
        }

        public abstract class Builder : BuilderBase<ViewportState>
        {
            private static readonly ConstructorInfo Sneaky = typeof(ViewportState).GetAnyConstructor(
                typeof(IEnumerable<Viewport>),
                typeof(IEnumerable<Rect2D>)
            );
            private readonly GraphicsPipeline.Builder builder;
            private readonly List<Viewport> viewports = new List<Viewport>();
            private readonly List<Rect2D> scissors = new List<Rect2D>();

            protected Builder(GraphicsPipeline.Builder builder, out ViewportState viewportState) : base(new ViewportState())
            {
                this.builder = builder;
                viewportState = Object;
            }

            protected Builder Viewport(Viewport? viewport=null, Rect2D? scissor=null)
            {
                AssertNotDone();
                var limits = builder.Object.Device.PhysicalDevice.Limits;
                if(viewport != null)
                {
                    if(viewports.Count > limits.MaxViewports)
                        throw new ArgumentOutOfRangeException();
                    viewports.Add(viewport.Value);
                }
                if(scissor != null)
                {
                    if(scissors.Count > limits.MaxViewports)
                        throw new ArgumentOutOfRangeException();
                    scissors.Add(scissor.Value);
                }
                return this;
            }

            private void Done()
            {
                AssertNotDone();
                MarkDone();
                Sneaky.Invoke(Object, viewports, scissors);
            }

            public GraphicsPipeline.Builder Stage(ShaderStageFlags stage, ShaderModule shader, string entryPoint, object specialization=null)
            {
                Done();
                return builder.Stage(stage, shader, entryPoint, specialization);
            }
            public VertexInputState.Builder VertexInput()
            {
                Done();
                return builder.VertexInput();
            }
            public GraphicsPipeline.Builder InputAssembly(PrimitiveTopology topology, bool primitiveRestartEnable)
            {
                Done();
                return builder.InputAssembly(topology, primitiveRestartEnable);
            }
            public GraphicsPipeline.Builder Tesselation(uint patchControlPoints)
            {
                Done();
                return builder.Tesselation(patchControlPoints);
            }
            public GraphicsPipeline.Builder Rasterization(bool depthClampEnable, bool rasterizerDiscardEnable, PolygonMode polygonMode, CullModeFlags cullMode, FrontFace frontFace, bool depthBiasEnable, float depthBiasConstantFactor=0, float depthBiasClamp=0, float depthBiasSlopeFactor=0, float lineWidth=1)
            {
                Done();
                return builder.Rasterization(depthClampEnable, rasterizerDiscardEnable, polygonMode, cullMode, frontFace, depthBiasEnable, depthBiasConstantFactor, depthBiasClamp, depthBiasSlopeFactor, lineWidth);
            }
            public GraphicsPipeline.Builder Multisample(SampleCountFlags rasterizationSamples, bool sampleShadingEnable, float minSampleShading, bool alphaToCoverageEnable, bool alphaToOneEnable, params uint[] masks)
            {
                Done();
                return builder.Multisample(rasterizationSamples, sampleShadingEnable, minSampleShading, alphaToCoverageEnable, alphaToOneEnable, masks);
            }
            public GraphicsPipeline.Builder DepthStencil(bool depthTestEnable, bool depthWriteEnable, CompareOp depthCompareOp, bool depthBoundsTestEnable, bool stencilTestEnable, StencilOpState front, StencilOpState back, (float Min, float Max)? depthBounds=null)
            {
                Done();
                return builder.DepthStencil(depthTestEnable, depthWriteEnable, depthCompareOp, depthBoundsTestEnable, stencilTestEnable, front, back, depthBounds);
            }
            public ColorBlendState.Builder ColorBlend((float R, float G, float B, float A) blendConstants, LogicOp? logicOp = null)
            {
                Done();
                return builder.ColorBlend(blendConstants, logicOp);
            }
            public GraphicsPipeline.Builder Dynamic(params DynamicState[] states)
            {
                Done();
                return builder.Dynamic(states);
            }
        }

        public class ViewportBuilder : Builder
        {
            internal ViewportBuilder(GraphicsPipeline.Builder builder, out ViewportState viewportState)
                : base(builder, out viewportState) { }

            public ViewportBuilder Viewport(Viewport viewport)
            {
                return (ViewportBuilder)base.Viewport(viewport: viewport);
            }
        }
        public class ScissorBuilder : Builder
        {
            internal ScissorBuilder(GraphicsPipeline.Builder builder, out ViewportState viewportState)
                : base(builder, out viewportState) { }

            public ScissorBuilder Viewport(Rect2D scissor)
            {
                return (ScissorBuilder)base.Viewport(scissor: scissor);
            }
        }
        public class ViewportScissorBuilder : Builder
        {
            internal ViewportScissorBuilder(GraphicsPipeline.Builder builder, out ViewportState viewportState)
                : base(builder, out viewportState) { }

            public ViewportScissorBuilder Viewport(Viewport viewport, Rect2D scissor)
            {
                return (ViewportScissorBuilder)base.Viewport(viewport, scissor);
            }
        }
    }
}