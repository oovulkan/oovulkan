﻿using System.Runtime.InteropServices;

namespace oovulkan.pipeline.graphics.viewport
{
    [StructLayout(LayoutKind.Sequential)]
    public struct Viewport
    {
        public readonly float X;
        public readonly float Y;
        public readonly float Width;
        public readonly float Height;
        public readonly float MinDepth;
        public readonly float MaxDepth;

        private Viewport(float x, float y, float width, float height, float minDepth, float maxDepth)
        {
            X = x;
            Y = y;
            Width = width;
            Height = height;
            MinDepth = minDepth;
            MaxDepth = maxDepth;
        }

        public static implicit operator (float X, float Y, float Width, float Height, float MinDepth, float MaxDepth)(Viewport viewport)
            => (viewport.X, viewport.Y, viewport.Width, viewport.Height, viewport.MinDepth, viewport.MaxDepth);
        public static implicit operator Viewport((float X, float Y, float Width, float Height, float MinDepth, float MaxDepth) viewport)
            => new Viewport(viewport.X, viewport.Y, viewport.Width, viewport.Height, viewport.MinDepth, viewport.MaxDepth);
    }
}