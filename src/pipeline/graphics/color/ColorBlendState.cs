﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using oovulkan.pipeline.graphics.assembly;
using oovulkan.pipeline.graphics.depthstencil;
using oovulkan.pipeline.graphics.dynamic;
using oovulkan.pipeline.graphics.rasterization;
using oovulkan.pipeline.graphics.vertex;
using oovulkan.pipeline.graphics.viewport;
using oovulkan.shader;
using oovulkan.util;
using oovulkan.util.collection;

namespace oovulkan.pipeline.graphics.color
{
    public enum LogicOp : int
    {
        Clear = 0,
        And = 1,
        AndReverse = 2,
        Copy = 3,
        AndInverted = 4,
        NoOp = 5,
        Xor = 6,
        Or = 7,
        Nor = 8,
        Equivalent = 9,
        Invert = 10,
        OrReverse = 11,
        CopyInverted = 12,
        OrInverted = 13,
        Nand = 14,
        Set = 15
    }

    public enum BlendFactor : int
    {
        Zero = 0,
        One = 1,
        SrcColor = 2,
        OneMinusSrcColor = 3,
        DstColor = 4,
        OneMinusDstColor = 5,
        SrcAlpha = 6,
        OneMinusSrcAlpha = 7,
        DstAlpha = 8,
        OneMinusDstAlpha = 9,
        ConstantColor = 10,
        OneMinusConstantColor = 11,
        ConstantAlpha = 12,
        OneMinusConstantAlpha = 13,
        SrcAlphaSaturate = 14,
        Src1Color = 15,
        OneMinusSrc1Color = 16,
        Src1Alpha = 17,
        OneMinusSrc1Alpha = 18
    }

    public enum BlendOp : int
    {
        Add = 0,
        Subtract = 1,
        ReverseSubtract = 2,
        Min = 3,
        Max = 4
    }

    [Flags]
    public enum ColorComponentFlags : int
    {
        R = 1,
        G = 2,
        B = 4,
        A = 8,
        Color = R | G | B,
        All = Color | A
    }

    public class ColorBlendAttachment
    {
        public readonly bool BlendEnable;
        public readonly BlendFactor SrcColorFactor;
        public readonly BlendFactor DstColorFactor;
        public readonly BlendOp ColorOp;
        public readonly BlendFactor SrcAlphaFactor;
        public readonly BlendFactor DstAlphaFactor;
        public readonly BlendOp AlphaOp;
        public readonly ColorComponentFlags ColorWriteMask;

        internal ColorBlendAttachment(bool blendEnable, BlendFactor srcColorFactor, BlendFactor dstColorFactor, BlendOp colorOp, BlendFactor srcAlphaFactor, BlendFactor dstAlphaFactor, BlendOp alphaOp, ColorComponentFlags colorWriteMask)
        {
            BlendEnable = blendEnable;
            SrcColorFactor = srcColorFactor;
            DstColorFactor = dstColorFactor;
            ColorOp = colorOp;
            SrcAlphaFactor = srcAlphaFactor;
            DstAlphaFactor = dstAlphaFactor;
            AlphaOp = alphaOp;
            ColorWriteMask = colorWriteMask;
        }

        protected bool Equals(ColorBlendAttachment other)
        {
            return this == other;
        }
        public override bool Equals(object obj)
        {
            return Equals(obj as ColorBlendAttachment);
        }
        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = BlendEnable.GetHashCode();
                hashCode = (hashCode * 397) ^ (int)SrcColorFactor;
                hashCode = (hashCode * 397) ^ (int)DstColorFactor;
                hashCode = (hashCode * 397) ^ (int)ColorOp;
                hashCode = (hashCode * 397) ^ (int)SrcAlphaFactor;
                hashCode = (hashCode * 397) ^ (int)DstAlphaFactor;
                hashCode = (hashCode * 397) ^ (int)AlphaOp;
                hashCode = (hashCode * 397) ^ (int)ColorWriteMask;
                return hashCode;
            }
        }

        public static bool operator ==(ColorBlendAttachment left, ColorBlendAttachment right)
        {
            if(left == null && right == null)
                return true;
            if(left == null || right == null)
                return false;
            return left.BlendEnable == right.BlendEnable &&
                   left.SrcColorFactor == right.SrcColorFactor && left.DstColorFactor == right.DstColorFactor && left.ColorOp == right.ColorOp &&
                   left.SrcAlphaFactor == right.SrcAlphaFactor &&  left.DstAlphaFactor == right.DstAlphaFactor && left.AlphaOp == right.AlphaOp &&
                   left.ColorWriteMask == right.ColorWriteMask;
        }
        public static bool operator !=(ColorBlendAttachment left, ColorBlendAttachment right)
        {
            return !(left == right);
        }
    }

    public class ColorBlendState
    {
        public readonly bool LogicOpEnable;
        public readonly LogicOp LogicOp;
        public readonly (float R, float G, float B, float A) BlendConstants;
        public readonly ReadonlyList<ColorBlendAttachment> Attachments;

        private ColorBlendState(bool logicOpEnable, LogicOp logicOp, (float R, float G, float B, float A) blendConstants)
        {
            LogicOpEnable = logicOpEnable;
            LogicOp = logicOp;
            BlendConstants = blendConstants;
        }

        //Building "constructor"
        // ReSharper disable once UnusedMember.Local
        private ColorBlendState(IEnumerable<ColorBlendAttachment> attachments)
        {
            Attachments = attachments.ToList();
        }

        public class Builder : BuilderBase<ColorBlendState>
        {
            protected static readonly ConstructorInfo Sneaky = typeof(ColorBlendState).GetAnyConstructor(
                typeof(IEnumerable<ColorBlendAttachment>)
            );

            private readonly GraphicsPipeline.Builder builder;
            private readonly List<ColorBlendAttachment> attachments = new List<ColorBlendAttachment>();

            internal Builder(GraphicsPipeline.Builder builder, bool logicOpEnable, LogicOp logicOp, (float R, float G, float B, float A) blendConstants, out ColorBlendState colorBlendState)
                : base(new ColorBlendState(logicOpEnable, logicOp, blendConstants))
            {
                this.builder = builder;
                colorBlendState = Object;
            }

            public Builder Attachment(bool blendEnable, BlendFactor srcColorFactor=BlendFactor.One, BlendFactor dstColorFactor=BlendFactor.Zero, BlendOp colorOp=BlendOp.Add, BlendFactor srcAlphaFactor=BlendFactor.One, BlendFactor dstAlphaFactor=BlendFactor.Zero, BlendOp alphaOp=BlendOp.Add, ColorComponentFlags colorWriteMask=ColorComponentFlags.All)
            {
                AssertNotDone();
                if(!builder.Object.Device.Features.DualSrcBlend)
                {
                    if(IsDual(srcColorFactor))
                        throw new NotSupportedException(nameof(srcColorFactor));
                    if(IsDual(dstColorFactor))
                        throw new NotSupportedException(nameof(dstColorFactor));
                    if(IsDual(srcAlphaFactor))
                        throw new NotSupportedException(nameof(srcAlphaFactor));
                    if(IsDual(dstAlphaFactor))
                        throw new NotSupportedException(nameof(dstAlphaFactor));

                    bool IsDual(BlendFactor blend) => blend == BlendFactor.Src1Color || blend == BlendFactor.Src1Alpha || blend == BlendFactor.OneMinusSrc1Color || blend == BlendFactor.OneMinusSrc1Alpha;
                }
                var attachment = new ColorBlendAttachment(blendEnable, srcColorFactor, dstColorFactor, colorOp, srcAlphaFactor, dstAlphaFactor, alphaOp, colorWriteMask);
                if(attachments.Count > 0 && !builder.Object.Device.Features.IndependentBlend && attachment != attachments[0])
                    throw new NotSupportedException();
                attachments.Add(attachment);
                return this;
            }

            private void Done()
            {
                AssertNotDone();
                MarkDone();
                Sneaky.Invoke(Object, attachments);
            }

            public GraphicsPipeline.Builder Stage(ShaderStageFlags stage, ShaderModule shader, string entryPoint, object specialization=null)
            {
                Done();
                return builder.Stage(stage, shader, entryPoint, specialization);
            }
            public VertexInputState.Builder VertexInput()
            {
                Done();
                return builder.VertexInput();
            }
            public GraphicsPipeline.Builder InputAssembly(PrimitiveTopology topology, bool primitiveRestartEnable)
            {
                Done();
                return builder.InputAssembly(topology, primitiveRestartEnable);
            }
            public GraphicsPipeline.Builder Tesselation(uint patchControlPoints)
            {
                Done();
                return builder.Tesselation(patchControlPoints);
            }
            public ViewportState.ViewportBuilder Viewport(Viewport viewport)
            {
                Done();
                return builder.Viewport(viewport);
            }
            public ViewportState.ScissorBuilder Viewport(Rect2D scissor)
            {
                Done();
                return builder.Viewport(scissor);
            }
            public ViewportState.ViewportScissorBuilder Viewport(Viewport viewport, Rect2D scissor)
            {
                Done();
                return builder.Viewport(viewport, scissor);
            }
            public GraphicsPipeline.Builder Rasterization(bool depthClampEnable, bool rasterizerDiscardEnable, PolygonMode polygonMode, CullModeFlags cullMode, FrontFace frontFace, bool depthBiasEnable, float depthBiasConstantFactor, float depthBiasClamp, float depthBiasSlopeFactor, float lineWidth)
            {
                Done();
                return builder.Rasterization(depthClampEnable, rasterizerDiscardEnable, polygonMode, cullMode, frontFace, depthBiasEnable, depthBiasConstantFactor, depthBiasClamp, depthBiasSlopeFactor, lineWidth);
            }
            public GraphicsPipeline.Builder Multisample(SampleCountFlags rasterizationSamples, bool sampleShadingEnable, float minSampleShading, bool alphaToCoverageEnable, bool alphaToOneEnable, params uint[] masks)
            {
                Done();
                return builder.Multisample(rasterizationSamples, sampleShadingEnable, minSampleShading, alphaToCoverageEnable, alphaToOneEnable, masks);
            }
            public GraphicsPipeline.Builder DepthStencil(bool depthTestEnable, bool depthWriteEnable, CompareOp depthCompareOp, bool depthBoundsTestEnable, bool stencilTestEnable, StencilOpState front, StencilOpState back, (float Min, float Max)? depthBounds=null)
            {
                Done();
                return builder.DepthStencil(depthTestEnable, depthWriteEnable, depthCompareOp, depthBoundsTestEnable, stencilTestEnable, front, back, depthBounds);
            }
            public Builder ColorBlend((float R, float G, float B, float A) blendConstants, LogicOp? logicOp = null)
            {
                Done();
                return builder.ColorBlend(blendConstants, logicOp);
            }
            public GraphicsPipeline.Builder Dynamic(params DynamicState[] states)
            {
                Done();
                return builder.Dynamic(states);
            }
        }
    }
}