﻿using System;
using System.Runtime.InteropServices;
using oovulkan.util;
using util;

// ReSharper disable PrivateFieldCanBeConvertedToLocalVariable

namespace oovulkan.pipeline.graphics.color
{
    [Flags]
    internal enum PipelineColorBlendStateCreateFlags : int
    {
        None = 0
    }

    [StructLayout(LayoutKind.Sequential)]
    internal struct PipelineColorBlendAttachmentState
    {
        public readonly Bool BlendEnable;
        public readonly BlendFactor SrcColorFactor;
        public readonly BlendFactor DstColorFactor;
        public readonly BlendOp ColorOp;
        public readonly BlendFactor SrcAlphaFactor;
        public readonly BlendFactor DstAlphaFactor;
        public readonly BlendOp AlphaOp;
        public readonly ColorComponentFlags ColorWriteMask;

        private PipelineColorBlendAttachmentState(bool blendEnable, BlendFactor srcColorFactor, BlendFactor dstColorFactor, BlendOp colorOp, BlendFactor srcAlphaFactor, BlendFactor dstAlphaFactor, BlendOp alphaOp, ColorComponentFlags colorWriteMask)
        {
            BlendEnable = blendEnable;
            SrcColorFactor = srcColorFactor;
            DstColorFactor = dstColorFactor;
            ColorOp = colorOp;
            SrcAlphaFactor = srcAlphaFactor;
            DstAlphaFactor = dstAlphaFactor;
            AlphaOp = alphaOp;
            ColorWriteMask = colorWriteMask;
        }

        public static implicit operator PipelineColorBlendAttachmentState(ColorBlendAttachment attachment)
            => new PipelineColorBlendAttachmentState(attachment.BlendEnable,
                attachment.SrcColorFactor, attachment.DstColorFactor, attachment.ColorOp,
                attachment.SrcAlphaFactor, attachment.DstAlphaFactor, attachment.AlphaOp,
                attachment.ColorWriteMask);
    }

    [StructLayout(LayoutKind.Sequential)]
    internal unsafe struct PipelineColorBlendStateCreateInfo
    {
        private readonly StructureType Type;
        private readonly void* Next;
        private readonly PipelineColorBlendStateCreateFlags Flags;
        public readonly Bool LogicOpEnable;
        public readonly LogicOp LogicOp;
        public readonly uint AttachmentCount;
        public readonly PipelineColorBlendAttachmentState* AttachmentPtr;
        public readonly F4 BlendConstants;

        public PipelineColorBlendStateCreateInfo(bool logicOpEnable, LogicOp logicOp, uint attachmentCount, PipelineColorBlendAttachmentState* attachmentPtr, F4 blendConstants)
        {
            Type = StructureType.PipelineColorBlendStateCreateInfo;
            Next = null;
            Flags = PipelineColorBlendStateCreateFlags.None;
            LogicOpEnable = logicOpEnable;
            LogicOp = logicOp;
            AttachmentCount = attachmentCount;
            AttachmentPtr = attachmentPtr;
            BlendConstants = blendConstants;
        }
    }
}