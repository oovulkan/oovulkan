﻿namespace oovulkan.pipeline.graphics.depthstencil
{
    public class DepthStencilState
    {
        public readonly bool DepthTestEnable;
        public readonly bool DepthWriteEnable;
        public readonly CompareOp DepthCompareOp;
        public readonly bool DepthBoundsTestEnable;
        public readonly bool StencilTestEnable;
        public readonly StencilOpState Front;
        public readonly StencilOpState Back;
        public readonly (float Min, float Max) DepthBounds;

        public DepthStencilState(bool depthTestEnable, bool depthWriteEnable, CompareOp depthCompareOp, bool depthBoundsTestEnable, bool stencilTestEnable, StencilOpState front, StencilOpState back, (float, float) depthBounds)
        {
            DepthTestEnable = depthTestEnable;
            DepthWriteEnable = depthWriteEnable;
            DepthCompareOp = depthCompareOp;
            DepthBoundsTestEnable = depthBoundsTestEnable;
            StencilTestEnable = stencilTestEnable;
            Front = front;
            Back = back;
            DepthBounds = depthBounds;
        }
    }
}