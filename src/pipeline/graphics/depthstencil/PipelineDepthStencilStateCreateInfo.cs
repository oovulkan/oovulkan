﻿using System;
using System.Runtime.InteropServices;
using util;

// ReSharper disable PrivateFieldCanBeConvertedToLocalVariable

namespace oovulkan.pipeline.graphics.depthstencil
{
    [Flags]
    internal enum PipelineDepthStencilStateCreateFlags : int
    {
        None = 0
    }

    [StructLayout(LayoutKind.Sequential)]
    internal unsafe struct PipelineDepthStencilStateCreateInfo
    {
        private readonly StructureType Type;
        private readonly void* Next;
        private readonly PipelineDepthStencilStateCreateFlags Flags;
        public readonly Bool DepthTestEnable;
        public readonly Bool DepthWriteEnable;
        public readonly CompareOp DepthCompareOp;
        public readonly Bool DepthBoundsTestEnable;
        public readonly Bool StencilTestEnable;
        public readonly StencilOpState Front;
        public readonly StencilOpState Back;
        public readonly float MinDepthBounds;
        public readonly float MaxDepthBounds;

        private PipelineDepthStencilStateCreateInfo(bool depthTestEnable, bool depthWriteEnable, CompareOp depthCompareOp, bool depthBoundsTestEnable, bool stencilTestEnable, StencilOpState front, StencilOpState back, float minDepthBounds, float maxDepthBounds)
        {
            Type = StructureType.PipelineTessellationStateCreateInfo;
            Next = null;
            Flags = PipelineDepthStencilStateCreateFlags.None;
            DepthTestEnable = depthTestEnable;
            DepthWriteEnable = depthWriteEnable;
            DepthCompareOp = depthCompareOp;
            DepthBoundsTestEnable = depthBoundsTestEnable;
            StencilTestEnable = stencilTestEnable;
            Front = front;
            Back = back;
            MinDepthBounds = minDepthBounds;
            MaxDepthBounds = maxDepthBounds;
        }

        public static implicit operator PipelineDepthStencilStateCreateInfo(DepthStencilState state)
            => new PipelineDepthStencilStateCreateInfo(state.DepthTestEnable, state.DepthWriteEnable, state.DepthCompareOp,
                state.DepthBoundsTestEnable, state.StencilTestEnable, state.Front, state.Back, state.DepthBounds.Min,
                state.DepthBounds.Max);
    }
}