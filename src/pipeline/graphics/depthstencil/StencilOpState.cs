﻿using System.Runtime.InteropServices;

namespace oovulkan.pipeline.graphics.depthstencil
{
    public enum StencilOp : int
    {
        Keep = 0,
        Zero = 1,
        Replace = 2,
        IncClamp = 3,
        DecClamp = 4,
        Invert = 5,
        IncWrap = 6,
        DecWrap = 7
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct StencilOpState
    {
        public readonly StencilOp FailOp;
        public readonly StencilOp PassOp;
        public readonly StencilOp DepthFailOp;
        public readonly CompareOp CompareOp;
        public readonly uint CompareMask;
        public readonly uint WriteMask;
        public readonly uint Reference;

        private StencilOpState(StencilOp failOp, StencilOp passOp, StencilOp depthFailOp, CompareOp compareOp, uint compareMask, uint writeMask, uint reference)
        {
            FailOp = failOp;
            PassOp = passOp;
            DepthFailOp = depthFailOp;
            CompareOp = compareOp;
            CompareMask = compareMask;
            WriteMask = writeMask;
            Reference = reference;
        }

        public static implicit operator StencilOpState((StencilOp FailOp, StencilOp PassOp, StencilOp DepthFailOp, CompareOp CompareOp, uint CompareMask, uint WriteMask, uint Reference) tuple)
            => new StencilOpState(tuple.FailOp, tuple.PassOp, tuple.DepthFailOp, tuple.CompareOp, tuple.CompareMask, tuple.WriteMask, tuple.Reference);
        public static implicit operator (StencilOp FailOp, StencilOp PassOp, StencilOp DepthFailOp, CompareOp CompareOp, uint CompareMask, uint WriteMask, uint Reference)(StencilOpState state)
            => (state.FailOp, state.PassOp, state.DepthFailOp, state.CompareOp, state.CompareMask, state.WriteMask, state.Reference);
    }
}