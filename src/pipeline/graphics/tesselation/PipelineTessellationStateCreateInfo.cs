﻿using System;
using System.Runtime.InteropServices;

// ReSharper disable PrivateFieldCanBeConvertedToLocalVariable

namespace oovulkan.pipeline.graphics.tesselation
{
    [Flags]
    internal enum PipelineTessellationStateCreateFlags : int
    {
        None = 0
    }

    [StructLayout(LayoutKind.Sequential)]
    internal unsafe struct PipelineTessellationStateCreateInfo
    {
        private readonly StructureType Type;
        private readonly void* Next;
        private readonly PipelineTessellationStateCreateFlags Flags;
        public readonly uint PatchControlPoints;

        private PipelineTessellationStateCreateInfo(uint patchControlPoints)
        {
            Type = StructureType.PipelineTessellationStateCreateInfo;
            Next = null;
            Flags = PipelineTessellationStateCreateFlags.None;
            PatchControlPoints = patchControlPoints;
        }

        public static implicit operator PipelineTessellationStateCreateInfo(TesselationState state)
            => new PipelineTessellationStateCreateInfo(state.PatchControlPoints);
    }
}