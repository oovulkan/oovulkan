﻿namespace oovulkan.pipeline.graphics.tesselation
{
    public class TesselationState
    {
        public readonly uint PatchControlPoints;

        public TesselationState(uint patchControlPoints)
        {
            PatchControlPoints = patchControlPoints;
        }
    }
}