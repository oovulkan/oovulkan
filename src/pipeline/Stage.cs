﻿using System;
using System.Collections.Generic;
using System.Linq;
using oovulkan.util;

namespace oovulkan.pipeline
{
    [Flags]
    public enum Stage
    {
        None = 0,
        TopOfPipe = 1,
        DrawIndirect = 2,
        VertexInput = 4,
        VertexShader = 8,
        TessellationControlShader = 16,
        TessellationEvaluationShader = 32,
        GeometryShader = 64,
        FragmentShader = 128,
        EarlyFragmentTests = 256,
        LateFragmentTests = 512,
        ColorAttachmentOutput = 1024,
        ComputeShader = 2048,
        Transfer = 4096,
        BottomOfPipe = 8192,
        Host = 16384,
        AllGraphics = 32768,
        AllCommands = 65536,
        CommandProcessNvx = 131072,

        FrameBufferSpaceStages = FragmentShader | EarlyFragmentTests | LateFragmentTests | ColorAttachmentOutput
    }

    public static class StageHelper
    {
        private static readonly Dictionary<Stage, AccessFlags> SupportedAccesses = new Dictionary<Stage, AccessFlags>
        {
            {Stage.DrawIndirect, AccessFlags.IndirectCommandRead},
            {Stage.VertexInput, AccessFlags.IndexRead | AccessFlags.VertexAttributeRead},
            {Stage.VertexShader, AccessFlags.UniformRead | AccessFlags.ShaderRead | AccessFlags.ShaderWrite},
            {Stage.TessellationControlShader, AccessFlags.UniformRead | AccessFlags.ShaderRead | AccessFlags.ShaderWrite},
            {Stage.TessellationEvaluationShader, AccessFlags.UniformRead | AccessFlags.ShaderRead | AccessFlags.ShaderWrite},
            {Stage.GeometryShader, AccessFlags.UniformRead | AccessFlags.ShaderRead | AccessFlags.ShaderWrite},
            {Stage.FragmentShader, AccessFlags.UniformRead | AccessFlags.InputAttachmentRead | AccessFlags.ShaderRead | AccessFlags.ShaderWrite},
            {Stage.EarlyFragmentTests, AccessFlags.DepthStencilAttachmentRead | AccessFlags.DepthStencilAttachmentWrite},
            {Stage.LateFragmentTests, AccessFlags.DepthStencilAttachmentRead | AccessFlags.DepthStencilAttachmentWrite},
            {Stage.ColorAttachmentOutput, AccessFlags.ColorAttachmentRead | AccessFlags.ColorAttachmentWrite},
            {Stage.ComputeShader, AccessFlags.UniformRead | AccessFlags.ShaderRead | AccessFlags.ShaderWrite},
            {Stage.Transfer, AccessFlags.TransferRead | AccessFlags.TransferWrite},
            {Stage.Host, AccessFlags.HostRead | AccessFlags.HostWrite}
        };

        public static bool Supports(this Stage flags, AccessFlags desiredAccesses)
        {
            AccessFlags accesses = AccessFlags.None;
            foreach(var flag in Util.GetFlags(flags))
                accesses |= SupportedAccesses.GetOrDefault(flag);
            return (desiredAccesses & ~accesses) == AccessFlags.None;
        }

        public static int Ordinal(this Stage flag, PipelineType type, int? @default=null)
        {
            switch(type)
            {
                case PipelineType.Graphics:
                    switch(flag)
                    {
                        case Stage.TopOfPipe:
                            return 0;
                        case Stage.DrawIndirect:
                            return 1;
                        case Stage.VertexInput:
                            return 2;
                        case Stage.VertexShader:
                            return 3;
                        case Stage.TessellationControlShader:
                            return 4;
                        case Stage.TessellationEvaluationShader:
                            return 5;
                        case Stage.GeometryShader:
                            return 6;
                        case Stage.EarlyFragmentTests:
                            return 7;
                        case Stage.FragmentShader:
                            return 8;
                        case Stage.LateFragmentTests:
                            return 9;
                        case Stage.ColorAttachmentOutput:
                            return 10;
                        case Stage.BottomOfPipe:
                            return 11;
                        default:
                            if(@default != null)
                                return @default.Value;
                            throw new ArgumentException();
                    }
                case PipelineType.Compute:
                    switch(flag)
                    {
                        case Stage.TopOfPipe:
                            return 0;
                        case Stage.DrawIndirect:
                            return 1;
                        case Stage.ComputeShader:
                            return 2;
                        case Stage.BottomOfPipe:
                            return 3;
                        default:
                            if(@default != null)
                                return @default.Value;
                            throw new ArgumentException();
                    }
                case PipelineType.Transfer:
                    switch(flag)
                    {
                        case Stage.TopOfPipe:
                            return 0;
                        case Stage.Transfer:
                            return 1;
                        case Stage.BottomOfPipe:
                            return 3;
                        default:
                            if(@default != null)
                                return @default.Value;
                            throw new ArgumentException();
                    }
                case PipelineType.Host:
                    switch(flag)
                    {
                        case Stage.Host:
                            return 0;
                        default:
                            if(@default != null)
                                return @default.Value;
                            throw new ArgumentException();
                    }
                default:
                    if(@default != null)
                        return @default.Value;
                    throw new ArgumentException();
            }
        }
        public static int Latest(this Stage flags, PipelineType type, int? @default=null)
        {
            int value = Util.GetFlags(flags)
                .Select(flag => flag.Ordinal(type, int.MinValue))
                .Max();
            if(value == int.MinValue)
            {
                if(@default != null)
                    return @default.Value;
                throw new ArgumentException();
            }
            return value;
        }
        public static int Earliest(this Stage flags, PipelineType type, int? @default=null)
        {
            int value = Util.GetFlags(flags)
                .Select(flag => flag.Ordinal(type, int.MaxValue))
                .Min();
            if(value == int.MaxValue)
            {
                if(@default != null)
                    return @default.Value;
                throw new ArgumentException();
            }
            return value;
        }
    }
}