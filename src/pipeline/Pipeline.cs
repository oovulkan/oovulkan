﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using oovulkan.device;
using oovulkan.pipeline.cache;
using oovulkan.pipeline.layout;
using oovulkan.util;
using oovulkan.util.collection;

#pragma warning disable 649

namespace oovulkan.pipeline
{
    [StructLayout(LayoutKind.Sequential)]
    public struct PipelineHandle
    {
        private long Value;

        public static implicit operator PipelineHandle(long? value) => new PipelineHandle {Value = value ?? 0};
        public static implicit operator long(PipelineHandle handle) => handle.Value;
    }

    public abstract unsafe class Pipeline : VulkanObject<PipelineHandle>
    {
        protected internal override bool IsChild => true;
        protected internal override VulkanObject Parent => Device;

        public readonly Device Device;
        public readonly PipelineBindPoint Type;
        public readonly PipelineCreateFlags Flags;
        public readonly PipelineLayout Layout;
        public readonly Pipeline BasePipeline;
        public readonly ReadonlyList<PipelineStage> Stages;

        protected Pipeline(Device device, PipelineBindPoint type, PipelineCreateFlags flags, PipelineLayout layout, Pipeline basePipeline)
        {
            Device = device;
            this.Link(Device);
            Type = type;
            Flags = flags;
            Layout = layout;
            BasePipeline = basePipeline;
        }

        //Building "constructor"
        protected Pipeline(IEnumerable<PipelineStage> stages)
        {
            Stages = stages.ToList();
        }

        protected void Validate(PipelineCache cache)
        {
            if(Layout.Device != Device)
                throw new ArgumentException(nameof(Layout));
            if(BasePipeline != null && BasePipeline.Device != Device)
                throw new ArgumentException(nameof(BasePipeline));
            if(cache != null && cache.Device != Device)
                throw new ArgumentException(nameof(cache));
            if(Flags.HasFlag(PipelineCreateFlags.Derivative))
            {
                if(BasePipeline == null)
                    throw new ArgumentNullException(nameof(BasePipeline));
                if(!BasePipeline.Flags.HasFlag(PipelineCreateFlags.AllowDerivatives))
                    throw new ArgumentException(nameof(BasePipeline));
            }
            else
            {
                if(BasePipeline != null)
                    throw new ArgumentException(nameof(BasePipeline));
            }
        }

        internal delegate void WithStageInfosBody(uint stageCount, PipelineShaderStageCreateInfo* stages);

        internal void UsingStageInfos(WithStageInfosBody body)
        {
            var stages = stackalloc PipelineShaderStageCreateInfo[Stages.Count];
            uint stageCount = 0;
            try
            {
                foreach(var stage in Stages)
                {
                    var specInfo = stackalloc SpecializationInfo[1];
                    if(stage.Specialization != null)
                    {
                        var ranges = SpecConstants.GetRanges(stage.Specialization.GetType()).ToArray();
                        var entries = stackalloc SpecializationMapEntry[ranges.Length];
                        for(int i = 0; i < ranges.Length; i++)
                            entries[i] = ranges[i];
                        *specInfo = new SpecializationInfo((uint)ranges.Length, entries, stage.Specialization.Size, (byte*)stage.Specialization.Ptr);
                    }
                    else
                    {
                        specInfo = null;
                    }
                    stages[stageCount++] = new PipelineShaderStageCreateInfo(stage.Stage, stage.Shader, (AnsiString)stage.EntryPoint, specInfo);
                }
                body(stageCount, stages);
            }
            finally
            {
                for(uint i = 0; i < stageCount; i++)
                    stages[i].EntryPoint.Dispose();
            }
        }

        protected override void Destroy()
        {
            using(Device.WeakLock)
                Device.Remove(this);
            using(Device.Allocator?.WeakLock)
                vkDestroyPipeline(Device, this, Device.Allocator);
        }

        private readonly VkDestroyPipeline vkDestroyPipeline;

        private delegate void VkDestroyPipeline(DeviceHandle device, PipelineHandle pipeline, AllocationCallbacks* allocator);
    }
}