﻿using System;

namespace oovulkan.pipeline
{
    [Flags]
    public enum PipelineCreateFlags : int
    {
        None = 0,
        DisableOptimization = 1,
        AllowDerivatives = 2,
        Derivative = 4
    }
}