﻿namespace oovulkan.pipeline
{
    public enum PipelineBindPoint : int
    {
        Graphics = 0,
        Compute = 1
    }
}