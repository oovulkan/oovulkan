﻿using System;
using System.Runtime.InteropServices;
using oovulkan.shader;
using oovulkan.util;

// ReSharper disable PrivateFieldCanBeConvertedToLocalVariable

namespace oovulkan.pipeline
{
    [Flags]
    internal enum PipelineShaderStageCreateFlags : int
    {
        None = 0
    }

    [StructLayout(LayoutKind.Sequential)]
    internal struct SpecializationMapEntry
    {
        public readonly uint ConstantID;
        public readonly uint Offset;
        public readonly Word Size;

        private SpecializationMapEntry(uint constantId, uint offset, Word size)
        {
            ConstantID = constantId;
            Offset = offset;
            Size = size;
        }

        public static implicit operator SpecializationMapEntry((uint ID, uint Offset, Word Size) entry) => new SpecializationMapEntry(entry.ID, entry.Offset, entry.Size);
    }

    [StructLayout(LayoutKind.Sequential)]
    internal unsafe struct SpecializationInfo
    {
        public readonly uint MapEntryCount;
        public readonly SpecializationMapEntry* MapEntryPtr;
        public readonly Word DataSize;
        public readonly byte* DataPtr;

        public SpecializationInfo(uint mapEntryCount, SpecializationMapEntry* mapEntryPtr, Word dataSize, byte* dataPtr)
        {
            MapEntryCount = mapEntryCount;
            MapEntryPtr = mapEntryPtr;
            DataSize = dataSize;
            DataPtr = dataPtr;
        }
    }

    [StructLayout(LayoutKind.Sequential)]
    internal unsafe struct PipelineShaderStageCreateInfo
    {
        private readonly StructureType Type;
        private readonly void* Next;
        private readonly PipelineShaderStageCreateFlags Flags;
        public readonly ShaderStageFlags Stage;
        public readonly ShaderModuleHandle Module;
        public readonly AnsiString EntryPoint;
        public readonly SpecializationInfo* SpecializationInfo;

        public PipelineShaderStageCreateInfo(ShaderStageFlags stage, ShaderModule module, AnsiString entryPoint, SpecializationInfo* specializationInfo)
        {
            Type = StructureType.PipelineShaderStageCreateInfo;
            Next = null;
            Flags = PipelineShaderStageCreateFlags.None;
            Stage = stage;
            Module = module;
            EntryPoint = entryPoint;
            SpecializationInfo = specializationInfo;
        }
    }
}