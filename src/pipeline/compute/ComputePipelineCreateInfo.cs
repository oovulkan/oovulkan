﻿using System.Runtime.InteropServices;
using oovulkan.pipeline.layout;

// ReSharper disable PrivateFieldCanBeConvertedToLocalVariable

namespace oovulkan.pipeline.compute
{
    [StructLayout(LayoutKind.Sequential)]
    internal unsafe struct ComputePipelineCreateInfo
    {
        private readonly StructureType Type;
        private readonly void* Next;
        public readonly PipelineCreateFlags Flags;
        public readonly PipelineShaderStageCreateInfo Stage;
        public readonly PipelineLayoutHandle Layout;
        public readonly PipelineHandle BasePipeline;
        public readonly int BasePipelineIndex;

        public ComputePipelineCreateInfo(PipelineCreateFlags flags, PipelineShaderStageCreateInfo stage, PipelineLayout layout, Pipeline basePipeline, int basePipelineIndex)
        {
            Type = StructureType.ComputePipelineCreateInfo;
            Next = null;
            Flags = flags;
            Stage = stage;
            Layout = layout;
            BasePipeline = basePipeline;
            BasePipelineIndex = basePipelineIndex;
        }
    }
}