﻿using System;
using System.Collections.Generic;
using System.Reflection;
using oovulkan.device;
using oovulkan.pipeline.cache;
using oovulkan.pipeline.layout;
using oovulkan.shader;
using oovulkan.util;

#pragma warning disable 649

namespace oovulkan.pipeline.compute
{
    public unsafe class ComputePipeline : Pipeline
    {
        private ComputePipeline(Device device, PipelineCreateFlags flags, PipelineLayout layout, Pipeline basePipeline)
            : base(device, PipelineBindPoint.Compute, flags, layout, basePipeline)
        {

        }

        //Building "constructor"
        // ReSharper disable once UnusedMember.Local
        private ComputePipeline(PipelineCache cache, IEnumerable<PipelineStage> stages)
            : base(stages)
        {
            Create(cache);
        }

        private void Create(PipelineCache cache)
        {
            Validate(cache);

            UsingStageInfos((count, stages) =>
            {
                var info = new ComputePipelineCreateInfo(Flags, *stages, Layout, BasePipeline, -1);
                using(Device.WeakLock)
                {
                    PipelineHandle handle;
                    using(Device.Allocator?.WeakLock)
                        vkCreateComputePipelines(Device, cache, 1, &info, Device.Allocator, &handle).Throw();
                    Handle = handle;
                    Device.Add(this);
                }
            });
        }

        private readonly VkCreateComputePipelines vkCreateComputePipelines;

        private delegate Result VkCreateComputePipelines(DeviceHandle device, PipelineCacheHandle cache, uint count, ComputePipelineCreateInfo* infos, AllocationCallbacks* allocator, PipelineHandle* pipelines);

        public class Builder : BuilderBase<ComputePipeline>
        {
            private static readonly ConstructorInfo Sneaky = typeof(ComputePipeline).GetAnyConstructor(
                typeof(PipelineCache),
                typeof(IEnumerable<PipelineStage>)
            );

            private readonly PipelineCache cache;

            internal Builder(Device device, PipelineCreateFlags flags, PipelineLayout layout, Pipeline basePipeline, PipelineCache cache)
                : base(new ComputePipeline(device, flags, layout, basePipeline))
            {
                this.cache = cache;
            }

            public ComputePipeline Stage(ShaderModule shader, string entryPoint, object specialization=null)
            {
                AssertNotDone();
                if(shader.Device != Object.Device)
                    throw new ArgumentException(nameof(shader));
                if(specialization != null && !(specialization is SpecConstants))
                    throw new ArgumentException(nameof(specialization));
                MarkDone();
                var stages = new List<PipelineStage> {(ShaderStageFlags.Compute, shader, entryPoint, (SpecConstants)specialization)};
                Sneaky.Invoke(Object, cache, stages);
                return Object;
            }
        }
    }
}