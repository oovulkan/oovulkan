﻿using oovulkan.shader;

namespace oovulkan.pipeline
{
    public class PipelineStage
    {
        public readonly ShaderStageFlags Stage;
        public readonly ShaderModule Shader;
        public readonly string EntryPoint;
        public readonly SpecConstants Specialization;

        public PipelineStage(ShaderStageFlags stage, ShaderModule shader, string entryPoint, SpecConstants specialization)
        {
            Stage = stage;
            Shader = shader;
            EntryPoint = entryPoint;
            Specialization = specialization;
        }

        public static implicit operator PipelineStage((ShaderStageFlags Stage, ShaderModule Shader, string EntryPoint, SpecConstants Specialization) stage)
            => new PipelineStage(stage.Stage, stage.Shader, stage.EntryPoint, stage.Specialization);
        public static implicit operator (ShaderStageFlags Stage, ShaderModule Shader, string EntryPoint, SpecConstants Specialization)(PipelineStage stage)
            => (stage.Stage, stage.Shader, stage.EntryPoint, stage.Specialization);
    }
}