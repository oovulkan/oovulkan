﻿using System;
using System.Runtime.InteropServices;
using oovulkan.util;

// ReSharper disable PrivateFieldCanBeConvertedToLocalVariable

namespace oovulkan.pipeline.cache
{
    [Flags]
    internal enum PipelineCacheCreateFlags : int
    {
        None = 0
    }

    [StructLayout(LayoutKind.Sequential)]
    internal unsafe struct PipelineCacheCreateInfo
    {
        private readonly StructureType Type;
        private readonly void* Next;
        private readonly PipelineCacheCreateFlags Flags;
        public readonly Word InitialDataSize;
        public readonly byte* InitialDataPtr;

        public PipelineCacheCreateInfo(long initialDataSize, byte* initialDataPtr)
        {
            Type = StructureType.PipelineCacheCreateInfo;
            Next = null;
            Flags = PipelineCacheCreateFlags.None;
            InitialDataSize = initialDataSize;
            InitialDataPtr = initialDataPtr;
        }
    }
}