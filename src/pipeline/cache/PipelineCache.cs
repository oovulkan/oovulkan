﻿using System;
using System.Linq;
using System.Runtime.InteropServices;
using oovulkan.device;
using oovulkan.util;

#pragma warning disable 649

namespace oovulkan.pipeline.cache
{
    [StructLayout(LayoutKind.Sequential)]
    public struct PipelineCacheHandle
    {
        private long Value;

        public static implicit operator PipelineCacheHandle(long? value) => new PipelineCacheHandle {Value = value ?? 0};
        public static implicit operator long(PipelineCacheHandle handle) => handle.Value;
    }

    public unsafe class PipelineCache : VulkanObject<PipelineCacheHandle>
    {
        protected internal override bool IsChild => true;
        protected internal override VulkanObject Parent => Device;

        public readonly Device Device;

        public byte[] Data => GetData();

        internal PipelineCache(Device device, byte[] data)
        {
            Device = device;
            this.Link(Device);
            Create(data);
        }

        private void Create(byte[] data)
        {
            fixed(byte* ptr = data)
            {
                PipelineCacheCreateInfo info = new PipelineCacheCreateInfo(data?.LongLength ?? 0, ptr);
                using(Device.WeakLock)
                {
                    PipelineCacheHandle handle;
                    using(Device.Allocator?.WeakLock)
                        vkCreatePipelineCache(Device, &info, Device.Allocator, out handle).Throw();
                    Handle = handle;
                    Device.Add(this);
                }
            }
        }
        protected override void Destroy()
        {
            using(Device.WeakLock)
                Device.Remove(this);
            using(Device.Allocator?.WeakLock)
                vkDestroyPipelineCache(Device, this, Device.Allocator);
        }
        private byte[] GetData()
        {
            using(Lock)
            {
                Word size = 0;
                vkGetPipelineCacheData(Device, Handle, ref size, null).Throw();
                var arr = new byte[size];
                fixed(byte* ptr = arr)
                    vkGetPipelineCacheData(Device, Handle, ref size, ptr).Throw();
                if(size != arr.LongLength)
                    CollectionUtil.Resize(ref arr, size);
                return arr;
            }
        }
        public void Merge(params PipelineCache[] caches)
        {
            if(caches.LongLength == 0)
                throw new ArgumentException(nameof(caches));
            if(caches.Any(cache => cache == this || cache.Device != Device))
                throw new ArgumentException(nameof(caches));
            using(caches.LockAll(cache => cache.WeakLock))
            {
                var handles = stackalloc PipelineCacheHandle[(int)caches.LongLength];
                for(uint i = 0; i < caches.LongLength; i++)
                    handles[i] = caches[i].Handle;
                using(Lock)
                {
                    vkMergePipelineCaches(Device, Handle, (uint)caches.LongLength, handles).Throw();
                }
            }
        }

        private readonly VkCreatePipelineCache vkCreatePipelineCache;
        private readonly VkDestroyPipelineCache vkDestroyPipelineCache;
        private readonly VkMergePipelineCaches vkMergePipelineCaches;
        private readonly VkGetPipelineCacheData vkGetPipelineCacheData;

        private delegate Result VkCreatePipelineCache(DeviceHandle device, PipelineCacheCreateInfo* info, AllocationCallbacks* allocator, out PipelineCacheHandle cache);
        private delegate void VkDestroyPipelineCache(DeviceHandle device, PipelineCacheHandle cache, AllocationCallbacks* allocator);
        private delegate Result VkMergePipelineCaches(DeviceHandle device, PipelineCacheHandle cache, uint count, PipelineCacheHandle* caches);
        private delegate Result VkGetPipelineCacheData(DeviceHandle device, PipelineCacheHandle cache, ref Word size, byte* data);
    }
}