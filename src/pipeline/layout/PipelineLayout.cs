﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using oovulkan.descriptor;
using oovulkan.descriptor.layout;
using oovulkan.device;
using oovulkan.util;
using oovulkan.util.collection;

#pragma warning disable 649

namespace oovulkan.pipeline.layout
{
    [StructLayout(LayoutKind.Sequential)]
    public struct PipelineLayoutHandle
    {
        private long Value;

        public static implicit operator PipelineLayoutHandle(long? value) => new PipelineLayoutHandle {Value = value ?? 0};
        public static implicit operator long(PipelineLayoutHandle handle) => handle.Value;
    }

    public unsafe class PipelineLayout : VulkanObject<PipelineLayoutHandle>
    {
        protected internal override bool IsChild => true;
        protected internal override VulkanObject Parent => Device;

        public readonly Device Device;
        public readonly ReadonlyList<DescriptorSetLayout> Layouts;
        public readonly ReadonlyList<(ShaderStageFlags Stage, uint Offset, uint Size)> PushConstantRanges;

        internal PipelineLayout(Device device, IEnumerable<DescriptorSetLayout> layouts, IEnumerable<(ShaderStageFlags Stage, uint Offset, uint Size)> constants)
        {
            Device = device;
            this.Link(Device);
            Layouts = layouts.ToList();
            PushConstantRanges = constants.ToList();
            Create();
        }

        internal ShaderStageFlags GetStage(uint offset, uint size)
        {
            var stage = ShaderStageFlags.None;
            uint min = offset, max = offset + size;
            foreach(var range in PushConstantRanges)
            {
                uint start = range.Offset, end = range.Offset + range.Size;
                if((min >= start && min < end) || (max > start && max <= end))
                    stage |= range.Stage;
            }
            return stage;
        }

        private void Create()
        {
            #region Validation

            var limits = Device.PhysicalDevice.Limits;
            if(Layouts.Any(layout => layout.Device != Device))
                throw new ArgumentException(nameof(Layouts));
            if(Layouts.Count > limits.MaxBoundDescriptorSets)
                throw new ArgumentOutOfRangeException(nameof(Layouts));

            ShaderStageFlags constantFlags = ShaderStageFlags.None;
            foreach(var range in PushConstantRanges)
            {
                if(range.Stage == 0)
                    throw new ArgumentException(nameof(PushConstantRanges));
                if(range.Offset % 4 != 0 || range.Size == 0 || range.Size % 4 != 0)
                    throw new ArgumentException(nameof(PushConstantRanges));
                if(range.Offset + range.Size >= limits.MaxPushConstantsSize)
                    throw new ArgumentException(nameof(PushConstantRanges));
                if((range.Stage & constantFlags) != 0)
                    throw new ArgumentException(nameof(PushConstantRanges));
                constantFlags |= range.Stage;
            }

            var samplers = new Dictionary<ShaderStageFlags, uint>();
            var uniformBuffers = new Dictionary<ShaderStageFlags, uint>();
            var storageBuffers = new Dictionary<ShaderStageFlags, uint>();
            var sampledImages = new Dictionary<ShaderStageFlags, uint>();
            var storageImages = new Dictionary<ShaderStageFlags, uint>();
            foreach(var layout in Layouts)
            foreach(var binding in layout.Bindings)
            {
                switch(binding.Type)
                {
                    case DescriptorType.Sampler:
                        Add(samplers);
                        break;
                    case DescriptorType.UniformBuffer:
                    case DescriptorType.UniformBufferDynamic:
                        Add(uniformBuffers);
                        break;
                    case DescriptorType.StorageBuffer:
                    case DescriptorType.StorageBufferDynamic:
                        Add(storageBuffers);
                        break;
                    case DescriptorType.SampledImage:
                    case DescriptorType.UniformTexelBuffer:
                        Add(sampledImages);
                        break;
                    case DescriptorType.StorageImage:
                    case DescriptorType.StorageTexelBuffer:
                        Add(storageImages);
                        break;
                    case DescriptorType.CombinedImageSampler:
                        Add(samplers);
                        Add(sampledImages);
                        break;
                }

                void Add(IDictionary<ShaderStageFlags, uint> dict)
                {
                    foreach(var stage in Util.GetFlags(binding.StageFlags))
                        dict[stage] = dict.GetOrDefault(stage) + binding.Count;
                }
            }
            if(samplers.Count > 0 && samplers.Values.Max() > limits.MaxPerStageDescriptorSamplers)
                throw new NotSupportedException(nameof(Layouts));
            if(uniformBuffers.Count > 0 && uniformBuffers.Values.Max() > limits.MaxPerStageDescriptorUniformBuffers)
                throw new NotSupportedException(nameof(Layouts));
            if(storageBuffers.Count > 0 && storageBuffers.Values.Max() > limits.MaxPerStageDescriptorStorageBuffers)
                throw new NotSupportedException(nameof(Layouts));
            if(sampledImages.Count > 0 && sampledImages.Values.Max() > limits.MaxPerStageDescriptorSampledImages)
                throw new NotSupportedException(nameof(Layouts));
            if(storageImages.Count > 0 && storageImages.Values.Max() > limits.MaxPerStageDescriptorStorageImages)
                throw new NotSupportedException(nameof(Layouts));

            #endregion

            var layoutPtr = stackalloc DescriptorSetLayoutHandle[Layouts.Count];
            var rangePtr = stackalloc PushConstantRange[PushConstantRanges.Count];
            for(uint i = 0; i < Layouts.Count; i++)
                layoutPtr[i] = Layouts[(int)i];
            for(uint i = 0; i < PushConstantRanges.Count; i++)
                rangePtr[i] = PushConstantRanges[(int)i];
            var info = new PipelineLayoutCreateInfo((uint)Layouts.Count, layoutPtr, (uint)PushConstantRanges.Count, rangePtr);
            using(Device.WeakLock)
            {
                PipelineLayoutHandle handle;
                using(Device.Allocator?.WeakLock)
                    vkCreatePipelineLayout(Device, &info, Device.Allocator, out handle).Throw();
                Handle = handle;
                Device.Add(this);
            }
        }
        protected override void Destroy()
        {
            using(Device.WeakLock)
                Device.Remove(this);
            using(Device.Allocator?.WeakLock)
                vkDestroyPipelineLayout(Device, this, Device.Allocator);
        }

        private readonly VkCreatePipelineLayout vkCreatePipelineLayout;
        private readonly VkDestroyPipelineLayout vkDestroyPipelineLayout;

        private delegate Result VkCreatePipelineLayout(DeviceHandle device, PipelineLayoutCreateInfo* info, AllocationCallbacks* allocator, out PipelineLayoutHandle layout);
        private delegate void VkDestroyPipelineLayout(DeviceHandle device, PipelineLayoutHandle layout, AllocationCallbacks* allocator);
    }
}