﻿using System;
using System.Runtime.InteropServices;
using oovulkan.descriptor.layout;

// ReSharper disable PrivateFieldCanBeConvertedToLocalVariable

namespace oovulkan.pipeline.layout
{
    [Flags]
    internal enum PipelineLayoutCreateFlags : int
    {
        None = 0
    }

    [StructLayout(LayoutKind.Sequential)]
    internal struct PushConstantRange
    {
        public readonly ShaderStageFlags Stage;
        public readonly uint Offset;
        public readonly uint Size;

        private PushConstantRange(ShaderStageFlags stage, uint offset, uint size)
        {
            Stage = stage;
            Offset = offset;
            Size = size;
        }

        public static implicit operator PushConstantRange((ShaderStageFlags Stage, uint Offset, uint Size) range) => new PushConstantRange(range.Stage, range.Offset, range.Size);
        public static implicit operator (ShaderStageFlags Stage, uint Offset, uint Size)(PushConstantRange range) => (range.Stage, range.Offset, range.Size);
    }

    [StructLayout(LayoutKind.Sequential)]
    internal unsafe struct PipelineLayoutCreateInfo
    {
        private readonly StructureType Type;
        private readonly void* Next;
        private readonly PipelineLayoutCreateFlags Flags;
        public readonly uint LayoutCount;
        public readonly DescriptorSetLayoutHandle* LayoutPtr;
        public readonly uint RangeCount;
        public readonly PushConstantRange* RangePtr;

        public PipelineLayoutCreateInfo(uint layoutCount, DescriptorSetLayoutHandle* layoutPtr, uint rangeCount, PushConstantRange* rangePtr)
        {
            Type = StructureType.PipelineLayoutCreateInfo;
            Next = null;
            Flags = PipelineLayoutCreateFlags.None;
            LayoutCount = layoutCount;
            LayoutPtr = layoutPtr;
            RangeCount = rangeCount;
            RangePtr = rangePtr;
        }
    }
}