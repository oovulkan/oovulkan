﻿namespace oovulkan.pipeline
{
    public enum PipelineType
    {
        Graphics,
        Compute,
        Transfer,
        Host
    }
}