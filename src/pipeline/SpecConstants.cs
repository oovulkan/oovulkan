﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using oovulkan.util;

namespace oovulkan.pipeline
{
    [AttributeUsage(AttributeTargets.Interface)]
    public class SpecConstantsAttribute : DataInterfaceAttribute
    {
        public SpecConstantsAttribute(bool isExplicitLayout=false) : base(isExplicitLayout) { }
    }
    [AttributeUsage(AttributeTargets.Property)]
    public class SpecConstantAttribute : DataAttribute
    {
        public readonly uint ID;

        public SpecConstantAttribute(uint id, uint offset=uint.MaxValue, [CallerLineNumber]int line=-1) : base(offset, line, false)
        {
            ID = id;
        }
    }

    public abstract class SpecConstants : DataInterface
    {
        protected SpecConstants(int size) : base(size) { }

        internal static IEnumerable<(uint ID, uint Offset, Word Size)> GetRanges(Type type)
        {
            Validate(type, typeof(SpecConstants), typeof(SpecConstantsAttribute), typeof(SpecConstantAttribute));
            return GetProperties(type, typeof(SpecConstantsAttribute), typeof(SpecConstantAttribute))
                .Select(prop => (prop.Info.GetCustomAttribute<SpecConstantAttribute>().ID, prop.Offset, (Word)(long)prop.Size));
        }

        public static T Implement<T>()
        {
            return Implement<T>(typeof(SpecConstants), typeof(SpecConstantsAttribute), typeof(SpecConstantAttribute));
        }
    }
}