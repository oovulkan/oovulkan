﻿using System;

namespace oovulkan
{
    public abstract class BuilderBase<T>
    {
        private bool isDone;
        public readonly T Object;

        protected BuilderBase(T obj)
        {
            Object = obj;
        }

        protected internal void AssertNotDone()
        {
            if(isDone)
                throw new InvalidOperationException();
        }

        protected virtual void MarkDone()
        {
            isDone = true;
        }
    }
}