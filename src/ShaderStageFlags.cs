﻿using System;

namespace oovulkan
{
    [Flags]
    public enum ShaderStageFlags : int
    {
        None = 0,
        Vertex = 1,
        TessallationControl = 2,
        TessallationEvaluation = 4,
        Geometery = 8,
        Fragment = 16,
        Compute = 32,
        AllGraphics = Vertex | TessallationControl | TessallationEvaluation | Geometery | Fragment,
        All = int.MaxValue
    }
}