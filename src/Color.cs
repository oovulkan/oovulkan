﻿using System.Runtime.InteropServices;

namespace oovulkan
{
    [StructLayout(LayoutKind.Explicit)]
    public struct Color
    {
        [FieldOffset(0)]
        private readonly float FR;
        [FieldOffset(0)]
        private readonly int IR;
        [FieldOffset(0)]
        private readonly uint UR;

        [FieldOffset(4)]
        private readonly float FG;
        [FieldOffset(4)]
        private readonly int IG;
        [FieldOffset(4)]
        private readonly uint UG;

        [FieldOffset(8)]
        private readonly float FB;
        [FieldOffset(8)]
        private readonly int IB;
        [FieldOffset(8)]
        private readonly uint UB;

        [FieldOffset(12)]
        private readonly float FA;
        [FieldOffset(12)]
        private readonly int IA;
        [FieldOffset(12)]
        private readonly uint UA;

        private Color(float fr, float fg, float fb, float fa) : this()
        {
            FR = fr;
            FG = fg;
            FB = fb;
            FA = fa;
        }
        private Color(int ir, int ig, int ib, int ia) : this()
        {
            IR = ir;
            IG = ig;
            IB = ib;
            IA = ia;
        }
        private Color(uint ur, uint ug, uint ub, uint ua) : this()
        {
            UR = ur;
            UG = ug;
            UB = ub;
            UA = ua;
        }

        public static implicit operator Color((float R, float G, float B, float A) color) => new Color(color.R, color.G, color.B, color.A);
        public static implicit operator Color((int R, int G, int B, int A) color) => new Color(color.R, color.G, color.B, color.A);
        public static implicit operator Color((uint R, uint G, uint B, uint A) color) => new Color(color.R, color.G, color.B, color.A);

        public static implicit operator (float R, float G, float B, float A)(Color color) => (color.FR, color.FG, color.FB, color.FA);
        public static implicit operator (int R, int G, int B, int A)(Color color) => (color.IR, color.IG, color.IB, color.IA);
        public static implicit operator (uint R, uint G, uint B, uint A)(Color color) => (color.UR, color.UG, color.UB, color.UA);
    }
}