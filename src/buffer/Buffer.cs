﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using oovulkan.buffer.view;
using oovulkan.device;
using oovulkan.image;
using oovulkan.memory;
using oovulkan.queue.family;
using oovulkan.util;
using oovulkan.util.collection;

#pragma warning disable 649

namespace oovulkan.buffer
{
    [StructLayout(LayoutKind.Sequential)]
    public struct BufferHandle
    {
        private long Value;

        public static implicit operator BufferHandle(long? value) => new BufferHandle {Value = value ?? 0};
        public static implicit operator long(BufferHandle handle) => handle.Value;
    }

    public unsafe class Buffer : VulkanObject<BufferHandle>
    {
        public const ulong WHOLE_SIZE = ulong.MaxValue;

        protected internal override bool IsChild => true;
        protected internal override VulkanObject Parent => Device;

        public readonly Device Device;
        public readonly BufferCreateFlags Flags;
        public readonly ulong Size;
        public readonly BufferUsageFlags Usage;
        public readonly SharingMode SharingMode;
        public Memory BoundMemory{ get; private set; }
        public Word BoundOffset{ get; private set; }
        protected internal readonly ReadonlyList<QueueFamily> Families;

        private MemoryRequirements memoryRequirements;
        private readonly HashSet<BufferView> views = new HashSet<BufferView>();

        public MemoryRequirements MemoryRequirements => memoryRequirements ?? (memoryRequirements = GetMemoryRequirements());
        public ReadonlySet<BufferView> Views => views;

        internal Buffer(Device device, BufferCreateFlags flags, ulong size, BufferUsageFlags usage, SharingMode sharingMode, IEnumerable<QueueFamily> families)
        {
            Device = device;
            this.Link(Device);
            Flags = flags;
            Size = size;
            Usage = usage;
            SharingMode = sharingMode;
            Families = families.ToList();
            Create();
        }

        internal void Add(BufferView view) { views.Add(view); }
        internal void Remove(BufferView view) { views.Remove(view); }

        private void Create()
        {
            #region Validation

            if(Usage == BufferUsageFlags.None)
                throw new ArgumentException(nameof(Usage));
            if(Size == 0)
                throw new ArgumentOutOfRangeException(nameof(Size));
            if(SharingMode == SharingMode.Concurrent)
            {
                if(Families.Count == 0)
                    throw new ArgumentNullException(nameof(Families));
                if(Families.Any(family => family.PhysicalDevice != Device.PhysicalDevice))
                    throw new ArgumentException(nameof(Families));
                if(Families.Distinct().Count() != Families.Count)
                    throw new ArgumentException(nameof(Families));
            }
            var features = Device.Features;
            if(!features.SparseBinding && Flags.HasFlag(BufferCreateFlags.SparseBinding))
                throw new NotSupportedException(nameof(Flags));
            if(!features.SparseResidencyBuffer && Flags.HasFlag(BufferCreateFlags.SparseResidency))
                throw new NotSupportedException(nameof(Flags));
            if(!features.SparseResidencyAliased && Flags.HasFlag(BufferCreateFlags.SparseAliased))
                throw new NotSupportedException(nameof(Flags));
            if((Flags.HasFlag(BufferCreateFlags.SparseResidency) || Flags.HasFlag(BufferCreateFlags.SparseAliased)) && !Flags.HasFlag(BufferCreateFlags.SparseBinding))
                throw new ArgumentException(nameof(Flags));

            #endregion

            uint familyCount = (uint)(Families?.Count ?? 0);
            var familyPtr = stackalloc QueueFamilyHandle[(int)familyCount];
            for(uint i = 0; i < familyCount; i++)
                // ReSharper disable once PossibleNullReferenceException
                familyPtr[i] = Families[(int)i];
            var info = new BufferCreateInfo(Flags, (Word)Size, Usage, SharingMode, familyCount, familyPtr);
            using(Device.WeakLock)
            {
                BufferHandle handle;
                using(Device.Allocator?.WeakLock)
                    vkCreateBuffer(Device, &info, Device.Allocator, out handle).Throw();
                Handle = handle;
                Device.Add(this);
            }
        }
        protected override void Destroy()
        {
            using(Device.WeakLock)
                Device.Remove(this);
            using(Device.Allocator?.WeakLock)
                vkDestroyBuffer(Device, this, Device.Allocator);
        }

        public BufferView GetView(Format format, ulong offset=0, ulong range=BufferView.WHOLE_SIZE)
        {
            return new BufferView(this, format, offset, range);
        }

        private MemoryRequirements GetMemoryRequirements()
        {
            using(Lock)
            {
                vkGetBufferMemoryRequirements(Device, this, out var requirements);
                return new MemoryRequirements(requirements, Device.PhysicalDevice.MemoryProperties.Types);
            }
        }

        public void Bind(Memory memory, Word offset=default(Word))
        {
            if(memory.Device != Device)
                throw new ArgumentException(nameof(memory));
            if(Flags.HasFlag(BufferCreateFlags.SparseAliased) || Flags.HasFlag(BufferCreateFlags.SparseBinding) || Flags.HasFlag(BufferCreateFlags.SparseResidency))
                throw new InvalidOperationException();
            if(offset >= memory.Size)
                throw new ArgumentOutOfRangeException();
            var limits = Device.PhysicalDevice.Limits;
            if((Usage.HasFlag(BufferUsageFlags.UniformTexelBuffer) || Usage.HasFlag(BufferUsageFlags.StorageTexelBuffer))
               && offset % limits.MinTexelBufferOffsetAlignment != 0)
                throw new ArgumentException(nameof(offset));
            if(Usage.HasFlag(BufferUsageFlags.UniformBuffer) && offset % limits.MinUniformBufferOffsetAlignment != 0)
                throw new ArgumentException(nameof(offset));
            if(Usage.HasFlag(BufferUsageFlags.StorageBuffer) && offset % limits.MinStorageBufferOffsetAlignment != 0)
                throw new ArgumentException(nameof(offset));
            if(!MemoryRequirements.Types.Contains(memory.Type))
                throw new ArgumentException(nameof(memory));
            if(offset % MemoryRequirements.Alignment != 0)
                throw new ArgumentException(nameof(offset));
            if(offset + MemoryRequirements.Size > memory.Size)
                throw new ArgumentException(nameof(memory));

            using(Lock)
            using(memory.Lock)
            {
                if(BoundMemory != null)
                    throw new InvalidOperationException();
                vkBindBufferMemory(Device, this, memory, offset).Throw();
                BoundMemory = memory;
                BoundOffset = offset;
            }
        }

        private readonly VkCreateBuffer vkCreateBuffer;
        private readonly VkDestroyBuffer vkDestroyBuffer;
        private readonly VkGetBufferMemoryRequirements vkGetBufferMemoryRequirements;
        private readonly VkBindBufferMemory vkBindBufferMemory;

        private delegate Result VkCreateBuffer(DeviceHandle device, BufferCreateInfo* info, AllocationCallbacks* allocator, out BufferHandle buffer);
        private delegate void VkDestroyBuffer(DeviceHandle device, BufferHandle buffer, AllocationCallbacks* allocator);
        private delegate void VkGetBufferMemoryRequirements(DeviceHandle device, BufferHandle buffer, out MemoryRequirements.Native requirements);
        private delegate Result VkBindBufferMemory(DeviceHandle device, BufferHandle buffer, MemoryHandle memory, Word offset);

        //TODO: more buffer functions
    }
}