﻿using System;
using System.Runtime.InteropServices;
using oovulkan.util;

// ReSharper disable PrivateFieldCanBeConvertedToLocalVariable

namespace oovulkan.buffer.view
{
    [Flags]
    public enum BufferViewCreateFlags : int
    {
        None = 0
    }

    [StructLayout(LayoutKind.Sequential)]
    internal unsafe struct BufferViewCreateInfo
    {
        private readonly StructureType Type;
        private readonly void* Next;
        private readonly BufferViewCreateFlags Flags;
        public readonly BufferHandle Buffer;
        public readonly Format Format;
        public readonly Word Offset;
        public readonly Word Range;

        public BufferViewCreateInfo(Buffer buffer, Format format, Word offset, Word range)
        {
            Type = StructureType.BufferViewCreateInfo;
            Next = null;
            Flags = BufferViewCreateFlags.None;
            Buffer = buffer;
            Format = format;
            Offset = offset;
            Range = range;
        }
    }
}