﻿using System;
using System.Runtime.InteropServices;
using oovulkan.device;
using oovulkan.device.physical;
using oovulkan.util;

#pragma warning disable 649

namespace oovulkan.buffer.view
{
    [StructLayout(LayoutKind.Sequential)]
    public struct BufferViewHandle
    {
        private long Value;

        public static implicit operator BufferViewHandle(long? value) => new BufferViewHandle {Value = value ?? 0};
        public static implicit operator long(BufferViewHandle handle) => handle.Value;
    }

    public unsafe class BufferView : VulkanObject<BufferViewHandle>
    {
        public const ulong WHOLE_SIZE = 0xFFFFFFFFFFFFFFFF;

        protected internal override bool IsChild => true;
        protected internal override VulkanObject Parent => Buffer;

        public readonly Buffer Buffer;
        public readonly Format Format;
        public readonly ulong Offset;
        public readonly ulong Range;

        internal BufferView(Buffer buffer, Format format, ulong offset, ulong range)
        {
            Buffer = buffer;
            this.Link(Buffer.Device);
            Format = format;
            Offset = offset;
            Range = range;
            Create();
        }

        private void Create()
        {
            #region Validation

            var limits = Buffer.Device.PhysicalDevice.Limits;

            if(Offset > Buffer.Size)
                throw new ArgumentException(nameof(Offset));
            if(Offset % limits.MinTexelBufferOffsetAlignment != 0)
                throw new ArgumentException(nameof(Offset));
            if(Range != WHOLE_SIZE)
            {
                if(Range == 0)
                    throw new ArgumentException(nameof(Range));
                var eClass = Format.GetClass();
                if(eClass != FormatClass.Other)
                {
                    var eSize = (uint)eClass.GetElementSize();
                    if(Range % eSize != 0)
                        throw new ArgumentException(nameof(Range));
                    if((Range / eSize) > limits.MaxTexelBufferElements)
                        throw new ArgumentException(nameof(Range));
                }
                if(Offset + Range > Buffer.Size)
                    throw new ArgumentException(nameof(Range));
            }
            if(!Buffer.Usage.HasFlag(BufferUsageFlags.UniformTexelBuffer) && !Buffer.Usage.HasFlag(BufferUsageFlags.StorageTexelBuffer))
                throw new InvalidOperationException();
            var formatProperties = Buffer.Device.PhysicalDevice.FormatProperties(Format);
            if(Buffer.Usage.HasFlag(BufferUsageFlags.UniformTexelBuffer) && !formatProperties.BufferFeatures.HasFlag(FormatFeatureFlags.UniformTexelBuffer))
                throw new NotSupportedException();
            if(Buffer.Usage.HasFlag(BufferUsageFlags.StorageTexelBuffer) && !formatProperties.BufferFeatures.HasFlag(FormatFeatureFlags.StorageTexelBuffer))
                throw new NotSupportedException();
            //TODO: If buffer is non-sparse then it must be bound completely and contiguously to a single VkDeviceMemory object

            #endregion

            var info = new BufferViewCreateInfo(Buffer, Format, (Word)Offset, (Word)Range);
            using(Buffer.WeakLock)
            {
                BufferViewHandle handle;
                using(Buffer.Device.Allocator?.WeakLock)
                    vkCreateBufferView(Buffer.Device, &info, Buffer.Device.Allocator, out handle).Throw();
                Handle = handle;
                Buffer.Add(this);
            }
        }
        protected override void Destroy()
        {
            using(Buffer.WeakLock)
                Buffer.Remove(this);
            using(Buffer.Device.Allocator?.WeakLock)
                vkDestroyBufferView(Buffer.Device, this, Buffer.Device.Allocator);
        }

        private readonly VkCreateBufferView vkCreateBufferView;
        private readonly VkDestroyBufferView vkDestroyBufferView;

        private delegate Result VkCreateBufferView(DeviceHandle device, BufferViewCreateInfo* info, AllocationCallbacks* allocator, out BufferViewHandle view);
        private delegate void VkDestroyBufferView(DeviceHandle device, BufferViewHandle image, AllocationCallbacks* allocator);
    }
}