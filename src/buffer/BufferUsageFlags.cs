﻿using System;

namespace oovulkan.buffer
{
    [Flags]
    public enum BufferUsageFlags : int
    {
        None = 0,
        TransferSrc = 1,
        TransferDst = 2,
        UniformTexelBuffer = 4,
        StorageTexelBuffer = 8,
        UniformBuffer = 16,
        StorageBuffer = 32,
        IndexBuffer = 64,
        VertexBuffer = 128,
        IndirectBuffer = 256
    }
}