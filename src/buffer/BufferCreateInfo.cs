﻿using System;
using System.Runtime.InteropServices;
using oovulkan.image;
using oovulkan.queue.family;
using oovulkan.util;

// ReSharper disable PrivateFieldCanBeConvertedToLocalVariable

namespace oovulkan.buffer
{
    [Flags]
    public enum BufferCreateFlags : int
    {
        None = 0,
        SparseBinding = 1,
        SparseResidency = 2,
        SparseAliased = 4
    }

    [StructLayout(LayoutKind.Sequential)]
    internal unsafe struct BufferCreateInfo
    {
        private readonly StructureType Type;
        private readonly void* Next;
        public readonly BufferCreateFlags Flags;
        public readonly Word Size;
        public readonly BufferUsageFlags Usage;
        public readonly SharingMode SharingMode;
        public readonly uint QueueFamilyCount;
        public readonly QueueFamilyHandle* QueueFamilyPtr;

        public BufferCreateInfo(BufferCreateFlags flags, Word size, BufferUsageFlags usage, SharingMode sharingMode, uint queueFamilyCount, QueueFamilyHandle* queueFamilyPtr)
        {
            Type = StructureType.BufferCreateInfo;
            Next = null;
            Flags = flags;
            Size = size;
            Usage = usage;
            SharingMode = sharingMode;
            QueueFamilyCount = queueFamilyCount;
            QueueFamilyPtr = queueFamilyPtr;
        }
    }
}