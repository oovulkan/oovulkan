﻿namespace oovulkan.buffer
{
    public struct BufferPosition
    {
        public readonly Buffer Buffer;
        public readonly ulong Offset;

        private BufferPosition(Buffer buffer, ulong offset)
        {
            Buffer = buffer;
            Offset = offset;
        }

        public static implicit operator BufferPosition(Buffer buffer) => new BufferPosition(buffer, 0);
        public static implicit operator BufferPosition((Buffer Buffer, ulong Offset) pos) => new BufferPosition(pos.Buffer, pos.Offset);
    }
}