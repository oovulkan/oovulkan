﻿using System.Runtime.InteropServices;
using oovulkan.image.view;
using oovulkan.util;

namespace oovulkan
{
    [StructLayout(LayoutKind.Sequential)]
    internal struct BufferCopy
    {
        public readonly Word SrcOffset;
        public readonly Word DstOffset;
        public readonly Word Size;

        private BufferCopy(Word srcOffset, Word dstOffset, Word size)
        {
            SrcOffset = srcOffset;
            DstOffset = dstOffset;
            Size = size;
        }

        public static implicit operator BufferCopy((ulong SrcOffset, ulong DstOffset, ulong Size) region) => new BufferCopy(region.SrcOffset, region.DstOffset, region.Size);
        public static implicit operator (ulong SrcOffset, ulong DstOffset, ulong Size)(BufferCopy region) => (region.SrcOffset, region.DstOffset, region.Size);
    }

    [StructLayout(LayoutKind.Sequential)]
    internal struct ImageSubresourceLayers
    {
        public readonly ImageAspectFlags Aspect;
        public readonly uint MipLevel;
        public readonly uint BaseArrayLayer;
        public readonly uint LayerCount;

        public ImageSubresourceLayers(ImageAspectFlags aspect, uint mipLevel, uint baseArrayLayer, uint layerCount)
        {
            Aspect = aspect;
            MipLevel = mipLevel;
            BaseArrayLayer = baseArrayLayer;
            LayerCount = layerCount;
        }
    }

    [StructLayout(LayoutKind.Sequential)]
    internal struct ImageCopy
    {
        public readonly ImageSubresourceLayers SrcSubresource;
        public readonly I3 SrcOffset;
        public readonly ImageSubresourceLayers DstSubresource;
        public readonly I3 DstOffset;
        public readonly U3 Extent;

        public ImageCopy(ImageAspectFlags aspect, uint srcMip, uint dstMip, uint srcBaseArrayLayer, uint dstBaseArrayLayer,
            uint layerCount, I3 srcOffset, I3 dstOffset, U3 extent)
        {
            SrcSubresource = new ImageSubresourceLayers(aspect, srcMip, srcBaseArrayLayer, layerCount);
            SrcOffset = srcOffset;
            DstSubresource = new ImageSubresourceLayers(aspect, dstMip, dstBaseArrayLayer, layerCount);
            DstOffset = dstOffset;
            Extent = extent;
        }
    }

    [StructLayout(LayoutKind.Sequential)]
    internal struct ImageBlit
    {
        public readonly ImageSubresourceLayers SrcSubresource;
        public readonly I3 SrcMinOffset;
        public readonly I3 SrcMaxOffset;
        public readonly ImageSubresourceLayers DstSubresource;
        public readonly I3 DstMinOffset;
        public readonly I3 DstMaxOffset;

        public ImageBlit(ImageAspectFlags aspect, uint srcMip, uint dstMip, uint srcBaseArrayLayer, uint dstBaseArrayLayer,
            uint layerCount, I3 srcMin, I3 srcMax, I3 dstMin, I3 dstMax)
        {
            SrcSubresource = new ImageSubresourceLayers(aspect, srcMip, srcBaseArrayLayer, layerCount);
            SrcMinOffset = srcMin;
            SrcMaxOffset = srcMax;
            DstSubresource = new ImageSubresourceLayers(aspect, dstMip, dstBaseArrayLayer, layerCount);
            DstMinOffset = dstMin;
            DstMaxOffset = dstMax;
        }
    }

    [StructLayout(LayoutKind.Sequential)]
    internal struct BufferImageCopy
    {
        public readonly Word BufferOffset;
        public readonly uint BufferRowLength;
        public readonly uint BufferImageHeight;
        public readonly ImageSubresourceLayers ImageSubresource;
        public readonly I3 ImageOffset;
        public readonly U3 ImageExtent;

        public BufferImageCopy(Word bufferOffset, uint bufferRowLength, uint bufferImageHeight,
            ImageAspectFlags aspect, uint mip, uint baseArrayLayer, uint layerCount, I3 imageOffset, U3 imageExtent)
        {
            BufferOffset = bufferOffset;
            BufferRowLength = bufferRowLength;
            BufferImageHeight = bufferImageHeight;
            ImageSubresource = new ImageSubresourceLayers(aspect, mip, baseArrayLayer, layerCount);
            ImageOffset = imageOffset;
            ImageExtent = imageExtent;
        }
    }

    [StructLayout(LayoutKind.Sequential)]
    internal struct ImageResolve
    {
        public readonly ImageSubresourceLayers SrcSubresource;
        public readonly I3 SrcOffset;
        public readonly ImageSubresourceLayers DstSubresource;
        public readonly I3 DstOffset;
        public readonly U3 Extent;

        public ImageResolve(uint srcMip, uint dstMip, uint srcBaseArrayLayer, uint dstBaseArrayLayer,
            uint layerCount, I3 srcOffset, I3 dstOffset, U3 extent)
        {
            SrcSubresource = new ImageSubresourceLayers(ImageAspectFlags.Color, srcMip, srcBaseArrayLayer, layerCount);
            SrcOffset = srcOffset;
            DstSubresource = new ImageSubresourceLayers(ImageAspectFlags.Color, dstMip, dstBaseArrayLayer, layerCount);
            DstOffset = dstOffset;
            Extent = extent;
        }
    }
}