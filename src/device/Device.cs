﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using oovulkan.buffer;
using oovulkan.command.pool;
using oovulkan.descriptor.layout;
using oovulkan.descriptor.pool;
using oovulkan.device.physical;
using oovulkan.image;
using oovulkan.memory;
using oovulkan.pipeline;
using oovulkan.pipeline.cache;
using oovulkan.pipeline.graphics;
using oovulkan.pipeline.layout;
using oovulkan.query;
using oovulkan.queue;
using oovulkan.queue.family;
using oovulkan.render;
using oovulkan.sampler;
using oovulkan.shader;
using oovulkan.surface;
using oovulkan.synchronization.@event;
using oovulkan.synchronization.fence;
using oovulkan.synchronization.semaphore;
using oovulkan.util;
using oovulkan.util.collection;
using Buffer = oovulkan.buffer.Buffer;

// ReSharper disable InconsistentNaming
#pragma warning disable 649

namespace oovulkan.device
{
    [StructLayout(LayoutKind.Sequential)]
    public unsafe struct DeviceHandle
    {
        private void* Ptr;

        public static implicit operator DeviceHandle(void* ptr) => new DeviceHandle {Ptr = ptr};
        public static implicit operator void*(DeviceHandle handle) => handle.Ptr;
    }

    public unsafe class Device : VulkanObject<DeviceHandle>
    {
        protected internal override bool IsChild => true;
        protected internal override VulkanObject Parent => PhysicalDevice;
        protected internal override bool IsParent => true;
        protected internal override IEnumerable<VulkanObject> Children => CollectionUtil.All<VulkanObject>(
            semaphores, fences, events, shaderModules, images, samplers, descriptorSetLayouts, descriptorPools,
            buffers, renderpasses, pipelineLayouts, pipelineCaches, pipelines, queryPools, surfaces, commandPools,
            memories
        );

        public readonly PhysicalDevice PhysicalDevice;
        public readonly Allocator Allocator;

        public readonly ReadonlyDictionary<QueueFamily, ReadonlyList<Queue>> QueueGroups;
        public readonly ReadonlySet<string> Layers;
        public readonly ReadonlySet<string> Extensions;
        public readonly PhysicalDeviceFeatures Features;

        private readonly HashSet<Semaphore> semaphores = new HashSet<Semaphore>();
        private readonly HashSet<Fence> fences = new HashSet<Fence>();
        private readonly HashSet<Event> events = new HashSet<Event>();
        private readonly HashSet<ShaderModule> shaderModules = new HashSet<ShaderModule>();
        private readonly HashSet<Image> images = new HashSet<Image>();
        private readonly HashSet<Sampler> samplers = new HashSet<Sampler>();
        private readonly HashSet<DescriptorSetLayout> descriptorSetLayouts = new HashSet<DescriptorSetLayout>();
        private readonly HashSet<DescriptorPool> descriptorPools = new HashSet<DescriptorPool>();
        private readonly HashSet<Buffer> buffers = new HashSet<Buffer>();
        private readonly HashSet<Renderpass> renderpasses = new HashSet<Renderpass>();
        private readonly HashSet<PipelineLayout> pipelineLayouts = new HashSet<PipelineLayout>();
        private readonly HashSet<PipelineCache> pipelineCaches = new HashSet<PipelineCache>();
        private readonly HashSet<Pipeline> pipelines = new HashSet<Pipeline>();
        private readonly HashSet<QueryPool> queryPools = new HashSet<QueryPool>();
        private readonly HashSet<Surface> surfaces = new HashSet<Surface>();
        private readonly HashSet<CommandPool> commandPools = new HashSet<CommandPool>();
        private readonly HashSet<Memory> memories = new HashSet<Memory>();

        public ReadonlySet<Semaphore> Semaphores => semaphores;
        public ReadonlySet<Fence> Fences => fences;
        public ReadonlySet<Event> Events => events;
        public ReadonlySet<ShaderModule> ShaderModules => shaderModules;
        public ReadonlySet<Image> Images => images;
        public ReadonlySet<Sampler> Samplers => samplers;
        public ReadonlySet<DescriptorSetLayout> DescriptorSetLayouts => descriptorSetLayouts;
        public ReadonlySet<DescriptorPool> DescriptorPools => descriptorPools;
        public ReadonlySet<Buffer> Buffers => buffers;
        public ReadonlySet<Renderpass> Renderpasses => renderpasses;
        public ReadonlySet<PipelineLayout> PipelineLayouts => pipelineLayouts;
        public ReadonlySet<PipelineCache> PipelineCaches => pipelineCaches;
        public ReadonlySet<Pipeline> Pipelines => pipelines;
        public ReadonlySet<QueryPool> QueryPools => queryPools;
        public ReadonlySet<Surface> Surfaces => surfaces;
        public ReadonlySet<CommandPool> CommandPools => commandPools;
        public ReadonlySet<Memory> Memories => memories;

        public readonly Pool<Semaphore> SemaphorePool;
        public readonly Pool<Fence> FencePool;
        public readonly Pool<Event> EventPool;

        private IDisposable QueueLock => QueueGroups.Values.SelectMany(list => list).LockAll(queue => queue.Lock);

        private Device(PhysicalDevice physicalDevice)
        {
            PhysicalDevice = physicalDevice;
            Allocator = PhysicalDevice.Instance.Allocator;

            SemaphorePool = new Pool<Semaphore>(GetSemaphore, semaphore => !semaphore.IsDestroyed);
            FencePool = new Pool<Fence>(() => GetFence(), fence => !fence.IsDestroyed, fence => fence.Reset());
            EventPool = new Pool<Event>(GetEvent, @event => !@event.IsDestroyed);
        }

        //Building "constructor"
        // ReSharper disable once UnusedMember.Local
        private Device(IEnumerable<(QueueFamily Family, List<float> Weights)> queueGroups, IEnumerable<string> layers, IEnumerable<string> extensions, DeviceFeatures features)
        {
            Layers = new ReadonlySet<string>(layers);
            Extensions = new ReadonlySet<string>(extensions);
            Features = features;

            vkGetDeviceProcAddr = PhysicalDevice.Instance.GetFunction<VkGetDeviceProcAddr>("vkGetDeviceProcAddr");
            vkCreateDevice = PhysicalDevice.Instance.GetFunction<VkCreateDevice>("vkCreateDevice");

            var groups = queueGroups.ToArray();
            Create(groups);

            this.Link(this);

            QueueGroups = groups.Select(group => (group.Family, GetQueues(group.Family, group.Weights))).ToDictionary();
        }

        internal void Add(Semaphore semaphore) { semaphores.Add(semaphore); }
        internal void Remove(Semaphore semaphore) { semaphores.Remove(semaphore); }
        internal void Add(Fence fence) { fences.Add(fence); }
        internal void Remove(Fence fence) { fences.Remove(fence); }
        internal void Add(Event @event) { events.Add(@event); }
        internal void Remove(Event @event) { events.Remove(@event); }
        internal void Add(ShaderModule module) { shaderModules.Add(module); }
        internal void Remove(ShaderModule module) { shaderModules.Remove(module); }
        internal void Add(Image image) { images.Add(image); }
        internal void Remove(Image image) { images.Remove(image); }
        internal void Add(Sampler sampler) { samplers.Add(sampler); }
        internal void Remove(Sampler sampler) { samplers.Remove(sampler); }
        internal void Add(DescriptorSetLayout layout) { descriptorSetLayouts.Add(layout); }
        internal void Remove(DescriptorSetLayout layout) { descriptorSetLayouts.Remove(layout); }
        internal void Add(DescriptorPool pool) { descriptorPools.Add(pool); }
        internal void Remove(DescriptorPool pool) { descriptorPools.Remove(pool); }
        internal void Add(Buffer buffer) { buffers.Add(buffer); }
        internal void Remove(Buffer buffer) { buffers.Remove(buffer); }
        internal void Add(Renderpass renderpass) { renderpasses.Add(renderpass); }
        internal void Remove(Renderpass renderpass) { renderpasses.Remove(renderpass); }
        internal void Add(PipelineLayout layout) { pipelineLayouts.Add(layout); }
        internal void Remove(PipelineLayout layout) { pipelineLayouts.Remove(layout); }
        internal void Add(PipelineCache cache) { pipelineCaches.Add(cache); }
        internal void Remove(PipelineCache cache) { pipelineCaches.Remove(cache); }
        internal void Add(Pipeline pipeline) { pipelines.Add(pipeline); }
        internal void Remove(Pipeline pipeline) { pipelines.Remove(pipeline); }
        internal void Add(QueryPool queryPool) { queryPools.Add(queryPool); }
        internal void Remove(QueryPool queryPool) { queryPools.Remove(queryPool); }
        internal void Add(Surface surface) { surfaces.Add(surface); }
        internal void Remove(Surface surface) { surfaces.Remove(surface); }
        internal void Add(CommandPool pool) { commandPools.Add(pool); }
        internal void Remove(CommandPool pool) { commandPools.Remove(pool); }
        internal void Add(Memory memory) { memories.Add(memory); }
        internal void Remove(Memory memory) { memories.Remove(memory); }

        private void Create((QueueFamily Family, List<float> Weights)[] queueGroups)
        {
            var groups = stackalloc DeviceQueueCreateInfo[queueGroups.Length];
            var layers = stackalloc AnsiString[Layers.Count];
            var extensions = stackalloc AnsiString[Extensions.Count];

            uint groupCount = 0;
            uint layerCount = 0;
            uint extensionCount = 0;
            foreach(var group in queueGroups)
            {
                var weights = stackalloc float[group.Weights.Count];
                for(uint i = 0; i < group.Weights.Count; i++)
                    weights[i] = group.Weights[(int)i];
                groups[groupCount++] = new DeviceQueueCreateInfo(group.Family, (uint)group.Weights.Count, weights);
            }
            foreach(var layer in Layers)
                layers[layerCount++] = (AnsiString)layer;
            foreach(var extension in Extensions)
                extensions[extensionCount++] = (AnsiString)extension;

            try
            {
                var features = (PhysicalDeviceFeatures.Native)Features;
                var info = new DeviceCreateInfo(groupCount, groups, layerCount, layers, extensionCount, extensions, &features);
                using(PhysicalDevice.WeakLock)
                {
                    DeviceHandle handle;
                    using(Allocator?.WeakLock)
                        vkCreateDevice(PhysicalDevice, &info, Allocator, out handle);
                    Handle = handle;
                    PhysicalDevice.Add(this);
                }
            }
            finally
            {
                for(uint i = 0; i < layerCount; i++)
                    layers[i].Dispose();
                for(uint i = 0; i < extensionCount; i++)
                    extensions[i].Dispose();
            }
        }
        private ReadonlyList<Queue> GetQueues(QueueFamily family, IEnumerable<float> weights)
        {
            using(WeakLock)
            {
                return weights.Select((weight, i) =>
                {
                    vkGetDeviceQueue(this, family, (uint)i, out QueueHandle handle);
                    return new Queue(this, handle, family, weight);
                }).ToList();
            }
        }
        protected override void Destroy()
        {
            using(PhysicalDevice.WeakLock)
                PhysicalDevice.Remove(this);
            using(Allocator?.WeakLock)
                vkDestroyDevice(this, Allocator);
        }
        public void Wait()
        {
            using(Lock)
            using(QueueLock)
                vkDeviceWaitIdle(Handle).Throw();
        }
        public Task WaitAsync() => Task.Run(() => Wait());

        public Semaphore GetSemaphore() => new Semaphore(this);
        public Fence GetFence(bool status = false) => new Fence(this, status);
        public Event GetEvent() => new Event(this);

        public ShaderModule GetShader(byte[] code) => new ShaderModule(this, code);
        public ShaderModule GetShaderFromStream(Stream stream)
        {
            long length = stream.Length;
            //round length up to the nearest multiple of 4
            length += (4 - length % 4) % 4;
            var code = new byte[length];
            stream.ReadTo(code);
            return GetShader(code);
        }
        public ShaderModule GetShaderFromFile(string name)
        {
            using(Stream stream = File.OpenRead(name))
                return GetShaderFromStream(stream);
        }
        public ShaderModule GetShaderFromResource(string name)
        {
            using(Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(name))
            {
                if(stream == null)
                    throw new FileNotFoundException();
                return GetShaderFromStream(stream);
            }
        }

        public Image1D GetImage1D(ImageCreateFlags flags, Format format, uint size, ImageUsageFlags usage, uint mipLevels=1, uint arrayLayers=1, SampleCountFlags samples=SampleCountFlags.Bit1, bool linearTiling=false, ImageLayout initialLayout=ImageLayout.Undefined)
        {
            var tiling = linearTiling ? ImageTiling.Linear : ImageTiling.Optimal;
            return new Image1D(this, flags, format, ImageType.X1D, (size, 1, 1), mipLevels, arrayLayers, samples, tiling, usage, initialLayout, SharingMode.Exclusive, null);
        }
        public ConcurrentImage1D GetConcurrentImage1D(ImageCreateFlags flags, Format format, uint size, ImageUsageFlags usage, uint mipLevels=1, uint arrayLayers=1, SampleCountFlags samples=SampleCountFlags.Bit1, bool linearTiling=false, ImageLayout initialLayout=ImageLayout.Undefined, params QueueFamily[] families)
        {
            var tiling = linearTiling ? ImageTiling.Linear : ImageTiling.Optimal;
            return new ConcurrentImage1D(this, flags, format, ImageType.X1D, (size, 1, 1), mipLevels, arrayLayers, samples, tiling, usage, initialLayout, SharingMode.Concurrent, families);
        }
        public Image2D GetImage2D(ImageCreateFlags flags, Format format, (uint Width, uint Height) size, ImageUsageFlags usage, uint mipLevels=1, uint arrayLayers=1, SampleCountFlags samples=SampleCountFlags.Bit1, bool linearTiling=false, ImageLayout initialLayout=ImageLayout.Undefined)
        {
            var tiling = linearTiling ? ImageTiling.Linear : ImageTiling.Optimal;
            return new Image2D(this, flags, format, ImageType.X2D, (size.Width, size.Height, 1), mipLevels, arrayLayers, samples, tiling, usage, initialLayout, SharingMode.Exclusive, null);
        }
        public ConcurrentImage2D GetConcurrentImage2D(ImageCreateFlags flags, Format format, (uint Width, uint Height) size, ImageUsageFlags usage, uint mipLevels=1, uint arrayLayers=1, SampleCountFlags samples=SampleCountFlags.Bit1, bool linearTiling=false, ImageLayout initialLayout=ImageLayout.Undefined, params QueueFamily[] families)
        {
            var tiling = linearTiling ? ImageTiling.Linear : ImageTiling.Optimal;
            return new ConcurrentImage2D(this, flags, format, ImageType.X2D, (size.Width, size.Height, 1), mipLevels, arrayLayers, samples, tiling, usage, initialLayout, SharingMode.Concurrent, families);
        }
        public Image3D GetImage3D(ImageCreateFlags flags, Format format, (uint Width, uint Height, uint Depth) size, ImageUsageFlags usage, uint mipLevels=1, uint arrayLayers=1, SampleCountFlags samples=SampleCountFlags.Bit1, bool linearTiling=false, ImageLayout initialLayout=ImageLayout.Undefined)
        {
            var tiling = linearTiling ? ImageTiling.Linear : ImageTiling.Optimal;
            return new Image3D(this, flags, format, ImageType.X3D, size, mipLevels, arrayLayers, samples, tiling, usage, initialLayout, SharingMode.Exclusive, null);
        }
        public ConcurrentImage3D GetConcurrentImage3D(ImageCreateFlags flags, Format format, (uint Width, uint Height, uint Depth) size, ImageUsageFlags usage, uint mipLevels=1, uint arrayLayers=1, SampleCountFlags samples=SampleCountFlags.Bit1, bool linearTiling=false, ImageLayout initialLayout=ImageLayout.Undefined, params QueueFamily[] families)
        {
            var tiling = linearTiling ? ImageTiling.Linear : ImageTiling.Optimal;
            return new ConcurrentImage3D(this, flags, format, ImageType.X2D, size, mipLevels, arrayLayers, samples, tiling, usage, initialLayout, SharingMode.Concurrent, families);
        }

        public Sampler GetSampler(Filter magFilter, Filter minFilter, SamplerMipmapMode mipmapMode, (SamplerAddressMode U, SamplerAddressMode V, SamplerAddressMode W) addressMode, float mipLoadBias, bool anisotropyEnable = false, float? maxAnisotropy = null, bool compareEnable = false, CompareOp? compareOp = null, float minLod = 0, float maxLod = 0, BorderColor? borderColor = null, bool unnormalizedCoordinates = false)
        {
            if(anisotropyEnable && maxAnisotropy == null)
                throw new ArgumentNullException(nameof(maxAnisotropy));
            if(compareEnable && compareOp == null)
                throw new ArgumentNullException(nameof(compareOp));
            if((addressMode.U == SamplerAddressMode.ClampToBorder || addressMode.V == SamplerAddressMode.ClampToBorder
                || addressMode.W == SamplerAddressMode.ClampToBorder) && borderColor == null)
                throw new ArgumentNullException(nameof(borderColor));
            return new Sampler(this, magFilter, minFilter, mipmapMode, addressMode, mipLoadBias, anisotropyEnable,
                maxAnisotropy ?? default(float), compareEnable, compareOp ?? default(CompareOp), minLod, maxLod,
                borderColor ?? default(BorderColor), unnormalizedCoordinates);
        }

        public DescriptorSetLayout GetDescriptorSetLayout(params DescriptorSetLayoutBinding[] bindings) => new DescriptorSetLayout(this, bindings);
        public DescriptorPool GetDescriptorPool(bool freeableSets, uint maxSets, params DescriptorPoolSize[] sizes) => new DescriptorPool(this, freeableSets, maxSets, sizes);

        public Buffer GetBuffer(ulong size, BufferUsageFlags usage, SharingMode sharingMode, BufferCreateFlags flags=BufferCreateFlags.None, params QueueFamily[] families) => new Buffer(this, flags, size, usage, sharingMode, families);

        public Renderpass.Builder GetRenderpass() => new Renderpass.Builder(this);

        public PipelineLayout GetPipelineLayout(Type pushConstants=null, params DescriptorSetLayout[] layouts)
        {
            var ranges = PushConstants.GetRanges(pushConstants);
            return new PipelineLayout(this, layouts, ranges);
        }
        public PipelineCache GetPipelineCache(byte[] data=null) => new PipelineCache(this, data);
        public GraphicsPipeline.Builder GetGraphicsPipeline(PipelineCreateFlags flags, PipelineLayout layout, Subpass subpass, Pipeline basePipeline=null, PipelineCache cache=null) => new GraphicsPipeline.Builder(this, flags, layout, subpass, basePipeline, cache);

        public QueryPool GetQueryPool(QueryType type, uint count)
        {
            if(type == QueryType.PipelineStatistics)
                throw new InvalidOperationException();
            return new QueryPool(this, type, count);
        }
        public PipelineStatisticQueryPool GetQueryPool(QueryType type, uint count, PipelineStatisticFlags statistics)
        {
            if(type != QueryType.PipelineStatistics)
                throw new InvalidOperationException();
            return new PipelineStatisticQueryPool(this, type, count, statistics);
        }

        public CommandPool GetCommandPool(QueueFamily family, CommandPoolCreateFlags flags=CommandPoolCreateFlags.None) => new CommandPool(this, family, flags);

        public Memory GetMemory(Word size, MemoryType type) => new Memory(this, size, type);

        [VulkanInterop.IgnoreAttribute]
        internal readonly VkGetDeviceProcAddr vkGetDeviceProcAddr;
        [VulkanInterop.IgnoreAttribute]
        private readonly VkCreateDevice vkCreateDevice;
        private readonly VkGetDeviceQueue vkGetDeviceQueue;
        private readonly VkDestroyDevice vkDestroyDevice;
        private readonly VkDeviceWaitIdle vkDeviceWaitIdle;

        internal delegate IntPtr VkGetDeviceProcAddr(DeviceHandle handle, AnsiString name);
        private delegate Result VkCreateDevice(PhysicalDeviceHandle physicalDevice, DeviceCreateInfo* info, AllocationCallbacks* allocator, out DeviceHandle device);
        private delegate void VkGetDeviceQueue(DeviceHandle device, QueueFamilyHandle family, uint index, out QueueHandle queue);
        private delegate void VkDestroyDevice(DeviceHandle device, AllocationCallbacks* allocator);
        private delegate Result VkDeviceWaitIdle(DeviceHandle device);

        public class Builder : BuilderBase<Device>
        {
            private static readonly ConstructorInfo Sneaky = typeof(Device).GetAnyConstructor(
                typeof(IEnumerable<(QueueFamily Family, List<float> Weights)>),
                typeof(IEnumerable<string>),
                typeof(IEnumerable<string>),
                typeof(DeviceFeatures)
            );
            private readonly Dictionary<QueueFamily, List<float>> queueGroups = new Dictionary<QueueFamily, List<float>>();
            private readonly HashSet<string> layers = new HashSet<string>();
            private readonly HashSet<string> extensions = new HashSet<string>();
            private DeviceFeatures features;

            internal Builder(PhysicalDevice physicalDevice) : base(new Device(physicalDevice)) { }

            public Builder Layers(params string[] layers)
            {
                return Layers((IEnumerable<string>)layers);
            }
            public Builder Layers(IEnumerable<string> layers)
            {
                AssertNotDone();
                this.layers.UnionWith(layers);
                return this;
            }

            public Builder Extensions(params string[] extensions)
            {
                return Extensions((IEnumerable<string>)extensions);
            }
            public Builder Extensions(IEnumerable<string> extensions)
            {
                AssertNotDone();
                this.extensions.UnionWith(extensions);
                return this;
            }

            public Builder Features(DeviceFeatures features)
            {
                AssertNotDone();
                this.features = features;
                return this;
            }

            public Builder QueueGroup(QueueFamily family, params float[] weights)
            {
                AssertNotDone();
                if(family == null || family.PhysicalDevice != Object.PhysicalDevice)
                    throw new ArgumentException(nameof(family));
                if(weights.LongLength == 0 || weights.LongLength > family.Length)
                    throw new ArgumentException(nameof(weights));
                List<float> list;
                if(!queueGroups.TryGetValue(family, out list))
                    list = queueGroups[family] = new List<float>();
                list.AddRange(weights);
                return this;
            }

            public Device Done()
            {
                AssertNotDone();
                MarkDone();
                using(Object.PhysicalDevice.Lock)
                    Sneaky.Invoke(Object, queueGroups.ToTuples(), layers, extensions, features);
                return Object;
            }
        }
    }
}