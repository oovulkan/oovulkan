﻿using System;
using System.Runtime.InteropServices;
using oovulkan.device.physical;
using oovulkan.util;

// ReSharper disable PrivateFieldCanBeConvertedToLocalVariable

namespace oovulkan.device
{
    [Flags]
    internal enum DeviceCreateFlags : int
    {
        None = 0
    }

    [StructLayout(LayoutKind.Sequential)]
    internal unsafe struct DeviceCreateInfo
    {
        private readonly StructureType Type;
        private readonly void* Next;
        public readonly DeviceCreateFlags Flags;
        public readonly uint QueueCount;
        public readonly DeviceQueueCreateInfo* QueuePtr;
        public readonly uint LayerCount;
        public readonly AnsiString* LayerPtr;
        public readonly uint ExtensionCount;
        public readonly AnsiString* ExtensionPtr;
        public readonly PhysicalDeviceFeatures.Native* Features;

        public DeviceCreateInfo(uint queueCount, DeviceQueueCreateInfo* queuePtr, uint layerCount, AnsiString* layerPtr, uint extensionCount, AnsiString* extensionPtr, PhysicalDeviceFeatures.Native* features)
        {
            Type = StructureType.DeviceCreateInfo;
            Next = null;
            Flags = DeviceCreateFlags.None;
            QueueCount = queueCount;
            QueuePtr = queuePtr;
            LayerCount = layerCount;
            LayerPtr = layerPtr;
            ExtensionCount = extensionCount;
            ExtensionPtr = extensionPtr;
            Features = features;
        }
    }
}