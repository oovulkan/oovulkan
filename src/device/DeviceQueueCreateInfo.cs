﻿using System;
using System.Runtime.InteropServices;
using oovulkan.queue.family;

// ReSharper disable PrivateFieldCanBeConvertedToLocalVariable

namespace oovulkan.device
{
    [Flags]
    public enum DeviceQueueCreateFlags : int
    {
        None = 0
    }

    [StructLayout(LayoutKind.Sequential)]
    internal unsafe struct DeviceQueueCreateInfo
    {
        private readonly StructureType Type;
        private readonly void* Next;
        private readonly DeviceQueueCreateFlags Flags;
        public readonly QueueFamilyHandle QueueFamily;
        public readonly uint Count;
        public readonly float* Priorities;

        public DeviceQueueCreateInfo(QueueFamily queueFamily, uint count, float* priorities)
        {
            Type = StructureType.DeviceQueueCreateInfo;
            Next = null;
            Flags = DeviceQueueCreateFlags.None;
            QueueFamily = queueFamily;
            Count = count;
            Priorities = priorities;
        }
    }
}