﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using oovulkan.image;
using oovulkan.instance;
using oovulkan.queue.family;
using oovulkan.util;
using oovulkan.util.collection;

#pragma warning disable 649

// ReSharper disable InconsistentNaming

namespace oovulkan.device.physical
{
    [StructLayout(LayoutKind.Sequential)]
    public unsafe struct PhysicalDeviceHandle
    {
        private void* Ptr;

        public IntPtr IPtr => (IntPtr)Ptr;

        public static implicit operator PhysicalDeviceHandle(void* ptr) => new PhysicalDeviceHandle {Ptr = ptr};
        public static implicit operator void*(PhysicalDeviceHandle handle) => handle.Ptr;
    }

    public unsafe class PhysicalDevice : VulkanObject<PhysicalDeviceHandle>
    {
        protected internal override bool IsChild => true;
        protected internal override VulkanObject Parent => Instance;
        protected internal override bool IsParent => true;
        protected internal override IEnumerable<VulkanObject> Children => devices;

        public readonly Instance Instance;

        private readonly HashSet<Device> devices = new HashSet<Device>();

        public ReadonlySet<Device> Devices => devices;

        private PhysicalDeviceProperties properties;
        private ReadonlyList<QueueFamily> queueFamilies;
        private PhysicalDeviceFeatures features;
        private PhysicalDeviceMemoryProperties memoryProperties;
        private readonly Dictionary<Format, FormatProperties> formatPropertiesCache = new Dictionary<Format, FormatProperties>();
        private readonly Dictionary<(Format, ImageType, ImageTiling, ImageUsageFlags, ImageCreateFlags), ImageFormatProperties> imageFormatPropertiesCache = new Dictionary<(Format, ImageType, ImageTiling, ImageUsageFlags, ImageCreateFlags), ImageFormatProperties>();

        private PhysicalDeviceProperties Properties => properties ?? (properties = GetProperties());

        public Version ApiVersion => Properties.ApiVersion;
        public Version DriverVersion => Properties.DriverVersion;
        public uint VendorID => Properties.VendorID;
        public uint DeviceID => Properties.DeviceID;
        public PhysicalDeviceType DeviceType => Properties.DeviceType;
        public string DeviceName => Properties.DeviceName;
        public Guid PipelineCacheUUID => Properties.PipelineCacheUUID;
        public PhysicalDeviceLimits Limits => Properties.Limits;
        public PhysicalDeviceSparseProperties SparseProperties => Properties.SparseProperties;
        public ReadonlyList<QueueFamily> QueueFamilies => queueFamilies ?? (queueFamilies = GetQueueFamilies());
        public PhysicalDeviceFeatures Features => features ?? (features = GetFeatures());
        public PhysicalDeviceMemoryProperties MemoryProperties => memoryProperties ?? (memoryProperties = GetMemoryProperties());

        public FormatProperties FormatProperties(Format format) => formatPropertiesCache.GetOrDefault(format, GetFormatProperties);
        public ImageFormatProperties ImageFormatProperties(Format format, ImageType type, ImageTiling tiling, ImageUsageFlags usage, ImageCreateFlags flags)
            => imageFormatPropertiesCache.GetOrDefault((format, type, tiling, usage, flags), GetImageFormatProperties);

        internal PhysicalDevice(Instance instance, PhysicalDeviceHandle handle)
        {
            Instance = instance;
            this.Link(Instance);
            Handle = handle;
        }

        internal void Add(Device device) { devices.Add(device); }
        internal void Remove(Device device) { devices.Remove(device); }

        public override void Dispose() => throw new InvalidOperationException();
        protected override void Destroy() { }

        private PhysicalDeviceProperties GetProperties()
        {
            using(Lock)
            {
                vkGetPhysicalDeviceProperties(Handle, out PhysicalDeviceProperties.Native props);
                return props;
            }
        }
        private ReadonlyList<QueueFamily> GetQueueFamilies()
        {
            using(Lock)
            {
                uint count = 0;
                vkGetPhysicalDeviceQueueFamilyProperties(Handle, ref count, null);
                QueueFamilyProperties.Native* props = stackalloc QueueFamilyProperties.Native[(int)count];
                vkGetPhysicalDeviceQueueFamilyProperties(Handle, ref count, props);
                var families = new QueueFamily[count];
                for(uint i = 0; i < count; i++)
                    families[i] = new QueueFamily(this, i, props[i]);
                return new ReadonlyList<QueueFamily>(families);
            }
        }
        private PhysicalDeviceFeatures GetFeatures()
        {
            using(Lock)
            {
                vkGetPhysicalDeviceFeatures(this, out PhysicalDeviceFeatures.Native features);
                return features;
            }
        }
        private PhysicalDeviceMemoryProperties GetMemoryProperties()
        {
            using(Lock)
            {
                vkGetPhysicalDeviceMemoryProperties(this, out PhysicalDeviceMemoryProperties.Native props);
                return new PhysicalDeviceMemoryProperties(this, props);
            }
        }
        private FormatProperties GetFormatProperties(Format format)
        {
            using(Lock)
            {
                vkGetPhysicalDeviceFormatProperties(this, format, out FormatProperties.Native props);
                return props;
            }
        }
        private ImageFormatProperties GetImageFormatProperties((Format Format, ImageType Type, ImageTiling Tiling, ImageUsageFlags Usage, ImageCreateFlags Flags) args)
        {
            using(Lock)
            {
                if(args.Usage == ImageUsageFlags.None)
                    throw new ArgumentException(nameof(args.Usage));
                vkGetPhysicalDeviceImageFormatProperties(this, args.Format, args.Type, args.Tiling, args.Usage, args.Flags, out ImageFormatProperties.Native props).Throw();
                return props;
            }
        }
        public Device.Builder CreateDevice() => new Device.Builder(this);

        private readonly VkGetPhysicalDeviceProperties vkGetPhysicalDeviceProperties;
        private readonly VkGetPhysicalDeviceQueueFamilyProperties vkGetPhysicalDeviceQueueFamilyProperties;
        private readonly VkGetPhysicalDeviceFeatures vkGetPhysicalDeviceFeatures;
        private readonly VkGetPhysicalDeviceMemoryProperties vkGetPhysicalDeviceMemoryProperties;
        private readonly VkGetPhysicalDeviceFormatProperties vkGetPhysicalDeviceFormatProperties;
        private readonly VkGetPhysicalDeviceImageFormatProperties vkGetPhysicalDeviceImageFormatProperties;

        private delegate void VkGetPhysicalDeviceProperties(PhysicalDeviceHandle physicalDevice, out PhysicalDeviceProperties.Native props);
        private delegate void VkGetPhysicalDeviceQueueFamilyProperties(PhysicalDeviceHandle physicalDevice, ref uint count, QueueFamilyProperties.Native* families);
        private delegate void VkGetPhysicalDeviceFeatures(PhysicalDeviceHandle physicalDevice, out PhysicalDeviceFeatures.Native features);
        private delegate void VkGetPhysicalDeviceMemoryProperties(PhysicalDeviceHandle physicalDevice, out PhysicalDeviceMemoryProperties.Native props);
        private delegate void VkGetPhysicalDeviceFormatProperties(PhysicalDeviceHandle physicalDevice, Format format, out FormatProperties.Native props);
        private delegate Result VkGetPhysicalDeviceImageFormatProperties(PhysicalDeviceHandle physicalDevice, Format format, ImageType type, ImageTiling tiling, ImageUsageFlags usage, ImageCreateFlags flags, out ImageFormatProperties.Native props);
    }
}