﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using oovulkan.util.collection;

// ReSharper disable InconsistentNaming

namespace oovulkan.device.physical
{
    [Flags]
    public enum MemoryPropertyFlags
    {
        None = 0,
        DeviceLocal = 1,
        HostVisible = 2,
        HostCoherent = 4,
        HostCached = 8,
        LazilyAllocated = 16
    }

    public class PhysicalDeviceMemoryProperties
    {
        public readonly ReadonlyList<MemoryHeap> Heaps;
        public readonly ReadonlyList<MemoryType> Types;

        internal PhysicalDeviceMemoryProperties(PhysicalDevice device, Native native)
        {
            Heaps = native.GetHeaps(device).ToList();
            Types = native.GetTypes(device, Heaps).ToList();
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct Native
        {
            public const int MAX_MEMORY_TYPES = 32;
            public const int MAX_MEMORY_HEAPS = 16;

            private readonly uint MemoryTypeCount;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = MAX_MEMORY_TYPES)]
            private readonly MemoryType.Native[] MemoryTypes;
            private readonly uint MemoryHeapCount;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = MAX_MEMORY_HEAPS)]
            private readonly MemoryHeap.Native[] MemoryHeaps;

            public IEnumerable<MemoryHeap> GetHeaps(PhysicalDevice device)
            {
                var heaps = new MemoryHeap[MemoryHeapCount];
                for(uint i = 0; i < heaps.Length; i++)
                    heaps[i] = new MemoryHeap(device, i, MemoryHeaps[i]);
                return heaps;
            }

            public IEnumerable<MemoryType> GetTypes(PhysicalDevice device, ReadonlyList<MemoryHeap> heaps)
            {
                var types = new MemoryType[MemoryTypeCount];
                for(uint i = 0; i < types.Length; i++)
                    types[i] = new MemoryType(device, i, MemoryTypes[i], heaps);
                return types;
            }
        }
    }
}