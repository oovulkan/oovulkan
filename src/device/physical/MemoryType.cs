﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace oovulkan.device.physical
{
    [StructLayout(LayoutKind.Sequential)]
    public struct MemoryTypeHandle
    {
        private uint Value;

        public static implicit operator MemoryTypeHandle(uint? value) => new MemoryTypeHandle {Value = value ?? 0};
        public static implicit operator uint(MemoryTypeHandle handle) => handle.Value;
    }

    public class MemoryType : VulkanObject<MemoryTypeHandle>
    {
        protected internal override bool IsChild => true;
        protected internal override VulkanObject Parent => PhysicalDevice;

        public PhysicalDevice PhysicalDevice;
        public readonly MemoryPropertyFlags Properties;
        public readonly MemoryHeap Heap;

        internal MemoryType(PhysicalDevice device, MemoryTypeHandle handle, Native native, IReadOnlyList<MemoryHeap> heaps)
        {
            PhysicalDevice = device;
            Handle = handle;
            Properties = native.Properties;
            Heap = heaps[(int)native.HeapIndex];
        }

        public override void Dispose() => throw new InvalidOperationException();
        protected override void Destroy() { }

        [StructLayout(LayoutKind.Sequential)]
        public struct Native
        {
            public const int SIZE = sizeof(MemoryPropertyFlags) + sizeof(uint);

            public readonly MemoryPropertyFlags Properties;
            public readonly uint HeapIndex;
        }
    }
}