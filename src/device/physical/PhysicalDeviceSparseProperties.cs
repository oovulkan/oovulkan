﻿using System.Runtime.InteropServices;
using util;

namespace oovulkan.device.physical
{
    public class PhysicalDeviceSparseProperties
    {
        public readonly bool ResidencyStandard2DBlockShape;
        public readonly bool ResidencyStandard2DMultisampleBlockShape;
        public readonly bool ResidencyStandard3DBlockShape;
        public readonly bool ResidencyAlignedMipSize;
        public readonly bool ResidencyNonResidentStrict;

        private PhysicalDeviceSparseProperties(Native native)
        {
            ResidencyStandard2DBlockShape = native.ResidencyStandard2DBlockShape;
            ResidencyStandard2DMultisampleBlockShape = native.ResidencyStandard2DMultisampleBlockShape;
            ResidencyStandard3DBlockShape = native.ResidencyStandard3DBlockShape;
            ResidencyAlignedMipSize = native.ResidencyAlignedMipSize;
            ResidencyNonResidentStrict = native.ResidencyNonResidentStrict;
        }

        public static implicit operator PhysicalDeviceSparseProperties(Native native) => new PhysicalDeviceSparseProperties(native);

        [StructLayout(LayoutKind.Sequential)]
        public struct Native
        {
            public readonly Bool ResidencyStandard2DBlockShape;
            public readonly Bool ResidencyStandard2DMultisampleBlockShape;
            public readonly Bool ResidencyStandard3DBlockShape;
            public readonly Bool ResidencyAlignedMipSize;
            public readonly Bool ResidencyNonResidentStrict;
        }
    }
}