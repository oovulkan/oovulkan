﻿using System;
using System.Runtime.InteropServices;
using oovulkan.image;

namespace oovulkan.device.physical
{
    [Flags]
    public enum FormatFeatureFlags : int
    {
        None = 0,
        SampledImage = 1,
        StorageImage = 2,
        StorageImageAtomic = 4,
        UniformTexelBuffer = 8,
        StorageTexelBuffer = 16,
        StorageTexelBufferAtomic = 32,
        VertexBuffer = 64,
        ColorAttachment = 128,
        ColorAttachmentBlend = 256,
        DepthStencilAttachment = 512,
        BlitSrc = 1024,
        BlitDst = 2048,
        SampledImageFilterLinear = 4096
    }

    public class FormatProperties
    {
        public readonly FormatFeatureFlags LinearTilingFeatures;
        public readonly FormatFeatureFlags OptimalTilingFeatures;
        public readonly FormatFeatureFlags BufferFeatures;

        private FormatProperties(Native native)
        {
            LinearTilingFeatures = native.LinearTilingFeatures;
            OptimalTilingFeatures = native.OptimalTilingFeatures;
            BufferFeatures = native.BufferFeatures;
        }

        public FormatFeatureFlags GetFeatures(ImageTiling tiling)
        {
            switch(tiling)
            {
                case ImageTiling.Optimal:
                    return OptimalTilingFeatures;
                case ImageTiling.Linear:
                    return LinearTilingFeatures;
                default:
                    throw new NotImplementedException();
            }
        }

        public static implicit operator FormatProperties(Native native) => new FormatProperties(native);

        [StructLayout(LayoutKind.Sequential)]
        public struct Native
        {
            public readonly FormatFeatureFlags LinearTilingFeatures;
            public readonly FormatFeatureFlags OptimalTilingFeatures;
            public readonly FormatFeatureFlags BufferFeatures;
        }
    }
}