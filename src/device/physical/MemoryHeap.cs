﻿using System;
using System.Runtime.InteropServices;
using oovulkan.util;

namespace oovulkan.device.physical
{
    [Flags]
    public enum MemoryHeapFlags
    {
        None = 0,
        DeviceLocal = 1
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct MemoryHeapHandle
    {
        private uint Value;

        public static implicit operator MemoryHeapHandle(uint? value) => new MemoryHeapHandle {Value = value ?? 0};
        public static implicit operator uint(MemoryHeapHandle handle) => handle.Value;
    }

    public class MemoryHeap : VulkanObject<MemoryHeapHandle>
    {
        protected internal override bool IsChild => true;
        protected internal override VulkanObject Parent => PhysicalDevice;

        public PhysicalDevice PhysicalDevice;
        public readonly Word Size;
        public readonly MemoryHeapFlags Flags;

        internal MemoryHeap(PhysicalDevice device, MemoryHeapHandle handle, Native native)
        {
            PhysicalDevice = device;
            Handle = handle;
            Size = native.Size;
            Flags = native.Flags;
        }

        public override void Dispose() => throw new InvalidOperationException();
        protected override void Destroy() { }

        [StructLayout(LayoutKind.Sequential)]
        public struct Native
        {
            public readonly Word Size;
            public readonly MemoryHeapFlags Flags;
        }
    }
}