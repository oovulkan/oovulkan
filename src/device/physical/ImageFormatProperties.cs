﻿using System.Runtime.InteropServices;
using oovulkan.util;

namespace oovulkan.device.physical
{
    public class ImageFormatProperties
    {
        public readonly (uint Width, uint Height, uint Depth) MaxExtent;
        public readonly uint MaxMipLevels;
        public readonly uint MaxArrayLayers;
        public readonly SampleCountFlags SampleCounts;
        public readonly long MaxResourceSize;

        private ImageFormatProperties(Native native)
        {
            MaxExtent = native.MaxExtent;
            MaxMipLevels = native.MaxMipLevels;
            MaxArrayLayers = native.MaxArrayLayers;
            SampleCounts = native.SampleCounts;
            MaxResourceSize = native.MaxResourceSize;
        }

        public static implicit operator ImageFormatProperties(Native native) => new ImageFormatProperties(native);

        [StructLayout(LayoutKind.Sequential)]
        public struct Native
        {
            public readonly U3 MaxExtent;
            public readonly uint MaxMipLevels;
            public readonly uint MaxArrayLayers;
            public readonly SampleCountFlags SampleCounts;
            public readonly Word MaxResourceSize;
        }
    }
}