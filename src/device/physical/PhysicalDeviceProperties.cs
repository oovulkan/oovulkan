﻿using System;
using System.Runtime.InteropServices;

// ReSharper disable InconsistentNaming

namespace oovulkan.device.physical
{
    public enum PhysicalDeviceType : int
    {
        Other = 0,
        IntegratedGPU = 1,
        DiscreteGPU = 2,
        VirtualGPU = 3,
        CPU = 4
    }

    public class PhysicalDeviceProperties
    {
        public readonly Version ApiVersion;
        public readonly Version DriverVersion;
        public readonly uint VendorID;
        public readonly uint DeviceID;
        public readonly PhysicalDeviceType DeviceType;
        public readonly string DeviceName;
        public readonly Guid PipelineCacheUUID;
        public readonly PhysicalDeviceLimits Limits;
        public readonly PhysicalDeviceSparseProperties SparseProperties;

        private PhysicalDeviceProperties(Native native)
        {
            ApiVersion = native.ApiVersion;
            DriverVersion = native.DriverVersion;
            VendorID = native.VendorID;
            DeviceID = native.DeviceID;
            DeviceType = native.DeviceType;
            DeviceName = native.DeviceName;
            PipelineCacheUUID = native.PipelineCacheUUID;
            Limits = native.Limits;
            SparseProperties = native.SparseProperties;
        }

        public static implicit operator PhysicalDeviceProperties(Native native) => new PhysicalDeviceProperties(native);

        [StructLayout(LayoutKind.Sequential)]
        public unsafe struct Native
        {
            public const int MAX_PHYSICAL_DEVICE_NAME_SIZE = 256;

            public readonly Version ApiVersion;
            public readonly Version DriverVersion;
            public readonly uint VendorID;
            public readonly uint DeviceID;
            public readonly PhysicalDeviceType DeviceType;
            private fixed byte deviceName[MAX_PHYSICAL_DEVICE_NAME_SIZE];
            private fixed byte pipelineCacheUUID[16];
            public readonly PhysicalDeviceLimits.Native Limits;
            public readonly PhysicalDeviceSparseProperties.Native SparseProperties;

            public string DeviceName
            {
                get
                {
                    fixed(byte* ptr = deviceName)
                        return Marshal.PtrToStringAnsi((IntPtr)ptr);
                }
            }
            public Guid PipelineCacheUUID
            {
                get
                {
                    fixed(byte* ptr = pipelineCacheUUID)
                    {
                        return new Guid(
                            ptr[3] << 24 | ptr[2] << 16 | ptr[1] << 8 | ptr[0],
                            (short)(ptr[5] << 8 | ptr[4]),
                            (short)(ptr[7] << 8 | ptr[6]),
                            ptr[8],
                            ptr[9],
                            ptr[10],
                            ptr[11],
                            ptr[12],
                            ptr[13],
                            ptr[14],
                            ptr[15]
                        );
                    }
                }
            }
        }
    }
}