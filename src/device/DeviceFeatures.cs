﻿using oovulkan.device.physical;

// ReSharper disable InconsistentNaming

namespace oovulkan.device
{
    public class DeviceFeatures
    {
        public bool RobustBufferAccess;
        public bool FullDrawIndexUint32;
        public bool ImageCubeArray;
        public bool IndependentBlend;
        public bool GeometryShader;
        public bool TessellationShader;
        public bool SampleRateShading;
        public bool DualSrcBlend;
        public bool LogicOp;
        public bool MultiDrawIndirect;
        public bool DrawIndirectFirstInstance;
        public bool DepthClamp;
        public bool DepthBiasClamp;
        public bool FillModeNonSolid;
        public bool DepthBounds;
        public bool WideLines;
        public bool LargePoints;
        public bool AlphaToOne;
        public bool MultiViewport;
        public bool SamplerAnisotropy;
        public bool TextureCompressionETC2;
        public bool TextureCompressionASTC_LDR;
        public bool TextureCompressionBC;
        public bool OcclusionQueryPrecise;
        public bool PipelineStatisticsQuery;
        public bool VertexPipelineStoresAndAtomics;
        public bool FragmentStoresAndAtomics;
        public bool ShaderTessellationAndGeometryPointSize;
        public bool ShaderImageGatherExtended;
        public bool ShaderStorageImageExtendedFormats;
        public bool ShaderStorageImageMultisample;
        public bool ShaderStorageImageReadWithoutFormat;
        public bool ShaderStorageImageWriteWithoutFormat;
        public bool ShaderUniformBufferArrayDynamicIndexing;
        public bool ShaderSampledImageArrayDynamicIndexing;
        public bool ShaderStorageBufferArrayDynamicIndexing;
        public bool ShaderStorageImageArrayDynamicIndexing;
        public bool ShaderClipDistance;
        public bool ShaderCullDistance;
        public bool ShaderFloat64;
        public bool ShaderInt64;
        public bool ShaderInt16;
        public bool ShaderResourceResidency;
        public bool ShaderResourceMinLod;
        public bool SparseBinding;
        public bool SparseResidencyBuffer;
        public bool SparseResidencyImage2D;
        public bool SparseResidencyImage3D;
        public bool SparseResidency2Samples;
        public bool SparseResidency4Samples;
        public bool SparseResidency8Samples;
        public bool SparseResidency16Samples;
        public bool SparseResidencyAliased;
        public bool VariableMultisampleRate;
        public bool InheritedQueries;

        public DeviceFeatures(){}

        private DeviceFeatures(PhysicalDeviceFeatures features)
        {
            RobustBufferAccess = features.RobustBufferAccess;
            FullDrawIndexUint32 = features.FullDrawIndexUint32;
            ImageCubeArray = features.ImageCubeArray;
            IndependentBlend = features.IndependentBlend;
            GeometryShader = features.GeometryShader;
            TessellationShader = features.TessellationShader;
            SampleRateShading = features.SampleRateShading;
            DualSrcBlend = features.DualSrcBlend;
            LogicOp = features.LogicOp;
            MultiDrawIndirect = features.MultiDrawIndirect;
            DrawIndirectFirstInstance = features.DrawIndirectFirstInstance;
            DepthClamp = features.DepthClamp;
            DepthBiasClamp = features.DepthBiasClamp;
            FillModeNonSolid = features.FillModeNonSolid;
            DepthBounds = features.DepthBounds;
            WideLines = features.WideLines;
            LargePoints = features.LargePoints;
            AlphaToOne = features.AlphaToOne;
            MultiViewport = features.MultiViewport;
            SamplerAnisotropy = features.SamplerAnisotropy;
            TextureCompressionETC2 = features.TextureCompressionETC2;
            TextureCompressionASTC_LDR = features.TextureCompressionASTC_LDR;
            TextureCompressionBC = features.TextureCompressionBC;
            OcclusionQueryPrecise = features.OcclusionQueryPrecise;
            PipelineStatisticsQuery = features.PipelineStatisticsQuery;
            VertexPipelineStoresAndAtomics = features.VertexPipelineStoresAndAtomics;
            FragmentStoresAndAtomics = features.FragmentStoresAndAtomics;
            ShaderTessellationAndGeometryPointSize = features.ShaderTessellationAndGeometryPointSize;
            ShaderImageGatherExtended = features.ShaderImageGatherExtended;
            ShaderStorageImageExtendedFormats = features.ShaderStorageImageExtendedFormats;
            ShaderStorageImageMultisample = features.ShaderStorageImageMultisample;
            ShaderStorageImageReadWithoutFormat = features.ShaderStorageImageReadWithoutFormat;
            ShaderStorageImageWriteWithoutFormat = features.ShaderStorageImageWriteWithoutFormat;
            ShaderUniformBufferArrayDynamicIndexing = features.ShaderUniformBufferArrayDynamicIndexing;
            ShaderSampledImageArrayDynamicIndexing = features.ShaderSampledImageArrayDynamicIndexing;
            ShaderStorageBufferArrayDynamicIndexing = features.ShaderStorageBufferArrayDynamicIndexing;
            ShaderStorageImageArrayDynamicIndexing = features.ShaderStorageImageArrayDynamicIndexing;
            ShaderClipDistance = features.ShaderClipDistance;
            ShaderCullDistance = features.ShaderCullDistance;
            ShaderFloat64 = features.ShaderFloat64;
            ShaderInt64 = features.ShaderInt64;
            ShaderInt16 = features.ShaderInt16;
            ShaderResourceResidency = features.ShaderResourceResidency;
            ShaderResourceMinLod = features.ShaderResourceMinLod;
            SparseBinding = features.SparseBinding;
            SparseResidencyBuffer = features.SparseResidencyBuffer;
            SparseResidencyImage2D = features.SparseResidencyImage2D;
            SparseResidencyImage3D = features.SparseResidencyImage3D;
            SparseResidency2Samples = features.SparseResidency2Samples;
            SparseResidency4Samples = features.SparseResidency4Samples;
            SparseResidency8Samples = features.SparseResidency8Samples;
            SparseResidency16Samples = features.SparseResidency16Samples;
            SparseResidencyAliased = features.SparseResidencyAliased;
            VariableMultisampleRate = features.VariableMultisampleRate;
            InheritedQueries = features.InheritedQueries;
        }

        public static implicit operator DeviceFeatures(PhysicalDeviceFeatures features) => new DeviceFeatures(features);
    }
}