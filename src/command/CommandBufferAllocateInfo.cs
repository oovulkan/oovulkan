﻿using System.Runtime.InteropServices;
using oovulkan.command.pool;

// ReSharper disable PrivateFieldCanBeConvertedToLocalVariable

namespace oovulkan.command
{
    internal enum CommandBufferLevel : int
    {
        Primary = 0,
        Secondary = 1
    }

    [StructLayout(LayoutKind.Sequential)]
    internal unsafe struct CommandBufferAllocateInfo
    {
        private readonly StructureType Type;
        private readonly void* Next;
        public readonly CommandPoolHandle CommandPool;
        public readonly CommandBufferLevel Level;
        public readonly uint Count;

        public CommandBufferAllocateInfo(CommandPoolHandle commandPool, CommandBufferLevel level, uint count)
        {
            Type = StructureType.CommandBufferAllocateInfo;
            Next = null;
            CommandPool = commandPool;
            Level = level;
            Count = count;
        }
    }
}