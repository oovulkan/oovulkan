﻿using System.Runtime.InteropServices;
using oovulkan.framebuffer;
using oovulkan.query;
using oovulkan.render;
using util;

// ReSharper disable PrivateFieldCanBeConvertedToLocalVariable

namespace oovulkan.command
{
    [StructLayout(LayoutKind.Sequential)]
    public unsafe struct CommandBufferInheritanceInfo
    {
        private readonly StructureType Type;
        private readonly void* Next;
        public readonly RenderpassHandle Renderpass;
        public readonly SubpassHandle Subpass;
        public readonly FrameBufferHandle FrameBuffer;
        public readonly Bool OcclusionQueryEnable;
        public readonly QueryControlFlags QueryFlags;
        public readonly PipelineStatisticFlags PipelineStatistics;

        public CommandBufferInheritanceInfo(RenderpassHandle renderpass, SubpassHandle subpass, FrameBufferHandle frameBuffer, Bool occlusionQueryEnable, QueryControlFlags queryFlags, PipelineStatisticFlags pipelineStatistics)
        {
            Type = StructureType.CommandBufferInheritanceInfo;
            Next = null;
            Renderpass = renderpass;
            Subpass = subpass;
            FrameBuffer = frameBuffer;
            OcclusionQueryEnable = occlusionQueryEnable;
            QueryFlags = queryFlags;
            PipelineStatistics = pipelineStatistics;
        }
    }
}