﻿using System;
using System.Runtime.InteropServices;
using oovulkan.queue.family;

// ReSharper disable PrivateFieldCanBeConvertedToLocalVariable

namespace oovulkan.command.pool
{
    [Flags]
    public enum CommandPoolCreateFlags : int
    {
        None = 0,
        Transient = 1,
        ResetCommandBuffer = 2
    }

    [StructLayout(LayoutKind.Sequential)]
    internal unsafe struct CommandPoolCreateInfo
    {
        private readonly StructureType Type;
        private readonly void* Next;
        public readonly CommandPoolCreateFlags Flags;
        public readonly QueueFamilyHandle Family;

        public CommandPoolCreateInfo(CommandPoolCreateFlags flags, QueueFamily family)
        {
            Type = StructureType.CommandPoolCreateInfo;
            Next = null;
            Flags = flags;
            Family = family;
        }
    }
}