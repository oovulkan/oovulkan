﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using oovulkan.device;
using oovulkan.queue.family;
using oovulkan.util.collection;

#pragma warning disable 649

namespace oovulkan.command.pool
{
    [StructLayout(LayoutKind.Sequential)]
    public struct CommandPoolHandle
    {
        private long Value;

        public static implicit operator CommandPoolHandle(long? value) => new CommandPoolHandle {Value = value ?? 0};
        public static implicit operator long(CommandPoolHandle handle) => handle.Value;
    }

    public unsafe class CommandPool : VulkanObject<CommandPoolHandle>
    {
        protected internal override bool IsChild => true;
        protected internal override VulkanObject Parent => Device;
        protected internal override bool IsParent => true;
        protected internal override IEnumerable<VulkanObject> Children => buffers;

        public readonly Device Device;
        public readonly QueueFamily Family;
        public readonly CommandPoolCreateFlags Flags;

        private readonly HashSet<CommandBuffer> buffers = new HashSet<CommandBuffer>();

        public ReadonlySet<CommandBuffer> Buffers => buffers;

        internal CommandPool(Device device, QueueFamily family, CommandPoolCreateFlags flags)
        {
            Device = device;
            this.Link(Device);
            Family = family;
            Flags = flags;
            Create();
        }

        private void Create()
        {
            #region Validation

            if(Family.PhysicalDevice != Device.PhysicalDevice)
                throw new ArgumentException(nameof(Family));

            #endregion

            var info = new CommandPoolCreateInfo(Flags, Family);
            using(Device.WeakLock)
            {
                CommandPoolHandle handle;
                using(Device.Allocator?.WeakLock)
                    vkCreateCommandPool(Device, &info, Device.Allocator, out handle).Throw();
                Handle = handle;
                Device.Add(this);
            }
        }
        protected override void Destroy()
        {
            using(Device.WeakLock)
                Device.Remove(this);
            using(Device.Allocator?.WeakLock)
                vkDestroyCommandPool(Device, this, Device.Allocator);
        }

        public CommandBuffer Allocate(bool isPrimary=true)
        {
            return Allocate(1, isPrimary)[0];
        }
        public CommandBuffer[] Allocate(uint count, bool isPrimary=true)
        {
            #region Validation

            if(count == 0)
                return new CommandBuffer[0];

            #endregion

            var handles = stackalloc CommandBufferHandle[(int)count];
            var info = new CommandBufferAllocateInfo(this, isPrimary ? CommandBufferLevel.Primary : CommandBufferLevel.Secondary, count);
            using(Lock)
            {
                vkAllocateCommandBuffers(Device, &info, handles).Throw();

                var buffers = new CommandBuffer[count];
                for(uint i = 0; i < buffers.LongLength; i++)
                {
                    var buffer = new CommandBuffer(this, isPrimary, handles[i]);
                    this.buffers.Add(buffer);
                    buffers[i] = buffer;
                }
                return buffers;
            }
        }
        public void Free(params CommandBuffer[] buffers)
        {
            #region Validation

            //prevent recursive freeing
            if(buffers.LongLength == 1 && !this.buffers.Contains(buffers[0]))
                return;
            if(buffers.LongLength == 0)
                return;
            if(buffers.Any(set => set.Pool != this))
                throw new ArgumentException(nameof(buffers));
            if(buffers.Distinct().LongCount() != buffers.LongLength)
                throw new ArgumentException(nameof(buffers));

            #endregion

            //sort to prevent deadlock by ensuring constant enter order
            Array.Sort(buffers, (buffer1, buffer2) => buffer1.GetHashCode() - buffer2.GetHashCode());

            var handles = stackalloc CommandBufferHandle[(int)buffers.LongLength];
            for(uint i = 0; i < buffers.LongLength; i++)
                handles[i] = buffers[i];
            using(Lock)
            using(buffers.LockAll(set => set.StrongLock))
            {
                vkFreeCommandBuffers(Device, this, (uint)buffers.LongLength, handles);
                foreach(var buffer in buffers)
                {
                    this.buffers.Remove(buffer);
                    buffer.Dispose();
                }
            }
        }

        private readonly VkCreateCommandPool vkCreateCommandPool;
        private readonly VkDestroyCommandPool vkDestroyCommandPool;
        private readonly VkAllocateCommandBuffers vkAllocateCommandBuffers;
        private readonly VkFreeCommandBuffers vkFreeCommandBuffers;

        private delegate Result VkCreateCommandPool(DeviceHandle device, CommandPoolCreateInfo* info, AllocationCallbacks* allocator, out CommandPoolHandle pool);
        private delegate void VkDestroyCommandPool(DeviceHandle device, CommandPoolHandle pool, AllocationCallbacks* allocator);
        private delegate Result VkAllocateCommandBuffers(DeviceHandle device, CommandBufferAllocateInfo* info, CommandBufferHandle* buffers);
        private delegate void VkFreeCommandBuffers(DeviceHandle device, CommandPoolHandle pool, uint count, CommandBufferHandle* buffers);
    }
}