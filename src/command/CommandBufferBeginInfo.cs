﻿using System;
using System.Runtime.InteropServices;

// ReSharper disable PrivateFieldCanBeConvertedToLocalVariable

namespace oovulkan.command
{
    [Flags]
    public enum CommandBufferUsageFlags
    {
        None = 0,
        OneTimeSubmit = 1,
        RenderPassContinue = 2,
        SimultaneousUse = 4
    }

    [StructLayout(LayoutKind.Sequential)]
    public unsafe struct CommandBufferBeginInfo
    {
        private readonly StructureType Type;
        private readonly void* Next;
        public readonly CommandBufferUsageFlags Flags;
        public readonly CommandBufferInheritanceInfo* InheritanceInfo;

        public CommandBufferBeginInfo(CommandBufferUsageFlags flags, CommandBufferInheritanceInfo* inheritanceInfo)
        {
            Type = StructureType.CommandBufferBeginInfo;
            Next = null;
            Flags = flags;
            InheritanceInfo = inheritanceInfo;
        }
    }
}