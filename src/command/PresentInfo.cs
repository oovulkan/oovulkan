﻿using System.Runtime.InteropServices;
using oovulkan.swapchain;
using oovulkan.synchronization.semaphore;

// ReSharper disable PrivateFieldCanBeConvertedToLocalVariable

namespace oovulkan.command
{
    [StructLayout(LayoutKind.Sequential)]
    internal unsafe struct PresentInfo
    {
        private readonly StructureType Type;
        private readonly void* Next;
        public readonly uint WaitCount;
        public readonly SemaphoreHandle* WaitPtr;
        public readonly uint SwapchainCount;
        public readonly SwapchainHandle* SwapchainPtr;
        public readonly uint* IndexPtr;
        public readonly Result* Results;

        public PresentInfo(uint waitCount, SemaphoreHandle* waitPtr, uint swapchainCount, SwapchainHandle* swapchainPtr, uint* indexPtr, Result* results)
        {
            Type = StructureType.PresentInfoKhr;
            Next = null;
            WaitCount = waitCount;
            WaitPtr = waitPtr;
            SwapchainCount = swapchainCount;
            SwapchainPtr = swapchainPtr;
            IndexPtr = indexPtr;
            Results = results;
        }
    }
}