﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using oovulkan.buffer;
using oovulkan.command.pool;
using oovulkan.descriptor;
using oovulkan.device.physical;
using oovulkan.framebuffer;
using oovulkan.image;
using oovulkan.image.view;
using oovulkan.pipeline;
using oovulkan.pipeline.compute;
using oovulkan.pipeline.graphics;
using oovulkan.pipeline.graphics.dynamic;
using oovulkan.pipeline.graphics.viewport;
using oovulkan.pipeline.layout;
using oovulkan.query;
using oovulkan.queue;
using oovulkan.queue.family;
using oovulkan.render;
using oovulkan.sampler;
using oovulkan.synchronization;
using oovulkan.synchronization.@event;
using oovulkan.util;
using oovulkan.util.collection;
using Buffer = oovulkan.buffer.Buffer;
using DependencyFlags = oovulkan.synchronization.DependencyFlags;

#pragma warning disable 649

namespace oovulkan.command
{
    public enum CommandBufferState
    {
        Initial,
        Recording,
        Executable,
        Pending,
        Invalid
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct CommandBufferHandle
    {
        private long Value;

        public static implicit operator CommandBufferHandle(long? value) => new CommandBufferHandle {Value = value ?? 0};
        public static implicit operator long(CommandBufferHandle handle) => handle.Value;
    }

    public unsafe class CommandBuffer : VulkanObject<CommandBufferHandle>
    {
        protected internal override bool IsChild => true;
        protected internal override VulkanObject Parent => Pool;

        public readonly CommandPool Pool;
        public readonly bool IsPrimary;
        public CommandBufferState State{ get; private set; }
        public CommandBufferUsageFlags Usage{ get; private set; }
        private InheritanceInfo inheritanceInfo;

        private readonly HashSet<CommandBuffer> relatives = new HashSet<CommandBuffer>();

        public IEnumerable<CommandBuffer> Primaries => IsPrimary ? Enumerable.Empty<CommandBuffer>() : relatives;
        public IEnumerable<CommandBuffer> Secondaries => IsPrimary ? relatives : Enumerable.Empty<CommandBuffer>();

        private readonly HashSet<VulkanObject> parts = new HashSet<VulkanObject>();

        private RenderpassInfo renderpassInfo;
        private readonly Dictionary<PipelineBindPoint, Pipeline> currentPipelines = new Dictionary<PipelineBindPoint, Pipeline>();
        private (Buffer Buffer, ulong Offset, IndexType Type)? currentIndexBuffer;
        private bool isFirstPipelineBind;
        private readonly Dictionary<Query, QueryControlFlags> activeQueries = new Dictionary<Query, QueryControlFlags>();

        public ReadonlySet<VulkanObject> Parts => parts;

        internal CommandBuffer(CommandPool pool, bool isPrimary, CommandBufferHandle handle)
        {
            parts.Add(this);
            Pool = pool;
            this.Link(Pool.Device);
            IsPrimary = isPrimary;
            Handle = handle;
            State = CommandBufferState.Initial;
        }

        protected override void Destroy()
        {
            Pool.Free(this);
        }

        #region Recording
        public void Begin(CommandBufferUsageFlags flags=CommandBufferUsageFlags.None)
        {
            if(!IsPrimary)
                throw new InvalidOperationException();
            if(flags.HasFlag(CommandBufferUsageFlags.RenderPassContinue))
                throw new ArgumentException(nameof(flags));
            using(Lock)
            {
                Begin(flags, null);
                inheritanceInfo = null;
            }
        }
        // ReSharper disable once MethodOverloadWithOptionalParameter
        public void Begin(CommandBufferUsageFlags flags=CommandBufferUsageFlags.None, Subpass subpass=null, FrameBuffer frameBuffer=null, bool occlusionQueryEnable=false, QueryControlFlags queryFlags=QueryControlFlags.None, PipelineStatisticFlags pipelineStatistics=PipelineStatisticFlags.None)
        {
            if(IsPrimary)
                throw new InvalidOperationException();
            var features = Pool.Device.Features;
            if((!occlusionQueryEnable || !features.OcclusionQueryPrecise) && queryFlags.HasFlag(QueryControlFlags.Precise))
                throw new NotSupportedException(nameof(queryFlags));
            if(!features.InheritedQueries && occlusionQueryEnable)
                throw new NotSupportedException(nameof(occlusionQueryEnable));
            if(!features.PipelineStatisticsQuery && pipelineStatistics != PipelineStatisticFlags.None)
                throw new NotSupportedException(nameof(pipelineStatistics));
            if(flags.HasFlag(CommandBufferUsageFlags.RenderPassContinue))
            {
                if(subpass == null)
                    throw new ArgumentNullException(nameof(subpass));
                if(frameBuffer != null && !frameBuffer.Renderpass.IsCompatible(subpass.Renderpass))
                    throw new ArgumentException(nameof(frameBuffer));
            }
            var info = new CommandBufferInheritanceInfo(subpass?.Renderpass, subpass, frameBuffer, occlusionQueryEnable, queryFlags, pipelineStatistics);
            using(Lock)
            {
                Begin(flags, &info);
                inheritanceInfo = new InheritanceInfo
                {
                    Subpass = subpass, FrameBuffer = frameBuffer, OcclusionQueryEnable = occlusionQueryEnable,
                    PipelineStatistics = pipelineStatistics, QueryFlags = queryFlags
                };
            }
        }
        private void Begin(CommandBufferUsageFlags flags, CommandBufferInheritanceInfo* iinfo)
        {
            if(State == CommandBufferState.Recording || State == CommandBufferState.Pending)
                throw new InvalidOperationException();
            if(!Pool.Flags.HasFlag(CommandPoolCreateFlags.ResetCommandBuffer) && State != CommandBufferState.Initial)
                throw new InvalidOperationException();
            var info = new CommandBufferBeginInfo(flags, iinfo);
            vkBeginCommandBuffer(this, &info).Throw();
            State = CommandBufferState.Recording;
            Usage = flags;
        }

        public void End()
        {
            using(Lock)
            {
                if(State != CommandBufferState.Recording)
                    throw new InvalidOperationException();
                //TODO: If commandBuffer is a primary command buffer, there must not be an active render pass instance
                //TODO: All queries made active during the recording of commandBuffer must have been made inactive
                try
                {
                    vkEndCommandBuffer(this).Throw();
                    State = CommandBufferState.Executable;
                }
                catch(Exception)
                {
                    State = CommandBufferState.Invalid;
                    throw;
                }
            }
        }
        #endregion
        #region Binding
        public void Bind(Pipeline pipeline)
        {
            var current = currentPipelines.GetOrDefault(pipeline.Type);
            #region Validation
            if(pipeline.Device != Pool.Device)
                throw new ArgumentException(nameof(pipeline));
            if(!Pool.Family.Flags.Supports(pipeline.Type))
                throw new NotSupportedException(nameof(pipeline));
            if(pipeline.Type == PipelineBindPoint.Graphics && renderpassInfo != null)
            {
                var gpipeline = (GraphicsPipeline)pipeline;
                if(!Pool.Device.Features.VariableMultisampleRate && renderpassInfo.Subpass.Attachments.IsEmpty() && !isFirstPipelineBind
                   && gpipeline.MultisampleState.RasterizationSamples != ((GraphicsPipeline)current).MultisampleState.RasterizationSamples)
                    throw new NotSupportedException(nameof(pipeline));
            }
            #endregion

            using(Lock)
            {
                if(State != CommandBufferState.Recording)
                    throw new InvalidOperationException();
                vkCmdBindPipeline(this, pipeline.Type, pipeline);
                currentPipelines[pipeline.Type] = pipeline;
            }
        }
        public void Bind(uint first=0, params BufferPosition[] buffers)
        {
            if(!Pool.Family.Flags.HasFlag(QueueFlags.Graphics))
                throw new NotSupportedException();
            if(buffers.Length == 0)
                throw new ArgumentException();
            if(first + buffers.Length > Pool.Device.PhysicalDevice.Limits.MaxVertexInputBindings)
                throw new ArgumentOutOfRangeException(nameof(first));
            foreach(var pos in buffers)
            {
                if(pos.Buffer.Device != Pool.Device)
                    throw new ArgumentException(nameof(buffers));
                if(pos.Offset >= pos.Buffer.Size)
                    throw new ArgumentOutOfRangeException(nameof(buffers));
                if(!pos.Buffer.Usage.HasFlag(BufferUsageFlags.VertexBuffer))
                    throw new ArgumentException(nameof(buffers));
                //Each element of pBuffers that is non-sparse must be bound completely and contiguously to a single VkDeviceMemory object
            }

            var bufferPtr = stackalloc BufferHandle[buffers.Length];
            var offsetPtr = stackalloc Word[buffers.Length];
            for(int i = 0; i < buffers.Length; i++)
            {
                bufferPtr[i] = buffers[i].Buffer;
                offsetPtr[i] = buffers[i].Offset;
            }

            using(Lock)
            {
                if(State != CommandBufferState.Recording)
                    throw new InvalidOperationException();
                vkCmdBindVertexBuffers(this, first, (uint)buffers.Length, bufferPtr, offsetPtr);
            }
        }
        public void Bind(Pipeline pipeline, uint first, params DescriptorSetPosition[] sets)
        {
            Bind(pipeline.Type, pipeline.Layout, first, sets);
        }
        public void Bind(PipelineBindPoint bind, PipelineLayout layout, uint first, params DescriptorSetPosition[] sets)
        {
            #region Validation
            if(layout.Device != Pool.Device)
                throw new ArgumentException(nameof(pipeline));
            if(sets.Any(pos => pos.Set.Pool.Device != Pool.Device))
                throw new ArgumentException(nameof(sets));
            if(!Pool.Family.Flags.Supports(bind))
                throw new NotSupportedException(nameof(pipeline));
            if(first + sets.Length > layout.Layouts.Count)
                throw new ArgumentOutOfRangeException(nameof(first));
            for(int i = 0; i < sets.Length; i++)
            {
                if(layout.Layouts[(int)(first + i)] != sets[i].Set.Layout)
                    throw new ArgumentException(nameof(sets));
            }
            //TODO: Any given element of pDynamicOffsets must satisfy the required alignment for the corresponding descriptor binding’s descriptor type
            #endregion

            var offsetCount = sets.Sum(pos => pos.DynamicOffsets.Length);
            var setPtr = stackalloc DescriptorSetHandle[sets.Length];
            var offsetPtr = stackalloc uint[offsetCount];

            using(Lock)
            {
                if(State != CommandBufferState.Recording)
                    throw new InvalidOperationException();
                vkCmdBindDescriptorSets(this, bind, layout, first, (uint)sets.Length, setPtr, (uint)offsetCount, offsetPtr);
            }
        }
        public void Bind(Buffer buffer, ulong offset, IndexType indexType)
        {
            #region Validation
            if(buffer.Device != Pool.Device)
                throw new ArgumentException(nameof(buffer));
            if(!Pool.Family.Flags.HasFlag(QueueFlags.Graphics))
                throw new NotSupportedException();
            if(offset > buffer.Size)
                throw new ArgumentOutOfRangeException(nameof(offset));
            //TODO: The sum of offset and the address of the range of VkDeviceMemory object that is backing buffer, must be a multiple of the type indicated by indexType
            if(!buffer.Usage.HasFlag(BufferUsageFlags.IndexBuffer))
                throw new ArgumentException(nameof(buffer));
            //TODO: If buffer is non-sparse then it must be bound completely and contiguously to a single VkDeviceMemory object
            #endregion

            using(Lock)
            {
                if(State != CommandBufferState.Recording)
                    throw new InvalidOperationException();
                vkCmdBindIndexBuffer(this, buffer, offset, indexType);
                currentIndexBuffer = (buffer, offset, indexType);
            }
        }
        #endregion
        #region DynamicState
        public Viewport Viewport { set => SetViewport(0, value); }
        public Rect2D Scissor { set => SetScissor(0, value); }
        public void SetViewport(uint first, params Viewport[] viewports)
        {
            if(!Pool.Family.Flags.HasFlag(QueueFlags.Graphics))
                throw new NotSupportedException();
            if(viewports.Length == 0)
                throw new ArgumentException(nameof(viewports));
            if(first + viewports.Length > Pool.Device.PhysicalDevice.Limits.MaxViewports)
                throw new ArgumentOutOfRangeException(nameof(first));
            if(!Pool.Device.Features.MultiViewport)
            {
                if(first != 0)
                    throw new NotSupportedException(nameof(first));
                if(viewports.Length != 1)
                    throw new NotSupportedException(nameof(viewports));
            }
            using(Lock)
            {
                if(State != CommandBufferState.Recording)
                    throw new InvalidOperationException();
                var pipeline = (GraphicsPipeline)currentPipelines.GetOrDefault(PipelineBindPoint.Graphics);
                if(pipeline == null || !pipeline.DynamicStates.Contains(DynamicState.Viewport))
                    throw new InvalidOperationException();
                fixed(Viewport* viewportPtr = viewports)
                    vkCmdSetViewport(this, first, (uint)viewports.Length, viewportPtr);
            }
        }
        public void SetScissor(uint first, params Rect2D[] scissors)
        {
            if(!Pool.Family.Flags.HasFlag(QueueFlags.Graphics))
                throw new NotSupportedException();
            if(scissors.Length == 0)
                throw new ArgumentException(nameof(scissors));
            if(first + scissors.Length > Pool.Device.PhysicalDevice.Limits.MaxViewports)
                throw new ArgumentOutOfRangeException(nameof(first));
            if(!Pool.Device.Features.MultiViewport)
            {
                if(first != 0)
                    throw new NotSupportedException(nameof(first));
                if(scissors.Length != 1)
                    throw new NotSupportedException(nameof(scissors));
            }
            foreach(var (x, y, w, h) in scissors)
            {
                if(x < 0 || y < 0)
                    throw new ArgumentException(nameof(scissors));
                if(x + w > int.MaxValue || y + h > int.MaxValue)
                    throw new ArgumentException(nameof(scissors));
            }
            using(Lock)
            {
                if(State != CommandBufferState.Recording)
                    throw new InvalidOperationException();
                var pipeline = (GraphicsPipeline)currentPipelines.GetOrDefault(PipelineBindPoint.Graphics);
                if(pipeline == null || !pipeline.DynamicStates.Contains(DynamicState.Scissor))
                    throw new InvalidOperationException();
                fixed(Rect2D* scissorPtr = scissors)
                    vkCmdSetScissor(this, first, (uint)scissors.Length, scissorPtr);
            }
        }
        public (float Min, float Max) DepthBounds
        {
            set
            {
                if(value.Min < 0 || value.Min > 1 || value.Max < 0 || value.Max > 1)
                    throw new ArgumentOutOfRangeException(nameof(value));
                using(Lock)
                {
                    if(State != CommandBufferState.Recording)
                        throw new InvalidOperationException();
                    var pipeline = (GraphicsPipeline)currentPipelines.GetOrDefault(PipelineBindPoint.Graphics);
                    if(pipeline == null || !pipeline.DynamicStates.Contains(DynamicState.DepthBounds))
                        throw new InvalidOperationException();
                    vkCmdSetDepthBounds(this, value.Min, value.Max);
                }
            }
        }
        public float LineWidth
        {
            set
            {
                // ReSharper disable once CompareOfFloatsByEqualityOperator
                if(!Pool.Device.Features.WideLines && value != 1)
                    throw new ArgumentException(nameof(value));
                using(Lock)
                {
                    if(State != CommandBufferState.Recording)
                        throw new InvalidOperationException();
                    var pipeline = (GraphicsPipeline)currentPipelines.GetOrDefault(PipelineBindPoint.Graphics);
                    if(pipeline == null || !pipeline.DynamicStates.Contains(DynamicState.LineWidth))
                        throw new InvalidOperationException();
                    vkCmdSetLineWidth(this, value);
                }
            }
        }
        public (float Constant, float Clamp, float Slope) DepthBias
        {
            set
            {
                // ReSharper disable once CompareOfFloatsByEqualityOperator
                if(!Pool.Device.Features.DepthBiasClamp && value.Clamp != 0)
                    throw new ArgumentException(nameof(value));
                using(Lock)
                {
                    if(State != CommandBufferState.Recording)
                        throw new InvalidOperationException();
                    var pipeline = (GraphicsPipeline)currentPipelines.GetOrDefault(PipelineBindPoint.Graphics);
                    if(pipeline == null || !pipeline.DynamicStates.Contains(DynamicState.DepthBias))
                        throw new InvalidOperationException();
                    vkCmdSetDepthBias(this, value.Constant, value.Clamp, value.Slope);
                }
            }
        }
        public void SetStencilCompare(StencilFaceFlags faces, uint compareMask)
        {
            if(faces == StencilFaceFlags.None)
                throw new ArgumentException(nameof(faces));
            using(Lock)
            {
                if(State != CommandBufferState.Recording)
                    throw new InvalidOperationException();
                var pipeline = (GraphicsPipeline)currentPipelines.GetOrDefault(PipelineBindPoint.Graphics);
                if(pipeline == null || !pipeline.DynamicStates.Contains(DynamicState.StencilCompareMask))
                    throw new InvalidOperationException();
                vkCmdSetStencilCompareMask(this, faces, compareMask);
            }
        }
        public void SetStencilWrite(StencilFaceFlags faces, uint writeMask)
        {
            if(faces == StencilFaceFlags.None)
                throw new ArgumentException(nameof(faces));
            using(Lock)
            {
                if(State != CommandBufferState.Recording)
                    throw new InvalidOperationException();
                var pipeline = (GraphicsPipeline)currentPipelines.GetOrDefault(PipelineBindPoint.Graphics);
                if(pipeline == null || !pipeline.DynamicStates.Contains(DynamicState.StencilWriteMask))
                    throw new InvalidOperationException();
                vkCmdSetStencilWriteMask(this, faces, writeMask);
            }
        }
        public void SetStencilReference(StencilFaceFlags faces, uint reference)
        {
            if(faces == StencilFaceFlags.None)
                throw new ArgumentException(nameof(faces));
            using(Lock)
            {
                if(State != CommandBufferState.Recording)
                    throw new InvalidOperationException();
                var pipeline = (GraphicsPipeline)currentPipelines.GetOrDefault(PipelineBindPoint.Graphics);
                if(pipeline == null || !pipeline.DynamicStates.Contains(DynamicState.StencilReference))
                    throw new InvalidOperationException();
                vkCmdSetStencilReference(this, faces, reference);
            }
        }
        public (float R, float G, float B, float A) BlendConstants
        {
            set
            {
                using(Lock)
                {
                    if(State != CommandBufferState.Recording)
                        throw new InvalidOperationException();
                    var pipeline = (GraphicsPipeline)currentPipelines.GetOrDefault(PipelineBindPoint.Graphics);
                    if(pipeline == null || !pipeline.DynamicStates.Contains(DynamicState.BlendConstants))
                        throw new InvalidOperationException();
                    vkCmdSetBlendConstants(this, value);
                }
            }
        }
        #endregion
        #region Processing
        public void Draw(uint firstVertex, uint vertexCount, uint firstInstance, uint instanceCount)
        {
            using(Lock)
            {
                #region Validation
                if(State != CommandBufferState.Recording)
                    throw new InvalidOperationException();
                if(renderpassInfo == null)
                    throw new InvalidOperationException();
                var pipeline = (GraphicsPipeline)currentPipelines.GetOrDefault(PipelineBindPoint.Graphics);
                if(pipeline == null)
                    throw new InvalidOperationException();
                if(!renderpassInfo.Renderpass.IsCompatible(pipeline.Renderpass))
                    throw new NotSupportedException();
                if(renderpassInfo.Subpass.Index != pipeline.Subpass.Index)
                    throw new InvalidOperationException();
                #endregion

                vkCmdDraw(this, vertexCount, instanceCount, firstVertex, firstInstance);
            }
        }
        public void DrawIndexed(uint firstIndex, uint indexCount, uint firstInstance, uint instanceCount, uint vertexOffset)
        {
            using(Lock)
            {
                #region Validation
                if(State != CommandBufferState.Recording)
                    throw new InvalidOperationException();
                if(renderpassInfo == null)
                    throw new InvalidOperationException();
                var pipeline = (GraphicsPipeline)currentPipelines.GetOrDefault(PipelineBindPoint.Graphics);
                if(pipeline == null)
                    throw new InvalidOperationException();
                if(!renderpassInfo.Renderpass.IsCompatible(pipeline.Renderpass))
                    throw new NotSupportedException();
                if(renderpassInfo.Subpass.Index != pipeline.Subpass.Index)
                    throw new InvalidOperationException();
                if(currentIndexBuffer == null)
                    throw new InvalidOperationException();
                var (buffer, offset, type) = currentIndexBuffer.Value;
                if(type.Size() * (firstIndex + indexCount) + offset > buffer.Size)
                    throw new IndexOutOfRangeException(nameof(firstIndex));
                #endregion

                vkCmdDrawIndexed(this, indexCount, instanceCount, firstIndex, (int)vertexOffset, firstInstance);
            }
        }
        public void DrawIndirect(Buffer buffer, ulong offset, uint drawCount, uint stride)
        {
            #region Validation
            if(buffer.Device != Pool.Device)
                throw new ArgumentException(nameof(buffer));
            //TODO: If buffer is non-sparse then it must be bound completely and contiguously to a single VkDeviceMemory object
            if(offset % 4 != 0)
                throw new ArgumentException(nameof(offset));
            if(drawCount > 1 && (stride % 4 != 0 || stride < sizeof(DrawIndirectCommand)))
                throw new ArgumentException(nameof(stride));
            if(!Pool.Device.Features.MultiDrawIndirect && drawCount > 1)
                throw new ArgumentOutOfRangeException(nameof(drawCount));
            //TODO: If the drawIndirectFirstInstance feature is not enabled, all the firstInstance members of the VkDrawIndirectCommand structures accessed by this command must be 0
            if(drawCount != 0 && (stride * (drawCount - 1)) + offset + (uint)sizeof(DrawIndirectCommand) > buffer.Size)
                throw new IndexOutOfRangeException(nameof(offset));
            if(drawCount > Pool.Device.PhysicalDevice.Limits.MaxDrawIndirectCount)
                throw new NotSupportedException(nameof(drawCount));
            #endregion

            using(Lock)
            {
                #region Validation
                if(State != CommandBufferState.Recording)
                    throw new InvalidOperationException();
                if(renderpassInfo == null)
                    throw new InvalidOperationException();
                var pipeline = (GraphicsPipeline)currentPipelines.GetOrDefault(PipelineBindPoint.Graphics);
                if(pipeline == null)
                    throw new InvalidOperationException();
                if(!renderpassInfo.Renderpass.IsCompatible(pipeline.Renderpass))
                    throw new NotSupportedException();
                if(renderpassInfo.Subpass.Index != pipeline.Subpass.Index)
                    throw new InvalidOperationException();
                #endregion

                vkCmdDrawIndirect(this, buffer, offset, drawCount, stride);
            }
        }
        public void DrawIndexedIndirect(Buffer buffer, ulong offset, uint drawCount, uint stride)
        {
            #region Validation
            if(buffer.Device != Pool.Device)
                throw new ArgumentException(nameof(buffer));
            //TODO: If buffer is non-sparse then it must be bound completely and contiguously to a single VkDeviceMemory object
            if(offset % 4 != 0)
                throw new ArgumentException(nameof(offset));
            if(drawCount > 1 && (stride % 4 != 0 || stride < sizeof(DrawIndexedIndirectCommand)))
                throw new ArgumentException(nameof(stride));
            if(!Pool.Device.Features.MultiDrawIndirect && drawCount > 1)
                throw new ArgumentOutOfRangeException(nameof(drawCount));
            //TODO: If the drawIndirectFirstInstance feature is not enabled, all the firstInstance members of the VkDrawIndirectCommand structures accessed by this command must be 0
            if(drawCount != 0 && (stride * (drawCount - 1)) + offset + (uint)sizeof(DrawIndexedIndirectCommand) > buffer.Size)
                throw new IndexOutOfRangeException(nameof(offset));
            if(drawCount > Pool.Device.PhysicalDevice.Limits.MaxDrawIndirectCount)
                throw new NotSupportedException(nameof(drawCount));
            #endregion

            using(Lock)
            {
                #region Validation
                if(State != CommandBufferState.Recording)
                    throw new InvalidOperationException();
                if(renderpassInfo == null)
                    throw new InvalidOperationException();
                var pipeline = (GraphicsPipeline)currentPipelines.GetOrDefault(PipelineBindPoint.Graphics);
                if(pipeline == null)
                    throw new InvalidOperationException();
                if(!renderpassInfo.Renderpass.IsCompatible(pipeline.Renderpass))
                    throw new NotSupportedException();
                if(renderpassInfo.Subpass.Index != pipeline.Subpass.Index)
                    throw new InvalidOperationException();
                #endregion

                vkCmdDrawIndexedIndirect(this, buffer, offset, drawCount, stride);
            }
        }
        #endregion
        #region Compute
        public void Dispatch(uint x, uint y, uint z)
        {
            #region Validation
            if(!Pool.Family.Flags.Supports(PipelineBindPoint.Compute))
                throw new NotSupportedException();
            var limits = Pool.Device.PhysicalDevice.Limits;
            if(x > limits.MaxComputeWorkGroupCount.X)
                throw new ArgumentOutOfRangeException(nameof(x));
            if(y > limits.MaxComputeWorkGroupCount.Y)
                throw new ArgumentOutOfRangeException(nameof(y));
            if(z > limits.MaxComputeWorkGroupCount.Z)
                throw new ArgumentOutOfRangeException(nameof(z));
            #endregion

            using(Lock)
            {
                if(State != CommandBufferState.Recording)
                    throw new InvalidOperationException();
                if(renderpassInfo != null)
                    throw new InvalidOperationException();
                var pipeline = (ComputePipeline)currentPipelines.GetOrDefault(PipelineBindPoint.Compute);
                if(pipeline == null)
                    throw new InvalidOperationException();
                vkCmdDispatch(this, x, y, z);
            }
        }
        public void DispatchIndirect(Buffer buffer, ulong offset)
        {
            #region Validation
            if(buffer.Device != Pool.Device)
                throw new ArgumentException(nameof(buffer));
            if(!Pool.Family.Flags.Supports(PipelineBindPoint.Compute))
                throw new NotSupportedException();
            if(!buffer.Usage.HasFlag(BufferUsageFlags.IndirectBuffer))
                throw new NotSupportedException(nameof(buffer));
            if(offset % 4 != 0)
                throw new ArgumentException(nameof(offset));
            if(offset + (uint)sizeof(DispatchIndirectCommand) > buffer.Size)
                throw new ArgumentOutOfRangeException(nameof(offset));
            #endregion

            using(Lock)
            {
                if(State != CommandBufferState.Recording)
                    throw new InvalidOperationException();
                if(renderpassInfo != null)
                    throw new InvalidOperationException();
                var pipeline = (ComputePipeline)currentPipelines.GetOrDefault(PipelineBindPoint.Compute);
                if(pipeline == null)
                    throw new InvalidOperationException();
                vkCmdDispatchIndirect(this, buffer, offset);
            }
        }
        #endregion
        #region ImageBuffer
        public BufferCopyBuilder Copy(Buffer src, Buffer dst)
        {
            #region Validation
            if(src.Device != Pool.Device)
                throw new ArgumentException(nameof(src));
            if(dst.Device != Pool.Device)
                throw new ArgumentException(nameof(dst));
            if(!src.Usage.HasFlag(BufferUsageFlags.TransferSrc))
                throw new NotSupportedException(nameof(src));
            if(!dst.Usage.HasFlag(BufferUsageFlags.TransferDst))
                throw new NotSupportedException(nameof(dst));
            #endregion

            return new BufferCopyBuilder(this, src, dst, regions =>
            {
                if(regions.Count == 0)
                    throw new ArgumentException(nameof(regions));

                using(var regionPtr = regions.FixList())
                using(Lock)
                {
                    if(State != CommandBufferState.Recording)
                        throw new InvalidOperationException();
                    if(renderpassInfo != null)
                        throw new InvalidOperationException();
                    vkCmdCopyBuffer(this, src, dst, (uint)regions.Count, (BufferCopy*)regionPtr);
                }
            });
        }
        public ImageCopyBuilder Copy(ImageBase src, ImageBase dst, ImageLayout? srcLayout=null, ImageLayout? dstLayout=null)
        {
            #region Validation
            if(src.Device != Pool.Device)
                throw new ArgumentException(nameof(src));
            if(dst.Device != Pool.Device)
                throw new ArgumentException(nameof(dst));
            srcLayout = srcLayout ?? src.InitialLayout;
            dstLayout = dstLayout ?? dst.InitialLayout;
            if(srcLayout != ImageLayout.TransferSrcOptimal && srcLayout != ImageLayout.General)
                throw new ArgumentException(nameof(srcLayout));
            if(dstLayout != ImageLayout.TransferDstOptimal && dstLayout != ImageLayout.General)
                throw new ArgumentException(nameof(dstLayout));
            if(!src.Usage.HasFlag(ImageUsageFlags.TransferSrc))
                throw new ArgumentException(nameof(src));
            if(!dst.Usage.HasFlag(ImageUsageFlags.TransferDst))
                throw new ArgumentException(nameof(dst));
            if(!src.Format.IsCompatible(dst.Format))
                throw new ArgumentException();
            #endregion

            return new ImageCopyBuilder(this, src, dst, regions =>
            {
                if(regions.Count == 0)
                    throw new ArgumentException(nameof(regions));

                using(var regionPtr = regions.FixList())
                using(Lock)
                {
                    if(State != CommandBufferState.Recording)
                        throw new InvalidOperationException();
                    if(renderpassInfo != null)
                        throw new InvalidOperationException();
                    vkCmdCopyImage(this, src, srcLayout.Value, dst, dstLayout.Value, (uint)regions.Count, (ImageCopy*)regionPtr);
                }
            });
        }
        public ImageBlitBuilder Blit(ImageBase src, ImageBase dst, Filter filter, ImageLayout? srcLayout = null, ImageLayout? dstLayout = null)
        {
            #region Validation
            if(src.Device != Pool.Device)
                throw new ArgumentException(nameof(src));
            if(dst.Device != Pool.Device)
                throw new ArgumentException(nameof(dst));
            srcLayout = srcLayout ?? src.InitialLayout;
            dstLayout = dstLayout ?? dst.InitialLayout;
            if(srcLayout != ImageLayout.TransferSrcOptimal && srcLayout != ImageLayout.General)
                throw new ArgumentException(nameof(srcLayout));
            if(dstLayout != ImageLayout.TransferDstOptimal && dstLayout != ImageLayout.General)
                throw new ArgumentException(nameof(dstLayout));
            if(!src.Usage.HasFlag(ImageUsageFlags.TransferSrc))
                throw new ArgumentException(nameof(src));
            if(!dst.Usage.HasFlag(ImageUsageFlags.TransferDst))
                throw new ArgumentException(nameof(dst));

            var srcProps = Pool.Device.PhysicalDevice.FormatProperties(src.Format);
            if(!srcProps.GetFeatures(src.Tiling).HasFlag(FormatFeatureFlags.BlitSrc))
                throw new NotSupportedException(nameof(src));
            var dstProps = Pool.Device.PhysicalDevice.FormatProperties(dst.Format);
            if(!dstProps.GetFeatures(dst.Tiling).HasFlag(FormatFeatureFlags.BlitDst))
                throw new NotSupportedException(nameof(dst));

            if(src.Samples != SampleCountFlags.Bit1)
                throw new ArgumentException(nameof(src));
            if(dst.Samples != SampleCountFlags.Bit1)
                throw new ArgumentException(nameof(dst));

            if(filter == Filter.Linear && !srcProps.GetFeatures(src.Tiling).HasFlag(FormatFeatureFlags.SampledImageFilterLinear))
                throw new NotSupportedException(nameof(filter));
            //TODO: If either of srcImage or dstImage was created with a signed integer VkFormat, the other must also have been created with a signed integer VkFormat
            //TODO: If either of srcImage or dstImage was created with an unsigned integer VkFormat, the other must also have been created with an unsigned integer VkFormat
            //TODO: If either of srcImage or dstImage was created with a depth/stencil format, the other must have exactly the same format
            //TODO: If srcImage was created with a depth/stencil format, filter must be VK_FILTER_NEAREST
            #endregion

            return new ImageBlitBuilder(this, src, dst, regions =>
            {
                if(regions.Count == 0)
                    throw new ArgumentException(nameof(regions));

                using(var regionPtr = regions.FixList())
                using(Lock)
                {
                    if(State != CommandBufferState.Recording)
                        throw new InvalidOperationException();
                    if(renderpassInfo != null)
                        throw new InvalidOperationException();
                    vkCmdBlitImage(this, src, srcLayout.Value, dst, dstLayout.Value, (uint)regions.Count, (ImageBlit*)regionPtr, filter);
                }
            });
        }
        public BufferImageCopyBuilder Copy(Buffer src, ImageBase dst, ImageLayout? dstLayout=null)
        {
            #region Validation
            if(src.Device != Pool.Device)
                throw new ArgumentException(nameof(src));
            if(dst.Device != Pool.Device)
                throw new ArgumentException(nameof(dst));
            dstLayout = dstLayout ?? dst.InitialLayout;
            if(dstLayout != ImageLayout.TransferDstOptimal && dstLayout != ImageLayout.General)
                throw new ArgumentException(nameof(dstLayout));
            if(!src.Usage.HasFlag(BufferUsageFlags.TransferSrc))
                throw new NotSupportedException(nameof(src));
            if(!dst.Usage.HasFlag(ImageUsageFlags.TransferDst))
                throw new ArgumentException(nameof(dst));
            if(dst.Samples != SampleCountFlags.Bit1)
                throw new ArgumentException(nameof(dst));
            #endregion

            return new BufferImageCopyBuilder(this, src, dst, regions =>
            {
                if(regions.Count == 0)
                    throw new ArgumentException(nameof(regions));

                using(var regionPtr = regions.FixList())
                using(Lock)
                {
                    if(State != CommandBufferState.Recording)
                        throw new InvalidOperationException();
                    if(renderpassInfo != null)
                        throw new InvalidOperationException();
                    vkCmdCopyBufferToImage(this, src, dst, dstLayout.Value, (uint)regions.Count, (BufferImageCopy*)regionPtr);
                }
            });
        }
        public ImageBufferCopyBuilder Copy(ImageBase src, Buffer dst, ImageLayout? srcLayout=null)
        {
            #region Validation
            if(src.Device != Pool.Device)
                throw new ArgumentException(nameof(src));
            if(dst.Device != Pool.Device)
                throw new ArgumentException(nameof(dst));
            srcLayout = srcLayout ?? src.InitialLayout;
            if(srcLayout != ImageLayout.TransferDstOptimal && srcLayout != ImageLayout.General)
                throw new ArgumentException(nameof(srcLayout));
            if(!src.Usage.HasFlag(BufferUsageFlags.TransferSrc))
                throw new NotSupportedException(nameof(src));
            if(!dst.Usage.HasFlag(ImageUsageFlags.TransferDst))
                throw new ArgumentException(nameof(dst));
            if(src.Samples != SampleCountFlags.Bit1)
                throw new ArgumentException(nameof(src));
            #endregion

            return new ImageBufferCopyBuilder(this, src, dst, regions =>
            {
                if(regions.Count == 0)
                    throw new ArgumentException(nameof(regions));

                using(var regionPtr = regions.FixList())
                using(Lock)
                {
                    if(State != CommandBufferState.Recording)
                        throw new InvalidOperationException();
                    if(renderpassInfo != null)
                        throw new InvalidOperationException();
                    vkCmdCopyImageToBuffer(this, src, srcLayout.Value, dst, (uint)regions.Count, (BufferImageCopy*)regionPtr);
                }
            });
        }
        public void Update(Buffer dst, byte[] data, ulong offset=0)
        {
            fixed(byte* ptr = data)
                Update(dst, ptr, (ulong)data.LongLength, offset);
        }
        public void Update(Buffer dst, void* data, ulong? size=null, ulong offset=0)
        {
            #region Validation
            size = size ?? (dst.Size - offset);
            if(dst.Device != Pool.Device)
                throw new ArgumentException(nameof(dst));
            if(!dst.Usage.HasFlag(BufferUsageFlags.TransferDst))
                throw new NotSupportedException(nameof(dst));
            if(offset >= dst.Size)
                throw new ArgumentOutOfRangeException(nameof(offset));
            if(size > dst.Size - offset)
                throw new ArgumentOutOfRangeException(nameof(size));
            if(offset % 4 != 0)
                throw new ArgumentException(nameof(offset));
            if(size > 65536)
                throw new ArgumentOutOfRangeException(nameof(size));
            if(size % 4 != 0)
                throw new ArgumentException(nameof(size));
            #endregion

            using(Lock)
            {
                if(State != CommandBufferState.Recording)
                    throw new InvalidOperationException();
                if(renderpassInfo != null)
                    throw new InvalidOperationException();
                vkCmdUpdateBuffer(this, dst, offset, size.Value, data);
            }
        }
        public void Fill(Buffer dst, uint data, ulong offset=0, ulong size=Buffer.WHOLE_SIZE)
        {
            #region Validation
            if(dst.Device != Pool.Device)
                throw new ArgumentException(nameof(dst));
            if(!dst.Usage.HasFlag(BufferUsageFlags.TransferDst))
                throw new NotSupportedException(nameof(dst));
            if(offset >= dst.Size)
                throw new ArgumentOutOfRangeException(nameof(offset));
            if(offset % 4 != 0)
                throw new ArgumentException(nameof(offset));
            if(size != Buffer.WHOLE_SIZE)
            {
                if(size == 0)
                    throw new ArgumentException(nameof(size));
                if(size > dst.Size - offset)
                    throw new ArgumentOutOfRangeException(nameof(size));
                if(size % 4 != 0)
                    throw new ArgumentException(nameof(size));
            }
            #endregion

            using(Lock)
            {
                if(State != CommandBufferState.Recording)
                    throw new InvalidOperationException();
                if(renderpassInfo != null)
                    throw new InvalidOperationException();
                vkCmdFillBuffer(this, data, offset, size, data);
            }
        }
        public void Clear(ImageBase image, ImageLayout layout, Color color, params ImageSubresourceRange[] ranges)
        {
            #region Validation
            if(image.Device != Pool.Device)
                throw new ArgumentException(nameof(image));
            if(ranges.Length == 0)
                throw new ArgumentException(nameof(ranges));
            if(!image.Usage.HasFlag(ImageUsageFlags.TransferDst))
                throw new NotSupportedException(nameof(image));
            //TODO: If image is non-sparse then it must be bound completely and contiguously to a single VkDeviceMemory object
            //TODO: imageLayout must specify the layout of the image subresource ranges of image specified in pRanges at the time this command is executed on a VkDevice
            if(layout != ImageLayout.TransferDstOptimal && layout != ImageLayout.General)
                throw new ArgumentException(nameof(layout));
            foreach(var range in ranges)
            {
                if(range.MipBase >= image.MipLevels)
                    throw new ArgumentOutOfRangeException(nameof(ranges));
                if(range.MipCount != ImageBase.REMAINING && (range.MipCount == 0 || range.MipBase + range.MipCount > image.MipLevels))
                    throw new ArgumentOutOfRangeException(nameof(ranges));
                if(range.LayerBase >= image.ArrayLayers)
                    throw new ArgumentOutOfRangeException(nameof(ranges));
                if(range.LayerCount != ImageBase.REMAINING && (range.LayerCount == 0 || range.LayerBase + range.LayerCount > image.ArrayLayers))
                    throw new ArgumentOutOfRangeException(nameof(ranges));
            }
            //TODO: image must not have a compressed or depth/stencil format
            #endregion

            fixed(ImageSubresourceRange* ptr = ranges)
            using(Lock)
            {
                if(State != CommandBufferState.Recording)
                    throw new InvalidOperationException();
                if(renderpassInfo != null)
                    throw new InvalidOperationException();
                vkCmdClearColorImage(this, image, layout, &color, (uint)ranges.Length, ptr);
                parts.Add(image);
            }
        }
        public void Clear(ImageBase image, ImageLayout layout, DepthStencilValue value, params ImageSubresourceRange[] ranges)
        {
            #region Validation
            if(image.Device != Pool.Device)
                throw new ArgumentException(nameof(image));
            if(ranges.Length == 0)
                throw new ArgumentException(nameof(ranges));
            if(!image.Usage.HasFlag(ImageUsageFlags.TransferDst))
                throw new NotSupportedException(nameof(image));
            //TODO: If image is non-sparse then it must be bound completely and contiguously to a single VkDeviceMemory object
            if(layout != ImageLayout.TransferDstOptimal && layout != ImageLayout.General)
                throw new ArgumentException(nameof(layout));
            foreach(var range in ranges)
            {
                if(range.MipBase >= image.MipLevels)
                    throw new ArgumentOutOfRangeException(nameof(ranges));
                if(range.MipCount != ImageBase.REMAINING && (range.MipCount == 0 || range.MipBase + range.MipCount > image.MipLevels))
                    throw new ArgumentOutOfRangeException(nameof(ranges));
                if(range.LayerBase >= image.ArrayLayers)
                    throw new ArgumentOutOfRangeException(nameof(ranges));
                if(range.LayerCount != ImageBase.REMAINING && (range.LayerCount == 0 || range.LayerBase + range.LayerCount > image.ArrayLayers))
                    throw new ArgumentOutOfRangeException(nameof(ranges));
            }
            //TODO: image must have a depth/stencil format
            #endregion

            fixed(ImageSubresourceRange* ptr = ranges)
            using(Lock)
            {
                if(State != CommandBufferState.Recording)
                    throw new InvalidOperationException();
                if(renderpassInfo != null)
                    throw new InvalidOperationException();
                vkCmdClearDepthStencilImage(this, image, layout, &value, (uint)ranges.Length, ptr);
                parts.Add(image);
            }
        }
        public ImageResolveBuilder Resolve(ImageBase src, ImageBase dst, ImageLayout? srcLayout = null, ImageLayout? dstLayout = null)
        {
            #region Validation
            if(src.Device != Pool.Device)
                throw new ArgumentException(nameof(src));
            if(dst.Device != Pool.Device)
                throw new ArgumentException(nameof(dst));
            srcLayout = srcLayout ?? src.InitialLayout;
            dstLayout = dstLayout ?? dst.InitialLayout;
            if(srcLayout != ImageLayout.TransferSrcOptimal && srcLayout != ImageLayout.General)
                throw new ArgumentException(nameof(srcLayout));
            if(dstLayout != ImageLayout.TransferDstOptimal && dstLayout != ImageLayout.General)
                throw new ArgumentException(nameof(dstLayout));
            if(!src.Usage.HasFlag(ImageUsageFlags.TransferSrc))
                throw new ArgumentException(nameof(src));
            if(!dst.Usage.HasFlag(ImageUsageFlags.TransferDst))
                throw new ArgumentException(nameof(dst));
            if(src.Samples == SampleCountFlags.Bit1)
                throw new ArgumentException(nameof(src));
            if(dst.Samples != SampleCountFlags.Bit1)
                throw new ArgumentException(nameof(dst));
            var srcProps = Pool.Device.PhysicalDevice.FormatProperties(src.Format);
            if(!srcProps.GetFeatures(src.Tiling).HasFlag(FormatFeatureFlags.ColorAttachment))
                throw new NotSupportedException(nameof(src));
            var dstProps = Pool.Device.PhysicalDevice.FormatProperties(dst.Format);
            if(!dstProps.GetFeatures(dst.Tiling).HasFlag(FormatFeatureFlags.ColorAttachment))
                throw new NotSupportedException(nameof(dst));
            if(src.Format != dst.Format)
                throw new ArgumentException();
            #endregion

            return new ImageResolveBuilder(this, src, dst, regions =>
            {
                if(regions.Count == 0)
                    throw new ArgumentException(nameof(regions));

                using(var regionPtr = regions.FixList())
                using(Lock)
                {
                    if(State != CommandBufferState.Recording)
                        throw new InvalidOperationException();
                    if(renderpassInfo != null)
                        throw new InvalidOperationException();
                    vkCmdResolveImage(this, src, srcLayout.Value, dst, dstLayout.Value, (uint)regions.Count, (ImageResolve*)regionPtr);
                    parts.Add(src);
                    parts.Add(dst);
                }
            });
        }
        #endregion
        #region Framebuffer
        public ClearAttachmentsBuilder ClearAttachments()
        {
            return new ClearAttachmentsBuilder(this, (attachments, rects) =>
            {
                using(var attachmentPtr = attachments.FixList())
                using(var rectPtr = rects.FixList())
                using(Lock)
                {
                    if(State != CommandBufferState.Recording)
                        throw new InvalidOperationException();
                    if(renderpassInfo == null)
                        throw new InvalidOperationException();
                    if(attachments.Any(attachment => attachment.Aspect.HasFlag(ImageAspectFlags.Color) && attachment.Attachment > renderpassInfo.Subpass.Colors.Count))
                        throw new ArgumentException();
                    vkCmdClearAttachments(this, (uint)attachments.Count, (ClearAttachment*)attachmentPtr, (uint)rects.Count, (ClearRect*)rectPtr);
                }
            });
        }
        #endregion
        #region Synchronization
        public void SetEvent(Event @event, bool value, Stage stage)
        {
            if(stage.HasFlag(Stage.Host))
                throw new ArgumentException(nameof(stage));
            var features = Pool.Device.Features;
            if(!features.GeometryShader && stage.HasFlag(Stage.GeometryShader))
                throw new ArgumentException(nameof(stage));
            if(!features.TessellationShader && (stage.HasFlag(Stage.TessellationControlShader) || stage.HasFlag(Stage.TessellationEvaluationShader)))
                throw new ArgumentException(nameof(stage));
            using(Lock)
            {
                if(State != CommandBufferState.Recording)
                    throw new InvalidOperationException();
                if(renderpassInfo != null)
                    throw new InvalidOperationException();
                if(value)
                    vkCmdSetEvent(this, @event, stage);
                else
                    vkCmdResetEvent(this, @event, stage);
                parts.Add(@event);
            }
        }
        public BarrierBuilder WaitEvents(Stage src, Stage dst, params Event[] events)
        {
            #region Validation
            var features = Pool.Device.Features;
            if(!features.GeometryShader)
            {
                if(src.HasFlag(Stage.GeometryShader))
                    throw new NotSupportedException(nameof(src));
                if(dst.HasFlag(Stage.GeometryShader))
                    throw new NotSupportedException(nameof(dst));
            }
            if(!features.TessellationShader)
            {
                if(src.HasFlag(Stage.TessellationControlShader) || src.HasFlag(Stage.TessellationEvaluationShader))
                    throw new NotSupportedException(nameof(src));
                if(dst.HasFlag(Stage.TessellationControlShader) || dst.HasFlag(Stage.TessellationEvaluationShader))
                    throw new NotSupportedException(nameof(dst));
            }
            if(!Pool.Family.Flags.Supports(src | dst))
                throw new NotSupportedException();
            if(events.Length == 0)
                throw new ArgumentException(nameof(events));
            if(events.Any(e => e.Device != Pool.Device))
                throw new ArgumentException(nameof(events));
            #endregion

            return new BarrierBuilder(this, (memories, buffers, images) =>
            {
                #region Validation
                var srcAccess = AccessFlags.None;
                var dstAccess = AccessFlags.None;
                foreach(var memory in memories)
                {
                    srcAccess |= memory.SrcAccess;
                    dstAccess |= memory.DstAccess;
                }
                foreach(var buffer in buffers)
                {
                    srcAccess |= buffer.SrcAccess;
                    dstAccess |= buffer.DstAccess;
                }
                foreach(var image in images)
                {
                    srcAccess |= image.SrcAccess;
                    dstAccess |= image.DstAccess;
                }
                if(!src.Supports(srcAccess) || !dst.Supports(dstAccess))
                    throw new NotSupportedException();
                #endregion

                var eventPtr = stackalloc EventHandle[events.Length];
                uint eventsCount = 0;
                foreach(var @event in events)
                    eventPtr[eventsCount++] = @event;

                using(var memoryPtr = memories.FixList())
                using(var bufferPtr = buffers.FixList())
                using(var imagePtr = images.FixList())
                using(Lock)
                {
                    if(State != CommandBufferState.Recording)
                        throw new InvalidOperationException();
                    vkCmdWaitEvents(this, eventsCount, eventPtr, src, dst,
                        (uint)memories.Count, (MemoryBarrier*)memoryPtr,
                        (uint)buffers.Count, (BufferMemoryBarrier*)bufferPtr,
                        (uint)images.Count, (ImageMemoryBarrier*)imagePtr
                    );
                    foreach(var e in events)
                        parts.Add(e);
                    //TODO: buffers and images
                }
            });
        }
        public BarrierBuilder PipelineBarrier(Stage src, Stage dst, bool byRegion=false)
        {
            #region Validation

            var features = Pool.Device.Features;
            if(!features.GeometryShader)
            {
                if(src.HasFlag(Stage.GeometryShader))
                    throw new NotSupportedException(nameof(src));
                if(dst.HasFlag(Stage.GeometryShader))
                    throw new NotSupportedException(nameof(dst));
            }
            if(!features.TessellationShader)
            {
                if(src.HasFlag(Stage.TessellationControlShader) || src.HasFlag(Stage.TessellationEvaluationShader))
                    throw new NotSupportedException(nameof(src));
                if(dst.HasFlag(Stage.TessellationControlShader) || dst.HasFlag(Stage.TessellationEvaluationShader))
                    throw new NotSupportedException(nameof(dst));
            }
            if(!Pool.Family.Flags.Supports(src | dst))
                throw new NotSupportedException();

            #endregion

            return new BarrierBuilder(this, (memories, buffers, images) =>
            {
                #region Validation

                //TODO: If vkCmdPipelineBarrier is called within a render pass instance, the render pass must have been created with a VkSubpassDependency instance in pDependencies that expresses a dependency from the current subpass to itself.
                //TODO: If vkCmdPipelineBarrier is called within a render pass instance, srcStageMask must contain a subset of the bit values in the srcStageMask member of that instance of VkSubpassDependency
                //TODO: If vkCmdPipelineBarrier is called within a render pass instance, dstStageMask must contain a subset of the bit values in the dstStageMask member of that instance of VkSubpassDependency
                //TODO: If vkCmdPipelineBarrier is called within a render pass instance, the srcAccessMask of any element of pMemoryBarriers or pImageMemoryBarriers must contain a subset of the bit values the srcAccessMask member of that instance of VkSubpassDependency
                //TODO: If vkCmdPipelineBarrier is called within a render pass instance, the dstAccessMask of any element of pMemoryBarriers or pImageMemoryBarriers must contain a subset of the bit values the dstAccessMask member of that instance of VkSubpassDependency
                //TODO: If vkCmdPipelineBarrier is called within a render pass instance, dependencyFlags must be equal to the dependencyFlags member of that instance of VkSubpassDependency
                //TODO: If vkCmdPipelineBarrier is called within a render pass instance, bufferMemoryBarrierCount must be 0
                //TODO: If vkCmdPipelineBarrier is called within a render pass instance, the image member of any element of pImageMemoryBarriers must be equal to one of the elements of pAttachments that the current framebuffer was created with, that is also referred to by one of the elements of the pColorAttachments, pResolveAttachments or pDepthStencilAttachment members of the VkSubpassDescription instance that the current subpass was created with
                //TODO: If vkCmdPipelineBarrier is called within a render pass instance, the oldLayout and newLayout members of any element of pImageMemoryBarriers must be equal to the layout member of an element of the pColorAttachments, pResolveAttachments or pDepthStencilAttachment members of the VkSubpassDescription instance that the current subpass was created with, that refers to the same image
                //TODO: If vkCmdPipelineBarrier is called within a render pass instance, the oldLayout and newLayout members of an element of pImageMemoryBarriers must be equal
                //TODO: If vkCmdPipelineBarrier is called within a render pass instance, the srcQueueFamilyIndex and dstQueueFamilyIndex members of any element of pImageMemoryBarriers must be VK_QUEUE_FAMILY_IGNORED
                var srcAccess = AccessFlags.None;
                var dstAccess = AccessFlags.None;
                foreach(var memory in memories)
                {
                    srcAccess |= memory.SrcAccess;
                    dstAccess |= memory.DstAccess;
                }
                if(!src.Supports(srcAccess) || !dst.Supports(dstAccess))
                    throw new NotSupportedException();

                #endregion

                var flags = DependencyFlags.None;
                if(byRegion)
                    flags |= DependencyFlags.ByRegion;
                using(var memoryPtr = memories.FixList())
                using(var bufferPtr = buffers.FixList())
                using(var imagePtr = images.FixList())
                using(Lock)
                {
                    if(State != CommandBufferState.Recording)
                        throw new InvalidOperationException();
                    vkCmdPipelineBarrier(this, src, dst, flags,
                        (uint)memories.Count, (MemoryBarrier*)memoryPtr,
                        (uint)buffers.Count, (BufferMemoryBarrier*)bufferPtr,
                        (uint)images.Count, (ImageMemoryBarrier*)imagePtr
                    );
                    //TODO: buffers and images
                }
            });
        }
        #endregion
        #region Queries
        public void BeginQuery(Query query, QueryControlFlags flags=QueryControlFlags.None)
        {
            #region Validation
            if(query.Pool.Device != Pool.Device)
                throw new ArgumentException(nameof(query));
            if(flags.HasFlag(QueryControlFlags.Precise))
            {
                if(!Pool.Device.Features.OcclusionQueryPrecise)
                    throw new NotSupportedException(nameof(flags));
                if(query.Pool.Type != QueryType.Occlusion)
                    throw new ArgumentException(nameof(flags));
            }
            var required = QueueFlags.None;
            switch(query.Pool.Type)
            {
                case QueryType.Occlusion:
                    required = QueueFlags.Graphics;
                    break;
                case QueryType.PipelineStatistics:
                    var stats = ((PipelineStatisticQueryPool)query.Pool).Statistics;
                    required = stats.GetQueueTypes();
                    break;
            }
            if(!Pool.Family.Flags.HasFlag(required))
                throw new NotSupportedException(nameof(pool));
            #endregion

            using(Lock)
            {
                if(State != CommandBufferState.Recording)
                    throw new InvalidOperationException();
                if(activeQueries.Keys.Contains(query) || activeQueries.Keys.Any(active => query.Pool.Type == active.Pool.Type))
                    throw new InvalidOperationException();
                vkCmdBeginQuery(this, query.Pool, query.Index, flags);
                activeQueries[query] = flags;
                parts.Add(query.Pool);
            }
        }
        public void EndQuery(Query query)
        {
            #region Validation
            if(query.Pool.Device != Pool.Device)
                throw new ArgumentException(nameof(query));
            #endregion

            using(Lock)
            {
                if(State != CommandBufferState.Recording)
                    throw new InvalidOperationException();
                if(!activeQueries.Keys.Contains(query))
                    throw new InvalidOperationException();
                vkCmdEndQuery(this, query.Pool, query.Index);
                activeQueries.Remove(query);
            }
        }
        public void ResetQuery(Query query, uint count=1)
        {
            #region Validation
            if(query.Pool.Device != Pool.Device)
                throw new ArgumentException(nameof(query));
            if(query.Index + count > query.Pool.Count)
                throw new ArgumentOutOfRangeException(nameof(count));
            #endregion

            using(Lock)
            {
                if(State != CommandBufferState.Recording)
                    throw new InvalidOperationException();
                if(renderpassInfo != null)
                    throw new InvalidOperationException();
                vkCmdResetQueryPool(this, query.Pool, query.Index, count);
            }
        }
        public void CopyQuery(Query query, Buffer dst, ulong offset, ulong stride, uint count=1, QueryResultFlags flags=QueryResultFlags.None)
        {
            #region Validation
            if(query.Pool.Device != Pool.Device)
                throw new ArgumentException(nameof(pool));
            if(dst.Device != Pool.Device)
                throw new ArgumentException(nameof(dst));
            if(query.Index + count > query.Pool.Count)
                throw new ArgumentOutOfRangeException(nameof(count));
            if(offset >= dst.Size)
                throw new ArgumentOutOfRangeException(nameof(offset));
            var unit = flags.HasFlag(QueryResultFlags.Is64) ? 8u : 4u;
            if(offset % unit != 0)
                throw new ArgumentException(nameof(offset));
            if(stride % unit != 0)
                throw new ArgumentException(nameof(stride));
            ulong resultSize;
            switch(query.Pool.Type)
            {
                case QueryType.Occlusion:
                case QueryType.Timestamp:
                    resultSize = unit;
                    break;
                case QueryType.PipelineStatistics:
                    resultSize = (ulong)Util.GetFlags(((PipelineStatisticQueryPool)query.Pool).Statistics).Count() * unit;
                    break;
                default:
                    throw new NotImplementedException();
            }
            if(flags.HasFlag(QueryResultFlags.WithAvailability))
                resultSize += unit;
            var size = stride * (count - 1) + resultSize;
            if(offset + size > dst.Size)
                throw new ArgumentOutOfRangeException(nameof(count));
            if(!dst.Usage.HasFlag(BufferUsageFlags.TransferDst))
                throw new ArgumentException(nameof(dst));
            if(query.Pool.Type == QueryType.Timestamp && flags.HasFlag(QueryResultFlags.Partial))
                throw new ArgumentException(nameof(flags));
            #endregion

            using(Lock)
            {
                if(State != CommandBufferState.Recording)
                    throw new InvalidOperationException();
                if(renderpassInfo != null)
                    throw new InvalidOperationException();
                vkCmdCopyQueryPoolResults(this, query.Pool, query.Index, count, dst, offset, stride, flags);
            }
        }
        public void WriteTimestamp(Stage stage, Query query)
        {
            #region Validation
            if(query.Pool.Device != Pool.Device)
                throw new ArgumentException(nameof(query));
            if(query.Pool.Type != QueryType.Timestamp)
                throw new ArgumentException(nameof(query));
            if(Pool.Family.TimestampValidBits == 0)
                throw new NotSupportedException();
            #endregion

            using(Lock)
            {
                if(State != CommandBufferState.Recording)
                    throw new InvalidOperationException();
                vkCmdWriteTimestamp(this, stage, query.Pool, query.Index);
            }
        }
        #endregion
        #region PushConstants
        public void PushConstants(PipelineLayout layout, PushConstants constants)
        {
            using(Lock)
            {
                if(State != CommandBufferState.Recording)
                    throw new InvalidOperationException();
                foreach(var (offset, size) in constants.DirtyRegions)
                    vkCmdPushConstants(this, layout, layout.GetStage((uint)offset, (uint)size), (uint)offset, (uint)size, (void*)constants.Ptr);
                parts.Add(layout);
            }
        }
        #endregion
        #region Renderpasses
        public RenderpassBeginBuilder BeginRenderpass(Renderpass renderpass, FrameBuffer frameBuffer, Rect2D? area=null, SubpassContents subpassContents=SubpassContents.Inline)
        {
            #region Validation
            if(!IsPrimary)
                throw new InvalidOperationException();
            if(!Pool.Family.Flags.HasFlag(QueueFlags.Graphics))
                throw new NotSupportedException();
            if(renderpass.Device != Pool.Device)
                throw new ArgumentException(nameof(renderpass));
            if(frameBuffer.Renderpass.Device != Pool.Device)
                throw new ArgumentException(nameof(frameBuffer));
            if(!renderpass.IsCompatible(frameBuffer.Renderpass))
                throw new ArgumentException(nameof(renderpass));
            #endregion

            return new RenderpassBeginBuilder(this, clears =>
            {
                #region Validation
                //TODO: ? If any of the initialLayout or finalLayout member of the VkAttachmentDescription structures or the layout member of the VkAttachmentReference structures specified when creating the render pass specified in the renderPass member of pRenderPassBegin is VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL then the corresponding attachment image subresource of the framebuffer specified in the framebuffer member of pRenderPassBegin must have been created with VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT set
                //TODO: ? If any of the initialLayout or finalLayout member of the VkAttachmentDescription structures or the layout member of the VkAttachmentReference structures specified when creating the render pass specified in the renderPass member of pRenderPassBegin is VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL or VK_IMAGE_LAYOUT_DEPTH_STENCIL_READ_ONLY_OPTIMAL then the corresponding attachment image subresource of the framebuffer specified in the framebuffer member of pRenderPassBegin must have been created with VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT set
                //TODO: ? If any of the initialLayout or finalLayout member of the VkAttachmentDescription structures or the layout member of the VkAttachmentReference structures specified when creating the render pass specified in the renderPass member of pRenderPassBegin is VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL then the corresponding attachment image subresource of the framebuffer specified in the framebuffer member of pRenderPassBegin must have been created with VK_IMAGE_USAGE_SAMPLED_BIT or VK_IMAGE_USAGE_INPUT_ATTACHMENT_BIT set
                //TODO: ? If any of the initialLayout or finalLayout member of the VkAttachmentDescription structures or the layout member of the VkAttachmentReference structures specified when creating the render pass specified in the renderPass member of pRenderPassBegin is VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL then the corresponding attachment image subresource of the framebuffer specified in the framebuffer member of pRenderPassBegin must have been created with VK_IMAGE_USAGE_TRANSFER_SRC_BIT set
                //TODO: ? If any of the initialLayout or finalLayout member of the VkAttachmentDescription structures or the layout member of the VkAttachmentReference structures specified when creating the render pass specified in the renderPass member of pRenderPassBegin is VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL then the corresponding attachment image subresource of the framebuffer specified in the framebuffer member of pRenderPassBegin must have been created with VK_IMAGE_USAGE_TRANSFER_DST_BIT set
                var stages = Stage.None;
                foreach(var dependency in renderpass.Dependencies)
                    stages |= dependency.Src.Stage | dependency.Dst.Stage;
                if(!Pool.Family.Flags.Supports(stages))
                    throw new NotSupportedException(nameof(renderpass));
                #endregion

                using(var clearPtr = clears.FixList())
                {
                    area = area ?? frameBuffer.Size;
                    var info = new RenderpassBeginInfo(renderpass, frameBuffer, area.Value, (uint)clears.Count, (ClearValue*)clearPtr);
                    using(Lock)
                    {
                        if(State != CommandBufferState.Recording)
                            throw new InvalidOperationException();
                        if(renderpassInfo != null)
                            throw new InvalidOperationException();
                        vkCmdBeginRenderPass(this, &info, subpassContents);
                        renderpassInfo = new RenderpassInfo {Renderpass = renderpass, FrameBuffer = frameBuffer, Area = area.Value, Subpass = renderpass.Subpasses.First(), SubpassContents = subpassContents};
                        isFirstPipelineBind = true;
                        parts.Add(renderpass);
                        parts.Add(frameBuffer);
                    }
                }
            });
        }
        public void EndRenderpass()
        {
            using(Lock)
            {
                if(State != CommandBufferState.Recording)
                    throw new InvalidOperationException();
                if(renderpassInfo == null)
                    throw new InvalidOperationException();
                if(renderpassInfo.Subpass != renderpassInfo.Renderpass.Subpasses.Last())
                    throw new InvalidOperationException();
                vkCmdEndRenderPass(this);
                renderpassInfo = null;
            }
        }
        public void NextSubpass(SubpassContents subpassContents=SubpassContents.Inline)
        {
            using(Lock)
            {
                if(State != CommandBufferState.Recording)
                    throw new InvalidOperationException();
                if(renderpassInfo == null)
                    throw new InvalidOperationException();
                if(renderpassInfo.Subpass.Index >= renderpassInfo.Renderpass.Subpasses.Count)
                    throw new IndexOutOfRangeException();
                vkCmdNextSubpass(this, subpassContents);
                renderpassInfo.Subpass = renderpassInfo.Renderpass.Subpasses[(int)(renderpassInfo.Subpass.Index + 1)];
                isFirstPipelineBind = true;
            }
        }
        #endregion
        #region Execute
        public void Execute(params CommandBuffer[] buffers)
        {
            #region Validation
            if(!IsPrimary)
                throw new InvalidOperationException();
            var seen = new HashSet<CommandBuffer>();
            foreach(var buffer in buffers)
            {
                if(buffer.IsPrimary)
                    throw new ArgumentException(nameof(buffers));
                if(buffer.State != CommandBufferState.Pending && buffer.State != CommandBufferState.Executable)
                    throw new InvalidOperationException();
                if(!buffer.Usage.HasFlag(CommandBufferUsageFlags.SimultaneousUse))
                {
                    foreach(var primary in buffer.Primaries)
                    {
                        if(primary.State == CommandBufferState.Pending)
                            throw new NotSupportedException();
                    }
                    if(buffer.State == CommandBufferState.Pending)
                        throw new NotSupportedException();
                    if(relatives.Contains(buffer) || seen.Contains(buffer))
                        throw new NotSupportedException();
                    seen.Add(buffer);
                }
                if(buffer.Pool.Family != Pool.Family)
                    throw new ArgumentException(nameof(buffers));
            }
            #endregion

            var bufferPtr = stackalloc CommandBufferHandle[buffers.Length];
            uint bufferCount = 0;
            foreach(var buffer in buffers)
                bufferPtr[bufferCount++] = buffer;

            using(Lock)
            {
                #region Validation
                if(State != CommandBufferState.Recording)
                    throw new InvalidOperationException();
                if(renderpassInfo != null)
                {
                    if(renderpassInfo.SubpassContents != SubpassContents.SecondaryCommandBuffers)
                        throw new NotSupportedException();
                    foreach(var buffer in buffers)
                    {
                        if(!buffer.Usage.HasFlag(CommandBufferUsageFlags.RenderPassContinue))
                            throw new NotSupportedException();
                        if(buffer.inheritanceInfo.Subpass != renderpassInfo.Subpass)
                            throw new ArgumentException(nameof(buffers));
                        if(!buffer.inheritanceInfo.Subpass.Renderpass.IsCompatible(renderpassInfo.Renderpass))
                            throw new ArgumentException(nameof(buffers));
                        if(renderpassInfo.FrameBuffer != null && buffer.inheritanceInfo.FrameBuffer != renderpassInfo.FrameBuffer)
                            throw new ArgumentException(nameof(buffers));
                    }
                }
                else
                {
                    if(buffers.Any(buffer => buffer.Usage.HasFlag(CommandBufferUsageFlags.RenderPassContinue)))
                        throw new NotSupportedException();
                }
                if(!Pool.Device.Features.InheritedQueries && activeQueries.Count != 0)
                    throw new NotSupportedException();
                if(activeQueries.Keys.Any(query => query.Pool.Type == QueryType.Occlusion))
                {
                    if(buffers.Any(buffer => !buffer.inheritanceInfo.OcclusionQueryEnable))
                        throw new ArgumentException(nameof(buffers));
                }
                bool hasOcclusion = false;
                var requiredControl = QueryControlFlags.None;
                var requiredStats = PipelineStatisticFlags.None;
                foreach(var (query, flags) in activeQueries)
                {
                    switch(query.Pool.Type)
                    {
                        case QueryType.Occlusion:
                            hasOcclusion = true;
                            requiredControl |= flags;
                            break;
                        case QueryType.PipelineStatistics:
                            requiredStats |= ((PipelineStatisticQueryPool)query.Pool).Statistics;
                            break;
                    }
                }
                foreach(var buffer in buffers)
                {
                    if(hasOcclusion && !buffer.inheritanceInfo.OcclusionQueryEnable)
                        throw new ArgumentException(nameof(buffers));
                    if(!buffer.inheritanceInfo.QueryFlags.HasFlag(requiredControl))
                        throw new ArgumentException(nameof(buffers));
                    if(!buffer.inheritanceInfo.PipelineStatistics.HasFlag(requiredStats))
                        throw new ArgumentException(nameof(buffers));

                }
                #endregion

                vkCmdExecuteCommands(this, bufferCount, bufferPtr);
                foreach(var buffer in buffers)
                {
                    relatives.Add(buffer);
                    buffer.relatives.Add(this);
                    parts.Add(buffer);
                }
            }
        }
        #endregion

        private readonly VkBeginCommandBuffer vkBeginCommandBuffer;
        private readonly VkEndCommandBuffer vkEndCommandBuffer;
        private readonly VkCmdBindPipeline vkCmdBindPipeline;
        private readonly VkCmdBindVertexBuffers vkCmdBindVertexBuffers;
        private readonly VkCmdBindDescriptorSets vkCmdBindDescriptorSets;
        private readonly VkCmdBindIndexBuffer vkCmdBindIndexBuffer;
        private readonly VkCmdSetViewport vkCmdSetViewport;
        private readonly VkCmdSetScissor vkCmdSetScissor;
        private readonly VkCmdSetDepthBounds vkCmdSetDepthBounds;
        private readonly VkCmdSetLineWidth vkCmdSetLineWidth;
        private readonly VkCmdSetDepthBias vkCmdSetDepthBias;
        private readonly VkCmdSetStencilCompareMask vkCmdSetStencilCompareMask;
        private readonly VkCmdSetStencilWriteMask vkCmdSetStencilWriteMask;
        private readonly VkCmdSetStencilReference vkCmdSetStencilReference;
        private readonly VkCmdSetBlendConstants vkCmdSetBlendConstants;
        private readonly VkCmdDraw vkCmdDraw;
        private readonly VkCmdDrawIndexed vkCmdDrawIndexed;
        private readonly VkCmdDrawIndirect vkCmdDrawIndirect;
        private readonly VkCmdDrawIndexedIndirect vkCmdDrawIndexedIndirect;
        private readonly VkCmdDispatch vkCmdDispatch;
        private readonly VkCmdDispatchIndirect vkCmdDispatchIndirect;
        private readonly VkCmdCopyBuffer vkCmdCopyBuffer;
        private readonly VkCmdCopyImage vkCmdCopyImage;
        private readonly VkCmdBlitImage vkCmdBlitImage;
        private readonly VkCmdCopyBufferToImage vkCmdCopyBufferToImage;
        private readonly VkCmdCopyImageToBuffer vkCmdCopyImageToBuffer;
        private readonly VkCmdUpdateBuffer vkCmdUpdateBuffer;
        private readonly VkCmdFillBuffer vkCmdFillBuffer;
        private readonly VkCmdClearColorImage vkCmdClearColorImage;
        private readonly VkCmdClearDepthStencilImage vkCmdClearDepthStencilImage;
        private readonly VkCmdResolveImage vkCmdResolveImage;
        private readonly VkCmdClearAttachments vkCmdClearAttachments;
        private readonly VkCmdSetEvent vkCmdSetEvent;
        private readonly VkCmdResetEvent vkCmdResetEvent;
        private readonly VkCmdWaitEvents vkCmdWaitEvents;
        private readonly VkCmdPipelineBarrier vkCmdPipelineBarrier;
        private readonly VkCmdBeginQuery vkCmdBeginQuery;
        private readonly VkCmdEndQuery vkCmdEndQuery;
        private readonly VkCmdResetQueryPool vkCmdResetQueryPool;
        private readonly VkCmdCopyQueryPoolResults vkCmdCopyQueryPoolResults;
        private readonly VkCmdWriteTimestamp vkCmdWriteTimestamp;
        private readonly VkCmdPushConstants vkCmdPushConstants;
        private readonly VkCmdBeginRenderPass vkCmdBeginRenderPass;
        private readonly VkCmdEndRenderPass vkCmdEndRenderPass;
        private readonly VkCmdNextSubpass vkCmdNextSubpass;
        private readonly VkCmdExecuteCommands vkCmdExecuteCommands;

        private delegate Result VkBeginCommandBuffer(CommandBufferHandle buffer, CommandBufferBeginInfo* info);
        private delegate Result VkEndCommandBuffer(CommandBufferHandle buffer);
        private delegate void VkCmdBindPipeline(CommandBufferHandle buffer, PipelineBindPoint bindPoint, PipelineHandle pipeline);
        private delegate void VkCmdBindVertexBuffers(CommandBufferHandle buffer, uint first, uint count, BufferHandle* buffers, Word* offsets);
        private delegate void VkCmdBindDescriptorSets(CommandBufferHandle buffer, PipelineBindPoint bindPoint, PipelineLayout layout, uint first, uint count, DescriptorSetHandle* sets, uint dynamicOffsetCount, uint* dynamicOffsets);
        private delegate void VkCmdBindIndexBuffer(CommandBufferHandle buffer, BufferHandle bufferHandle, Word offset, IndexType type);
        private delegate void VkCmdSetViewport(CommandBufferHandle buffer, uint first, uint count, Viewport* viewports);
        private delegate void VkCmdSetScissor(CommandBufferHandle buffer, uint first, uint count, Rect2D* scissors);
        private delegate void VkCmdSetDepthBounds(CommandBufferHandle buffer, float min, float max);
        private delegate void VkCmdSetLineWidth(CommandBufferHandle buffer, float width);
        private delegate void VkCmdSetDepthBias(CommandBufferHandle buffer, float constantFactor, float clamp, float slopeFactor);
        private delegate void VkCmdSetStencilCompareMask(CommandBufferHandle buffer, StencilFaceFlags face, uint compareMask);
        private delegate void VkCmdSetStencilWriteMask(CommandBufferHandle buffer, StencilFaceFlags face, uint writeMask);
        private delegate void VkCmdSetStencilReference(CommandBufferHandle buffer, StencilFaceFlags face, uint reference);
        private delegate void VkCmdSetBlendConstants(CommandBufferHandle buffer, F4 constants);
        private delegate void VkCmdDraw(CommandBufferHandle buffer, uint vertexCount, uint instanceCount, uint firstVertex, uint firstInstance);
        private delegate void VkCmdDrawIndexed(CommandBufferHandle buffer, uint indexCount, uint instanceCount, uint firstIndex, int vertexOffset, uint firstInstance);
        private delegate void VkCmdDrawIndirect(CommandBufferHandle buffer, BufferHandle bufferHandle, Word offset, uint drawCount, uint stride);
        private delegate void VkCmdDrawIndexedIndirect(CommandBufferHandle buffer, BufferHandle bufferHandle, Word offset, uint drawCount, uint stride);
        private delegate void VkCmdDispatch(CommandBufferHandle buffer, uint groupCountX, uint groupCountY, uint groupCountZ);
        private delegate void VkCmdDispatchIndirect(CommandBufferHandle buffer, BufferHandle buf, Word offset);
        private delegate void VkCmdCopyBuffer(CommandBufferHandle buffer, BufferHandle src, BufferHandle dst, uint regionCount, BufferCopy* regions);
        private delegate void VkCmdCopyImage(CommandBufferHandle buffer, ImageHandle src, ImageLayout srcLayout, ImageHandle dst, ImageLayout dstLayout, uint regionCount, ImageCopy* regions);
        private delegate void VkCmdBlitImage(CommandBufferHandle buffer, ImageHandle src, ImageLayout srcLayout, ImageHandle dst, ImageLayout dstLayout, uint regionCount, ImageBlit* regions, Filter filer);
        private delegate void VkCmdCopyBufferToImage(CommandBufferHandle buffer, BufferHandle src, ImageHandle dst, ImageLayout dstLayout, uint regionCount, BufferImageCopy* regions);
        private delegate void VkCmdCopyImageToBuffer(CommandBufferHandle buffer, ImageHandle src, ImageLayout srcLayout, BufferHandle dst, uint regionCount, BufferImageCopy* regions);
        private delegate void VkCmdUpdateBuffer(CommandBufferHandle buffer, BufferHandle dst, Word dstOffset, Word size, void* data);
        private delegate void VkCmdFillBuffer(CommandBufferHandle buffer, BufferHandle dst, Word dstoffset, Word size, uint data);
        private delegate void VkCmdClearColorImage(CommandBufferHandle buffer, ImageHandle image, ImageLayout layout, Color* color, uint rangeCount, ImageSubresourceRange* ranges);
        private delegate void VkCmdClearDepthStencilImage(CommandBufferHandle buffer, ImageHandle image, ImageLayout layout, DepthStencilValue* depthStencil, uint rangeCount, ImageSubresourceRange* ranges);
        private delegate void VkCmdResolveImage(CommandBufferHandle buffer, ImageHandle src, ImageLayout srcLayout, ImageHandle dst, ImageLayout dstLayout, uint regionCount, ImageResolve* regions);
        private delegate void VkCmdClearAttachments(CommandBufferHandle buffer, uint attachmentCount, ClearAttachment* attachments, uint rectCount, ClearRect* rects);
        private delegate void VkCmdSetEvent(CommandBufferHandle buffer, EventHandle @event, Stage stage);
        private delegate void VkCmdResetEvent(CommandBufferHandle buffer, EventHandle @event, Stage stage);
        private delegate void VkCmdWaitEvents(CommandBufferHandle buffer, uint eventCount, EventHandle* events, Stage srcStage, Stage dstStage, uint memoryCount, MemoryBarrier* memories, uint bufferCount, BufferMemoryBarrier* buffers, uint imageCount, ImageMemoryBarrier* images);
        private delegate void VkCmdPipelineBarrier(CommandBufferHandle buffer, Stage srcStage, Stage dstStage, DependencyFlags dependencies, uint memoryCount, MemoryBarrier* memories, uint bufferCount, BufferMemoryBarrier* buffers, uint imageCount, ImageMemoryBarrier* images);
        private delegate void VkCmdBeginQuery(CommandBufferHandle buffer, QueryPoolHandle pool, uint query, QueryControlFlags flags);
        private delegate void VkCmdEndQuery(CommandBufferHandle buffer, QueryPoolHandle pool, uint query);
        private delegate void VkCmdResetQueryPool(CommandBufferHandle buffer, QueryPoolHandle pool, uint first, uint count);
        private delegate void VkCmdCopyQueryPoolResults(CommandBufferHandle buffer, QueryPoolHandle pool, uint first, uint count, BufferHandle dst, Word offset, Word stride, QueryResultFlags flags);
        private delegate void VkCmdWriteTimestamp(CommandBufferHandle buffer, Stage stage, QueryPoolHandle pool, uint query);
        private delegate void VkCmdPushConstants(CommandBufferHandle buffer, PipelineLayoutHandle layout, ShaderStageFlags stage, uint offset, uint size, void* values);
        private delegate void VkCmdBeginRenderPass(CommandBufferHandle buffer, RenderpassBeginInfo* info, SubpassContents subpassContents);
        private delegate void VkCmdEndRenderPass(CommandBufferHandle buffer);
        private delegate void VkCmdNextSubpass(CommandBufferHandle buffer, SubpassContents subpassContents);
        private delegate void VkCmdExecuteCommands(CommandBufferHandle buffer, uint count, CommandBufferHandle* buffers);

        private class InheritanceInfo
        {
            public Subpass Subpass;
            public FrameBuffer FrameBuffer;
            public bool OcclusionQueryEnable;
            public QueryControlFlags QueryFlags;
            public PipelineStatisticFlags PipelineStatistics;
        }
        private class RenderpassInfo
        {
            public Renderpass Renderpass;
            public FrameBuffer FrameBuffer;
            public Rect2D Area;
            public Subpass Subpass;
            public SubpassContents SubpassContents;
        }

        public class RenderpassBeginBuilder : BuilderBase<CommandBuffer>
        {
            internal delegate void DoneDelegate(List<ClearValue> clears);
            private readonly DoneDelegate done;
            private readonly List<ClearValue> clears = new List<ClearValue>();

            internal RenderpassBeginBuilder(CommandBuffer buffer, DoneDelegate done) : base(buffer)
            {
                this.done = done;
            }
            public RenderpassBeginBuilder Clear(Attachment attachment, ClearValue value)
            {
                if(attachment.LoadOp != AttachmentLoadOp.Clear && attachment.StencilLoadOp != AttachmentLoadOp.Clear)
                    throw new ArgumentException(nameof(attachment));
                clears.Insert((int)attachment.Index, value);
                return this;
            }
            public void Done()
            {
                AssertNotDone();
                MarkDone();
                done(clears);
            }
        }
        public class BufferCopyBuilder : BuilderBase<CommandBuffer>
        {
            internal delegate void DoneDelegate(List<BufferCopy> regions);
            private readonly Buffer src;
            private readonly Buffer dst;
            private readonly DoneDelegate done;
            private readonly List<BufferCopy> regions = new List<BufferCopy>();

            internal BufferCopyBuilder(CommandBuffer obj, Buffer src, Buffer dst, DoneDelegate done) : base(obj)
            {
                this.src = src;
                this.dst = dst;
                this.done = done;
            }
            public BufferCopyBuilder Region(ulong size, ulong srcOffset=0, ulong dstOffset=0)
            {
                AssertNotDone();

                #region Validation
                if(size == 0)
                    throw new ArgumentOutOfRangeException(nameof(size));
                if(srcOffset + size > src.Size)
                    throw new ArgumentOutOfRangeException(nameof(srcOffset));
                if(dstOffset + size > dst.Size)
                    throw new ArgumentOutOfRangeException(nameof(dstOffset));
                #endregion

                regions.Add((srcOffset, dstOffset, size));
                return this;
            }
            public void Done()
            {
                AssertNotDone();
                MarkDone();
                done(regions);
            }
            public void All(ulong srcOffset=0, ulong dstOffset=0)
            {
                Region(Math.Min(src.Size, dst.Size), srcOffset, dstOffset).Done();
            }
        }
        public class ImageCopyBuilder : BuilderBase<CommandBuffer>
        {
            internal delegate void DoneDelegate(List<ImageCopy> regions);
            private readonly ImageBase src;
            private readonly ImageBase dst;
            private readonly DoneDelegate done;
            private readonly List<ImageCopy> regions = new List<ImageCopy>();

            internal ImageCopyBuilder(CommandBuffer obj, ImageBase src, ImageBase dst, DoneDelegate done) : base(obj)
            {
                this.src = src;
                this.dst = dst;
                this.done = done;
            }
            public ImageCopyBuilder Region((uint W, uint H, uint D) size,
                (int X, int Y, int Z)? srcOffset=null, (int X, int Y, int Z)? dstOffset=null,
                ImageAspectFlags aspect=ImageAspectFlags.Color,
                uint srcMip=0, uint dstMip=0, uint srcBaseArrayLayer=0, uint dstBaseArrayLayer=0, uint layerCount=1)
            {
                AssertNotDone();

                var sOff = srcOffset ?? (0, 0, 0);
                var dOff = dstOffset ?? (0, 0, 0);

                #region Validation
                if(aspect == ImageAspectFlags.None)
                    throw new ArgumentException(nameof(aspect));
                if(aspect.HasFlag(ImageAspectFlags.Color) && (aspect.HasFlag(ImageAspectFlags.Depth) || aspect.HasFlag(ImageAspectFlags.Stencil)))
                    throw new ArgumentException(nameof(aspect));
                if(aspect.HasFlag(ImageAspectFlags.Metadata))
                    throw new ArgumentException(nameof(aspect));

                Check(src, sOff, srcMip, srcBaseArrayLayer);
                Check(dst, dOff, dstMip, dstBaseArrayLayer);

                void Check(ImageBase img, (int X, int Y, int Z) off, uint mip, uint baseArrayLayer)
                {
                    if(off.X < 0 || off.X + size.W > img.Extent.Width)
                        throw new ArgumentOutOfRangeException(nameof(off));
                    if(off.Y < 0 || off.Y + size.H > img.Extent.Height)
                        throw new ArgumentOutOfRangeException(nameof(off));
                    if(off.Z < 0 || off.Z + size.D > img.Extent.Depth)
                        throw new ArgumentOutOfRangeException(nameof(off));

                    if(mip >= img.MipLevels)
                        throw new ArgumentOutOfRangeException(nameof(mip));
                    if(baseArrayLayer + layerCount > img.ArrayLayers)
                        throw new ArgumentOutOfRangeException(nameof(baseArrayLayer));
                    if(img.Type == ImageType.X1D && (off.Y != 0 || size.H != 1))
                        throw new ArgumentException(nameof(img));
                    if(img.Type != ImageType.X3D && (off.Z != 0 || size.D != 1))
                        throw new ArgumentException(nameof(img));
                    if(baseArrayLayer + layerCount > img.ArrayLayers)
                        throw new ArgumentOutOfRangeException(nameof(baseArrayLayer));
                    if(img.Format.IsCompressed())
                    {
                        var unit = img.Format.GetElementSize();
                        if(off.X % unit != 0 || off.Y % unit != 0 || off.Z % unit != 0)
                            throw new ArgumentException(nameof(off));
                        if(size.W % unit != 0 && (off.X + size.W) != img.Extent.Width)
                            throw new ArgumentException(nameof(size));
                        if(size.H % unit != 0 && (off.Y + size.H) != img.Extent.Height)
                            throw new ArgumentException(nameof(size));
                        if(size.D % unit != 0 && (off.Z + size.D) != img.Extent.Depth)
                            throw new ArgumentException(nameof(size));
                    }
                }

                if(src.Type == ImageType.X3D || dst.Type == ImageType.X3D)
                {
                    if(srcBaseArrayLayer != 0)
                        throw new ArgumentException(nameof(srcBaseArrayLayer));
                    if(dstBaseArrayLayer != 0)
                        throw new ArgumentException(nameof(dstBaseArrayLayer));
                    if(layerCount != 1)
                        throw new ArgumentException(nameof(layerCount));
                }
                var (gX, gY, gZ) = Object.Pool.Family.MinImageTransferGranularity;
                if(gX == 0 && gY == 0 && gZ == 0)
                {
                    if(sOff.X != 0 || sOff.Y != 0 || sOff.Z != 0)
                        throw new ArgumentException(nameof(sOff));
                    if(dOff.X != 0 || dOff.Y != 0 || dOff.Z != 0)
                        throw new ArgumentException(nameof(dOff));
                    if(size.W != src.Extent.Width || size.H != src.Extent.Height || size.D != src.Extent.Depth)
                        throw new ArgumentException(nameof(size));
                    if(size.W != dst.Extent.Width || size.H != dst.Extent.Height || size.D != dst.Extent.Depth)
                        throw new ArgumentException(nameof(size));
                }
                else
                {
                    if(sOff.X % gX != 0 || sOff.Y % gY != 0 || sOff.Z % gZ != 0)
                        throw new ArgumentException(nameof(sOff));
                    if(dOff.X % gX != 0 || dOff.Y % gY != 0 || dOff.Z % gZ != 0)
                        throw new ArgumentException(nameof(dOff));
                    if(size.W % gX != 0)
                    {
                        if(sOff.X + size.W != src.Extent.Width)
                            throw new ArgumentException(nameof(size));
                        if(dOff.X + size.W != dst.Extent.Width)
                            throw new ArgumentException(nameof(size));
                    }
                    if(size.H % gY != 0)
                    {
                        if(sOff.Y + size.H != src.Extent.Height)
                            throw new ArgumentException(nameof(size));
                        if(dOff.Y + size.H != dst.Extent.Height)
                            throw new ArgumentException(nameof(size));
                    }
                    if(size.D % gZ != 0)
                    {
                        if(sOff.Z + size.D != src.Extent.Depth)
                            throw new ArgumentException(nameof(size));
                        if(dOff.Z + size.D != dst.Extent.Depth)
                            throw new ArgumentException(nameof(size));
                    }
                }
                #endregion

                regions.Add(new ImageCopy(aspect, srcMip, dstMip, srcBaseArrayLayer, dstBaseArrayLayer, layerCount, sOff, dOff, size));

                return this;
            }
            public void Done()
            {
                AssertNotDone();
                MarkDone();
                done(regions);
            }
            public void All(ImageAspectFlags aspect=ImageAspectFlags.Color,
                uint srcMip=0, uint dstMip=0, uint srcBaseArrayLayer=0, uint dstBaseArrayLayer=0, uint layerCount=1)
            {
                uint w = Math.Min(src.Extent.Width, dst.Extent.Width);
                uint h = Math.Min(src.Extent.Height, dst.Extent.Height);
                uint d = Math.Min(src.Extent.Depth, dst.Extent.Depth);
                Region((w, h, d), (0, 0, 0), (0, 0, 0), aspect, srcMip, dstMip, srcBaseArrayLayer, dstBaseArrayLayer, layerCount).Done();
            }
        }
        public class ImageBlitBuilder : BuilderBase<CommandBuffer>
        {
            internal delegate void DoneDelegate(List<ImageBlit> regions);
            private readonly ImageBase src;
            private readonly ImageBase dst;
            private readonly DoneDelegate done;
            private readonly List<ImageBlit> regions = new List<ImageBlit>();

            internal ImageBlitBuilder(CommandBuffer obj, ImageBase src, ImageBase dst, DoneDelegate done) : base(obj)
            {
                this.src = src;
                this.dst = dst;
                this.done = done;
            }
            public ImageBlitBuilder Region(
                (int X, int Y, int Z)? srcMin=null, (int X, int Y, int Z)? srcMax=null,
                (int X, int Y, int Z)? dstMin=null, (int X, int Y, int Z)? dstMax=null,
                ImageAspectFlags aspect=ImageAspectFlags.Color,
                uint srcMip=0, uint dstMip=0, uint srcBaseArrayLayer=0, uint dstBaseArrayLayer=0, uint layerCount=1)
            {
                AssertNotDone();

                var sMin = srcMin ?? (0, 0, 0);
                var sMax = srcMax ?? ((int, int, int))src.Extent;
                var dMin = dstMin ?? (0, 0, 0);
                var dMax = dstMax ?? ((int, int, int))src.Extent;

                #region Validation
                if(aspect == ImageAspectFlags.None)
                    throw new ArgumentException(nameof(aspect));
                if(aspect.HasFlag(ImageAspectFlags.Color) && (aspect.HasFlag(ImageAspectFlags.Depth) || aspect.HasFlag(ImageAspectFlags.Stencil)))
                    throw new ArgumentException(nameof(aspect));
                if(aspect.HasFlag(ImageAspectFlags.Metadata))
                    throw new ArgumentException(nameof(aspect));

                Check(src, sMin, sMax, srcMip, srcBaseArrayLayer, FormatFeatureFlags.BlitSrc);
                Check(dst, dMin, dMax, dstMip, dstBaseArrayLayer, FormatFeatureFlags.BlitDst);

                void Check(ImageBase img, (int X, int Y, int Z) min, (int X, int Y, int Z) max, uint mip, uint baseArrayLayer, FormatFeatureFlags requiredFeature)
                {
                    if(min.X < 0 || min.X > img.Extent.Width)
                        throw new ArgumentOutOfRangeException(nameof(min));
                    if(min.Y < 0 || min.Y > img.Extent.Height)
                        throw new ArgumentOutOfRangeException(nameof(min));
                    if(min.Z < 0 || min.Z > img.Extent.Depth)
                        throw new ArgumentOutOfRangeException(nameof(min));
                    if(max.X < 0 || max.X > img.Extent.Width)
                        throw new ArgumentOutOfRangeException(nameof(max));
                    if(max.Y < 0 || max.Y > img.Extent.Height)
                        throw new ArgumentOutOfRangeException(nameof(max));
                    if(max.Z < 0 || max.Z > img.Extent.Depth)
                        throw new ArgumentOutOfRangeException(nameof(max));

                    if(mip >= img.MipLevels)
                        throw new ArgumentOutOfRangeException(nameof(mip));
                    if(baseArrayLayer + layerCount > img.ArrayLayers)
                        throw new ArgumentOutOfRangeException(nameof(baseArrayLayer));
                    if(img.Type == ImageType.X1D && (min.Y != 0 || max.Y != 1))
                        throw new ArgumentException(nameof(img));
                    if(img.Type != ImageType.X3D && (min.Z != 0 || max.Z != 1))
                        throw new ArgumentException(nameof(img));
                    if(baseArrayLayer + layerCount > img.ArrayLayers)
                        throw new ArgumentOutOfRangeException(nameof(baseArrayLayer));
                }

                if(src.Type == ImageType.X3D || dst.Type == ImageType.X3D)
                {
                    if(srcBaseArrayLayer != 0)
                        throw new ArgumentException(nameof(srcBaseArrayLayer));
                    if(dstBaseArrayLayer != 0)
                        throw new ArgumentException(nameof(dstBaseArrayLayer));
                    if(layerCount != 1)
                        throw new ArgumentException(nameof(layerCount));
                }
                #endregion

                regions.Add(new ImageBlit(aspect, srcMip, dstMip, srcBaseArrayLayer, dstBaseArrayLayer, layerCount, sMin, sMax, dMin, dMax));

                return this;
            }
            public void Done()
            {
                AssertNotDone();
                MarkDone();
                done(regions);
            }
        }
        public class BufferImageCopyBuilder : BuilderBase<CommandBuffer>
        {
            internal delegate void DoneDelegate(List<BufferImageCopy> regions);
            private readonly Buffer src;
            private readonly ImageBase dst;
            private readonly DoneDelegate done;
            private readonly List<BufferImageCopy> regions = new List<BufferImageCopy>();

            internal BufferImageCopyBuilder(CommandBuffer obj, Buffer src, ImageBase dst, DoneDelegate done) : base(obj)
            {
                this.src = src;
                this.dst = dst;
                this.done = done;
            }
            public BufferImageCopyBuilder Region((uint W, uint H, uint D) dstSize, ulong srcOffset=0, (uint W, uint H)? srcSize=null,
                (int X, int Y, int Z)? dstOffset=null, ImageAspectFlags dstAspect=ImageAspectFlags.Color,
                uint dstMip=0, uint dstBaseArrayLayer=0, uint dstLayerCount=1)
            {
                AssertNotDone();

                var sSize = srcSize ?? (0, 0);
                var dOff = dstOffset ?? (0, 0, 0);

                #region Validation
                //TODO: If the the calling command’s VkImage parameter’s format is not a depth/stencil format, then bufferOffset must be a multiple of the format’s element size
                if(srcOffset % 4 != 0)
                    throw new ArgumentException(nameof(srcOffset));
                if((sSize.W != 0 && sSize.W < dstSize.W) || (sSize.H != 0 && sSize.H < dstSize.H))
                    throw new ArgumentOutOfRangeException(nameof(srcSize));

                Check(dst, dOff, dstSize, dstMip, dstBaseArrayLayer, dstLayerCount);

                void Check(ImageBase img, (int X, int Y, int Z) off, (uint W, uint H, uint D) size, uint mip, uint baseArrayLayer, uint layerCount)
                {
                    if(off.X < 0 || off.X + size.W > img.Extent.Width)
                        throw new ArgumentOutOfRangeException(nameof(off));
                    if(off.Y < 0 || off.Y + size.H > img.Extent.Height)
                        throw new ArgumentOutOfRangeException(nameof(off));
                    if(off.Z < 0 || off.Z + size.D > img.Extent.Depth)
                        throw new ArgumentOutOfRangeException(nameof(off));

                    if(mip >= img.MipLevels)
                        throw new ArgumentOutOfRangeException(nameof(mip));
                    if(baseArrayLayer + layerCount > img.ArrayLayers)
                        throw new ArgumentOutOfRangeException(nameof(baseArrayLayer));
                    if(img.Type == ImageType.X1D && (off.Y != 0 || size.H != 1))
                        throw new ArgumentException(nameof(img));
                    if(img.Type != ImageType.X3D && (off.Z != 0 || size.D != 1))
                        throw new ArgumentException(nameof(img));
                    if(baseArrayLayer + layerCount > img.ArrayLayers)
                        throw new ArgumentOutOfRangeException(nameof(baseArrayLayer));
                    if(img.Type == ImageType.X3D)
                    {
                        if(baseArrayLayer != 0)
                            throw new ArgumentException(nameof(baseArrayLayer));
                        if(layerCount != 1)
                            throw new ArgumentException(nameof(layerCount));
                    }
                    if(img.Format.IsCompressed())
                    {
                        var unit = img.Format.GetElementSize();
                        if(off.X % unit != 0 || off.Y % unit != 0 || off.Z % unit != 0)
                            throw new ArgumentException(nameof(off));
                        if(size.W % unit != 0 && (off.X + size.W) != img.Extent.Width)
                            throw new ArgumentException(nameof(size));
                        if(size.H % unit != 0 && (off.Y + size.H) != img.Extent.Height)
                            throw new ArgumentException(nameof(size));
                        if(size.D % unit != 0 && (off.Z + size.D) != img.Extent.Depth)
                            throw new ArgumentException(nameof(size));
                    }
                }

                if(!Util.IsPow2((int)dstAspect))
                    throw new ArgumentException(nameof(dstAspect));
                #endregion

                regions.Add(new BufferImageCopy(srcOffset, sSize.W, sSize.H, dstAspect, dstMip, dstBaseArrayLayer, dstLayerCount, dOff, dstSize));

                return this;
            }
            public void Done()
            {
                AssertNotDone();
                MarkDone();
                done(regions);
            }
        }
        public class ImageBufferCopyBuilder : BuilderBase<CommandBuffer>
        {
            internal delegate void DoneDelegate(List<BufferImageCopy> regions);
            private readonly ImageBase src;
            private readonly Buffer dst;
            private readonly DoneDelegate done;
            private readonly List<BufferImageCopy> regions = new List<BufferImageCopy>();

            internal ImageBufferCopyBuilder(CommandBuffer obj, ImageBase src, Buffer dst, DoneDelegate done) : base(obj)
            {
                this.src = src;
                this.dst = dst;
                this.done = done;
            }
            public ImageBufferCopyBuilder Region((uint W, uint H, uint D) srcSize, ulong dstOffset=0, (uint W, uint H)? dstSize=null,
                (int X, int Y, int Z)? srcOffset=null, ImageAspectFlags srcAspect=ImageAspectFlags.Color,
                uint srcMip=0, uint srcBaseArrayLayer=0, uint srcLayerCount=1)
            {
                AssertNotDone();

                var sOff = srcOffset ?? (0, 0, 0);
                var dSize = dstSize ?? (0, 0);

                #region Validation
                //TODO: If the the calling command’s VkImage parameter’s format is not a depth/stencil format, then bufferOffset must be a multiple of the format’s element size
                if(dstOffset % 4 != 0)
                    throw new ArgumentException(nameof(dstOffset));
                if((dSize.W != 0 && dSize.W < srcSize.W) || (dSize.H != 0 && dSize.H < srcSize.H))
                    throw new ArgumentOutOfRangeException(nameof(dstSize));

                Check(src, sOff, srcSize, srcMip, srcBaseArrayLayer, srcLayerCount);

                void Check(ImageBase img, (int X, int Y, int Z) off, (uint W, uint H, uint D) size, uint mip, uint baseArrayLayer, uint layerCount)
                {
                    if(off.X < 0 || off.X + size.W > img.Extent.Width)
                        throw new ArgumentOutOfRangeException(nameof(off));
                    if(off.Y < 0 || off.Y + size.H > img.Extent.Height)
                        throw new ArgumentOutOfRangeException(nameof(off));
                    if(off.Z < 0 || off.Z + size.D > img.Extent.Depth)
                        throw new ArgumentOutOfRangeException(nameof(off));

                    if(mip >= img.MipLevels)
                        throw new ArgumentOutOfRangeException(nameof(mip));
                    if(baseArrayLayer + layerCount > img.ArrayLayers)
                        throw new ArgumentOutOfRangeException(nameof(baseArrayLayer));
                    if(img.Type == ImageType.X1D && (off.Y != 0 || size.H != 1))
                        throw new ArgumentException(nameof(img));
                    if(img.Type != ImageType.X3D && (off.Z != 0 || size.D != 1))
                        throw new ArgumentException(nameof(img));
                    if(baseArrayLayer + layerCount > img.ArrayLayers)
                        throw new ArgumentOutOfRangeException(nameof(baseArrayLayer));
                    if(img.Type == ImageType.X3D)
                    {
                        if(baseArrayLayer != 0)
                            throw new ArgumentException(nameof(baseArrayLayer));
                        if(layerCount != 1)
                            throw new ArgumentException(nameof(layerCount));
                    }
                    if(img.Format.IsCompressed())
                    {
                        var unit = img.Format.GetElementSize();
                        if(off.X % unit != 0 || off.Y % unit != 0 || off.Z % unit != 0)
                            throw new ArgumentException(nameof(off));
                        if(size.W % unit != 0 && (off.X + size.W) != img.Extent.Width)
                            throw new ArgumentException(nameof(size));
                        if(size.H % unit != 0 && (off.Y + size.H) != img.Extent.Height)
                            throw new ArgumentException(nameof(size));
                        if(size.D % unit != 0 && (off.Z + size.D) != img.Extent.Depth)
                            throw new ArgumentException(nameof(size));
                    }
                }

                if(!Util.IsPow2((int)srcAspect))
                    throw new ArgumentException(nameof(srcAspect));
                #endregion

                regions.Add(new BufferImageCopy(dstOffset, dSize.W, dSize.H, srcAspect, srcMip, srcBaseArrayLayer, srcLayerCount, sOff, srcSize));

                return this;
            }
            public void Done()
            {
                AssertNotDone();
                MarkDone();
                done(regions);
            }
        }
        public class ImageResolveBuilder : BuilderBase<CommandBuffer>
        {
            internal delegate void DoneDelegate(List<ImageResolve> regions);
            private readonly ImageBase src;
            private readonly ImageBase dst;
            private readonly DoneDelegate done;
            private readonly List<ImageResolve> regions = new List<ImageResolve>();

            internal ImageResolveBuilder(CommandBuffer obj, ImageBase src, ImageBase dst, DoneDelegate done) : base(obj)
            {
                this.src = src;
                this.dst = dst;
                this.done = done;
            }
            public ImageResolveBuilder Region((uint W, uint H, uint D) size,
                (int X, int Y, int Z)? srcOffset=null, (int X, int Y, int Z)? dstOffset=null,
                uint srcMip=0, uint dstMip=0, uint srcBaseArrayLayer=0, uint dstBaseArrayLayer=0, uint layerCount=1)
            {
                AssertNotDone();

                var sOff = srcOffset ?? (0, 0, 0);
                var dOff = dstOffset ?? (0, 0, 0);

                #region Validation

                Check(src, sOff, srcMip, srcBaseArrayLayer);
                Check(dst, dOff, dstMip, dstBaseArrayLayer);

                void Check(ImageBase img, (int X, int Y, int Z) off, uint mip, uint baseArrayLayer)
                {
                    if(off.X < 0 || off.X + size.W > img.Extent.Width)
                        throw new ArgumentOutOfRangeException(nameof(off));
                    if(off.Y < 0 || off.Y + size.H > img.Extent.Height)
                        throw new ArgumentOutOfRangeException(nameof(off));
                    if(off.Z < 0 || off.Z + size.D > img.Extent.Depth)
                        throw new ArgumentOutOfRangeException(nameof(off));

                    if(mip >= img.MipLevels)
                        throw new ArgumentOutOfRangeException(nameof(mip));
                    if(baseArrayLayer + layerCount > img.ArrayLayers)
                        throw new ArgumentOutOfRangeException(nameof(baseArrayLayer));
                    if(img.Type == ImageType.X1D && (off.Y != 0 || size.H != 1))
                        throw new ArgumentException(nameof(img));
                    if(img.Type != ImageType.X3D && (off.Z != 0 || size.D != 1))
                        throw new ArgumentException(nameof(img));
                    if(baseArrayLayer + layerCount > img.ArrayLayers)
                        throw new ArgumentOutOfRangeException(nameof(baseArrayLayer));
                }

                if(src.Type == ImageType.X3D || dst.Type == ImageType.X3D)
                {
                    if(srcBaseArrayLayer != 0)
                        throw new ArgumentException(nameof(srcBaseArrayLayer));
                    if(dstBaseArrayLayer != 0)
                        throw new ArgumentException(nameof(dstBaseArrayLayer));
                    if(layerCount != 1)
                        throw new ArgumentException(nameof(layerCount));
                }
                #endregion

                regions.Add(new ImageResolve(srcMip, dstMip, srcBaseArrayLayer, dstBaseArrayLayer, layerCount, sOff, dOff, size));

                return this;
            }
            public void Done()
            {
                AssertNotDone();
                MarkDone();
                done(regions);
            }
            public void All(uint srcMip=0, uint dstMip=0, uint srcBaseArrayLayer=0, uint dstBaseArrayLayer=0, uint layerCount=1)
            {
                uint w = Math.Min(src.Extent.Width, dst.Extent.Width);
                uint h = Math.Min(src.Extent.Height, dst.Extent.Height);
                uint d = Math.Min(src.Extent.Depth, dst.Extent.Depth);
                Region((w, h, d), (0, 0, 0), (0, 0, 0), srcMip, dstMip, srcBaseArrayLayer, dstBaseArrayLayer, layerCount).Done();
            }
        }
        public class ClearAttachmentsBuilder : BuilderBase<CommandBuffer>
        {
            internal delegate void DoneDelegate(List<ClearAttachment> attachments, List<ClearRect> rects);
            private readonly DoneDelegate done;
            private readonly List<ClearAttachment> attachments = new List<ClearAttachment>();
            private readonly List<ClearRect> rects = new List<ClearRect>();

            internal ClearAttachmentsBuilder(CommandBuffer buffer, DoneDelegate done) : base(buffer)
            {
                this.done = done;
            }

            public ClearAttachmentsBuilder ColorAttachment(Attachment attachment, Color value) => Attachment(ImageAspectFlags.Color, attachment, value);
            public ClearAttachmentsBuilder DepthAttachment(DepthStencilValue value) => Attachment(ImageAspectFlags.Depth, render.Attachment.Unused, value);
            public ClearAttachmentsBuilder StencilAttachment(DepthStencilValue value) => Attachment(ImageAspectFlags.Stencil, render.Attachment.Unused, value);
            public ClearAttachmentsBuilder DepthStencilAttachment(DepthStencilValue value) => Attachment(ImageAspectFlags.Depth | ImageAspectFlags.Stencil, render.Attachment.Unused, value);
            private ClearAttachmentsBuilder Attachment(ImageAspectFlags aspect, AttachmentHandle attachment, ClearValue value)
            {
                AssertNotDone();
                attachments.Add(new ClearAttachment(aspect, attachment, value));
                return this;
            }
            public ClearAttachmentsBuilder Rect(Rect2D rect, uint baseArrayLayer=0, uint layerCount=1)
            {
                AssertNotDone();
                rects.Add(new ClearRect(rect, baseArrayLayer, layerCount));
                return this;
            }
            public void Done()
            {
                AssertNotDone();
                MarkDone();
                done(attachments, rects);
            }
        }
        public class BarrierBuilder : BuilderBase<CommandBuffer>
        {
            internal delegate void DoneDelegate(List<MemoryBarrier> memories, List<BufferMemoryBarrier> buffers, List<ImageMemoryBarrier> images);
            private readonly DoneDelegate done;
            private readonly List<MemoryBarrier> memoryBarriers = new List<MemoryBarrier>();
            private readonly List<BufferMemoryBarrier> bufferBarriers = new List<BufferMemoryBarrier>();
            private readonly List<ImageMemoryBarrier> imageBarriers = new List<ImageMemoryBarrier>();

            internal BarrierBuilder(CommandBuffer buffer, DoneDelegate done) : base(buffer)
            {
                this.done = done;
            }

            public BarrierBuilder Memory(AccessFlags src, AccessFlags dst)
            {
                AssertNotDone();
                memoryBarriers.Add(new MemoryBarrier(src, dst));
                return this;
            }
            public BarrierBuilder Buffer(Buffer buffer, AccessFlags src, AccessFlags dst, ulong offset=0, ulong size=buffer.Buffer.WHOLE_SIZE)
            {
                return Buffer(buffer, (src, QueueFamily.Ignored), (dst, QueueFamily.Ignored), offset, size);
            }
            public BarrierBuilder Buffer(Buffer buffer, (AccessFlags Access, QueueFamily Family) src, (AccessFlags Access, QueueFamily Family) dst, ulong offset=0, ulong size=buffer.Buffer.WHOLE_SIZE)
            {
                AssertNotDone();

                #region Validation
                if(buffer == null)
                    throw new ArgumentNullException(nameof(buffer));
                if(buffer.Device != Object.Pool.Device)
                    throw new ArgumentException(nameof(buffer));
                if(offset >= buffer.Size)
                    throw new ArgumentOutOfRangeException(nameof(offset));
                if(size != oovulkan.buffer.Buffer.WHOLE_SIZE && (size == 0 || offset + size > buffer.Size))
                    throw new ArgumentOutOfRangeException(nameof(size));
                if(src.Family != QueueFamily.Ignored || dst.Family != QueueFamily.Ignored)
                {
                    switch(buffer.SharingMode)
                    {
                        case SharingMode.Exclusive:
                            if(src.Family == QueueFamily.Ignored || dst.Family == QueueFamily.Ignored)
                                throw new ArgumentException();
                            if(src.Family.PhysicalDevice != Object.Pool.Device.PhysicalDevice)
                                throw new ArgumentException(nameof(src));
                            if(dst.Family.PhysicalDevice != Object.Pool.Device.PhysicalDevice)
                                throw new ArgumentException(nameof(dst));
                            if(src.Family != Object.Pool.Family && dst.Family != Object.Pool.Family)
                                throw new ArgumentException();
                            break;
                        case SharingMode.Concurrent:
                            throw new ArgumentException();
                    }
                }
                #endregion

                bufferBarriers.Add(new BufferMemoryBarrier(src.Access, dst.Access, src.Family, dst.Family, buffer, (Word)offset, (Word)size));
                return this;
            }
            public BarrierBuilder Image(ImageBase image, (AccessFlags Access, ImageLayout Layout, QueueFamily Family) src, (AccessFlags Access, ImageLayout Layout, QueueFamily Family) dst, ImageSubresourceRange range)
            {
                AssertNotDone();

                #region Validation
                if(image == null)
                    throw new ArgumentNullException(nameof(image));
                if(image.Device != Object.Pool.Device)
                    throw new ArgumentException(nameof(image));
                //TODO: oldLayout must be VK_IMAGE_LAYOUT_UNDEFINED or the current layout of the image subresources affected by the barrier
                if(dst.Layout == ImageLayout.Undefined || dst.Layout == ImageLayout.Preinitialized)
                    throw new ArgumentException(nameof(dst));
                if(src.Family != QueueFamily.Ignored || dst.Family != QueueFamily.Ignored)
                {
                    switch(image.SharingMode)
                    {
                        case SharingMode.Exclusive:
                            if(src.Family == QueueFamily.Ignored || dst.Family == QueueFamily.Ignored)
                                throw new ArgumentException();
                            if(src.Family.PhysicalDevice != Object.Pool.Device.PhysicalDevice)
                                throw new ArgumentException(nameof(src));
                            if(dst.Family.PhysicalDevice != Object.Pool.Device.PhysicalDevice)
                                throw new ArgumentException(nameof(dst));
                            if(src.Family != Object.Pool.Family && dst.Family != Object.Pool.Family)
                                throw new ArgumentException();
                            break;
                        case SharingMode.Concurrent:
                            throw new ArgumentException();
                    }
                }
                if(range.MipBase >= image.MipLevels)
                    throw new ArgumentOutOfRangeException(nameof(range));
                if(range.MipCount != ImageBase.REMAINING && (range.MipCount == 0 || range.MipBase + range.MipCount > image.MipLevels))
                    throw new ArgumentOutOfRangeException(nameof(range));
                if(range.LayerBase >= image.ArrayLayers)
                    throw new ArgumentOutOfRangeException(nameof(range));
                if(range.LayerCount != ImageBase.REMAINING && (range.LayerCount == 0 || range.LayerBase + range.LayerCount > image.ArrayLayers))
                    throw new ArgumentOutOfRangeException(nameof(range));
                //TODO: If image has a depth/stencil format with both depth and stencil components, then aspectMask member of subresourceRange must include both VK_IMAGE_ASPECT_DEPTH_BIT and VK_IMAGE_ASPECT_STENCIL_BIT
                if((src.Layout == ImageLayout.ColorAttachmentOptimal ||
                    dst.Layout == ImageLayout.ColorAttachmentOptimal)
                   && !image.Usage.HasFlag(ImageUsageFlags.ColorAttachment))
                    throw new NotSupportedException();
                if((src.Layout == ImageLayout.DepthStencilAttachmentOptimal || src.Layout == ImageLayout.DepthStencilReadOnlyOptimal ||
                    dst.Layout == ImageLayout.DepthStencilAttachmentOptimal || dst.Layout == ImageLayout.DepthStencilReadOnlyOptimal)
                   && !image.Usage.HasFlag(ImageUsageFlags.ColorAttachment))
                    throw new NotSupportedException();
                if((src.Layout == ImageLayout.ShaderReadOnlyOptimal ||
                    dst.Layout == ImageLayout.ShaderReadOnlyOptimal)
                   && !image.Usage.HasFlag(ImageUsageFlags.Sampled)
                   && !image.Usage.HasFlag(ImageUsageFlags.InputAttachment))
                    throw new NotSupportedException();
                if((src.Layout == ImageLayout.TransferSrcOptimal ||
                    dst.Layout == ImageLayout.TransferSrcOptimal)
                   && !image.Usage.HasFlag(ImageUsageFlags.TransferSrc))
                    throw new NotSupportedException();
                if((src.Layout == ImageLayout.TransferDstOptimal ||
                    dst.Layout == ImageLayout.TransferDstOptimal)
                   && !image.Usage.HasFlag(ImageUsageFlags.TransferDst))
                    throw new NotSupportedException();
                #endregion

                imageBarriers.Add(new ImageMemoryBarrier(src.Access, dst.Access, src.Layout, dst.Layout, src.Family, dst.Family, image, range));
                return this;
            }
            public void Done()
            {
                AssertNotDone();
                MarkDone();
                done(memoryBarriers, bufferBarriers, imageBarriers);
            }
        }
    }
}