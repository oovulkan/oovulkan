﻿using System.Runtime.InteropServices;
using oovulkan.device;

#pragma warning disable 649

namespace oovulkan.shader
{
    [StructLayout(LayoutKind.Sequential)]
    public struct ShaderModuleHandle
    {
        private long Value;

        public static implicit operator ShaderModuleHandle(long? value) => new ShaderModuleHandle {Value = value ?? 0};
        public static implicit operator long(ShaderModuleHandle handle) => handle.Value;
    }

    public unsafe class ShaderModule : VulkanObject<ShaderModuleHandle>
    {
        protected internal override bool IsChild => true;
        protected internal override VulkanObject Parent => Device;

        public readonly Device Device;
        public readonly byte[] Code;

        internal ShaderModule(Device device, byte[] code)
        {
            Device = device;
            this.Link(Device);
            Code = code;
            Create();
        }

        private void Create()
        {
            fixed(byte* ptr = Code)
            {
                ShaderModuleCreateInfo info = new ShaderModuleCreateInfo(Code.LongLength, ptr);
                using(Device.WeakLock)
                {
                    ShaderModuleHandle handle;
                    using(Device.Allocator?.WeakLock)
                        vkCreateShaderModule(Device, &info, Device.Allocator, out handle);
                    Handle = handle;
                    Device.Add(this);
                }
            }
        }

        protected override void Destroy()
        {
            using(Device.WeakLock)
                Device.Remove(this);
            using(Device.Allocator?.WeakLock)
                vkDestroyShaderModule(Device, this, Device.Allocator);
        }

        private readonly VkCreateShaderModule vkCreateShaderModule;
        private readonly VkDestroyShaderModule vkDestroyShaderModule;

        private delegate Result VkCreateShaderModule(DeviceHandle device, ShaderModuleCreateInfo* info, AllocationCallbacks* allocator, out ShaderModuleHandle shader);
        private delegate void VkDestroyShaderModule(DeviceHandle device, ShaderModuleHandle semaphore, AllocationCallbacks* allocator);
    }
}