﻿using System;
using System.Runtime.InteropServices;
using oovulkan.util;

// ReSharper disable PrivateFieldCanBeConvertedToLocalVariable

namespace oovulkan.shader
{
    [Flags]
    internal enum ShaderModuleCreateFlags : int
    {
        None = 0
    }

    [StructLayout(LayoutKind.Sequential)]
    internal unsafe struct ShaderModuleCreateInfo
    {
        private readonly StructureType Type;
        private readonly void* Next;
        private readonly ShaderModuleCreateFlags Flags;
        public readonly Word CodeSize;
        public readonly void* CodePtr;

        public ShaderModuleCreateInfo(Word codeSize, void* codePtr)
        {
            Type = StructureType.ShaderModuleCreateInfo;
            Next = null;
            Flags = ShaderModuleCreateFlags.None;
            CodeSize = codeSize;
            CodePtr = codePtr;
        }
    }
}