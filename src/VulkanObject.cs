﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using oovulkan.util;

// ReSharper disable VirtualMemberCallInConstructor

namespace oovulkan
{
    public abstract class VulkanObject : IDisposable
    {
        private readonly ThreeLock threeLock = new ThreeLock();

        public bool IsDestroyed => isDestroyed == 1;
        private int isDestroyed;

        protected internal IDisposable WeakLock => threeLock.WeakLock;
        protected internal IDisposable Lock => threeLock.Lock;
        protected internal IDisposable StrongLock => threeLock.StrongLock;

        protected internal virtual bool IsChild => false;
        protected internal virtual VulkanObject Parent => throw new NotSupportedException();
        protected internal virtual bool IsParent => false;
        protected internal virtual IEnumerable<VulkanObject> Children => throw new NotSupportedException();

        ~VulkanObject() => InternalDestroy();
        public virtual void Dispose() => InternalDestroy();

        private void InternalDestroy()
        {
            if(Interlocked.Exchange(ref isDestroyed, 1) != 0)
                return;
            using(StrongLock)
            {
                if(IsParent)
                {
                    //we copy children to an array so the collection can be modified without causing errors
                    foreach(var child in Children.ToArray())
                        child.InternalDestroy();
                }
                Destroy();
                threeLock.Dispose();
            }
        }
        protected abstract void Destroy();
    }

    public abstract class VulkanObject<H> : VulkanObject
    {
        public H Handle{ get; protected set; }

        public static implicit operator H(VulkanObject<H> obj) => obj != null ? obj.Handle : default(H);
    }

    public static class VulkanObjectHelper
    {
        public static IDisposable LockAll(this IEnumerable<VulkanObject> objs, Func<VulkanObject, IDisposable> locker)
        {
            var locks = new List<IDisposable>();
            try
            {
                foreach(var obj in objs.OrderBy(obj => obj.GetHashCode()))
                    locks.Add(locker(obj));
                return new LambdaDisposable(Unlock);
            }
            catch(Exception)
            {
                Unlock();
                throw;
            }

            void Unlock()
            {
                foreach(var loc in locks)
                    loc.Dispose();
            }
        }
    }
}