# OOVulkan
[OOVulkan](https://gitlab.com/oovulkan/oovulkan) (/u ˈvʌl kən/ pronounced "oo-vulkan") is a high-level object-oriented C# wrapper over the low-level [Vulkan](https://www.khronos.org/vulkan/) C api.

##### Features:
 * Behavior Encapsulation
   - Encapsulates functionality into familiar object-based interfaces using properties, iterators, and custom types.
 * Argument Validation and Type Safety  
   - Prevents deadly silent errors at runtime by automatically validating most [Vulkan](https://www.khronos.org/vulkan/) inputs. 
     High-level argument types make it immediately clear which values can be passed to a given method.
 * Native C# Types (`string`, `bool`, `Tuple`, etc)
   - Use native C# types as arguments without worrying about interop conversion details or memory layout. 
 * Memory Management
   - Use [OOVulkan](https://gitlab.com/oovulkan/oovulkan) types like regular objects without needing to manage disposal of the underlying vulkan types.
 * Thread-Safety
   - Access the vulkan api from multiple threads simultaneously without concern for locking and synchronization.
     [OOVulkan](https://gitlab.com/oovulkan/oovulkan) uses a custom three-level lock to maximize concurrent access and minimize blocking.
 * Cleaner Syntax
   - By separating out the boilerplate of accessing a lower-level api, [OOVulkan](https://gitlab.com/oovulkan/oovulkan) provides a cleaner way to access [Vulkan](https://www.khronos.org/vulkan/) ensuring that the resulting code is easy to read and write.

##### Dependencies:
 * OS - Windows, Mac OS, or Linux
 * [Vulkan](https://www.khronos.org/vulkan/) 1.0
 
[Vulkan](https://www.khronos.org/vulkan/) is provided by your graphics card drivers.
See your vendor's instructions for details on how to install it.  
Displaying the output to the screen requires an [OOVulkan](https://gitlab.com/oovulkan/oovulkan) extension specific to your windowing manager. 
At the moment, there is support for 
[Xlib](https://gitlab.com/oovulkan/oovulkan-xlib-extension),
[XCB](https://gitlab.com/oovulkan/oovulkan-xcb-extension), and
[GTK](https://gitlab.com/oovulkan/oovulkan-gtk-extension). 
Additional windowing manager support can be created using these extensions as examples.

##### Example:
See the [OOVulkan Example Project](https://gitlab.com/oovulkan/oovulkan-example) for a simple usage example.